#!/bin/bash

# LANG=go
LANG=javascript

echo "Generating ${LANG} code"

docker run --rm \
  -v ${PWD}:/local \
  -u "$(id -u):$(id -g)" \
  openapitools/openapi-generator-cli generate \
  -i /local/openapi.yaml \
  -g ${LANG} \
  -o /local/out/${LANG}

echo "Generated ${LANG} code in out/${LANG}"
