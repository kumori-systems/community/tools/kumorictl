#!/bin/bash

DOCKERBUILD_DIR="${CI_PROJECT_DIR}/dockerbuild"

# Determine compatible CUE version
CUE_VERSION=$( make -f ${CI_PROJECT_DIR}/Makefile print-cue-version )
echo "."
echo ". CUE version: ${CUE_VERSION}"
echo "."

# Determine docker image
IMAGE_TAG_FOR_TAGS="${DOCKERHUB_PUBLIC_REGISTRY_USERNAME}/kumorictl:${CI_COMMIT_REF_NAME}"
echo ". Docker image tag: ${IMAGE_TAG_FOR_TAGS}"
echo "."

# Create a pristine directory for building the Docker image
rm -rf ${DOCKERBUILD_DIR} && mkdir -p ${DOCKERBUILD_DIR}


# Copy local binary (compiled in previous jobs) to build dir
if [ -f "${CI_PROJECT_DIR}/kumorictl-linux-amd64" ]; then
  cp ${CI_PROJECT_DIR}/kumorictl-linux-amd64 ${DOCKERBUILD_DIR}/kumorictl-linux-amd64
  chmod +x ${DOCKERBUILD_DIR}/kumorictl-linux-amd64
else
  echo ". ERROR: Couldn't find kumorictl-linux-amd64 binary in ${CI_PROJECT_DIR}/kumorictl-linux-amd64"
  exit -1
fi


# Download CUE binary from CUE repository
echo "."
echo ". Downloading CUE ${CUE_VERSION} binary from CUE repository..."
echo "."
wget -O ${CI_PROJECT_DIR}/cueball.tgz \
  https://github.com/cue-lang/cue/releases/download/${CUE_VERSION}/cue_${CUE_VERSION}_linux_amd64.tar.gz
status=$?
if [ $status -eq 0 ]; then
  echo ". CUE ${CUE_VERSION} downloaded"
else
  echo ". ERROR: Unable to download CUE ${CUE_VERSION} from https://github.com/cue-lang/cue/releases/download/${CUE_VERSION}/cue_${CUE_VERSION}_linux_amd64.tar.gz"
  exit -1
fi
tar zxvf ${CI_PROJECT_DIR}/cueball.tgz -C ${DOCKERBUILD_DIR} cue
status=$?
rm ${CI_PROJECT_DIR}/cueball.tgz
if [ $status -eq 0 ]; then
  echo ". CUE ${CUE_VERSION} extracted"
  chmod +x ${DOCKERBUILD_DIR}/cue
else
  echo ". ERROR: Unable to extract CUE ${CUE_VERSION}"
  exit -1
fi


# Build, push and delete the Docker image
cd ${DOCKERBUILD_DIR}
echo "."
echo ". Building docker image ${IMAGE_TAG_FOR_TAGS}..."
echo "."
cp ${CI_PROJECT_DIR}/Dockerfile .
docker build -f Dockerfile -t ${IMAGE_TAG_FOR_TAGS} .
echo "."
docker images
echo "."
echo ". Login to GitLab Docker Registry with CI job credentials..."
echo "."
docker --config docker-ci-config login -u ${DOCKERHUB_PUBLIC_REGISTRY_USERNAME} -p ${DOCKERHUB_PUBLIC_REGISTRY_PASSWORD} ${DOCKERHUB_PUBLIC_REGISTRY}
echo "."
echo ". Pushing image to GitLab Docker Registry..."
echo "."
docker --config docker-ci-config push ${IMAGE_TAG_FOR_TAGS}
echo "."
echo ". Deleting local docker image..."
echo "."
docker rmi -f ${IMAGE_TAG_FOR_TAGS}
echo "."
echo ". Deleting docker temporary config files..."
echo "."
rm -rf docker-ci-config
echo "."
echo ". Deleting temporary build dir..."
cd -
rm -rf ${DOCKERBUILD_DIR}
echo "."
echo "."
echo ". Done."
echo "."
