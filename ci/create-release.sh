#!/bin/bash

# CI VARIABLES USED IN THIS SCRIPT (with example values)
#
# CI_PROJECT_ID="18033405"  # kediku/testing-cluster-manager
# CI_JOB_TOKEN="..."
# CI_COMMIT_REF_NAME="v1.0.0"
# CI_PROJECT_URL="https://gitlab.com/kumori/kv3/cluster-manager"

GITLAB_API="https://gitlab.com/api/v4"

RELEASE_BASE_TAG_NAME="${CI_COMMIT_REF_NAME}"

DOCKER_IMAGE="${DOCKERHUB_PUBLIC_REGISTRY_USERNAME}/kumorictl:${CI_COMMIT_REF_NAME}"

RELEASE_NAME="Release ${CI_COMMIT_REF_NAME}"
RELEASE_DESCRIPTION="#### RELEASE NOTES\n\nKumori Control CLI ${CI_COMMIT_REF_NAME}\n\nDocker image is available at ${DOCKER_IMAGE}\n\n#### CHANGELOG\n\n*TBD*"


RELEASE_POST_DATA=$(cat <<EOF
{
  "name": "${RELEASE_NAME}",
  "tag_name": "${RELEASE_BASE_TAG_NAME}",
  "description": "${RELEASE_DESCRIPTION}",
  "assets": {
    "links": [
      {
        "name": "Kumori Control CLI - kumorictl-${RELEASE_BASE_TAG_NAME}-linux-amd64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorictl-linux-amd64?job=build"
      },
      {
        "name": "Kumori Control CLI - kumorictl-${RELEASE_BASE_TAG_NAME}-darwin-amd64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorictl-darwin-amd64?job=build"
      },
      {
        "name": "Kumori Control CLI - kumorictl-${RELEASE_BASE_TAG_NAME}-darwin-arm64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorictl-darwin-arm64?job=build"
      }
    ]
  }
}
EOF
)

# IMPORTANT: Authentication must be done using the JOB-TOKEN headers instead
#            of PRIVATE-TOKEN. This is undocumented!
echo -e "Creating release \"${RELEASE_NAME}\" from tag \"${CI_COMMIT_REF_NAME}\"..."

curl -X POST \
  -H 'Content-Type: application/json' \
  -H "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --data "${RELEASE_POST_DATA}" \
  "${GITLAB_API}/projects/${CI_PROJECT_ID}/releases"

echo -e "Release \"${RELEASE_NAME}\" from tag \"${CI_COMMIT_REF_NAME}\" created."

# List Releases (requires jq)
# echo -e "\nListing releases:"
# curl -s -k -f -H "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" "${GITLAB_API}/projects/${CI_PROJECT_ID}/releases" | jq .[].name
