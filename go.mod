module gitlab.com/kumori/kumorictl

go 1.16

// replace gitlab.com/kumori-systems/community/libraries/cue-dependency-manager => gitlab.com/kumori/cue-dependencies-manager v0.1.3-alpha1

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20200131002437-cf55d5288a48 // indirect
	cuelang.org/go v0.0.15
	github.com/Jeffail/gabs/v2 v2.6.0
	github.com/cloudfoundry/bytefmt v0.0.0-20200131002437-cf55d5288a48
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/disiqueira/gotree/v3 v3.0.2
	github.com/fatih/color v1.9.0
	github.com/gorilla/websocket v1.4.2
	github.com/ldez/go-git-cmd-wrapper v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.3.2 // indirect
	github.com/olekukonko/tablewriter v0.0.5-0.20200416053754-163badb3bac6
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/sergi/go-diff v1.1.0
	github.com/spf13/afero v1.3.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.6.1
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.0
	gitlab.com/kumori-systems/community/libraries/cue-dependency-manager v0.1.5
	gitlab.com/kumori-systems/community/libraries/manifest-diff v1.0.0
	go.uber.org/zap v1.19.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f
	gopkg.in/ini.v1 v1.57.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	k8s.io/apimachinery v0.21.9
)

// replace gitlab.com/kumori-systems/community/libraries/manifest-diff => /home/jbgisber/projects/git/kumori/manifest-diff
// replace gitlab.com/kumori-systems/community/libraries/manifest-diff v0.0.1 => gitlab.com/kumori/manifest-diff v0.0.2-0.20220214171155-2abd32795fbd
