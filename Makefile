# Version of CUE recommended for this version of KumoriCtl
# NOTE: this version is currently used for including the appropriate CUE
# version in KumoriCtl Docker image.
CUE_VERSION ?= v0.4.2
print-cue-version:
	@echo ${CUE_VERSION}

run: fmt vet
	go run ./main.go

build: fmt vet
	echo "Building for local OS and architecture"
	go build -o bin/kumorictl ./main.go

build-linux-amd64: fmt vet
	echo "Building for Linux amd64"
	GOOS=linux GOARCH=amd64 go build -o bin/kumorictl-linux-amd64 ./main.go

build-macos-amd64: fmt vet
	echo "Building for Darwin amd64"
	GOOS=darwin GOARCH=amd64 go build -o bin/kumorictl-darwin-amd64 ./main.go

build-macos-arm64: fmt vet
	echo "Building for Darwin arm64"
	GOOS=darwin GOARCH=arm64 go build -o bin/kumorictl-darwin-arm64 ./main.go

docker-build:
	docker run --rm -it -v "$(HOME)/.ssh":/root/.ssh.ro:ro -v "$(HOME)/.gitconfig":/root/.gitconfig:ro -v "$(CURDIR)":/kumori -w /kumori golang:1.16-buster sh -c 'cp -r /root/.ssh.ro /root/.ssh; chown -R root /root/.ssh; make build'

install: fmt vet build
	mv bin/kumorictl /usr/local/bin/

docker-install: docker-build
	mv bin/kumorictl /usr/local/bin/

fmt:
	go fmt ./...

vet:
	go vet ./...
