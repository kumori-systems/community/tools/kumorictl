echo "# KumoriCtl"
bin/kumorictl | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help init | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help init | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help setup-bash-completion | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help setup-bash-completion | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help config | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help config | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help login | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help login | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help logout | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help logout | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help get | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help get | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help get deployment | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help get deployment | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help get resource | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help get resource | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help get link | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help get link | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help get legacy-registry | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help get legacy-registry | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help describe | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help describe | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help describe deployment | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help describe deployment | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help register | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help register | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help register deployment | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help register deployment | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help register resource | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help register resource | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help register http-inbound | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help register http-inbound | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help register certificate | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help register certificate | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help unregister | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help unregister | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help unregister deployment | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help unregister deployment | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help unregister resource | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help unregister resource | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help link | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help link | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help unlink | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help unlink | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help logs | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help logs | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help exec | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help exec | grep -A 100 "Usage:"
echo '```'
echo -n '## '
bin/kumorictl help dev | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help dev | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help fetch-dependencies | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo 'Alias of `kumorictl dev cue get`.'
echo '```'
bin/kumorictl help fetch-dependencies | grep -A 100 "Usage:"
echo '```'
echo -n '### '
bin/kumorictl help dev cue | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help dev cue | grep -A 100 "Usage:"
echo '```'
echo -n '#### '
bin/kumorictl help dev cue process | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e '0,/\.$/ s/\.$//'
echo '```'
bin/kumorictl help dev cue process | grep -A 100 "Usage:"
echo '```'
echo '#### Validate in detail given CUE module'
bin/kumorictl dev cue vet -h | grep -m 1 -B 100 "Usage:" | head -n -1 | sed -e 's/ cue/ kumorictl dev cue/' | sed -e 's/#/\*/'
echo '```'
bin/kumorictl dev cue vet -h | grep -A 100 "Usage:" | sed -e 's/cue/kumorictl dev cue/'
echo '```'
cat << 'EOF'
## Building KumoriCtl from source

Make sure you have configured SSH keys to access repositories and your `$HOME/.gitconfig` contains:

```
[url "ssh://git@gitlab.com/"]
    insteadOf = https://gitlab.com/
```
### With dev tools installed

To build:
```
make build
```
To install:
```
sudo make install
```
### With Docker

This still requires having a proper `.gitconfig` and SSH keys to access repositories.

To build:
```
make docker-build
```
To install:
```
sudo make docker-install
```
EOF
echo '---'
echo '__Copyright © 2020 Kumori Systems S.L. All rights reserved.__'