/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// registerSecretCmd represents the registerSecret command
var registerSecretCmd = &cobra.Command{
	Use:   "secret <name>",
	Short: "Create a secret resource and register it in the platform",
	// Long:  `Create a secret resource and register it in the platform.`,
	Args: checkArgsRegisterSecret,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterSecret(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerSecretCmd)

	viper.Global.SetDefault("docker-server", "https://index.docker.io/v1/")

	registerSecretCmd.Flags().StringP(
		"from-file", "f", "",
		"create a secret from the file content",
	)

	registerSecretCmd.Flags().StringP(
		"from-data", "d", "",
		"create a secret from the literal data provided",
	)
	registerSecretCmd.Flags().StringP(
		"docker-server", "s", "",
		"set the server address if the secret contains the credentials to access a private Docker hub. Defaults to configuration parameter 'docker-server'",
	)

	registerSecretCmd.Flags().StringP(
		"docker-username", "u", "",
		"set the user name if the secret contains the credentials to access a private Docker hub",
	)

	registerSecretCmd.Flags().StringP(
		"docker-password", "p", "",
		"set the password if the secret contains the credentials to access a private Docker hub",
	)
}

func checkArgsRegisterSecret(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	fromFile, err := cmd.Flags().GetString("from-file")
	if err != nil {
		return err
	}

	fromData, err := cmd.Flags().GetString("from-data")
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("docker-server")
	if err != nil {
		return err
	}

	dockerUser, err := cmd.Flags().GetString("docker-username")
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("docker-password")
	if err != nil {
		return err
	}

	if fromData == "" && fromFile == "" && dockerUser == "" {
		return errors.New("from-file, from-data or docker-username flag must be used")
	}

	if fromData != "" && fromFile != "" && dockerUser != "" {
		return errors.New("from-file, from-data and docker-username flags cannot be used simultaneously")
	}

	// if dockerUser != "" && dockerPassword == "" {
	// 	logger.Fatal("docker-password and docker-username must be used simultaneously")
	// }

	return nil
}

func runRegisterSecret(cmd *cobra.Command, args []string) {
	meth := "runRegisterSecret"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("Secret name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	fromFile, err := cmd.Flags().GetString("from-file")
	if err != nil {
		logger.Fatal(err.Error())
	}

	fromData, err := cmd.Flags().GetString("from-data")
	if err != nil {
		logger.Fatal(err.Error())
	}

	dockerServer, err := cmd.Flags().GetString("docker-server")
	if err != nil {
		logger.Fatal(err.Error())
	}
	if dockerServer == "" {
		dockerServer = viper.Global.GetString("docker-server")
	}

	dockerUser, err := cmd.Flags().GetString("docker-username")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	dockerPassword, err := cmd.Flags().GetString("docker-password")
	if err != nil {
		logger.Fatal(err.Error())
	}
	if dockerUser != "" && dockerPassword == "" {
		prompt := ""
		if output != JSONOutput {
			prompt = "Enter Password: "
		}
		dockerPassword, err = util.GetPasswd(prompt, true)
	}
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering secret",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"fromFile", fromFile,
		"fromData", fromData,
		"dockerServer", dockerServer,
		"dockerUser", dockerUser,
		"dockerPassword", dockerPassword,
		"output", output,
		"meth", meth,
	)

	var secretBase64 string
	if fromFile != "" {
		var err error
		secretBase64, err = util.ReadAsBase64(fromFile)
		if err != nil {
			logger.Fatal(err.Error())
		}
	} else if fromData != "" {
		secretBase64 = util.StringAsBase64(fromData)
	} else if dockerUser != "" {
		userData := fmt.Sprintf("%s:%s", dockerUser, dockerPassword)
		auth := util.StringAsBase64(userData)
		dockerconfig := fmt.Sprintf(`{
			"auths": {
				"%s": {
					"auth": "%s"
				}
			}
		}`, dockerServer, auth)
		secretBase64 = util.StringAsBase64(dockerconfig)
	}

	err = admission.CreateSecret(userDomain, name, owner, secretBase64)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Resource secret " + userDomain + "/" + name + " created")
	}
}
