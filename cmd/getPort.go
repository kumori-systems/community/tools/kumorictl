/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getPortCmd represents the getPort command
var getPortCmd = &cobra.Command{
	Use:     "port [<user>/<name>]",
	Aliases: []string{"port", "ports"},
	Short:   "Get Port resource from platform",
	Long: `Get Port resource from platform
If <user>/<name> is not provided, all Ports are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetPort,
	Run:  func(cmd *cobra.Command, args []string) { runGetPort(cmd, args) },
}

func init() {
	getCmd.AddCommand(getPortCmd)
	getPortCmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetPort(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetPort(cmd *cobra.Command, args []string) {
	meth := "runGetPort"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	if len(args) == 0 {
		// Get port list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting port",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listPorts(onlyInUse, output)

	} else if len(args) == 1 {
		// Get port manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting port",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getPort(userDomain, name, output)
	}

	return
}

func listPorts(onlyInUse bool, output string) {
	ports, err := admission.GetPorts(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {
		// If the data has not been set yet, then the ports are included directly
		// in the root part. If a Data object already exists, the ports are added
		// in the "ports" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing ports in JSON format", "error", err.Error())
		}
		for _, port := range ports {
			data.ArrayAppend(port.LongName, "ports")
		}

		if !dataExists {
			data = data.Search("ports")
		}
		response.SetData(data)

	} else {

		for _, port := range ports {
			fmt.Println(port.LongName)
		}
	}
	return
}

func getPort(userDomain, name string, output string) {
	port, err := admission.GetPort(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(port)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}
		} else {
			fmt.Println(port)
		}
	}
	return
}

func getPortNumber(longName string) (float64, error) {
	portDomain, portName, err := getUserDomainAndName(longName, "")
	if err != nil {
		return 0, err
	}

	portManifest, err := admission.GetPort(portDomain, portName)
	if err != nil {
		return 0, err
	}
	jsonParsed, err := gabs.ParseJSON([]byte(portManifest))
	if err != nil {
		return 0, err
	}
	if jsonParsed.Exists("description", "externalPort") && jsonParsed.Search("description", "externalPort").Data() != nil {
		portNumber := jsonParsed.Search("description", "externalPort").Data().(float64)
		return portNumber, nil
	}

	return 0, fmt.Errorf("Port %s has not a port number assigned", longName)
}
