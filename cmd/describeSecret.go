/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// describeSecretCmd represents the describe Secret command
var describeSecretCmd = &cobra.Command{
	Use:     "secret <user>/<name>",
	Aliases: []string{"secrets"},
	Short:   "Describe Secret on platform",
	Long: `Describe Secret on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeSecret,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeSecret(cmd, args) },
}

func init() {

	describeSecretCmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-secret")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describeSecretCmd)
}

func checkArgsDescribeSecret(cmd *cobra.Command, args []string) error {
	// Secret name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// Secret name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-secret can be omitted when user-secret is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribeSecret(cmd *cobra.Command, args []string) {
	meth := "runDescribeSecret"

	userSecret := ""
	name := ""
	var err error

	// Calculates the user secret and name. If arguments are not provided, then
	// the user secret
	userSecret, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing Secret",
		"name", name,
		"userSecret", userSecret,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribeSecretJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeSecretTable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeSecretJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getSecretData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribeSecretTable(cmd *cobra.Command, args []string) {
	_, volDetailsJSON, err := getSecretData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderSecret(volDetailsJSON)
	fmt.Println("")
}

func getSecretData(fullName string) (string, *gabs.Container, error) {
	userSecret, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	volManifest, volJSON, err := admission.DescribeSecret(userSecret, name)
	if err != nil {
		return "", nil, err
	}
	if volJSON == nil {
		return "", nil, fmt.Errorf("Secret not found")
	}

	return volManifest, volJSON, nil
}

func printHeaderSecret(json *gabs.Container) {

	volName, _ := admission.SecretURNToName(json.Path("urn").Data().(string))

	fmt.Println(toBold("Secret            "), ":", volName)
	fmt.Println(toBold("Public            "), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)

	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")
	fmt.Println(toBoldMaintainCase("Secret details are not shown."))
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("Secret not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("Secret is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}
}

func getSecretNameAndSecret(urn string) (
	secret string,
	err error,
) {
	userSecret, name, err := admission.SecretDecomposeUrn(urn)
	secret = fmt.Sprintf("%s/%s", userSecret, name)
	return
}
