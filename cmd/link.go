/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// linkCmd represents the link command
var linkCmd = &cobra.Command{
	Use:   "link <user>/<name>:<channel> <user>/<name>:<channel>",
	Short: "Link a pair of deployments' service channels",
	Long: `Link a pair of deployments' service channels.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsLink,
	Run:  func(cmd *cobra.Command, args []string) { runLink(cmd, args) },
}

func init() {
	rootCmd.AddCommand(linkCmd)
	setRegisterLinkFlags(linkCmd)
}

func checkArgsLink(cmd *cobra.Command, args []string) error {

	err := cobra.ExactArgs(2)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		return err
	}

	err = checkUserDomainAndNameAndChannel(args[0])
	if err != nil {
		return err
	}
	channel1, err := getChannel(args[0])
	if err != nil {
		return err
	}

	err = checkUserDomainAndNameAndChannel(args[1])
	if err != nil {
		return err
	}
	channel2, err := getChannel(args[1])
	if err != nil {
		return err
	}

	if channel1 == "" || channel2 == "" {
		return errors.New("channel missing in at least one argument. Arguments must match <userdomain/name:channel> format")
	}

	meta, err := cmd.Flags().GetString("meta")
	if err != nil {
		return err
	}

	metaFile, err := cmd.Flags().GetString("meta-file")
	if err != nil {
		return err
	}

	if (len(meta) > 0) && (len(metaFile) > 0) {
		return fmt.Errorf("meta and meta-file flags are mutually exclusive")
	}

	return nil
}

func runLink(cmd *cobra.Command, args []string) {
	meth := "runLink"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	meta, err := cmd.Flags().GetString("meta")
	if err != nil {
		logger.Fatal(err.Error())
	}

	metaFile, err := cmd.Flags().GetString("meta-file")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// Function checkArgsLink ensured that there is exactly 1 deployment and 1 inbound

	userDomain1, name1, channel1, err := getUserDomainAndNameAndChannel(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain2, name2, channel2, err := getUserDomainAndNameAndChannel(args[1], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Linking deployments",
		"userDomain1", userDomain1,
		"name1", name1,
		"channel1", channel1,
		"userDomain2", userDomain2,
		"name2", name2,
		"channel2", channel2,
		"owner", owner,
		"meta", meta,
		"metaFile", metaFile,
		"output", output,
		"meth", meth,
	)

	// If a metaFile is specified, reads the link metadata from that file. The metadata
	// must be a valid JSON document
	if (len(metaFile) > 0) && (util.FileExists(metaFile)) {
		data, err := os.ReadFile(metaFile)
		if err != nil {
			logger.Fatal(fmt.Sprintf("error reading file %s: %s", metaFile, err.Error()))
		}
		meta = string(data)
	}

	if len(meta) > 0 {
		_, err = gabs.ParseJSON([]byte(meta))
		if err != nil {
			logger.Fatal(fmt.Sprintf("error parsing metadata: %s", err.Error()))
		}
	}

	err = admission.CreateLink(userDomain1, name1, channel1, userDomain2, name2, channel2, owner, meta)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Link " + args[0] + " <=> " + args[1] + " created")
	}
}

func setRegisterLinkFlags(cmd *cobra.Command) {

	cmd.Flags().StringP(
		"owner", "", "",
		"owner of the new deployment (only for administrators)",
	)

	cmd.Flags().StringP(
		"meta", "m", "",
		`a JSON object containing the link metadata. Example: --meta '{"target": "production"}'. Cannot be used together with meta-file`,
	)

	cmd.Flags().StringP(
		"meta-file", "", "",
		"the path to a JSON file containing the link metadata. Example: --meta meta.json. Cannot be used together with meta",
	)
}
