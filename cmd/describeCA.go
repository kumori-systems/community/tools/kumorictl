/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
)

// describeCACmd represents the describe CA command
var describeCACmd = &cobra.Command{
	Use:     "ca <user>/<name>",
	Aliases: []string{"cas"},
	Short:   "Describe CA on platform",
	Long: `Describe CA on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeCA,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeCA(cmd, args) },
}

func init() {

	describeCACmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-domain")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describeCACmd)
}

func checkArgsDescribeCA(cmd *cobra.Command, args []string) error {
	// CA name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// CA name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribeCA(cmd *cobra.Command, args []string) {
	meth := "runDescribeCA"

	userDomain := ""
	name := ""
	var err error

	// Calculates the user domain and name. If arguments are not provided, then
	// the user domain
	userDomain, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing CA",
		"name", name,
		"userDomain", userDomain,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribeCAJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeCATable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeCAJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getCAData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribeCATable(cmd *cobra.Command, args []string) {
	_, caDetailsJSON, err := getCAData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderCA(caDetailsJSON)
	fmt.Println("")
}

func getCAData(fullName string) (string, *gabs.Container, error) {
	userDomain, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	caManifest, caJSON, err := admission.DescribeCA(userDomain, name)
	if err != nil {
		return "", nil, err
	}
	if caJSON == nil {
		return "", nil, fmt.Errorf("CA not found")
	}

	return caManifest, caJSON, nil
}

func printHeaderCA(json *gabs.Container) {

	caName, _ := admission.CAURNToName(json.Path("urn").Data().(string))

	caCertString := json.Path("description.ca").Data().(string)

	var parsedCertificate *x509.Certificate

	caCertData, err := base64.StdEncoding.DecodeString(caCertString)
	if err == nil {
		parsedCertificate, err = x509Validator.GetParsedCertificate(string(caCertData))
		if err != nil {
			logger.Warn(fmt.Sprintf("Failed parsing certificate."))
		}
	} else {
		logger.Warn(fmt.Sprintf("Failed parsing certificate."))
	}

	fmt.Println(toBold("CA                "), ":", caName)
	fmt.Println(toBold("Public            "), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)

	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")
	fmt.Println(toBoldMaintainCase("CA certificate:"))
	fmt.Println(toBoldMaintainCase("- Valid from "), ":", parsedCertificate.NotBefore.Format(time.UnixDate))
	fmt.Println(toBoldMaintainCase("- Valid until"), ":", parsedCertificate.NotAfter.Format(time.UnixDate))
	fmt.Println(toBoldMaintainCase("- Subject    "), ":", parsedCertificate.Subject)
	fmt.Println(toBoldMaintainCase("- Issuer     "), ":", parsedCertificate.Issuer)
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("CA not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("CA is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}
}

func getCANameAndDomain(urn string) (
	ca string,
	err error,
) {
	userDomain, name, err := admission.CADecomposeUrn(urn)
	ca = fmt.Sprintf("%s/%s", userDomain, name)
	return
}
