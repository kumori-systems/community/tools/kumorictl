/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// bashCompletionCmd represents the completion command
var bashCompletionCmd = &cobra.Command{
	Use:   "bash-completion",
	Short: "Generate bash completion scripts",
	Long: `Generate bash completion scripts.

To enable completion run ` + "`. <(kumorictl bash-completion)`" + `

To avoid doing it in each session, use ` + "`kumorictl setup-bash-completion`" + `.
`,
	Run: func(cmd *cobra.Command, args []string) {
		rootCmd.GenBashCompletion(os.Stdout)
	},
	Hidden: true,
}

func init() {
	// Hidden because it is not working
	bashCompletionCmd.Hidden = true
	// rootCmd.AddCommand(bashCompletionCmd)
}
