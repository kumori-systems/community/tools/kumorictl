/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"k8s.io/apimachinery/pkg/util/duration"
)

// describeVolumeCmd represents the describe volume command
var describeVolumeCmd = &cobra.Command{
	Use:     "volume <user>/<name>",
	Aliases: []string{"volumes"},
	Short:   "Describe volume on platform",
	Long: `Describe volume on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeVolume,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeVolume(cmd, args) },
}

func init() {

	describeVolumeCmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-domain")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describeVolumeCmd)
}

func checkArgsDescribeVolume(cmd *cobra.Command, args []string) error {
	// Volume name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// Volume name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribeVolume(cmd *cobra.Command, args []string) {
	meth := "runDescribeVolume"

	userDomain := ""
	name := ""
	var err error

	// Calculates the user domain and name. If arguments are not provided, then
	// the user domain
	userDomain, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing volume",
		"name", name,
		"userDomain", userDomain,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribeVolumeJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeVolumeTable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeVolumeJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getVolumeData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribeVolumeTable(cmd *cobra.Command, args []string) {
	_, volDetailsJSON, err := getVolumeData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderVolume(volDetailsJSON)
	fmt.Println("")
	tablePrintVolumeItems(volDetailsJSON)
	fmt.Println("")
}

func getVolumeData(fullName string) (string, *gabs.Container, error) {
	userDomain, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	volManifest, volJSON, err := admission.DescribeVolume(userDomain, name)
	if err != nil {
		return "", nil, err
	}
	if volJSON == nil {
		return "", nil, fmt.Errorf("Volume not found")
	}

	return volManifest, volJSON, nil
}

func printHeaderVolume(json *gabs.Container) {

	volName, _ := admission.VolumeURNToName(json.Path("urn").Data().(string))

	items := 0
	if json.Search("items").ChildrenMap() != nil {
		items = len(json.Search("items").ChildrenMap())
	}

	fmt.Println(toBold("Volume            "), ":", volName)
	fmt.Println(toBold("Items             "), ":", items, "of", json.Path("description.maxItems").Data().(float64))
	fmt.Println(toBold("Type              "), ":", json.Path("description.type").Data().(string))
	fmt.Println(toBold("Size              "), ":", json.Path("description.size").Data().(string))
	fmt.Println(toBold("Public            "), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)
	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("Volume not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("Volume is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}

}

func tablePrintVolumeItems(json *gabs.Container) {

	if (json.Search("items").ChildrenMap() == nil) || (len(json.Search("items").ChildrenMap()) == 0) {
		// No items information present
		fmt.Println(toBoldMaintainCase("Volume has no items yet."))
		fmt.Println("")
		return
	}

	fmt.Println(toBoldMaintainCase("List of volume items:"))

	volumeItemsTable := tablewriter.NewWriter(os.Stdout)
	// volumeItemsTable.SetHeader([]string{"Item", "Status", "Used %", "Age", "UsedBy"})
	volumeItemsTable.SetHeader([]string{"Item", "ID", "Status", "Used %", "Age", "UsedBy"})
	var volumeItemsData [][]string

	for itemId, itemJSON := range json.Search("items").ChildrenMap() {

		// Item ID is "kres-101523-9b599448-kcnvy". Take the last portion (split by '-').
		itemIdParts := strings.Split(itemId, "-")
		itemName := itemIdParts[len(itemIdParts)-1]

		status := itemJSON.Path("phase").Data().(string)

		usedPercentStr := "-"
		if (itemJSON.Exists("metrics", "usagePercent")) && (itemJSON.Search("metrics", "usagePercent").Data() != nil) {
			usedPercentStr = itemJSON.Search("metrics", "usagePercent").Data().(string)
			usedPercentFloat64, err := strconv.ParseFloat(usedPercentStr, 64)
			if err == nil {
				// int(math.Round(usedPercentFloat64))
				usedPercentInt := int(math.Round(usedPercentFloat64))
				usedPercentStr = colourPercentageHigh(usedPercentInt, 70, 90)
			}
		}

		// Determine item age
		age := "<unknown>"
		creationTimestampStr := itemJSON.Path("creationTimestamp").Data().(string)
		creationTime, err := time.Parse(time.RFC3339, creationTimestampStr)
		if err != nil {
			logger.Debug("Error while parsing date: " + err.Error())
		} else {
			age = duration.HumanDuration(time.Since(creationTime))
		}

		inUseBy := "-"
		if itemJSON.Path("inUseBy").Data() != nil {
			inUseBy = podNameToInstanceName(itemJSON.Path("inUseBy").Data().(string))
		}

		row := []string{
			creationTimestampStr, // only used for sorting
			itemName,
			status,
			usedPercentStr,
			age,
			inUseBy,
		}
		volumeItemsData = append(volumeItemsData, row)
	}

	// Sort items by creation timestamp
	sort.Slice(volumeItemsData, func(i, j int) bool {
		return (volumeItemsData[i][0] < volumeItemsData[j][0])
	})

	// Remove eventTimestamp property (only used for finer sorting)
	for i, item := range volumeItemsData {
		// volumeItemsData[i] = item[1:]
		newItem := item
		newItem[0] = strconv.Itoa(i)
		volumeItemsData[i] = newItem
	}

	// Render table
	volumeItemsTable.AppendBulk(volumeItemsData)
	volumeItemsTable.SetAutoFormatHeaders(false)
	volumeItemsTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	volumeItemsTable.SetAlignment(tablewriter.ALIGN_CENTER)
	volumeItemsTable.SetCenterSeparator("+")
	volumeItemsTable.SetColumnSeparator("|")
	volumeItemsTable.SetRowSeparator("-")
	volumeItemsTable.SetHeaderLine(true)
	volumeItemsTable.SetBorder(true)
	volumeItemsTable.SetNoWhiteSpace(false)
	volumeItemsTable.Render()
}

func getVolumeNameAndDomain(urn string) (
	volume string,
	err error,
) {
	userDomain, name, err := admission.VolumeDecomposeUrn(urn)
	volume = fmt.Sprintf("%s/%s", userDomain, name)
	return
}

func podNameToInstanceName(podName string) (instanceName string) {
	// StatefulSet: kd-132136-4df2cc58-worker000-deployment-0
	// Deployment: kd-121827-f80635b5-frontend000-deployment-94454bc7b-8w2nq
	podNameParts := strings.Split(podName, "-")
	podSuffix := podNameParts[5]
	instanceName = "instance-" + podSuffix
	return
}
