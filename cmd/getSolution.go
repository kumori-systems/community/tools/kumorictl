/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getSolutionCmd represents the get solution command
var getSolutionCmd = &cobra.Command{
	Use:     "deployment [<user>/<name>]...",
	Aliases: []string{"deployments"},
	Short:   "Get deployment manifest from platform",
	Long: `Get deployment manifest from platform.
If <user>/<name> is not provided, all deployments are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetSolution,
	Run:  func(cmd *cobra.Command, args []string) { runGetSolution(cmd, args) },
}

func init() {
	getCmd.AddCommand(getSolutionCmd)
}

func checkArgsGetSolution(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetSolution(cmd *cobra.Command, args []string) {
	meth := "runGetSolution"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	if len(args) == 0 {
		// Get httpinbound list
		logger.Debug(
			"Getting deployments",
			"output", output,
			"meth", meth,
		)
		listSolutions(output)

	} else if len(args) == 1 {
		// Get manifest
		// userDomain, name, err := getUserDomainAndName(args[0])
		userDomain, name, revision, err := getUserdDomainAndNameAndRevision(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting deployment",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getSolution(userDomain, name, revision, output)
	}
}

func listSolutions(output string) {
	solutions, err := admission.GetSolutions()
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {

		// If the data has not been set yet, then the solutions are included directly
		// in the root part. If a Data object already exists, the solutions are added
		// in the "solutions" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing solutions in JSON format", "error", err.Error())
		}
		for _, solution := range solutions {
			data.ArrayAppend(solution, "deployments")
		}

		if !dataExists {
			data = data.Search("deployments")
		}
		response.SetData(data)

	} else {
		for _, solution := range solutions {
			fmt.Println(solution)
		}
	}
}

func getSolution(userDomain, name string, revision string, output string) {
	var solution string
	if revision == "" {
		var err error
		solution, err = admission.GetSolution(userDomain, name)
		if err != nil {
			response.AddError(response.ErrorSeverityType, err.Error())
			logger.Error(err.Error())
			return
		}
	} else {
		solutionData, err := getSolutionManifest(userDomain, name, revision)
		if err != nil {
			response.AddError(response.ErrorSeverityType, err.Error())
			logger.Error(err.Error())
			return
		}
		solution = solutionData.StringIndent("", "  ")
	}
	if output == JSONOutput {
		if data, err := gabs.ParseJSON([]byte(solution)); err != nil {
			logger.Fatal("Error writing the JSON response", "error", err.Error())
		} else {
			response.SetData(data)
		}
	} else {
		fmt.Println(solution)
	}
}
