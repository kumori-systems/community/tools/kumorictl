/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// registerHTTPInboundCmd represents the register certificate command
var registerHTTPInboundCmd = &cobra.Command{
	Use:     "http-inbound <name>",
	Aliases: []string{"inbound", "httpinbound"},
	Short:   "Create a HTTP(S) Inbound deployment and register it in the platform",
	// Long:    `Create a HTTP(S) Inbound deployment and register it in the platform.`,
	Args: checkArgsRegisterHTTPInbound,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterHTTPInbound(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerHTTPInboundCmd)
	setRegisterHTTPInboundFlags(registerHTTPInboundCmd)
}

func checkArgsRegisterHTTPInbound(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		return err
	}

	// user-domain of the HTTPInbound can be omitted when user-domain is set in
	// configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// Idem, for the user-domain of the certificate
	cert, err := cmd.Flags().GetString("cert")
	if err != nil {
		return err
	}
	err = checkUserDomainAndName(cert)
	if err != nil {
		return err
	}

	// Idem, for the user-domain of the CA
	ca, err := cmd.Flags().GetString("ca")
	if err != nil {
		return err
	}
	if ca != "" {
		err = checkUserDomainAndName(ca)
		if err != nil {
			return err
		}
	}

	// client-cert-required and ca flags are allowed only if client-cert is enabled
	clientCert, err := cmd.Flags().GetBool("client-cert")
	if err != nil {
		return err
	}
	clientCertRequired, err := cmd.Flags().GetBool("client-cert-required")
	if err != nil {
		return err
	}
	if !clientCert {
		if ca != "" {
			err = fmt.Errorf("ca (certificate authority) flag can only be used with client-cert")
			return err
		}
		if clientCertRequired {
			err = fmt.Errorf("client-cert-required flag can only be used with client-cert")
			return err
		}
	}

	domain, err := cmd.Flags().GetString("domain")
	if err != nil {
		return err
	}
	err = checkUserDomainAndName(domain)
	if err != nil {
		return err
	}

	return nil
}

func runRegisterHTTPInbound(cmd *cobra.Command, args []string) {
	meth := "runRegisterHTTPInbound"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !validateName(name) {
		err = fmt.Errorf("Http-inbound name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if exists {
		err = errors.New("http-inbound " + userDomain + "/" + name + " already exists")
		logger.Fatal(err.Error())
	}
	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}
	logger.Debug(
		"Registering http-inbound",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"output", output,
		"meth", meth,
	)
	applyHTTPInbound(cmd, args, owner)
	if output != JSONOutput {
		fmt.Println("HttpInbound " + userDomain + "/" + name + " created")
	}
}

func applyHTTPInbound(cmd *cobra.Command, args []string, owner string) {
	meth := "applyHTTPInbound"

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	domain, err := cmd.Flags().GetString("domain")
	if err != nil {
		logger.Fatal(err.Error())
	}

	domainUserDomain, domainName, err := getUserDomainAndName(domain, owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	cert, err := cmd.Flags().GetString("cert")
	if err != nil {
		logger.Fatal(err.Error())
	}
	certUserDomain, certName, err := getUserDomainAndName(cert, owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	ca, err := cmd.Flags().GetString("ca")
	if err != nil {
		logger.Fatal(err.Error())
	}
	caUserDomain := ""
	caName := ""
	if ca != "" {
		caUserDomain, caName, err = getUserDomainAndName(ca, owner)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}

	remoteAddressHeader, err := cmd.Flags().GetString("remote-address-header")
	if err != nil {
		logger.Fatal(err.Error())
	}

	cleanXForwardedFor, err := cmd.Flags().GetBool("clean-x-forwarded-for")
	if err != nil {
		logger.Fatal(err.Error())
	}

	allowedIpList, err := cmd.Flags().GetStringSlice("allowed-ip-list")
	if err != nil {
		logger.Fatal(err.Error())
	}
	allowedIpList = trimList(allowedIpList) // Removes white-spaces

	deniedIpList, err := cmd.Flags().GetStringSlice("denied-ip-list")
	if err != nil {
		logger.Fatal(err.Error())
	}
	deniedIpList = trimList(deniedIpList) // Removes white-spaces

	clientCert, err := cmd.Flags().GetBool("client-cert")
	if err != nil {
		logger.Fatal(err.Error())
	}

	clientCertRequired, err := cmd.Flags().GetBool("client-cert-required")
	if err != nil {
		logger.Fatal(err.Error())
	}

	websockets, err := cmd.Flags().GetBool("websockets")
	if err != nil {
		logger.Fatal(err.Error())
	}

	comment, err := cmd.Flags().GetString("comment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Applying http-inbound",
		"name", name,
		"userDomain", userDomain,
		"domainName", domainName,
		"domainUserDomain", domainUserDomain,
		"owner", owner,
		"remoteAddressHeader", remoteAddressHeader,
		"cleanXForwardedFor", cleanXForwardedFor,
		"allowedIpList", allowedIpList,
		"deniedIpList", deniedIpList,
		"certName", certName,
		"certUserDomain", certUserDomain,
		"caName", caName,
		"caUserDomain", caUserDomain,
		"clientCert", clientCert,
		"clientCertRequired", clientCertRequired,
		"websockets", websockets,
		"comment", comment,
		"output", output,
		"meth", meth,
	)

	err = admission.CreateHTTPInbound(
		userDomain, name, owner, domainUserDomain, domainName,
		certUserDomain, certName, caUserDomain, caName,
		clientCert, clientCertRequired, websockets, comment,
		allowedIpList, deniedIpList, remoteAddressHeader, cleanXForwardedFor,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"http-inbound applied",
		"name", name,
		"userDomain", userDomain,
		"domainName", domainName,
		"domainUserDomain", domainUserDomain,
		"owner", owner,
		"remoteAddressHeader", remoteAddressHeader,
		"cleanXForwardedFor", cleanXForwardedFor,
		"allowedIpList", allowedIpList,
		"deniedIpList", deniedIpList,
		"certName", certName,
		"certUserDomain", certUserDomain,
		"clientCert", clientCert,
		"clientCertRequired", clientCertRequired,
		"caName", caName,
		"caUserDomain", caUserDomain,
		"websockets", websockets,
		"comment", comment,
		"output", output,
		"meth", meth,
	)
}

func getDomainUserDomain(longName, owner string) (domain string, err error) {
	vdomain, vname, err := getUserDomainAndName(longName, owner)
	if err != nil {
		return "", err
	}

	manifest, err := admission.GetDomain(vdomain, vname)
	if err != nil {
		return "", err
	}

	domainJSON, err := gabs.ParseJSON([]byte(manifest))
	if domainJSON.Exists("description", "domain") {
		domain = domainJSON.Search("description", "domain").Data().(string)
		return
	}

	return
}

func setRegisterHTTPInboundFlags(cmd *cobra.Command) {

	cmd.PersistentFlags().StringP(
		"domain", "d", "",
		"domain <user>/<name> resource assigned to this http-inbound. <user> can be ommited and defaults to the logged user",
	)

	// cmd.Flags().StringP(
	// 	"owner", "", "",
	// 	"owner of the new deployment (only for administrators)",
	// )

	cmd.Flags().String(
		"remote-address-header", "",
		"Name of the header (for example, 'X-Real-IP') where the remote address will be setted. This may not be the physical remote address of the peer if the address has been inferred from the x-forwarded-for (depends on cluster configuration)	",
	)

	cmd.Flags().Bool(
		"clean-x-forwarded-for", false,
		"Clean the x-forwarded-for header",
	)

	cmd.Flags().StringSlice(
		"allowed-ip-list", []string{},
		"Comma-separated list of IPs or CIDRs to be allowed (any traffic not matching the list will be denied). This list will only be processed on Kumori platforms enabling this feature.",
	)

	cmd.Flags().StringSlice(
		"denied-ip-list", []string{},
		"Comma-separated list of IPs or CIDRs to be denied (any traffic not matching the list will be allowed). If an Alowed-ip-list has also been defined, it will take precedence. This list will only be processed on Kumori platforms enabling this feature.",
	)

	cmd.Flags().StringP(
		"cert", "c", "",
		"certificate <user>/<name> resource assigned to this http-inbound. <user> can be ommited and defaults to the logged user",
	)

	cmd.Flags().String(
		"ca", "",
		"Certificate authority (CA) <user>/<name> resource assigned to this http-inbound (used only if client-cert is activated). <user> can be ommited and defaults to the logged user",
	)

	cmd.Flags().Bool(
		"client-cert", false,
		"activate client certificate",
	)

	cmd.Flags().Bool(
		"client-cert-required", false,
		"reject request if client certificate is not providad (used only if client-cert is activated)",
	)

	cmd.Flags().Bool(
		"websockets", false,
		"activate websockets support",
	)

	cmd.Flags().StringP(
		"comment", "m", "",
		"message describing the action",
	)
}

// trimList removes white-spaces from each string of a list
func trimList(input []string) (output []string) {
	for _, v := range input {
		output = append(output, strings.TrimSpace(v))
	}
	return
}
