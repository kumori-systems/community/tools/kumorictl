/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// registerDeploymentCmd represents the register deployment command
var restartDeploymentRoleCmd = &cobra.Command{
	Use:   "restart <name> <role> [instance]",
	Short: "Restart a deployment role or even a specific instance",
	Long:  `Restart a deployment role or even a specific instance.`,
	Args:  checkArgsRestartDeploymentRole,
	Run:   func(cmd *cobra.Command, args []string) { runRestartDeploymentRole(cmd, args) },
}

func init() {
	rootCmd.AddCommand(restartDeploymentRoleCmd)
	setRestartDeploymentRoleFlags(restartDeploymentRoleCmd)
}

func checkArgsRestartDeploymentRole(cmd *cobra.Command, args []string) error {
	err := cobra.MinimumNArgs(2)(cmd, args)
	if err != nil {
		return err
	}

	err = cobra.MaximumNArgs(3)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runRestartDeploymentRole(cmd *cobra.Command, args []string) {
	meth := "runRestartDeploymentRole"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	role := args[1]

	instance := ""
	if len(args) == 3 {
		instance = args[2]
	}

	forceDelete, err := cmd.Flags().GetBool("force-delete")
	if err != nil {
		logger.Fatal(err.Error())
	}

	assumeYes, err := cmd.Flags().GetBool("yes")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Applying deployment",
		"name", name,
		"userDomain", userDomain,
		"role", role,
		"instane", instance,
		"forceDelete", forceDelete,
		"waitDuration", waitDuration,
		"output", output,
		"meth", meth,
	)

	if !assumeYes && (output != JSONOutput) {
		reader := bufio.NewReader(os.Stdin)
		fmt.Println("")
		if instance == "" {
			color.Yellow(fmt.Sprintf("WARNING: All instances of role '%s' will be restarted.", role))
		} else {
			color.Yellow(fmt.Sprintf("WARNING: Instance '%s' of role '%s' will be restarted.", instance, role))
		}
		color.Yellow("Press <ENTER> to continue or <CTRL-c> to abort")
		_, _ = reader.ReadString('\n')
	}

	kubeInstance := strings.ReplaceAll(instance, "instance-", "")

	err = admission.RestartDeploymentRoleInstance(userDomain, name, role, kubeInstance, forceDelete, waitDuration)
	if err != nil {
		logger.Fatal(err.Error())
	}
}

func setRestartDeploymentRoleFlags(cmd *cobra.Command) {

	// Assume yes instead of asking for confirmation
	cmd.Flags().BoolP(
		"yes", "y", false,
		"Confirms automatically",
	)

	// Currently disabled because instances are always regenerated
	cmd.Flags().Bool(
		"force-delete", true,
		"Delete the old instance and create a new one instead of just restarting it",
	)
	cmd.Flags().MarkHidden("force-delete")

	cmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time for instances to be ready and running",
	)
}
