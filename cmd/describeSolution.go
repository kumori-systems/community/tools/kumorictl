/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/cloudfoundry/bytefmt"
	"github.com/disiqueira/gotree/v3"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/response/deployment"
	"gitlab.com/kumori/kumorictl/pkg/util"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
	"k8s.io/apimachinery/pkg/util/duration"
)

// describeSolutionCmd represents the describe solution command
var describeSolutionCmd = &cobra.Command{
	Use:     "deployment [<user>/<name> [role [instance [container]]]]",
	Aliases: []string{"deployments"},
	Short:   "Describe deployment on platform",
	Long: `Describe deployment on platform.
Optional output filtering up to container level is available for human readable output formats.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeSolution,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeSolution(cmd, args) },
}

var responseSpec = []int{1, 2}

var channelTypes = []string{"client", "server", "duplex"}

func init() {
	describeCmd.AddCommand(describeSolutionCmd)
}

func checkArgsDescribeSolution(cmd *cobra.Command, args []string) error {
	if len(args) > 4 {
		return createAndRegisterError(response.ErrorSeverityType, "missing required argument")
	}
	// user-domain can be omitted when user-domain is set in configuration
	if len(args) >= 1 {
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return registerError(response.ErrorSeverityType, err)
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}
	if format == "json" && len(args) > 1 {
		return errors.New("JSON output does not admit filtering")
	}
	if format == "details" && len(args) == 0 {
		return errors.New("Details output available only if a deployment name is specified")
	}

	return nil
}

func runDescribeSolution(cmd *cobra.Command, args []string) {
	meth := "runDescribeSolution"

	userDomain := ""
	name := ""
	role := ""
	instance := ""
	container := ""
	var err error

	// Calculates the user domain and name. If arguments are not provided, then
	// the user dom
	if len(args) > 0 {
		userDomain, name, err = getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
	}
	if len(args) > 1 {
		role = args[1]
	}
	if len(args) > 2 {
		instance = args[2]
	}
	if len(args) > 3 {
		container = args[3]
	}
	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing deployment",
		"name", name,
		"userDomain", userDomain,
		"role", role,
		"instance", instance,
		"container", container,
		"output", output,
		"meth", meth,
	)

	isList := name == ""

	if isList && output == "json" {
		runDescribeSolutionListJSON(cmd, args)
		return
	}

	if isList && output == "table" {
		runDescribeSolutionListTable(cmd, args)
		return
	}

	if isList && output == "details" {
		logger.Fatal("Format 'details' not available in lists", "meth", meth)
	}

	if output == "json" {
		runDescribeSolutionJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeSolutionTable(cmd, args)
		return
	}

	if output == "details" {
		runDescribeSolutionDetails(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeSolutionListJSON(cmd *cobra.Command, args []string) {
	table, _, _, err := getDescribeSolutionListTable(cmd, args)
	if err != nil {
		logger.Fatal(err.Error())
	}
	content, _ := json.Marshal(table)
	manifest, err := gabs.ParseJSON(content)
	if err != nil {
		logger.Fatal("Error generating JSON response", "error", err.Error())
	}
	response.SetData(manifest)
}

func runDescribeSolutionListTable(cmd *cobra.Command, args []string) {
	table, httpInboundsTable, tcpInboundsTable, err := getDescribeSolutionListTable(cmd, args)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if len(table) > 0 {
		tablePrintSolutions(table)

		// Prints HTTP Inbounds
		if len(httpInboundsTable) <= 0 {
			fmt.Println(
				toBold("HTTP Inbounds"),
				":",
				"None",
			)
		} else {

			fmt.Println(
				toBold("HTTP Inbounds"),
				":",
			)

			printTableHTTPInbounds(httpInboundsTable, true, true)
		}

		// Prints TCP Inbounds
		if len(tcpInboundsTable) <= 0 {
			fmt.Println(
				toBold("TCP Inbounds"),
				":",
				"None",
			)
		} else {

			fmt.Println(
				toBold("TCP Inbounds"),
				":",
			)

			printTableTCPInbounds(tcpInboundsTable, true, true)
		}
	} else {
		fmt.Printf("\nNo deployments found.\n")
	}
}

type solutionsTableRow struct {
	Name              string `json:"name"`
	Status            string `json:"status"`
	Ready             bool   `json:"ready"`
	Links             int32  `json:"links"`
	UnlinkedChannels  int32  `json:"unlinkedChannels"`
	Builtins          int32  `json:"builtins"`
	CreationTimestamp string `json:"creationTimestamp"`
	LastModification  string `json:"lastModification"`
}

const (
	UnknownValue     string = "Unknown"
	RunningValue     string = "Running"
	TerminatingValue string = "Terminating"
)

func getDescribeSolutionListTable(
	cmd *cobra.Command,
	args []string,
) (
	table []solutionsTableRow,
	httpInboundsTable []httpInboundTableRow,
	tcpInboundsTable []tcpInboundTableRow,
	err error,
) {
	userDomain := ""
	solutions, err := getSolutionsData(userDomain)
	// solutions, err := admission.DescribeSolutions(userDomain)
	if err != nil {
		return nil, nil, nil, err
	}

	table = make([]solutionsTableRow, 0, len(solutions))

	httpInboundsTable = []httpInboundTableRow{}
	tcpInboundsTable = []tcpInboundTableRow{}

	for solutionName, solutionData := range solutions {
		name := UnknownValue
		status := ""
		ready := true
		builtins := 0
		creationTimestamp := UnknownValue
		lastModification := UnknownValue

		solutionHttpInboundsTable, solutionTcpInboundsTable, _, err := getDescribeInboundTable(solutionData, "")
		if err != nil {
			logger.Error("Error getting inbounds information for solution '%s'. Skipping", solutionName)
		} else {
			httpInboundsTable = append(httpInboundsTable, solutionHttpInboundsTable...)
			tcpInboundsTable = append(tcpInboundsTable, solutionTcpInboundsTable...)
		}

		// Skips empty solutions
		if !solutionData.Exists("deployments") {
			logger.Debug("Deployments not found in solution '%s'. Skipping", solutionName)
			continue
		}

		// Checks if it is being deleted
		if solutionData.Exists("deletionTimestamp") {
			status = calculateSolutionStatus(status, TerminatingValue)
		}

		// Calculates the solution name
		if solutionData.Exists("ref", "name") {
			name = solutionData.Search("ref", "name").Data().(string)
			if solutionData.Exists("ref", "domain") {
				domain := solutionData.Search("ref", "domain").Data().(string)
				name = fmt.Sprintf("%s/%s", domain, name)
			}
		}

		// Calculates the solution creation timestamp
		if solutionData.Exists("creationTimestamp") {
			creationTimestamp = solutionData.Search("creationTimestamp").Data().(string)
		}

		// Calculates the solution last modification timestamp
		if solutionData.Exists("lastModification") {
			lastModification = solutionData.Search("lastModification").Data().(string)
		}

		// Calculates the solution status using its instances status.
		// The possible status are Unknown, Failed, Pending, Succeeded and Running, in
		// this priority. So, the solution status will equal the status of the instance
		// with a higher priority status. So, if an instance is in Unknown status, the solution
		// is in the Unknown status and so on.

		// Gets the solution deployments
		deployments := solutionData.Search("deployments").ChildrenMap()
		for _, deploymentData := range deployments {

			if deploymentData.Exists("artifact", "description", "builtin") {
				roleBuiltin := deploymentData.Search("artifact", "description", "builtin").Data().(bool)
				if roleBuiltin {
					builtins++
					// For now, we assume builin services are running since we don't have any
					// information about thir status
					status = calculateSolutionStatus(status, RunningValue)
				}
			}

			if deploymentData.Exists("artifact", "description", "role") {
				roles := deploymentData.Search("artifact", "description", "role").ChildrenMap()
				for _, roleData := range roles {

					if !roleData.Exists("instances") || roleData.Search("instances").Data() == nil {
						status = calculateSolutionStatus(status, UnknownValue)
						break
					}

					instancesData := roleData.Search("instances")
					for _, instanceData := range instancesData.ChildrenMap() {

						if instanceData.Exists("ready") && instanceData.Search("ready").Data() != nil {
							ready = ready && instanceData.Search("ready").Data().(bool)
						}

						if !instanceData.Exists("status") || instanceData.Search("status").Data() == nil {
							status = calculateSolutionStatus(status, UnknownValue)
							break
						}

						// Updates the solution status. The solution status is running if all instances
						// are running. Otherwise, it is pending.

						instanceStatus := instanceData.Search("status").Data().(string)
						status = calculateSolutionStatus(status, instanceStatus)
					}

					// If the current status is "Uknown" then the solution status will be "Unknown"
					if status == UnknownValue {
						break
					}
				}

			}

		}
		linkedChannels, unlinkedChannels := getLinkedChannelsInfo(solutionData)

		// Creates the new row and appends it to the table
		row := solutionsTableRow{
			Name:              name,
			Status:            status,
			Ready:             ready,
			Links:             linkedChannels,
			UnlinkedChannels:  unlinkedChannels,
			Builtins:          int32(builtins),
			CreationTimestamp: creationTimestamp,
			LastModification:  lastModification,
		}
		table = append(table, row)
	}

	return
}

func getLinkedChannelsInfo(solutionData *gabs.Container) (
	linkedChannels int32,
	unlinkedChannels int32,
) {

	// Calculates the top deployment
	if !solutionData.Exists("top") {
		return 0, 0
	}
	topName := solutionData.Search("top").Data().(string)
	if !solutionData.Exists("deployments", topName) {
		return 0, 0
	}
	topData := solutionData.Search("deployments", topName)

	// Gets the links section (links between deployments in deployments list)
	links := solutionData.Search("links")

	// Gets the number of service channels not linked to a subservice (is not a channel added
	// during the flattening process)
	numServiceChannels := int32(0)
	if topData.Exists("artifact", "description", "srv") {
		srv := topData.Search("artifact", "description", "srv")
		if srv.Exists("server") && srv.Search("server").Data() != nil {
			for serverName, _ := range srv.Search("server").ChildrenMap() {
				flattenedChannel := false
				if links != nil {
					for _, link := range links.Children() {
						targetDeployment := link.Search("t_d").Data().(string)
						targetChannel := link.Search("t_c").Data().(string)
						if targetDeployment == topName {
							if targetChannel == serverName {
								flattenedChannel = true
								break
							}
						}
					}
				}
				if !flattenedChannel {
					numServiceChannels++
				}
			}
		}
		if srv.Exists("duplex") && srv.Search("duplex").Data() != nil {
			for duplexName, _ := range srv.Search("duplex").ChildrenMap() {
				flattenedChannel := false
				if links != nil {
					for _, link := range links.Children() {
						targetDeployment := link.Search("t_d").Data().(string)
						targetChannel := link.Search("t_c").Data().(string)
						if targetDeployment == topName {
							if targetChannel == duplexName {
								flattenedChannel = true
								break
							}
						}
					}
				}
				if !flattenedChannel {
					numServiceChannels++
				}
			}
		}
		if srv.Exists("client") && srv.Search("client").Data() != nil {
			for clientName, _ := range srv.Search("client").ChildrenMap() {
				flattenedChannel := false
				if links != nil {
					for _, link := range links.Children() {
						sourceDeployment := link.Search("s_d").Data().(string)
						sourceChannel := link.Search("s_c").Data().(string)
						if sourceDeployment == topName {
							if sourceChannel == clientName {
								flattenedChannel = true
								break
							}
						}
					}
				}
				if !flattenedChannel {
					numServiceChannels++
				}
			}
		}

		// Calculates the solution number of external links and linked channels
		linkedChannels = 0
		if solutionData.Exists("externalLinks") && solutionData.Search("externalLinks").Data() != nil {
			for _, link := range solutionData.Search("externalLinks").ChildrenMap() {
				if len(link.ChildrenMap()) > 0 {
					linkedChannels++
				}
			}
		}

		unlinkedChannels = numServiceChannels - linkedChannels
	}

	return
}

func runDescribeSolutionJSON(cmd *cobra.Command, args []string) {
	response.SetSpec(responseSpec)
	_, manifest, err := getSolutionData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	test, err := deployment.NewResponse(manifest)
	if err != nil {
		logger.Fatal(fmt.Sprintf("%v", err))
	}
	content, _ := json.MarshalIndent(test, "", "  ")

	manifest, err = gabs.ParseJSON(content)
	if err != nil {
		logger.Fatal(fmt.Sprintf("%v", err))
	}

	response.SetData(manifest)
}

func runDescribeSolutionTable(cmd *cobra.Command, args []string) {
	_, depJSON, err := getSolutionData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	role, instance, container := getRoleInstanceContainer(args)

	printHeaderSolution(depJSON)
	// fmt.Println("")
	tablePrintSolution(depJSON, role, instance, container)
	// fmt.Println("")
	printTableInbounds(depJSON, role)
	// fmt.Println("")
	linkPrintSolution(depJSON)
}

func runDescribeSolutionDetails(cmd *cobra.Command, args []string) {
	_, depJSON, err := getSolutionData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	role, instance, container := getRoleInstanceContainer(args)

	printHeaderSolution(depJSON)
	fmt.Println("")
	detailPrintSolution(depJSON, role, instance, container)
	fmt.Println("")
	linkPrintSolution(depJSON)
	fmt.Println("")
	printEvents(depJSON, role, instance)

}

func calculateSolutionStatus(currentStatus string, instanceStatus string) string {

	states := [][]string{
		{"unknown", "Unknown"},
		{"failed", "Failed"},
		{"terminating", "Terminating"},
		{"pending", "Pending"},
		{"succeeded", "Succeeded"},
		{"running", "Running"},
	}

	for _, state := range states {
		if strings.EqualFold(instanceStatus, state[0]) ||
			strings.EqualFold(currentStatus, state[0]) {
			return state[1]
		}
	}

	return "Unknown"
}

func getSolutionData(fullName string) (string, *gabs.Container, error) {
	userDomain, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	depManifest, depJSON, err := admission.DescribeSolution(userDomain, name)
	if err != nil {
		return "", nil, err
	}
	if depJSON == nil {
		return "", nil, fmt.Errorf("Deployment not found")
	}

	return depManifest, depJSON, nil
}

func getRoleInstanceContainer(args []string) (role, instance, container string) {
	if len(args) > 1 {
		role = args[1]
	}
	if len(args) > 2 {
		instance = args[2]
	}
	if len(args) > 3 {
		container = args[3]
	}
	return
}

func printHeaderSolution(json *gabs.Container) {

	urn := json.Path("urn").Data().(string)
	solutionName := urnToPrintableName(urn)

	fmt.Println(
		toBold("Deployment        "),
		":",
		solutionName,
	)
	fmt.Println(
		toBold("Owner             "),
		":",
		json.Path("owner").Data().(string),
	)
	fmt.Println(
		toBold("Comment           "),
		":",
		json.Path("comment").Data().(string),
	)

	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)

	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)

	serviceName := "null"
	if json.Exists("top") {
		top := json.Search("top").Data().(string)
		if json.Exists("deployments", top, "artifact", "ref") {
			serviceRef := json.Search("deployments", top, "artifact", "ref")
			serviceName = serviceRef.Search("domain").Data().(string) + "/" +
				serviceRef.Search("kind").Data().(string) + "/" +
				serviceRef.Search("name").Data().(string) + "/" +
				serviceRef.Search("version", "0").String() + "_" +
				serviceRef.Search("version", "1").String() + "_" +
				serviceRef.Search("version", "2").String()
		}
	}
	fmt.Printf("%s: %s\n", toBold("Service            "), serviceName)

	if json.Exists("deletionTimestamp") {
		fmt.Println("")
		fmt.Println(toBoldYellowMaintainCase("Deployment is being deleted."))
	}
}

func detailPrintSolution(
	solutionData *gabs.Container,
	rolePrefix,
	instanceInfix,
	containerPrefix string,
) {

	// Initializes the tree
	var tree gotree.Tree

	// Gets the top deployment. Exists if it not exists
	if !solutionData.Exists("top") {
		return
	}
	top := solutionData.Search("top").Data().(string)
	if !solutionData.Exists("deployments", top) ||
		!solutionData.Exists("deployments", top, "artifact", "description") {
		return
	}
	topData := solutionData.Search("deployments", top)

	if rolePrefix == "" {
		tree = treePrint(tree, 0, top)
	} else {
		tree = treePrint(tree, 0, fmt.Sprintf("%s.%s", top, rolePrefix))
	}

	tree = printNode(tree, 0, "", topData, solutionData, rolePrefix, instanceInfix, containerPrefix)

	if tree != nil {
		fmt.Println(tree.Print())
	}
}

func printNode(
	tree gotree.Tree,
	level int,
	rolePath string,
	node *gabs.Container,
	solution *gabs.Container,
	rolePrefix string,
	instanceInfix string,
	containerPrefix string,
) gotree.Tree {

	// Calculates the role title
	roleTitle := ""
	if rolePrefix == "" {
		roleTitle = toBold("Roles") + ":"
	} else {
		roleTitle = toBold("Roles (filtered)") + ":"
	}

	// Prints the role information only if fits the prefix
	if strings.HasPrefix(rolePath, rolePrefix) {
		printNodeDetails(tree, level, rolePath, node, solution, rolePrefix, instanceInfix, containerPrefix)
	}

	// Gets the node ref. Skips this node if the ref is missing
	if !node.Exists("artifact", "ref", "kind") {
		return tree
	}
	nodeKind := node.Search("artifact", "ref", "kind").Data().(string)

	// Appends roles if this node is a service. Append only the nodes inside
	// the role prefix
	if nodeKind != "service" {
		return tree
	}

	// Gets the service roles (if any) represented by a component.
	rolesTree := tree
	rolesHeaderPrinted := false
	if node.Exists("artifact", "description", "role") {
		// Prints the role information only if fits the prefix
		if strings.HasPrefix(rolePath, rolePrefix) {
			rolesTree = treePrint(tree, level+1, roleTitle)
			rolesHeaderPrinted = true
		}
		rolesData := node.Search("artifact", "description", "role")
		if rolesData != nil {
			for roleName, roleData := range rolesData.ChildrenMap() {
				// Prints this role only if is in the rolePrefix
				var newPath string
				if rolePath == "" {
					newPath = roleName
				} else {
					newPath = fmt.Sprintf("%s.%s", rolePath, roleName)
				}
				// newPath = fmt.Sprintf("%s.%s", rolePath, roleName)
				if strings.HasPrefix(rolePrefix, newPath) || strings.HasPrefix(newPath, rolePrefix) {
					roleTree := rolesTree
					newLevel := level
					if rolesHeaderPrinted {
						roleTree = treePrint(rolesTree, level+2, fmt.Sprintf("%s:", roleName))
						newLevel = level + 3
					}
					roleTree = printNode(roleTree, newLevel, newPath, roleData, solution, rolePrefix, instanceInfix, containerPrefix)
				}
			}
		}
	}

	// Gets the service roles (if any) represented by a service
	for deploymentName, deploymentData := range solution.Search("deployments").ChildrenMap() {

		// Gets the node parent name
		if !deploymentData.Exists("up") {
			continue
		}
		upData := deploymentData.Search("up").Data()
		if upData == nil {
			continue
		}
		up := upData.(string)

		// Calculates the name of the parent node
		top := solution.Search("top").Data().(string)
		parent := top
		if rolePath != "" {
			parent = fmt.Sprintf("%s.%s", parent, rolePath)
		}

		// We are only interested in the deployments directly down to this node
		if up == parent {
			if !rolesHeaderPrinted && strings.HasPrefix(rolePath, rolePrefix) {
				rolesTree = treePrint(tree, level+1, roleTitle)
				rolesHeaderPrinted = true
			}
			// Prints this role only if is in the rolePrefix
			newPath := deploymentName[len(top)+1:]
			if strings.HasPrefix(rolePrefix, newPath) || strings.HasPrefix(newPath, rolePrefix) {
				roleName := deploymentName[len(up)+1:]
				roleTree := rolesTree
				newLevel := level
				if rolesHeaderPrinted {
					roleTree = treePrint(rolesTree, level+2, fmt.Sprintf("%s:", roleName))
					newLevel = level + 3
				}
				roleTree = printNode(roleTree, newLevel, newPath, deploymentData, solution, rolePrefix, instanceInfix, containerPrefix)
				if tree == nil {
					tree = roleTree
				}
			}
		}
	}

	return tree
}

func printNodeDetails(
	tree gotree.Tree,
	level int,
	rolePath string,
	node *gabs.Container,
	solution *gabs.Container,
	rolePrefix string,
	instanceInfix string,
	containerPrefix string,
) gotree.Tree {
	nodeRef := node.Search("artifact", "ref")
	refName := nodeRef.Search("name").Data().(string)
	refDomain := nodeRef.Search("domain").Data().(string)
	refKind := nodeRef.Search("kind").Data().(string)
	versionMajor := nodeRef.Search("version", "0").String()
	versionMinor := nodeRef.Search("version", "1").String()
	versionPatch := nodeRef.Search("version", "2").String()
	builtin := node.Search("artifact", "description", "builtin").String()
	treePrint(tree, level+2, fmt.Sprintf("Name: %s/%s/%s.%s.%s", refDomain, refName, versionMajor, versionMinor, versionPatch))
	treePrint(tree, level+2, fmt.Sprintf("Kind: %s", refKind))
	treePrint(tree, level+2, fmt.Sprintf("Builtin: %s", builtin))

	// Shows role's configuration parameters (if any) and their values
	if node.Exists("artifact", "config", "parameter") {
		configData := node.Search("artifact", "config")
		paramMap := configData.Path("parameter").ChildrenMap()
		if len(paramMap) > 0 {
			tree2 := treePrint(tree, level+2, "Parameters:")
			for paramName, paramValue := range paramMap {
				var value string
				valueObj := paramValue.Data()
				switch valueObj.(type) {
				case string:
					value = valueObj.(string)
				default:
					value = paramValue.String()
				}
				if len(value) > 50 {
					value = fmt.Sprintf("%s...", value[:50])
				}
				treePrint(tree2, level+3, fmt.Sprintf("%s:", paramName), value)
			}
		}
	}

	// Shows channels and their connections
	if node.Exists("artifact", "description", "srv") {
		tree := treePrint(tree, level+2, "Channels:")
		channelsJSON := node.Search("artifact", "description", "srv")
		for _, channelType := range channelTypes {
			if channelsJSON.Exists(channelType) {
				for channelName, channelConfig := range channelsJSON.Path(channelType).ChildrenMap() {
					text := fmt.Sprintf("%s:", channelName)
					var protocol string
					if channelConfig.Exists("protocol") && (channelConfig.Search("protocol").Data() != nil) {
						protocol = channelConfig.Search("protocol").Data().(string)
						text = fmt.Sprintf("%s %s", text, protocol)
					}
					if channelConfig.Exists("port") {
						port := channelConfig.Search("port").String()
						if len(protocol) == 0 {
							text = fmt.Sprintf("%s %s", text, port)
						} else {
							text = fmt.Sprintf("%s/%s", text, port)
						}
						if channelConfig.Exists("portnum") {
							portnum := channelConfig.Search("portnum").String()
							if iportnum, err := strconv.Atoi(portnum); err == nil && (iportnum > 1) {
								if iport, err := strconv.Atoi(port); err == nil {
									lastport := iport + iportnum - 1
									text = fmt.Sprintf("%s..%d", text, lastport)
								} else {
									if err != nil {
										logger.Warn("Port %s in %s is not a number", port, rolePath)
									}
								}
							} else if iportnum < 1 {
								logger.Warn("Portnum %s in %s is not a number", portnum, rolePath)
							}
						}
					}
					treePrint(tree, level+3, text)
				}
			}
		}
	}

	// Shows information about instances if this node is a component
	if refKind == "component" {
		if instanceInfix == "" {
			tree = treePrint(tree, level+2, "Instances:")
		} else {
			tree = treePrint(tree, level+2, "Instances (filtered):")
		}
		for instance, instanceJSON := range node.Path("instances").ChildrenMap() {

			// Convert Pod name to Kumori instance name. If an instace filter was provided
			// and it doesn't math the instance name, skip this instance.
			relativeName := instance[39:]
			instanceName := fmt.Sprintf("instance-%s", relativeName)

			if (instanceInfix != "") && (instanceName != instanceInfix) {
				continue
			}

			tree := treePrint(tree, level+3, instanceName+":")
			if instanceJSON.Exists("deletionTimestamp") {
				treePrint(tree, level+4, "Terminating:", "true")
			}
			if instanceJSON.Exists("status") && (instanceJSON.Path("status").Data() != nil) {
				treePrint(tree, 4, "Status:", instanceJSON.Path("status").Data().(string))
			} else {
				treePrint(tree, 4, "Status:", "<unknown>")
			}
			if instanceJSON.Exists("statusReason") && (instanceJSON.Path("statusReason").Data() != nil) {
				treePrint(tree, 4, "Reason:", instanceJSON.Path("statusReason").Data().(string))
			} else {
				treePrint(tree, 4, "Reason:", "<unknown>")
			}
			if instanceJSON.Exists("statusMessage") && (instanceJSON.Path("statusMessage").Data() != nil) {
				treePrint(tree, 4, "Message:", instanceJSON.Path("statusMessage").Data().(string))
			} else {
				treePrint(tree, 4, "Message:", "<unknown>")
			}

			if instanceJSON.Exists("ready") && (instanceJSON.Path("ready").Data() != nil) {
				ready := instanceJSON.Path("ready").Data().(bool)
				if ready {
					treePrint(tree, 4, "Ready: Yes")
				} else {
					treePrint(tree, 4, "Ready: No")
				}
			} else {
				treePrint(tree, 4, "Ready:", "<unknown>")
			}
			if instanceJSON.Exists("creationDate") && (instanceJSON.Path("creationDate").Data() != nil) {
				time1, err := time.Parse(time.RFC3339, instanceJSON.Path("creationDate").Data().(string))
				if err != nil {
					logger.Debug("Error while parsing date: " + err.Error())
					treePrint(tree, 4, "Creation time:", "<unknown>")
				} else {
					treePrint(tree, 4, "Creation time:", time1.Format(time.RFC1123Z))
				}
			} else {
				treePrint(tree, 4, "Creation time:", "<unknown>")
			}
			if instanceJSON.Exists("node") && (instanceJSON.Path("node").Data() != nil) {
				treePrint(tree, 4, "Node:", instanceJSON.Path("node").Data().(string))
			} else {
				treePrint(tree, 4, "Node:", "<unknown>")
			}
			tree4 := treePrint(tree, level+4, "Total resources:")
			if instanceJSON.Exists("metrics", "usage") {
				treePrint(tree4, 5, "CPU:", instanceJSON.Search("metrics", "usage", "cpu").String(), "/", instanceJSON.Search("metrics", "requests", "cpu").String(), "/", instanceJSON.Search("metrics", "limits", "cpu").String(), "(current / request / limit)")
				treePrint(tree4, 5, "Memory:", bytefmt.ByteSize(uint64(instanceJSON.Search("metrics", "usage", "memory").Data().(float64))), "/", bytefmt.ByteSize(uint64(instanceJSON.Search("metrics", "requests", "memory").Data().(float64))), "/", bytefmt.ByteSize(uint64(instanceJSON.Search("metrics", "limits", "memory").Data().(float64))), "(current / request / limit)")
			} else {
				treePrint(tree4, 5, "<not available>")
			}
			if containerPrefix == "" {
				tree = treePrint(tree, level+4, "Containers:")
			} else {
				tree = treePrint(tree, level+4, "Containers (filtered):")
			}
			for container, containerJSON := range instanceJSON.Path("containers").ChildrenMap() {
				if !strings.HasPrefix(container, containerPrefix) {
					continue
				}
				tree := treePrint(tree, level+5, container+":")
				for status, statusData := range containerJSON.Path("state").ChildrenMap() {
					treePrint(tree, level+6, "Status:", strings.Title(status))
					if statusData.Exists("reason") && (statusData.Search("reason").Data() != nil) {
						reason := statusData.Search("reason").Data().(string)
						treePrint(tree, level+6, "Reason:", strings.Title(reason))
					}
					if statusData.Exists("exitCode") && (statusData.Search("exitCode").Data() != nil) {
						exitCode := uint64(statusData.Search("exitCode").Data().(float64))
						treePrint(tree, level+6, "Exit code:", fmt.Sprintf("%d", exitCode))
					}
					break
				}
				// // Add reason when status is Waiting
				// if containerJSON.Exists("state", "waiting", "reason") {
				// 	if containerJSON.Search("state", "waiting", "reason").Data() != nil {
				// 		treePrint(tree, level+6, "Reason:", containerJSON.Search("state", "waiting", "reason").Data().(string))
				// 	}
				// }

				init := false
				if node.Exists("artifact", "description", "code", container, "init") &&
					(node.Search("artifact", "description", "code", container, "init").Data() != nil) {
					init = node.Search("artifact", "description", "code", container, "init").Data().(bool)
				}
				if init {
					treePrint(tree, level+6, "Init: Yes")
				} else {
					treePrint(tree, level+6, "Init: No")
				}

				if containerJSON.Exists("ready") && (containerJSON.Path("ready").Data() != nil) {
					ready := containerJSON.Path("ready").Data().(bool)
					if ready {
						treePrint(tree, level+6, "Ready: Yes")
					} else {
						treePrint(tree, level+6, "Ready: No")
					}
				} else {
					treePrint(tree, level+6, "Ready:", "<unknown>")
				}
				// Shows the container image and the secret name (if any)
				if node.Exists("artifact", "description", "code", container, "image") && (node.Search("artifact", "description", "code", container, "image", "hub", "name").Data() != nil) {
					hub := node.Search("artifact", "description", "code", container, "image", "hub", "name").Data().(string)
					tag := node.Search("artifact", "description", "code", container, "image", "tag").Data().(string)
					treePrint(tree, level+6, fmt.Sprintf("Image: %s/%s", hub, tag))
					if node.Exists("artifact", "description", "code", container, "image", "hub", "secret") && (node.Search("artifact", "description", "code", container, "image", "hub", "secret").Data() != nil) {
						secret := node.Search("artifact", "description", "code", container, "image", "hub", "secret").Data().(string)
						treePrint(tree, level+6, fmt.Sprintf("Secret: %s", secret))
					}
				}
				if containerJSON.Exists("state", "running", "startedAt") {
					if containerJSON.Search("state", "running", "startedAt").Data() == nil {
						treePrint(tree, level+6, "Start time:", "<unknown>")
						treePrint(tree, level+6, "Termination time:", "<none>")
					} else {
						time1, err := time.Parse(time.RFC3339, containerJSON.Search("state", "running", "startedAt").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
							treePrint(tree, level+6, "Start time:", "<unknown>")
							treePrint(tree, level+6, "Termination time:", "<none>")
						} else {
							treePrint(tree, level+6, "Start time:", time1.Format(time.RFC1123Z))
							treePrint(tree, level+6, "Termination time:", "<none>")
						}
					}
				} else if containerJSON.Exists("state", "terminated", "startedAt") {
					if containerJSON.Search("state", "terminated", "startedAt").Data() == nil {
						treePrint(tree, level+6, "Start time:", "<unknown>")
					} else {
						time1, err := time.Parse(time.RFC3339, containerJSON.Search("state", "terminated", "startedAt").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
							treePrint(tree, level+6, "Start time:", "<unknown>")
						} else {
							treePrint(tree, level+6, "Start time:", time1.Format(time.RFC1123Z))
						}
					}
					if containerJSON.Search("state", "terminated", "finishedAt").Data() == nil {
						treePrint(tree, level+6, "Termination time:", "<unknown>")
					} else {
						time1, err := time.Parse(time.RFC3339, containerJSON.Search("state", "terminated", "finishedAt").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
							treePrint(tree, level+6, "Termination time:", "<unknown>")
						} else {
							treePrint(tree, level+6, "Termination time:", time1.Format(time.RFC1123Z))
						}
					}
				} else {
					treePrint(tree, level+6, "Start time:", "<none>")
					treePrint(tree, level+6, "Termination time:", "<none>")
				}
				if containerJSON.Exists("restarts") && (containerJSON.Path("restarts").Data() != nil) {
					treePrint(tree, level+6, "# restarts:", containerJSON.Path("restarts").String())
				} else {
					treePrint(tree, level+6, "# restarts:", "<unknown>")
				}

				// container LastState information
				if containerJSON.Exists("lastState", "terminated") {
					lastStateSubtree := treePrint(tree, level+6, "LastState:")
					if containerJSON.Search("lastState", "terminated", "reason").Data() != nil {
						treePrint(lastStateSubtree, level+7, "Reason:   ", containerJSON.Search("lastState", "terminated", "reason").Data().(string))
					}
					if containerJSON.Exists("lastState", "terminated", "message") && containerJSON.Search("lastState", "terminated", "message").Data() != nil {
						treePrint(lastStateSubtree, level+7, "Message:   ", containerJSON.Search("lastState", "terminated", "message").Data().(string))
					}
					if containerJSON.Search("lastState", "terminated", "exitCode").Data() != nil {
						treePrint(lastStateSubtree, level+7, "Exit Code:", containerJSON.Search("lastState", "terminated", "exitCode").String())
					}
					if containerJSON.Search("lastState", "terminated", "startedAt").Data() != nil {
						time1, err := time.Parse(time.RFC3339, containerJSON.Search("lastState", "terminated", "startedAt").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
							treePrint(lastStateSubtree, level+7, "Started:  ", "<none>")
						} else {
							treePrint(lastStateSubtree, level+7, "Started:  ", time1.Format(time.RFC1123Z))
						}
					}
					if containerJSON.Search("lastState", "terminated", "finishedAt").Data() != nil {
						time1, err := time.Parse(time.RFC3339, containerJSON.Search("lastState", "terminated", "finishedAt").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
							treePrint(lastStateSubtree, level+7, "Finished: ", "<none>")
						} else {
							treePrint(lastStateSubtree, level+7, "Finished: ", time1.Format(time.RFC1123Z))
						}
					}
				}

				tree6 := treePrint(tree, level+6, "Resources:")
				if containerJSON.Exists("metrics") && (containerJSON.Search("metrics").Data() != nil) {
					treePrint(tree6, level+7, "CPU:", containerJSON.Search("metrics", "usage", "cpu").String(), "/", containerJSON.Search("metrics", "requests", "cpu").String(), "/", containerJSON.Search("metrics", "limits", "cpu").String(), "(current / request / limit)")
					treePrint(tree6, level+7, "Memory:", bytefmt.ByteSize(uint64(containerJSON.Search("metrics", "usage", "memory").Data().(float64))), "/", bytefmt.ByteSize(uint64(containerJSON.Search("metrics", "requests", "memory").Data().(float64))), "/", bytefmt.ByteSize(uint64(containerJSON.Search("metrics", "limits", "memory").Data().(float64))), "(current / request / limit)")
				} else {
					treePrint(tree6, level+7, "<not available>")
				}
				// Details the container mounted files and environment variables
				if node.Exists("artifact", "description", "code", container, "mapping") && (node.Search("artifact", "description", "code", container, "mapping").Data() != nil) {
					mappingJSON := node.Search("artifact", "description", "code", container, "mapping")
					// Details mounted files
					if node.Exists("artifact", "description", "code", container, "mapping", "filesystem") && (node.Search("artifact", "description", "code", container, "mapping", "filesystem").Data() != nil) {
						tree7 := treePrint(tree, level+6, fmt.Sprintf("Mount points:"))
						for _, mappedFile := range mappingJSON.Path("filesystem").ChildrenMap() {
							filePath := mappedFile.Search("path").Data().(string)
							tree8 := treePrint(tree7, level+7, filePath)
							fileMode := mappedFile.Search("mode").String()
							treePrint(tree8, level+8, "Mode", fileMode)
							if mappedFile.Exists("format") && (mappedFile.Search("format").Data() != nil) {
								fileFormat := mappedFile.Search("format").Data().(string)
								treePrint(tree8, level+8, "Format:", fileFormat)
							}
							reboot := mappedFile.Search("rebootOnUpdate").String()
							treePrint(tree8, level+8, "Reboot on update:", reboot)
							if mappedFile.Exists("data") && (mappedFile.Search("data").Data() != nil) {
								// The file data is also shown. However, if the content is larger then 50 chars,
								// only the first 50 chars are shown
								data := mappedFile.Search("data").String()
								if len(data) > 50 {
									data = fmt.Sprintf("%s...", data[:50])
								}
								treePrint(tree8, level+8, "Data:", data)
							}
							if mappedFile.Exists("secret") && (mappedFile.Search("secret").Data() != nil) {
								secret := mappedFile.Search("secret").Data().(string)
								treePrint(tree8, level+8, "Secret:", secret)
							}
						}
					}
					// Details environment variables
					if node.Exists("artifact", "description", "code", container, "mapping", "env") && (node.Search("artifact", "description", "code", container, "mapping", "env").Data() != nil) {
						tree7 := treePrint(tree, level+6, fmt.Sprintf("Environment variables:"))
						for envName, envContent := range mappingJSON.Path("env").ChildrenMap() {
							if envContent.Exists("secret") {
								secret := envContent.Search("secret").Data().(string)
								tree8 := treePrint(tree7, level+7, fmt.Sprintf("%s:", envName))
								treePrint(tree8, level+8, "Secret:", secret)
							}
							if envContent.Exists("value") {
								// The environment variable data is also shown. However, if the content is larger
								// then 50 chars, only the first 50 chars are shown
								value := envContent.Search("value").Data().(string)
								if len(value) > 50 {
									value = fmt.Sprintf("%s...", value[:50])
								}
								treePrint(tree7, level+7, fmt.Sprintf("%s:", envName), value)
							}
						}
					}
				}
			}
		}

	}

	return tree

}

func tablePrintSolutions(table []solutionsTableRow) {
	solutionTable := tablewriter.NewWriter(os.Stdout)
	solutionTable.SetHeader([]string{"Name", "Status", "Ready", "Links", "Unlinked Channels", "Builtins", "Created", "Last Modification"})
	solutionsData := make([][]string, 0, len(table))
	for _, row := range table {
		solutionData := []string{
			row.Name,
			colourInstanceStatus(row.Status),
			colourReady(row.Ready),
			strconv.Itoa(int(row.Links)),
			strconv.Itoa(int(row.UnlinkedChannels)),
			strconv.Itoa(int(row.Builtins)),
			row.CreationTimestamp,
			row.LastModification,
		}

		solutionsData = append(solutionsData, solutionData)
	}
	solutionTable.AppendBulk(solutionsData)
	solutionTable.SetAutoFormatHeaders(false)
	solutionTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	solutionTable.SetAlignment(tablewriter.ALIGN_LEFT)
	solutionTable.SetCenterSeparator("+")
	solutionTable.SetColumnSeparator("|")
	solutionTable.SetRowSeparator("-")
	solutionTable.SetHeaderLine(true)
	solutionTable.SetBorder(true)
	solutionTable.SetNoWhiteSpace(false)
	solutionTable.Render()
}

type solutionTableRow struct {
	Role      string                     `json:"role"`
	Init      *bool                      `json:"init"`
	Instance  *string                    `json:"instance"`
	Container *string                    `json:"container"`
	Status    *string                    `json:"status"`
	Ready     *bool                      `json:"ready"`
	CPU       *solutionTableRequestLimit `json:"cpu"`
	Memory    *solutionTableRequestLimit `json:"memory"`
	Restarts  *int32                     `json:"restarts"`
	Age       *string                    `json:"age"`
	ExitCode  *uint64                    `json:"exitCode"`
}

type solutionTableRequestLimit struct {
	Request *int `json:"request"`
	Limit   *int `json:"limit"`
}

func getDescribeSolutionTable(
	solutionData *gabs.Container,
	rolePrefix string,
	instanceInfix string,
	containerPrefix string,
) ([]solutionTableRow, error) {

	// Skips if a top deployment is not declared in this solution
	if !solutionData.Exists("top") {
		return nil, nil
	}
	top := solutionData.Search("top").Data().(string)
	if len(top) <= 0 {
		return nil, nil
	}

	// Skips if the top deployment is not found in this solution
	if !solutionData.Exists("deployments", top) {
		return nil, nil
	}

	// Sets the deploymentPrefix to be used. This prefix will filter which
	// deployments in this solutions are going to be included in the describe.
	// If a rolePrefix is provided, we are only interested in roles such
	// that the combination of the top and the rolePrefix is included in the
	// combination of the deployment holding that role and the role name. For
	// example, if top="calculator" and rolePrefix="hazel.datum" then we will
	// only take into account roles such that "calculator.hazel.datum" is included
	// in <DEPLOYMENT_NAME>.<ROLE_NAME>
	deploymentPrefix := top
	if len(rolePrefix) > 0 {
		deploymentPrefix = fmt.Sprintf("%s.%s", deploymentPrefix, rolePrefix)
	}

	// Initializes table
	table := []solutionTableRow{}

	// Inspects the solution deployments
	deployments := solutionData.Search("deployments").ChildrenMap()
	for deploymentName, deploymentData := range deployments {

		// Checks if this is a built-in service
		builtin := false
		if deploymentData.Exists("artifact", "description", "builtin") {
			builtin = deploymentData.Search("artifact", "description", "builtin").Data().(bool)
		}
		if builtin {
			logger.Debug(fmt.Sprintf("Skipping deployment '%s': contains a builtin service", deploymentName))
			continue
		}

		// Skips a deployment if it hasn't any role defined
		if !deploymentData.Exists("artifact", "description", "role") {
			logger.Debug(fmt.Sprintf("Skipping deployment '%s': contains a builtin service", deploymentName))
			continue
		}

		// Inspects the deployment roles
		roles := deploymentData.Search("artifact", "description", "role").ChildrenMap()
		for roleName, roleData := range roles {

			// Skips the role if it is a built-in role
			builtin := false
			if roleData.Exists("artifact", "description", "builtin") {
				builtin = roleData.Search("artifact", "description", "builtin").Data().(bool)
			}
			if builtin {
				logger.Debug(fmt.Sprintf("Skipping role '%s' in deployment '%s': is a builtin role", roleName, deploymentName))
				continue
			}

			// Skips the role if it not fits the rolePrefix
			roleCompleteName := fmt.Sprintf("%s.%s", deploymentName, roleName)
			if !strings.HasPrefix(roleCompleteName, deploymentPrefix) {
				logger.Debug(fmt.Sprintf("Skipping role '%s' in deployment '%s': does not fit the prefix '%s'", roleName, deploymentName, deploymentPrefix))
				continue
			}

			// Calculates the role name excluding the top deployment name (i.e., the role path). For
			// example, calculator.hazel will be converted to just "hazel"
			topLength := len(top) + 1
			roleNamePath := roleCompleteName[topLength:]

			// Inspects the role instances
			roleHasInstances := false
			if roleData.Exists("instances") {
				instances := roleData.Search("instances").ChildrenMap()
				for instanceName, instanceData := range instances {

					// Converts the instance name in the format used by the user in the command.
					// TODO: REVISAR ESTA DESCRIPCION PARA EL CASO DE LOS NUEVOS NOMBRES CON HASHES
					//       EJEMPLOS:
					//         - kd-185531-352a1f2b-fb10f1a1-deployment-6df5658c7f-cqb6s
					//         - kd-185531-352a1f2b-fb10f1a1-deployment-0

					// EXPLICACION VIEJA
					// For statefull instances:
					// * Internal id: kd-103528-81ea465d-frontend-deployment-0
					// * User id: instance-0
					// For stateless instances:
					// * Internal id: kd-103209-1dbd9647-frontend-deployment-5f84998b56-lt4s6
					// * User id: instance-5f84998b56-lt4s6
					// Prefix length:
					// * len("kd-103209-1dbd9647-") + len("role") + len("-deployment-")

					// internalIdPrefixLength := 19 + len(roleName) + 12
					internalIdPrefixLength := 39
					instanceSuffix := instanceName[internalIdPrefixLength:]
					userInstanceName := fmt.Sprintf("instance-%s", instanceSuffix)

					// kd-185531-352a1f2b-fb10f1a1-deployment-897dfd46c-4lftw mus be converted to
					// instance-897dfd46c-4lftw. Len(kd-185531-352a1f2b-fb10f1a1-deployment-) = 39

					// Checks if this instance fits the instanceInfix
					if len(instanceInfix) > 0 {

						// Skips the instance if it not fits the instance filter
						if instanceInfix != userInstanceName {
							continue
						}
					}

					roleHasInstances = true

					irow := solutionTableRow{
						Role:     roleNamePath,
						Instance: &userInstanceName,
					}

					// Determine if instance is being deleted
					isTerminating := false
					if instanceData.Exists("deletionTimestamp") {
						isTerminating = true
					}
					if isTerminating {
						status := "Terminating"
						irow.Status = &status
					} else {
						if instanceData.Exists("status") && (instanceData.Path("status").Data() != nil) {
							status := instanceData.Path("status").Data().(string)
							// If there is an instance status reason, show that as the Status
							if instanceData.Exists("statusReason") && (instanceData.Path("statusReason").Data() != nil) {
								status = instanceData.Path("statusReason").Data().(string)
							}
							irow.Status = &status
						}
					}

					// Calculates the instance age
					if instanceData.Exists("creationDate") && (instanceData.Path("creationDate").Data() != nil) {
						time1, err := time.Parse(time.RFC3339, instanceData.Path("creationDate").Data().(string))
						if err != nil {
							logger.Debug("Error while parsing date: " + err.Error())
						} else {
							age := duration.HumanDuration(time.Since(time1))
							irow.Age = &age
						}
					}

					// Calculates the instance CPU and memory consumption
					instanceCpuReqLim := solutionTableRequestLimit{}
					instanceMemReqLim := solutionTableRequestLimit{}
					if instanceData.Exists("metrics", "usage") && (instanceData.Search("metrics", "usage").Data() != nil) {
						if instanceData.Exists("metrics", "usage", "cpu") {
							usage := instanceData.Search("metrics", "usage", "cpu").Data().(float64)
							if instanceData.Exists("metrics", "requests", "cpu") &&
								instanceData.Search("metrics", "requests", "cpu").Data().(float64) != 0 {
								cpu := int(math.Round(usage / instanceData.Search("metrics", "requests", "cpu").Data().(float64) * 100))
								instanceCpuReqLim.Request = &cpu
							}
							if instanceData.Exists("metrics", "limits", "cpu") &&
								instanceData.Search("metrics", "limits", "cpu").Data().(float64) != 0 {
								cpu := int(math.Round(usage / instanceData.Search("metrics", "limits", "cpu").Data().(float64) * 100))
								instanceCpuReqLim.Limit = &cpu
							}

						}
						if instanceData.Exists("metrics", "usage", "memory") {
							usage := instanceData.Search("metrics", "usage", "memory").Data().(float64)
							if instanceData.Exists("metrics", "requests", "memory") &&
								instanceData.Search("metrics", "requests", "memory").Data().(float64) != 0 {
								memory := int(math.Round(usage / instanceData.Search("metrics", "requests", "memory").Data().(float64) * 100))
								instanceMemReqLim.Request = &memory
							}
							if instanceData.Exists("metrics", "limits", "memory") &&
								instanceData.Search("metrics", "limits", "memory").Data().(float64) != 0 {
								memory := int(math.Round(usage / instanceData.Search("metrics", "limits", "memory").Data().(float64) * 100))
								instanceMemReqLim.Request = &memory
							}
						}
					}

					if instanceCpuReqLim.Request != nil || instanceCpuReqLim.Limit != nil {
						irow.CPU = &instanceCpuReqLim
					}
					if instanceMemReqLim.Request != nil || instanceMemReqLim.Limit != nil {
						irow.Memory = &instanceMemReqLim
					}

					// Calculates if this instance is in ready state
					if instanceData.Exists("ready") && (instanceData.Path("ready").Data() != nil) {
						ready := instanceData.Path("ready").Data().(bool)
						irow.Ready = &ready
					}

					// Appends a new row in the result
					table = append(table, irow)

					// Adds new row per container, taking the container filter into account
					if instanceData.Exists("containers") && (instanceData.Path("containers").Data() != nil) {
						for containerName, containerData := range instanceData.Path("containers").ChildrenMap() {

							// Skips this container if it not fits the container prefix
							if !strings.HasPrefix(containerName, containerPrefix) {
								continue
							}

							containerNameCopy := containerName
							crow := solutionTableRow{
								Role:      irow.Role,
								Instance:  irow.Instance,
								Container: &containerNameCopy,
							}

							// Calculates the container status
							if containerData.Exists("state") && (containerData.Path("state").Data() != nil) {
								for statusName, statusData := range containerData.Path("state").ChildrenMap() {
									if strings.EqualFold(statusName, "waiting") && (statusData.Search("reason").Data() != nil) {
										status := statusData.Path("reason").Data().(string)
										crow.Status = &status
									} else if strings.EqualFold(statusName, "terminated") && (statusData.Search("reason").Data() != nil) {
										status := statusData.Path("reason").Data().(string)
										crow.Status = &status
									} else {
										status := statusName
										crow.Status = &status
									}

									if statusData.Exists("exitCode") && (statusData.Search("exitCode").Data() != nil) {
										exitCode := uint64(statusData.Search("exitCode").Data().(float64))
										crow.ExitCode = &exitCode
									}
									break
								}
							}

							// Calculates the container age
							if containerData.Exists("state", "running", "startedAt") && (containerData.Search("state", "running", "startedAt").Data() != nil) {
								time1, err := time.Parse(time.RFC3339, containerData.Search("state", "running", "startedAt").Data().(string))
								if err != nil {
									logger.Debug("Error while parsing date: " + err.Error())
								} else {
									age := duration.HumanDuration(time.Since(time1))
									crow.Age = &age
								}
							} else if containerData.Exists("state", "terminated", "finishedAt") && (containerData.Search("state", "terminated", "finishedAt").Data() != nil) {
								time1, err := time.Parse(time.RFC3339, containerData.Search("state", "terminated", "finishedAt").Data().(string))
								if err != nil {
									logger.Debug("Error while parsing date: " + err.Error())
								} else {
									age := duration.HumanDuration(time.Since(time1))
									crow.Age = &age
								}
							}

							// Calculates if the container is ready or not
							if containerData.Exists("ready") && (containerData.Path("ready").Data() != nil) {
								ready := instanceData.Path("ready").Data().(bool)
								crow.Ready = &ready
							}

							// Calculates the instance CPU and memory consumption
							containerCpuReqLim := solutionTableRequestLimit{}
							containerMemReqLim := solutionTableRequestLimit{}
							if containerData.Exists("metrics", "usage") && (containerData.Search("metrics", "usage").Data() != nil) {
								usage := containerData.Search("metrics", "usage", "cpu").Data().(float64)
								if containerData.Exists("metrics", "usage", "cpu") {
									if containerData.Exists("metrics", "requests", "cpu") &&
										(containerData.Search("metrics", "requests", "cpu").Data().(float64) != 0) {
										cpu := int(math.Round(usage / containerData.Search("metrics", "requests", "cpu").Data().(float64) * 100))
										containerCpuReqLim.Request = &cpu
									}
									if containerData.Exists("metrics", "limits", "cpu") &&
										(containerData.Search("metrics", "limits", "cpu").Data().(float64) != 0) {
										cpu := int(math.Round(usage / containerData.Search("metrics", "limits", "cpu").Data().(float64) * 100))
										containerCpuReqLim.Limit = &cpu
									}
								}
								if containerData.Exists("metrics", "usage", "memory") {
									usage := containerData.Search("metrics", "usage", "memory").Data().(float64)
									if containerData.Exists("metrics", "requests", "memory") &&
										(containerData.Search("metrics", "requests", "memory").Data().(float64) != 0) {
										memory := int(math.Round(usage / containerData.Search("metrics", "requests", "memory").Data().(float64) * 100))
										containerMemReqLim.Request = &memory
									}
									if containerData.Exists("metrics", "limits", "memory") &&
										(containerData.Search("metrics", "limits", "memory").Data().(float64) != 0) {
										memory := int(math.Round(usage / containerData.Search("metrics", "limits", "memory").Data().(float64) * 100))
										containerMemReqLim.Limit = &memory
									}
								}
							}
							if containerCpuReqLim.Request != nil || containerCpuReqLim.Limit != nil {
								crow.CPU = &containerCpuReqLim
							}
							if containerMemReqLim.Request != nil || containerMemReqLim.Limit != nil {
								crow.Memory = &containerMemReqLim
							}

							// Calculates restarts
							if containerData.Exists("restarts") && containerData.Search("restarts").Data() != nil {
								restarts := int32(math.Round(containerData.Path("restarts").Data().(float64)))
								crow.Restarts = &restarts
							}

							// Calculates init flag
							init := false
							if roleData.Exists("artifact", "description", "code", containerName, "init") && roleData.Search("artifact", "description", "code", containerName, "init").Data() != nil {
								init = roleData.Search("artifact", "description", "code", containerName, "init").Data().(bool)
							}
							crow.Init = &init

							table = append(table, crow)
						}
					}
				}
			}
			if !roleHasInstances {
				row := solutionTableRow{
					Role: roleNamePath,
				}
				table = append(table, row)
			}
		}
	}

	// Returns table
	return table, nil
}

func tablePrintSolution(
	solutionData *gabs.Container,
	rolePrefix string,
	instanceInfix string,
	containerPrefix string,
) {

	// Gets the solution table describing the solution elements
	table, err := getDescribeSolutionTable(
		solutionData,
		rolePrefix,
		instanceInfix,
		containerPrefix,
	)

	// If an error occurs, shows an erro including provided filters
	if err != nil {
		if instanceInfix == "" {
			logger.Error(fmt.Sprintf("Error getting solution data. Role: '%s': %s", rolePrefix, err.Error()))
			return
		}
		if containerPrefix == "" {
			logger.Error(fmt.Sprintf("Error getting solution data. Role: '%s'. Instance: '%s': %s", rolePrefix, instanceInfix, err.Error()))
			return
		}
		logger.Error(fmt.Sprintf("Error getting solution data. Role: '%s'. Instance: '%s'. Container: '%s': %s", rolePrefix, instanceInfix, containerPrefix, err.Error()))
		return
	}

	// Initializes headers
	solutionTable := tablewriter.NewWriter(os.Stdout)
	solutionTable.SetHeader([]string{"Role", "Instance", "Container", "Status", "Ready", "CPU (Request)", "CPU (Limit)", "Memory", "Restarts", "Init", "Age"})

	// If the number of rows is empty, just prints a message and exits
	if len(table) <= 0 {
		fmt.Println(
			toBold("Roles"),
			":",
			"None",
		)
		return
	}

	// Initializes the structure containing the table data
	solutionTableData := make([][]string, 0, len(table))

	// Creates the solution data from table rows
	for _, row := range table {

		// If this rows represents a role without instances, just print
		// a row containing the role name
		if row.Instance == nil {
			rowData := []string{
				row.Role,
				"<none>",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
			}
			solutionTableData = append(solutionTableData, rowData)
			continue
		}

		// Calculates container name
		var container string
		if row.Container != nil {
			container = *row.Container
		}

		// Calculates status
		var status string
		if row.Status != nil {
			if row.Container == nil {
				status = colourInstanceStatus(*row.Status)
			} else {
				status = colourContainerStatus(&row)
			}
		} else {
			status = toBoldRed(UnknownValue)
		}

		// Calculates age
		var age string
		if row.Age != nil {
			age = *row.Age
		} else {
			age = toBoldRed(UnknownValue)
		}

		// Calculates requested CPU consumption
		var requestedCpu string
		if row.CPU != nil && row.CPU.Request != nil {
			requestedCpu = colourPercentageHigh(*row.CPU.Request, 70, 90)
		} else {
			requestedCpu = "n/a"
		}

		// Calculates limit CPU consumption
		var limitCpu string
		if row.CPU != nil && row.CPU.Limit != nil {
			limitCpu = colourPercentageHigh(*row.CPU.Limit, 70, 90)
		} else {
			limitCpu = "n/a"
		}

		// Calculates memory
		var memory string
		if row.Memory != nil && row.Memory.Request != nil {
			memory = colourPercentageHigh(*row.Memory.Request, 70, 90)
		} else {
			memory = "n/a"
		}

		// Calculates ready
		var ready string
		if row.Ready != nil {
			ready = colourReady(*row.Ready)
		} else {
			ready = toBoldRed(UnknownValue)
		}

		// Calculates restarts
		var restarts string
		if row.Restarts != nil {
			restarts = fmt.Sprintf("%d", *row.Restarts)
		}

		// Calculates init
		var init string
		if row.Init != nil {
			if *row.Init {
				init = fmt.Sprintf("Yes")
			} else {
				init = fmt.Sprintf("No")
			}
		}

		// Adds the new row
		rowData := []string{
			row.Role,
			*row.Instance,
			container,
			status,
			ready,
			requestedCpu,
			limitCpu,
			memory,
			restarts,
			init,
			age,
		}
		solutionTableData = append(solutionTableData, rowData)

	}

	fmt.Println(
		toBold("Roles"),
		":",
	)

	// Orders solutionData
	// Example:
	// 	frontend  ---> subservices depth = 0 (lexicografic order)
	// 	worker
	// 	subd1.aa  ---> subservices depth = 1 (lexicografic order)
	// 	subd1.bb
	// 	subd2.cc
	// 	subd2.dd
	// 	subd1.subd3.ee ---> subservices depth = 2 (lexicografic order)
	// 	subd1.subd3.ff
	//
	sort.Slice(
		solutionTableData,
		func(i, j int) bool {
			role1 := strings.ToLower(solutionTableData[i][0])
			role2 := strings.ToLower(solutionTableData[j][0])
			role1Dots := strings.Count(role1, ".")
			role2Dots := strings.Count(role2, ".")
			instance1 := strings.ToLower(solutionTableData[i][1])
			instance2 := strings.ToLower(solutionTableData[j][1])
			container1 := strings.ToLower(solutionTableData[i][2])
			container2 := strings.ToLower(solutionTableData[j][2])
			if role1Dots != role2Dots {
				return role1Dots < role2Dots
			}
			if role1 != role2 {
				return role1 < role2
			}
			if instance1 != instance2 {
				return instance1 < instance2
			}
			if container1 != container2 {
				return container1 < container2
			}
			return true
		},
	)

	// Prints the table
	mergeColumns := []int{0, 1}
	solutionTableData = *kumoriTableMerge(&mergeColumns, &solutionTableData)
	solutionTable.AppendBulk(solutionTableData)
	solutionTable.SetAutoFormatHeaders(false)
	solutionTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	solutionTable.SetAlignment(tablewriter.ALIGN_LEFT)
	solutionTable.SetCenterSeparator("+")
	solutionTable.SetColumnSeparator("|")
	solutionTable.SetRowSeparator("-")
	solutionTable.SetHeaderLine(true)
	solutionTable.SetBorder(true)
	solutionTable.SetNoWhiteSpace(false)
	// solutionTable.SetAutoMergeCellsByColumnIndex(mergeColumns)
	solutionTable.Render()
}

type certificateInfo struct {
	Name       string     `json:"name"`
	Expiration *time.Time `json:"expires"`
}
type httpInboundTableRow struct {
	Solution           string           `json:"solution"`
	Domain             string           `json:"domain"`
	Role               *string          `json:"role"`
	Certificate        *certificateInfo `json:"certificate"`
	ClientCert         bool             `json:"clientcert"`
	ClientCertRequired bool             `json:"clientcertrequired"`
	CA                 *certificateInfo `json:"ca"`
	WebSockets         bool             `json:"websockets"`
	Linked             bool             `json:"linked"`
}
type tcpInboundTableRow struct {
	Solution string  `json:"solution"`
	Port     string  `json:"port"`
	Role     *string `json:"role"`
	Linked   bool    `json:"linked"`
}

func getDescribeInboundTable(
	solutionData *gabs.Container,
	rolePrefix string,
) (
	httpInboundsTable []httpInboundTableRow,
	tcpInboundsTable []tcpInboundTableRow,
	isSubService bool,
	err error,
) {

	// Skips if a top deployment is not declared in this solution
	if !solutionData.Exists("top") {
		return
	}
	top := solutionData.Search("top").Data().(string)
	if len(top) <= 0 {
		return
	}

	// Skips if the top deployment is not found in this solution
	if !solutionData.Exists("deployments", top) {
		return
	}

	// Sets the deploymentPrefix to be used. This prefix will filter which
	// deployments in this solutions are going to be included in the describe.
	// If a rolePrefix is provided, we are only interested in roles such
	// that the combination of the top and the rolePrefix is included in the
	// combination of the deployment holding that role and the role name. For
	// example, if top="calculator" and rolePrefix="hazel.datum" then we will
	// only take into account roles such that "calculator.hazel.datum" is included
	// in <DEPLOYMENT_NAME>.<ROLE_NAME>
	deploymentPrefix := top
	if len(rolePrefix) > 0 {
		deploymentPrefix = fmt.Sprintf("%s.%s", deploymentPrefix, rolePrefix)
	}

	// Initializes tables
	httpInboundsTable = []httpInboundTableRow{}
	tcpInboundsTable = []tcpInboundTableRow{}

	// Inspects the solution deployments
	isSubService = false
	deployments := solutionData.Search("deployments").ChildrenMap()
	for deploymentName, deploymentData := range deployments {

		// Skips the deployment if it not fits the deploymentPrefix
		if !strings.HasPrefix(deploymentName, deploymentPrefix) {
			logger.Debug(fmt.Sprintf("Skipping deployment '%s': does not fit the prefix '%s'", deploymentName, deploymentPrefix))
			continue
		}

		// Skips this deployment if hasn't a built-in service
		if !deploymentData.Exists("artifact", "description", "builtin") {
			continue
		}

		// Skips this deployment if it hasn't an inbound service
		if !deploymentData.Exists("artifact", "ref", "name") {
			continue
		}
		refName := deploymentData.Search("artifact", "ref", "name").Data().(string)
		if refName != "inbound" {
			continue
		}

		// The role name must be printed only if the inbound is linked as a role in one
		// of the deployments.
		if deploymentName != top {
			isSubService = true
		}
		// Gets the inbound type
		if !deploymentData.Exists("artifact", "description", "config", "parameter", "type") {
			logger.Warn(fmt.Sprintf("Inbound %s unknown type", deploymentName))
			continue
		}
		inboundType := deploymentData.Search("artifact", "description", "config", "parameter", "type").Data().(string)

		if inboundType == "https" || inboundType == "http" {
			// If the inbound is a HTTP inbound, creates a new HTTP inbound row
			row := createHttpInboundRow(solutionData, deploymentName, deploymentData)
			httpInboundsTable = append(httpInboundsTable, row)
		} else if inboundType == "tcp" {
			// If the inbound is a HTTP inbound, creates a new HTTP inbound row
			row := createTcpInboundRow(solutionData, deploymentName, deploymentData)
			tcpInboundsTable = append(tcpInboundsTable, row)
		} else {
			logger.Warn(fmt.Sprintf("Inbound %s unknown type: %s", deploymentName, inboundType))
			continue
		}
	}

	return
}

func printTableInbounds(
	inboundData *gabs.Container,
	rolePrefix string,
) {

	httpInboundsTable, tcpInboundsTable, isSubService, err := getDescribeInboundTable(inboundData, rolePrefix)

	if err != nil {
		logger.Error(fmt.Sprintf("Error processing inbound in role '%s': %s", rolePrefix, err.Error()))
		return
	}

	// Prints HTTP Inbounds
	if len(httpInboundsTable) <= 0 {
		fmt.Println(
			toBold("HTTP Inbounds"),
			":",
			"None",
		)
	} else {

		fmt.Println(
			toBold("HTTP Inbounds"),
			":",
		)

		printTableHTTPInbounds(httpInboundsTable, false, isSubService)
	}

	// Prints TCP Inbounds
	if len(tcpInboundsTable) <= 0 {
		fmt.Println(
			toBold("TCP Inbounds"),
			":",
			"None",
		)
	} else {

		fmt.Println(
			toBold("TCP Inbounds"),
			":",
		)

		printTableTCPInbounds(tcpInboundsTable, false, isSubService)
	}
}

func printTableTCPInbounds(
	table []tcpInboundTableRow,
	printSolution bool,
	printRole bool,
) {
	tcpInboundsTable := tablewriter.NewWriter(os.Stdout)

	// Generates table headers
	headers := make([]string, 0, 4)
	if printSolution {
		headers = append(headers, "Deployment")
	}
	if printRole {
		headers = append(headers, "Role")
	}
	headers = append(headers, "Port", "Linked")

	// Creates inbound data
	tcpInboundsData := make([][]string, 0, len(table))

	for _, row := range table {

		// Initializes the row
		rowData := make([]string, 0, 3)

		// Adds the solution name containing the inbound if the solution flag is set
		if printSolution {
			rowData = append(rowData, row.Solution)
		}

		// Adds the role containing the inbound if the role flag is set. If this inbound
		// is not a builtin inbounds, the role will be empty
		if printRole {
			var role string
			if row.Role != nil {
				role = *row.Role
			}
			rowData = append(rowData, role)
		}

		// Adds the port number
		rowData = append(rowData, row.Port)

		// Calculates linked
		linked := strconv.FormatBool(row.Linked)
		rowData = append(rowData, linked)

		// Appends row
		tcpInboundsData = append(tcpInboundsData, rowData)
	}

	sort.Slice(tcpInboundsData, func(i, j int) bool {
		domain1 := strings.ToLower(tcpInboundsData[i][0])
		domain2 := strings.ToLower(tcpInboundsData[j][0])

		return domain1 < domain2
	})

	mergeColumns := []int{0, 1}
	tcpInboundsData = *kumoriTableMerge(&mergeColumns, &tcpInboundsData)
	tcpInboundsTable.SetHeader(headers)
	tcpInboundsTable.AppendBulk(tcpInboundsData)
	tcpInboundsTable.SetAutoFormatHeaders(false)
	tcpInboundsTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	tcpInboundsTable.SetAlignment(tablewriter.ALIGN_LEFT)
	tcpInboundsTable.SetCenterSeparator("+")
	tcpInboundsTable.SetColumnSeparator("|")
	tcpInboundsTable.SetRowSeparator("-")
	tcpInboundsTable.SetHeaderLine(true)
	tcpInboundsTable.SetBorder(true)
	tcpInboundsTable.SetNoWhiteSpace(false)
	// tcpInboundsTable.SetAutoMergeCellsByColumnIndex(mergeColumns)
	tcpInboundsTable.Render()
}

func printTableHTTPInbounds(
	table []httpInboundTableRow,
	printSolution bool,
	printRole bool,
) {
	httpInboundsTable := tablewriter.NewWriter(os.Stdout)

	// Generates table headers
	headers := make([]string, 0, 9)
	if printSolution {
		headers = append(headers, "Deployment")
	}
	if printRole {
		headers = append(headers, "Role")
	}
	headers = append(headers,
		"Domain",
		"Certificate",
		"Expires",
		"ClientCert",
		"CA",
		"Expires",
		"Websockets",
		"Linked",
	)

	// Creates inbound data
	httpInboundsData := make([][]string, 0, len(table))

	for _, row := range table {

		// Calculates domain
		domain := row.Domain

		// Calculates certificate name and expiration date
		var certName string
		var certExpiration string
		if row.Certificate != nil {
			certName = (*row.Certificate).Name
			if row.Certificate.Expiration != nil {
				certExpiration = (*row.Certificate).Expiration.Format(time.RFC822)
			} else {
				certExpiration = UnknownValue
			}
		}

		// Calculates ClientCert description based on clientcert and clientcertrequired
		// properties
		clientCert := strconv.FormatBool(row.ClientCert)
		if row.ClientCert && row.ClientCertRequired {
			clientCert += " (required)"
		}

		// Calculates CA name and expiration date
		var caName string
		var caExpiration string
		if row.CA != nil {
			caName = (*row.CA).Name
			if row.CA.Expiration != nil {
				caExpiration = (*row.CA).Expiration.Format(time.RFC822)
			} else {
				caExpiration = UnknownValue
			}
		}

		// Calculates websockets
		webSockets := strconv.FormatBool(row.WebSockets)

		// Calculates linked
		linked := strconv.FormatBool(row.Linked)

		// Creates the row
		rowData := make([]string, 0, 9)

		// Adds the solution name containing the inbound if the solution flag is set
		if printSolution {
			rowData = append(rowData, row.Solution)
		}

		// Adds the role containing the inbound if the role flag is set. If this inbound
		// is not a builtin inbounds, the role will be empty
		if printRole {
			var role string
			if row.Role != nil {
				role = *row.Role
			}
			rowData = append(rowData, role)
		}

		// Adds the remaiing fields
		rowData = append(rowData,
			domain,
			certName,
			certExpiration,
			clientCert,
			caName,
			caExpiration,
			webSockets,
			linked,
		)

		httpInboundsData = append(httpInboundsData, rowData)
	}

	sort.Slice(httpInboundsData, func(i, j int) bool {
		domain1 := strings.ToLower(httpInboundsData[i][0])
		domain2 := strings.ToLower(httpInboundsData[j][0])

		return domain1 < domain2
	})

	mergeColumns := []int{0, 1}
	httpInboundsData = *kumoriTableMerge(&mergeColumns, &httpInboundsData)
	httpInboundsTable.SetHeader(headers)
	httpInboundsTable.AppendBulk(httpInboundsData)
	httpInboundsTable.SetAutoFormatHeaders(false)
	httpInboundsTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	httpInboundsTable.SetAlignment(tablewriter.ALIGN_LEFT)
	httpInboundsTable.SetCenterSeparator("+")
	httpInboundsTable.SetColumnSeparator("|")
	httpInboundsTable.SetRowSeparator("-")
	httpInboundsTable.SetHeaderLine(true)
	httpInboundsTable.SetBorder(true)
	httpInboundsTable.SetNoWhiteSpace(false)
	// httpInboundsTable.SetAutoMergeCellsByColumnIndex(mergeColumns)
	httpInboundsTable.Render()
}

func createTcpInboundRow(
	solutionData *gabs.Container,
	deploymentName string,
	deploymentData *gabs.Container,
) tcpInboundTableRow {

	top := solutionData.Search("top").Data().(string)
	isSubService := deploymentName != top

	// Calculates the inbound name
	var inboundName string
	if isSubService {
		inboundName = deploymentName[len(top)+1:]
	}

	// Calculates the inbound domain
	port := UnknownValue
	if deploymentData.Exists("config", "resource", "port", "port") {
		portName := deploymentData.Search("config", "resource", "port", "port").Data().(string)
		portNumber, err := getPortNumber(portName)
		if err != nil {
			logger.Warn(fmt.Sprintf("Failed getting port %s for HTTP inbound %s: %s", portName, deploymentName, err.Error()))
		} else {
			port = strconv.FormatFloat(portNumber, 'f', 0, 64)
		}
	}

	// Calculates if this inbound is linked to internal or external links
	linked := inboundIsLinked(solutionData, deploymentName, isSubService)

	// Creates and appends the row
	row := tcpInboundTableRow{
		Solution: top,
		Port:     port,
		Linked:   linked,
	}
	if isSubService {
		row.Role = &inboundName
	}

	// Port
	return row
}

func createHttpInboundRow(
	solutionData *gabs.Container,
	deploymentName string,
	deploymentData *gabs.Container,
) httpInboundTableRow {

	var err error

	top := solutionData.Search("top").Data().(string)
	isSubService := deploymentName != top

	// Calculates the inbound name
	var inboundName string
	if isSubService {
		inboundName = deploymentName[len(top)+1:]
	}

	// Calculates the inbound domain
	domain := UnknownValue
	if deploymentData.Exists("config", "resource", "serverdomain", "domain") {
		domainName := deploymentData.Search("config", "resource", "serverdomain", "domain").Data().(string)
		domain, err = getDomainUserDomain(domainName, "")
		if err != nil {
			logger.Warn(fmt.Sprintf("Failed getting domain %s for HTTP inbound %s: %s", domainName, deploymentName, err.Error()))
		}
	}

	// Calculates the inbound server certificate and its expiration time
	var cert *certificateInfo
	if deploymentData.Exists("config", "resource", "servercert", "certificate") {
		certName := deploymentData.Search("config", "resource", "servercert", "certificate").Data().(string)
		if certName != "" {
			cert = &certificateInfo{
				Name: certName,
			}
			cdomain, cname, err := getUserDomainAndName(cert.Name, "")
			if err == nil {
				certManifest, err := admission.GetCertificate(cdomain, cname)
				if err == nil {
					certData, err := gabs.ParseJSON([]byte(certManifest))
					if err == nil {
						if certData.Exists("description", "cert") &&
							certData.Search("description", "cert").Data() != nil {
							certString := certData.Search("description", "cert").Data().(string)
							certContent, err := base64.StdEncoding.DecodeString(certString)
							if err == nil {
								_, notAfter, err := x509Validator.GetValidityBounds(string(certContent))
								if err == nil {
									cert.Expiration = &notAfter
								} else {
									logger.Warn(fmt.Sprintf("Failed parsing HTTP inbound %s certificate %s cannot be parsed", deploymentName, cert.Name))
								}
							} else {
								logger.Warn(fmt.Sprintf("Failed parsing HTTP inbound %s certificate %s", deploymentName, cert.Name))
							}
						} else {
							logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s certificate %s: bad manifest format", deploymentName, cert.Name))
						}
					} else {
						logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s certificate %s: not a JSON document", deploymentName, cert.Name))
					}
				} else {
					logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s certificate %s: not found", deploymentName, cert.Name))
				}
			} else {
				logger.Warn(fmt.Sprintf("HTTP inbound %s certificate has a wrong name %s", deploymentName, cert.Name))
			}

		}
	}

	// Calculates the CA and its expiration time
	var ca *certificateInfo
	if deploymentData.Exists("config", "resource", "clientcertca", "ca") {
		caName := deploymentData.Search("config", "resource", "clientcertca", "ca").Data().(string)
		if caName != "" {
			ca = &certificateInfo{
				Name: caName,
			}
			cadomain, caname, err := getUserDomainAndName(ca.Name, "")
			if err == nil {
				caManifest, err := admission.GetCA(cadomain, caname)
				if err == nil {
					caData, err := gabs.ParseJSON([]byte(caManifest))
					if err == nil {
						if caData.Exists("description", "ca") &&
							caData.Search("description", "ca").Data() != nil {
							caString := caData.Search("description", "ca").Data().(string)
							caContent, err := base64.StdEncoding.DecodeString(caString)
							if err == nil {
								_, notAfter, err := x509Validator.GetValidityBounds(string(caContent))
								if err == nil {
									ca.Expiration = &notAfter
								} else {
									logger.Warn(fmt.Sprintf("Failed parsing HTTP inbound %s ca %s cannot be parsed", deploymentName, ca.Name))
								}

							} else {
								logger.Warn(fmt.Sprintf("Failed parsing HTTP inbound %s ca %s", deploymentName, ca.Name))
							}
						} else {
							logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s ca %s: bad manifest format", deploymentName, ca.Name))
						}
					} else {
						logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s CA %s: not a JSON document", deploymentName, ca.Name))
					}
				} else {
					logger.Warn(fmt.Sprintf("Failed retrieving HTTP inbound %s CA %s: not found", deploymentName, ca.Name))
				}
			} else {
				logger.Warn(fmt.Sprintf("HTTP inbound %s CA has a wrong name %s", deploymentName, ca.Name))
			}
		}
	}

	// Calculates if this inbound has websockets enabled
	websockets := false
	if deploymentData.Exists("artifact", "description", "config", "parameter", "websocket") {
		websockets = deploymentData.Search("artifact", "description", "config", "parameter", "websocket").Data().(bool)
	}

	// Calculates if this inbound has clientcert enabled, and if it is required
	clientcert := false
	if deploymentData.Exists("artifact", "description", "config", "parameter", "clientcert") {
		clientcert = deploymentData.Search("artifact", "description", "config", "parameter", "clientcert").Data().(bool)
	}
	clientcertrequired := false
	if deploymentData.Exists("artifact", "description", "config", "parameter", "certrequired") {
		clientcertrequired = deploymentData.Search("artifact", "description", "config", "parameter", "certrequired").Data().(bool)
	}

	// Calculates if this inbound is linked to internal or external links
	linked := inboundIsLinked(solutionData, deploymentName, isSubService)

	// Creates and returns the row
	row := httpInboundTableRow{
		Solution:           top,
		Domain:             domain,
		Certificate:        cert,
		ClientCert:         clientcert,
		ClientCertRequired: clientcertrequired,
		CA:                 ca,
		WebSockets:         websockets,
		Linked:             linked,
	}
	if isSubService {
		row.Role = &inboundName
	}
	return row
}

func inboundIsLinked(
	solutionData *gabs.Container,
	deploymentName string,
	isSubService bool,
) bool {

	// Checks internal links if the inbound is included as a built-in role
	if isSubService {
		if solutionData.Exists("links") && solutionData.Search("links").Data() != nil {
			links := solutionData.Search("links").Children()
			for _, link := range links {
				if link.Exists("s_d") && (link.Search("s_d").Data() != nil) {
					linkSourceDeployment := link.Search("s_d").Data().(string)
					if deploymentName == linkSourceDeployment {
						return true
					}
				}
			}
		}
	}

	// Checks external links if the inbound is included as the top service
	if solutionData.Exists("externalLinks") && solutionData.Search("externalLinks").Data() != nil {
		links := solutionData.Search("externalLinks").ChildrenMap()
		if len(links) > 0 {
			return true
		}
	}

	// Is not linked
	return false
}

func linkPrintSolution(json *gabs.Container) {

	// If this solution has not external links, just show None
	if !json.Exists("externalLinks") ||
		json.Search("externalLinks").Data() == nil ||
		len(json.Search("externalLinks").ChildrenMap()) == 0 {
		fmt.Println(toBold("Links"), ":", "None")
		return
	}

	// TBD : this code asumes depA is a regular solution and depB is a inbound
	fmt.Println(toBold("Links"), ":")

	urn := json.Path("urn").Data().(string)
	depA, err := admission.SolutionURNToName(urn)
	if err != nil {
		logger.Warn("Error getting information about deployment %s: %s", urn, err.Error())
		depA = urnToPrintableName(urn)
	}
	var domainNameA string
	isHTTPInbound, domain, err := getHTTPInboundDomain(urn)
	if err != nil {
		logger.Warn("Error getting information about deployment %s: %s", urn, err.Error())
	} else if isHTTPInbound {
		domainNameA = domain
	}
	if !isHTTPInbound {
		isTCPInbound, port, err := getTCPInboundDomain(urn)
		if err != nil {
			logger.Warn("Error getting information about deployment %s: %s", urn, err.Error())
		} else if isTCPInbound {
			domainNameA = port
		}

	}

	linksJSON := json.Path("externalLinks")
	for chanA, depBJSON := range linksJSON.ChildrenMap() {
		for depBUrn, chanBJSON := range depBJSON.ChildrenMap() {
			for chanB := range chanBJSON.ChildrenMap() {
				var depB, domainNameB string
				var err error

				if admission.SolutionCheckUrn(depBUrn) {
					depB, err = admission.SolutionURNToName(depBUrn)
					isHTTPInbound, domain, err := getHTTPInboundDomain(depBUrn)
					if err != nil {
						logger.Warn("Error getting information about deployment %s: %s", depBUrn, err.Error())
					} else if isHTTPInbound {
						domainNameB = domain
					}
					if !isHTTPInbound {
						isTCPInbound, port, err := getTCPInboundDomain(depBUrn)
						if err != nil {
							logger.Warn("Error getting information about deployment %s: %s", depBUrn, err.Error())
						} else if isTCPInbound {
							domainNameB = port
						}

					}
				} else {
					err = fmt.Errorf("Unknown type of linked deployment")
				}

				if err != nil {
					fmt.Printf("Cannot parse inbound %s manifest: %s\n", depBUrn, err.Error())
				}
				linkStr := depA
				if chanA != "" {
					linkStr += ":" + chanA
				}
				if domainNameA != "" {
					linkStr += " (" + domainNameA + ")"
				}
				linkStr += " <=> " + depB
				if chanB != "" {
					linkStr += ":" + chanB
				}
				if domainNameB != "" {
					linkStr += " (" + domainNameB + ")"
				}
				fmt.Println("  " + linkStr)
			}
		}
	}
}

func printEvents(fullJSON *gabs.Container, role, instance string) {

	eventsJSON := fullJSON.Path("events")

	// solutionURN := fullJSON.Path("name").Data().(string)

	urn := fullJSON.Path("urn").Data().(string)
	solutionName := urnToPrintableName(urn)

	if role == "" {
		fmt.Println(toBold("Events"), ":")
	} else {
		fmt.Println(toBold("Events (filtered)"), ":")
	}

	selectedRole := role
	selectedDeployment := ""
	selectedDeploymentId := ""

	if role != "" {
		top := fullJSON.Search("top").Data().(string)
		toplen := len(top)

		if strings.Contains(role, ".") {
			for deploymentName, deploymentData := range fullJSON.Search("deployments").ChildrenMap() {
				if deploymentName == top {
					continue
				}
				subdepName := deploymentName[toplen+1:]
				roleidx := strings.Index(role, subdepName)
				if roleidx != -1 {
					roleName := role[len(subdepName)+1:]
					if len(deploymentName) > len(selectedDeployment) {
						selectedDeployment = deploymentName
						if deploymentData.Exists("id") && (deploymentData.Search("id").Data() != nil) {
							selectedDeploymentId = deploymentData.Search("id").Data().(string)
						}
						selectedRole = roleName
					}
				}
			}
		} else {
			top := fullJSON.Search("top").Data().(string)
			topData := fullJSON.Path("deployments." + top)

			selectedDeployment = top
			if topData.Exists("id") && (topData.Search("id").Data() != nil) {
				selectedDeploymentId = topData.Search("id").Data().(string)
			}
			selectedRole = role
		}
	}

	baseObjectName := ""
	if selectedDeploymentId != "" {
		baseObjectName = selectedDeploymentId
		if selectedRole != "" {
			roleHash := Hash(selectedRole)
			baseObjectName = baseObjectName + "-" + roleHash
			if instance != "" {
				instanceSuffix := strings.TrimPrefix(instance, "instance-")
				baseObjectName = baseObjectName + "-deployment-" + instanceSuffix
			}
		}
	}

	// This map is used to store which name should be used in event objects instead of the
	// internal deployment names.
	deploymentNames := map[string]string{}
	// This map is used to store every deployment role name and its hash to be able to decode them.
	roleHashToName := map[string]string{}
	// This map is used to store every container name and its hash to be able to decode them.
	containerHashToName := map[string]string{}

	for deploymentName, deploymentData := range fullJSON.Search("deployments").ChildrenMap() {

		if deploymentData.Exists("id") && (deploymentData.Search("id").Data() != nil) {
			deploymentId := deploymentData.Search("id").Data().(string)
			deploymentNames[deploymentId] = deploymentName
		}

		depRoles := deploymentData.Path("artifact.description.role")
		if depRoles != nil {
			for roleName, roleData := range depRoles.ChildrenMap() {
				hashedRoleName := Hash(roleName)
				roleHashToName[hashedRoleName] = roleName

				roleConts := roleData.Path("artifact.description.code")
				if roleConts != nil {
					for contName, _ := range roleConts.ChildrenMap() {
						hashedContName := Hash(contName)
						containerHashToName[hashedContName] = contName
					}
				}
			}
		}
	}

	eventsTable := tablewriter.NewWriter(os.Stdout)
	eventsTable.SetHeader([]string{"Type", "Reason", "LastTimestamp", "Count", "Source (kind)", "Source (name)", "Message"})
	var eventsData [][]string
	somethingToDisplay := false
	for _, event := range eventsJSON.Children() {

		// If a filter is provided, only process the events related to it
		if !strings.Contains(event.Path("objectName").Data().(string), baseObjectName) {
			continue
		}

		// // If an instance filter was provided, only process the events related to it
		// if instance != "" {
		// 	// Get the instance/Pod suffix from the provided instance name
		// 	instanceSuffix := strings.TrimPrefix(instance, "instance-")
		// 	// If current event object name doesn't end with the suffix, skip the event
		// 	if !(strings.HasSuffix(event.Path("objectName").Data().(string), "-"+instanceSuffix)) {
		// 		continue
		// 	}
		// }

		// Events have several timestamp field, and not all must contain values. It is up to the
		// event emitter to decide what fileds are used or not.
		// So, we will determine the timestamp to use by looking at the different fields and looking
		// for one that is not null.
		eventTimestamp := ""
		if event.Path("lastTimestamp").String() != "null" {
			eventTimestamp = event.Path("lastTimestamp").String()
		} else if event.Path("firstTimestamp").String() != "null" {
			eventTimestamp = event.Path("firstTimestamp").String()
		} else if event.Path("time").String() != "null" {
			eventTimestamp = event.Path("time").String()
		}

		// Counter might not be set, in that case we default it to 1
		counter := "1"
		if event.Path("counter").String() != "null" {
			counter = event.Path("counter").String()
		}

		// Calculate the event "age" based on its timestamp
		age := "<none>"
		time1, err := time.Parse(time.RFC3339, strings.Trim(eventTimestamp, "\""))
		if err != nil {
			logger.Debug("Error while parsing date: " + err.Error())
		} else {
			age = time.Since(time1).Truncate(time.Second).String()
		}

		// Convert the event ObjectName property to Kumori human readable identifiers
		rawObjectName := event.Path("objectName").Data().(string)
		objectName := rawObjectName
		rawMessage := event.Path("message").Data().(string)
		evtMessage := rawMessage
		if event.Path("objectKind").Data().(string) == "Pod" {
			// Example Pod names:
			// - kd-064225-6b65c4dc-fb10f1a1-deployment-0
			// - kd-064225-6b65c4dc-fb10f1a1-deployment-5898b6bb9-4jgfl
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			instanceId := rawObjectName[39:]
			objectName = deploymentName + "." + clearRole + ".instance-" + instanceId

			// For Pod events messages, we try to detect container names hashes and replace them with clear names
			if strings.Contains(evtMessage, " container k-") {
				for hashedCont, clearCont := range containerHashToName {
					evtMessage = strings.ReplaceAll(evtMessage, " k-"+hashedCont, " "+clearCont)
				}
			}
		} else if event.Path("objectKind").Data().(string) == "ReplicaSet" {
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			objectName = deploymentName + "." + clearRole
		} else if event.Path("objectKind").Data().(string) == "StatefulSet" {
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			objectName = deploymentName + "." + clearRole
		} else if event.Path("objectKind").Data().(string) == "Deployment" {
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			objectName = deploymentName + "." + clearRole
		} else if event.Path("objectKind").Data().(string) == "PodDisruptionBudget" {
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			objectName = deploymentName + "." + clearRole
		} else if event.Path("objectKind").Data().(string) == "Endpoints" {
			deploymentId := rawObjectName[:18]
			deploymentName, ok := deploymentNames[deploymentId]
			if !ok {
				deploymentName = "<unknown>"
			}
			hashedRole := rawObjectName[19:27]
			clearRole := roleHashToName[hashedRole]
			tmp := strings.ReplaceAll(rawObjectName, deploymentId+"-", deploymentName+".")
			tmp = strings.ReplaceAll(tmp, hashedRole+"-", clearRole+".")
			objectName = tmp[:strings.LastIndex(tmp, "-service")]
		} else if event.Path("objectKind").Data().(string) == "PersistentVolumeClaim" {
			// PVC events have different naming schemes depending on whether they are from Persistent
			// or ephemeral volumes.
			if strings.HasPrefix(rawObjectName, "kd-") {
				// Volatile volume (ephemeral PVC)
				// Example: kd-064225-6b65c4dc-fb10f1a1-deployment-0-k-1ae7c7a0
				deploymentId := rawObjectName[:18]
				deploymentName, ok := deploymentNames[deploymentId]
				if !ok {
					deploymentName = "<unknown>"
				}
				hashedRole := rawObjectName[19:27]
				clearRole := roleHashToName[hashedRole]
				objectName = deploymentName + "." + clearRole
			} else if strings.HasPrefix(rawObjectName, "k-") {
				// Persistent volume
				// Example: k-7e021b81-kd-064225-6b65c4dc-fb10f1a1-deployment-0
				deploymentId := rawObjectName[11:29]
				deploymentName, ok := deploymentNames[deploymentId]
				if !ok {
					deploymentName = "<unknown>"
				}
				hashedRole := rawObjectName[30:38]
				clearRole := roleHashToName[hashedRole]
				objectName = deploymentName + "." + clearRole
			} else {
				continue
			}
		} else if event.Path("objectKind").Data().(string) == "V3Deployment" {
			objectName = solutionName
		} else if event.Path("objectKind").Data().(string) == "KukuSolution" {
			objectName = solutionName
		}

		somethingToDisplay = true
		// The first string "evetTimestamp" will only be used for ordering, then removed later
		eventRow := []string{
			eventTimestamp,
			event.Path("type").Data().(string),
			event.Path("reason").Data().(string),
			age,
			counter,
			event.Path("objectKind").Data().(string),
			objectName,
			evtMessage,
		}

		eventsData = append(eventsData, eventRow)
	}

	// Sort events by timestamp
	sort.Slice(eventsData, func(i, j int) bool {
		return (eventsData[i][0] < eventsData[j][0])
	})

	// Remove eventTimestamp property (only used for finer sorting)
	for i, event := range eventsData {
		eventsData[i] = event[1:]
	}

	// Prepend a header separator line
	separator := []string{"----", "------", "-------------", "-----", "-------------", "-------------", "-------"}
	eventsData = append([][]string{separator}, eventsData...)

	eventsTable.SetAutoWrapText(false)
	eventsTable.AppendBulk(eventsData)
	eventsTable.SetAutoFormatHeaders(false)
	eventsTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	eventsTable.SetAlignment(tablewriter.ALIGN_LEFT)
	eventsTable.SetCenterSeparator("")
	eventsTable.SetColumnSeparator("")
	eventsTable.SetRowSeparator("")
	eventsTable.SetHeaderLine(false)
	eventsTable.SetBorder(false)
	eventsTable.SetNoWhiteSpace(false)
	if somethingToDisplay {
		eventsTable.Render()
	} else {
		fmt.Println(pad(1), "<none>")
	}
}

func colourInstanceStatus(value string) (prettyValue string) {
	value = strings.Title(value)
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	yellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if strings.EqualFold(value, "pending") {
		prettyValue = yellow(value)
	} else if strings.EqualFold(value, "running") {
		prettyValue = green(value)
	} else if strings.EqualFold(value, "succeeded") {
		prettyValue = green(value)
	} else if strings.EqualFold(value, "failed") {
		prettyValue = red(value)
	} else if strings.EqualFold(value, "evicted") {
		prettyValue = red(value)
	} else if strings.EqualFold(value, "unknown") {
		prettyValue = yellow(value)
	} else if strings.EqualFold(value, "terminating") {
		prettyValue = yellow(value)
	} else {
		prettyValue = red(value)
	}
	return
}

func colourReady(ready bool) (prettyValue string) {
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if ready {
		prettyValue = green("Yes")
	} else {
		prettyValue = red("No")
	}
	return
}

func colourContainerStatus(row *solutionTableRow) (prettyValue string) {
	value := strings.Title(*row.Status)
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	yellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if strings.EqualFold(value, "pending") {
		prettyValue = yellow(value)
	} else if strings.EqualFold(value, "running") {
		prettyValue = green(value)
	} else if strings.EqualFold(value, "succeeded") {
		prettyValue = green(value)
	} else if strings.EqualFold(value, "failed") {
		prettyValue = red(value)
	} else if strings.EqualFold(value, "evicted") {
		prettyValue = red(value)
	} else if strings.EqualFold(value, "unknown") {
		prettyValue = yellow(value)
	} else if strings.EqualFold(value, "terminating") {
		prettyValue = yellow(value)
	} else if strings.EqualFold(value, "completed") {
		if (row.Init != nil) && (*row.Init) &&
			(row.ExitCode != nil) && (*row.ExitCode == uint64(0)) {
			prettyValue = green(value)
		} else {
			prettyValue = red(value)
		}
	} else if strings.EqualFold(value, "terminated") {
		prettyValue = red(value)
	} else {
		prettyValue = red(value)
	}
	return
}

// getSolutionsData DEPRECATED. Still using it because admission.DescribeSolutions returns
// empty descriptions
func getSolutionsData(userDomain string) ([]*gabs.Container, error) {
	names, err := admission.GetSolutions()

	if err != nil {
		return nil, err
	}

	solutions := make([]*gabs.Container, 0, len(names))

	for _, name := range names {
		_, solution, err := getSolutionData(name)
		if err != nil {
			return nil, err
		}

		if userDomain != "" {
			if solution.Path("ref.domain").Data().(string) != userDomain {
				continue
			}
		}

		solutions = append(solutions, solution)
	}

	return solutions, nil
}

func urnToPrintableName(urn string) string {
	// Sample URN : eslap://default.domain/solution/calculator
	// Returns    : calculator
	parts := strings.Split(urn, "/")

	if len(parts) == 5 {
		return parts[4]
	} else {
		return "UNABLE_TO_DETERMINE"
	}
}

func getHTTPInboundDomain(urn string) (
	isInbound bool,
	domain string,
	err error,
) {

	isInbound = false

	// Gets the solution name
	inboundDomain, inboundName, err := admission.SolutionDecomposeUrn(urn)
	if err != nil {
		return
	}

	// Gets the soluton manifest
	manifest, err := admission.GetSolution(inboundDomain, inboundName)
	if err != nil {
		err = fmt.Errorf("Cannot read deployment %s/%s: %s", inboundDomain, inboundName, err.Error())
		return
	}
	inboundJSON, err := gabs.ParseJSON([]byte(manifest))
	if err != nil {
		err = fmt.Errorf("Cannot parse deployment %s/%s manifest: %s", inboundDomain, inboundName, err.Error())
		return
	}

	// Gets the solution top
	if !inboundJSON.Exists("top") {
		err = fmt.Errorf("Top deployment not found in deployment %s/%s manifest", inboundDomain, inboundName)
		return
	}
	top := inboundJSON.Search("top").Data().(string)

	// Checks if this solution is an HTTPInbound
	if !inboundJSON.Exists("deployments", top, "artifact", "ref", "name") {
		err = fmt.Errorf("Wrong deployment %s/%s manifest. Top not founds", inboundDomain, inboundName)
		return
	}
	refName := inboundJSON.Search("deployments", top, "artifact", "ref", "name").Data().(string)
	if refName != "inbound" {
		// This deployment is not an inbound
		return
	}
	if !inboundJSON.Exists("deployments", top, "config", "parameter", "type") {
		// This deployment is not an inbound
		return
	}
	inboundType := inboundJSON.Search("deployments", top, "config", "parameter", "type").Data().(string)
	if inboundType != "https" {
		// This inbound is not an http inbound
		return
	}

	isInbound = true

	// Gets the name of the domain resource
	if !inboundJSON.Exists("deployments", top, "config", "resource", "serverdomain", "domain") {
		err = fmt.Errorf("Domain not found in HTTP inbound %s/%s manifest", inboundDomain, inboundName)
		return
	}
	kdomain := inboundJSON.Search("deployments", top, "config", "resource", "serverdomain", "domain").Data().(string)
	userDomain, name, err := getUserDomainAndName(kdomain, "")
	if err != nil {
		err = fmt.Errorf("Error getting domain %s from HTTP inbound %s/%s manifest: %s", kdomain, inboundDomain, inboundName, err.Error())
		return
	}
	domainManifest, err := admission.GetDomain(userDomain, name)
	if err != nil {
		err = fmt.Errorf("Error getting domain %s from HTTP inbound %s/%s manifest: %s", kdomain, inboundDomain, inboundName, err.Error())
		return
	}
	domainJSON, err := gabs.ParseJSON([]byte(domainManifest))
	if err != nil {
		err = fmt.Errorf("Cannot parse domain %s: %s", kdomain, err.Error())
		return
	}

	// Gets the domain in the domain resource
	if !domainJSON.Exists("description", "domain") {
		err = fmt.Errorf("Cannot retrieve domain %s", kdomain)
		return
	}
	domain = domainJSON.Search("description", "domain").Data().(string)

	return
}

func getTCPInboundDomain(urn string) (
	isInbound bool,
	port string,
	err error,
) {

	isInbound = false

	// Gets the solution name
	inboundDomain, inboundName, err := admission.SolutionDecomposeUrn(urn)
	if err != nil {
		return
	}

	// Gets the soluton manifest
	manifest, err := admission.GetSolution(inboundDomain, inboundName)
	if err != nil {
		err = fmt.Errorf("Cannot read deployment %s/%s: %s", inboundDomain, inboundName, err.Error())
		return
	}
	inboundJSON, err := gabs.ParseJSON([]byte(manifest))
	if err != nil {
		err = fmt.Errorf("Cannot parse deployment %s/%s manifest: %s", inboundDomain, inboundName, err.Error())
		return
	}

	// Gets the solution top
	if !inboundJSON.Exists("top") {
		err = fmt.Errorf("Top deployment not found in deployment %s/%s manifest", inboundDomain, inboundName)
		return
	}
	top := inboundJSON.Search("top").Data().(string)

	// Checks if this solution is an TCPInbound
	if !inboundJSON.Exists("deployments", top, "artifact", "ref", "name") {
		err = fmt.Errorf("Wrong deployment %s/%s manifest. Top not founds", inboundDomain, inboundName)
		return
	}
	refName := inboundJSON.Search("deployments", top, "artifact", "ref", "name").Data().(string)
	if refName != "inbound" {
		// This deployment is not an inbound
		return
	}
	if !inboundJSON.Exists("deployments", top, "config", "parameter", "type") {
		// This deployment is not an inbound
		return
	}
	inboundType := inboundJSON.Search("deployments", top, "config", "parameter", "type").Data().(string)
	if inboundType != "tcp" {
		// This inbound is not an tcp inbound
		return
	}

	isInbound = true

	// Gets the name of the port resource
	if !inboundJSON.Exists("deployments", top, "config", "resource", "port", "port") {
		err = fmt.Errorf("Port not found in TCP inbound %s/%s manifest", inboundDomain, inboundName)
		return
	}
	kport := inboundJSON.Search("deployments", top, "config", "resource", "port", "port").Data().(string)
	userDomain, name, err := getUserDomainAndName(kport, "")
	if err != nil {
		err = fmt.Errorf("Error getting port %s from TCP inbound %s/%s manifest: %s", kport, inboundDomain, inboundName, err.Error())
		return
	}
	portManifest, err := admission.GetPort(userDomain, name)
	if err != nil {
		err = fmt.Errorf("Error getting port %s from TCP inbound %s/%s manifest: %s", kport, inboundDomain, inboundName, err.Error())
		return
	}
	portJSON, err := gabs.ParseJSON([]byte(portManifest))
	if err != nil {
		err = fmt.Errorf("Cannot parse port %s: %s", kport, err.Error())
		return
	}

	// Gets the port in the port resource
	if !portJSON.Exists("description", "externalPort") {
		err = fmt.Errorf("Cannot retrieve port %s", kport)
		return
	}
	port = portJSON.Search("description", "externalPort").String()

	return
}
