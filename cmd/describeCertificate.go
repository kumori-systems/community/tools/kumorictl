/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
)

// describeCertificateCmd represents the describe Certificate command
var describeCertificateCmd = &cobra.Command{
	Use:     "certificate <user>/<name>",
	Aliases: []string{"certificates", "cert"},
	Short:   "Describe Certificate on platform",
	Long: `Describe Certificate on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeCertificate,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeCertificate(cmd, args) },
}

func init() {

	describeCertificateCmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-certificate")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describeCertificateCmd)
}

func checkArgsDescribeCertificate(cmd *cobra.Command, args []string) error {
	// Certificate name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// Certificate name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-certificate can be omitted when user-certificate is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribeCertificate(cmd *cobra.Command, args []string) {
	meth := "runDescribeCertificate"

	userCertificate := ""
	name := ""
	var err error

	// Calculates the user certificate and name. If arguments are not provided, then
	// the user certificate
	userCertificate, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing Certificate",
		"name", name,
		"userCertificate", userCertificate,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribeCertificateJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeCertificateTable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeCertificateJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getCertificateData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribeCertificateTable(cmd *cobra.Command, args []string) {
	_, certificateDetailsJSON, err := getCertificateData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderCertificate(certificateDetailsJSON)
	fmt.Println("")
}

func getCertificateData(fullName string) (string, *gabs.Container, error) {
	userCertificate, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	certificateManifest, certificateJSON, err := admission.DescribeCertificate(userCertificate, name)
	if err != nil {
		return "", nil, err
	}
	if certificateJSON == nil {
		return "", nil, fmt.Errorf("Certificate not found")
	}

	return certificateManifest, certificateJSON, nil
}

func printHeaderCertificate(json *gabs.Container) {

	certificateName, _ := admission.CertificateURNToName(json.Path("urn").Data().(string))

	certString := json.Path("description.cert").Data().(string)

	var parsedCertificate *x509.Certificate

	caCertData, err := base64.StdEncoding.DecodeString(certString)
	if err == nil {
		parsedCertificate, err = x509Validator.GetParsedCertificate(string(caCertData))
		if err != nil {
			response.AddError(response.WarnSeverityType, "Failed parsing certificate")
			logger.Warn(fmt.Sprintf("Failed parsing certificate."))
		}
	} else {
		response.AddError(response.WarnSeverityType, "Failed parsing certificate")
		logger.Warn(fmt.Sprintf("Failed parsing certificate."))
	}

	// The domain included in the KukuCert resource
	declaredDomain := json.Path("description.domain").Data().(string)

	fmt.Println(toBold("Certificate       "), ":", certificateName)
	fmt.Println(toBold("Public            "), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)

	lastModification := UnknownValue
	if json.Exists("lastModification ") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")

	// Validate that the declared domain in the KukuCert is valid for the given certificate
	found := false
	for _, dnsDom := range parsedCertificate.DNSNames {
		if dnsDom == declaredDomain {
			found = true
		}
	}
	if found {
		fmt.Println(toBoldMaintainCase("Declared domain"), ":", json.Path("description.domain").Data().(string))
	} else {
		fmt.Println(toBoldMaintainCase("Declared domain"), ":", declaredDomain)
		fmt.Println(toBoldYellowMaintainCase("                  The declared domain does not match any domain of the certificate."))
	}
	fmt.Println("")

	fmt.Println(toBold("Certificate:"))
	fmt.Println(toBoldMaintainCase("- Valid from   "), ":", parsedCertificate.NotBefore.Format(time.UnixDate))
	fmt.Println(toBoldMaintainCase("- Valid until  "), ":", parsedCertificate.NotAfter.Format(time.UnixDate))
	fmt.Println(toBoldMaintainCase("- Valid domains"), ":", strings.Join(parsedCertificate.DNSNames, " "))
	fmt.Println(toBoldMaintainCase("- Issuer       "), ":", parsedCertificate.Issuer)
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("Certificate not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("Certificate is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}
}

func getCertificateNameAndCertificate(urn string) (
	certificate string,
	err error,
) {
	userCertificate, name, err := admission.CertificateDecomposeUrn(urn)
	certificate = fmt.Sprintf("%s/%s", userCertificate, name)
	return
}
