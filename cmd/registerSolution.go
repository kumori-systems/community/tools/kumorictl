/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/cue2json"
	diff "gitlab.com/kumori/kumorictl/pkg/diff"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// registerSolutionCmd represents the register solution command
var registerSolutionCmd = &cobra.Command{
	Use:     "deployment <name>",
	Aliases: []string{"deployments"},
	Short:   "Register a deployment in the platform",
	// Long: `Register a deployment in the platform.`,
	Args: checkArgsRegisterSolution,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterSolution(cmd, args) },
}

// deployCmd is just an alias to the "register deployment" subcommand
var deployCmd = &cobra.Command{
	Use:   "deploy <user>/<name>",
	Short: registerSolutionCmd.Short + " (alias of register deployment)",
	Long:  registerSolutionCmd.Long,
	Args:  registerSolutionCmd.Args,
	Run:   registerSolutionCmd.Run,
}

func init() {
	registerCmd.AddCommand(registerSolutionCmd)
	setRegisterSolutionFlags(registerSolutionCmd)

	deployCmd.PersistentFlags().StringP(
		"owner", "", "",
		"owner of the new resource (only for administrators)",
	)
	rootCmd.AddCommand(deployCmd)
	setRegisterSolutionFlags(deployCmd)
}

func checkArgsRegisterSolution(cmd *cobra.Command, args []string) error {

	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runRegisterSolution(cmd *cobra.Command, args []string) {
	meth := "runRegisterSolution"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !validateName(name) {
		err = fmt.Errorf("Solution name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	updateIfExists, err := cmd.Flags().GetBool("update-if-exists")
	if err != nil {
		logger.Fatal(err.Error())
	}

	dryRun, err := cmd.Flags().GetBool("dry-run")
	if err != nil {
		logger.Fatal(err.Error())
	}

	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if exists && !updateIfExists {
		err = errors.New("solution " + userDomain + "/" + name + " already exists")
		logger.Fatal(err.Error())
	}

	deploymentPackage, err := cmd.Flags().GetString("package")
	if err != nil {
		logger.Fatal(err.Error())
	}

	assumeYes, err := cmd.Flags().GetBool("yes")
	if err != nil {
		logger.Fatal(err.Error())
	}

	allowJSONManifest, err := cmd.Flags().GetBool("allow-json-manifest")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering deployment",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"package", deploymentPackage,
		"assumeYes", assumeYes,
		"updateIfExtists", updateIfExists,
		"dryRun", dryRun,
		"allowJSONManifest", allowJSONManifest,
		"waitDuration", waitDuration,
		"output", output,
		"meth", meth,
	)
	applySolution(cmd, args, owner, assumeYes, dryRun, allowJSONManifest, waitDuration, false)
	if output != JSONOutput {
		fmt.Println("Deployment " + userDomain + "/" + name + " created")
	}
}

func applySolution(
	cmd *cobra.Command, args []string, owner string,
	assumeYes bool, dryRun bool, allowJSONManifest bool,
	waitDuration time.Duration, isUpdating bool,
) {
	meth := "applySolution"

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	solution, err := cmd.Flags().GetString("deployment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	deploymentPackage, err := cmd.Flags().GetString("package")
	if err != nil {
		logger.Fatal(err.Error())
	}

	comment, err := cmd.Flags().GetString("comment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	cuecmd := viper.Global.GetString("cue-command")
	if cuecmd == "" {
		logger.Fatal("CUE command not found")
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Applying deployment",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"deployment", solution,
		"package", deploymentPackage,
		"comment", comment,
		"assumeYes", assumeYes,
		"dryRun", dryRun,
		"allowJSONManifest", allowJSONManifest,
		"waitDuration", waitDuration,
		"cueCommand", cuecmd,
		"output", output,
		"meth", meth,
	)

	_, manifestGab, err := generateSolutionManifest(
		userDomain, name, owner, comment, solution, deploymentPackage,
		manifestsDir, allowJSONManifest, cuecmd,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}

	printUpdatedElements(userDomain, name, manifestGab, assumeYes, output)

	if dryRun {
		logger.Debug(
			"Dry-run: deployment is not sent to the cluster",
			"name", name,
			"userDomain", userDomain,
			"deployment", solution,
			"comment", comment,
			"output", output,
			"meth", meth,
		)
	} else {
		err = admission.CreateSolution(manifestsDir, tmpDir)
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Deployment applied",
			"name", name,
			"userDomain", userDomain,
			"deployment", solution,
			"comment", comment,
			"cueCommand", cuecmd,
			"output", output,
			"meth", meth,
		)

		if waitDuration != util.ZERO_DURATION {
			if isUpdating {
				// In an "update" operation, the service is already "running".
				// Therefore, we wait a while for the changes to begin to take place.
				time.Sleep(20 * time.Second)
			}
			err = admission.WaitSolutionToBeReady(userDomain, name, waitDuration)
			if err != nil {
				logger.Fatal(err.Error())
			}
		}
	}
}

func setRegisterSolutionFlags(cmd *cobra.Command) {
	cmd.PersistentFlags().StringP(
		"deployment", "d", "",
		"[required] path/to/module or git URI containing the CUE module",
	)
	cmd.MarkPersistentFlagRequired("deployment")
	cmd.MarkFlagRequired("deployment")

	cmd.Flags().StringP("package", "p", "deployment", "Deployment package")

	cmd.Flags().StringP(
		"comment", "m", "",
		"message describing the action",
	)

	// cmd.Flags().StringP(
	// 	"owner", "", "",
	// 	"owner of the new deployment (only for administrators)",
	// )

	cmd.Flags().Bool(
		"update-if-exists", false,
		"Update the deployment if exists instead of returning an error",
	)

	cmd.Flags().BoolP(
		"yes", "y", false,
		"Confirms automatically",
	)

	cmd.Flags().Bool(
		"dry-run", false,
		"Perform all local operations but do not deploy in the cluster",
	)

	cmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time for the service to be ready before returning",
	)

	cmd.Flags().Bool(
		"allow-json-manifest", false,
		`(experimental) Allow to provide a pre-generated JSON manifest as deployment
input parameter (filename or - for stdin), instead of a path to CUE module`,
	)
}

// printUpdatedElements prints the changes to be applied if the solution is updated
// and waits for confirmation.
func printUpdatedElements(
	userDomain string,
	name string,
	manifest *gabs.Container,
	assumeYes bool,
	output string,
) error {

	// Looks for the last revison for this solution
	revisonList, err := admission.GetSolutionRevisionList(userDomain, name)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return nil
		}
		return err
	}
	currentCommit := -1
	for _, item := range revisonList.Children() {
		commit := int(item.Path("revision").Data().(float64))
		if commit > currentCommit {
			currentCommit = commit
		}
	}

	// This service is being deployed from the first time and hence there are no
	// changes to be shown.
	if currentCommit == -1 {
		return nil
	}

	// Gets the manifest of the current version, the one is going to be updated.
	currentManifest, err := admission.GetSolutionRevisionManifest(userDomain, name, currentCommit)
	if err != nil {
		return err
	}
	if currentManifest == nil {
		return nil
	}

	err = diff.PrintUpdatedElements(currentManifest, manifest, assumeYes, output)
	if err != nil {
		return err
	}

	return nil
}

func generateSolutionManifest(
	userDomain string,
	name string,
	owner string,
	comment string,
	solutionPath string,
	deploymentPackage string,
	manifestDir string,
	allowJSONManifest bool,
	cuecmd string,
) (manifestFilePath string, manifest *gabs.Container, err error) {
	manifestFile := manifestsDir + "/Manifest.json"

	// Generally, manifestDir is a workspace containing a CUE module composed
	// of CUE files that must be compiled.
	// But, if "allowJSONManifest" is true, then manifestDir can be (not
	// mandatory) a JSON file, instead of a CUE module. This JSON file can be
	// provided as a filename, or via stdin.
	// This means that the result of the CUEmpilation of a CUE module is provided
	// directly, and therefore the CUEmpilation does not have to be executed.
	if allowJSONManifest && solutionPath == "-" {
		err = util.CreateFileFromStdin(manifestFile)
		if err != nil {
			logger.Fatal("Error procesing stdin: " + err.Error())
		}
	} else if allowJSONManifest && util.FileExists(solutionPath) {
		err = util.CopyFile(solutionPath, manifestFile)
		if err != nil {
			logger.Fatal("File copying failed: " + err.Error())
		}
	} else {
		err = cue2json.Export(cuecmd, solutionPath, manifestsDir, deploymentPackage)
		if err != nil {
			logger.Fatal("CUE processing failed: " + err.Error())
		}
	}

	// Modify generated manifest before send it: remove the solution-configuration
	// reference and create the solution wrapper
	manifestContent, err := os.ReadFile(manifestFile)
	if err != nil {
		logger.Fatal(err.Error())
	}
	manifest, err = gabs.ParseJSON(manifestContent)
	if err != nil {
		logger.Fatal(err.Error())
	}
	manifest.Delete("spec")
	manifest.Delete("ref", "version")
	manifest.Set(name, "ref", "name")
	manifest.Set(userDomain, "ref", "domain")
	manifest.Set("solution", "ref", "kind")
	manifest.Set(comment, "comment")
	if owner != "" {
		manifest.Set(owner, "owner")
	}

	top := manifest.Search("top").Data().(string)

	manifest.Set(name, "top")

	// Internal manifests key, name and up must be updated using the provided
	// solution name
	deployments := manifest.Search("deployments").ChildrenMap()
	newDeployments := make(map[string]interface{}, len(deployments))
	toDelete := make([]string, 0, len(deployments))

	for deplName, deployment := range deployments {
		var newName string
		if deplName == top {
			newName = name
		} else {
			newName = fmt.Sprintf("%s%s", name, deplName[len(top):])
		}
		deployment.Set(newName, "name")
		if deployment.Exists("up") {
			if updata := deployment.Search("up").Data(); updata != nil {
				oldup := updata.(string)
				var newup string
				if oldup == top {
					newup = name
				} else {
					newup = fmt.Sprintf("%s%s", name, oldup[len(top):])
				}
				deployment.Set(newup, "up")
			}
		}
		newDeployments[newName] = deployment.Data()
		toDelete = append(toDelete, deplName)
	}

	for _, depName := range toDelete {
		manifest.Delete("deployments", depName)
	}
	for depName, depGabs := range newDeployments {
		manifest.Set(depGabs, "deployments", depName)
	}

	links := manifest.Search("links")
	if links != nil {
		for _, link := range links.Children() {
			for _, elemName := range []string{"s_d", "t_d"} {
				depl := link.Search(elemName).Data().(string)
				if depl == top {
					depl = name
				} else {
					depl = fmt.Sprintf("%s%s", name, depl[len(top):])
				}
				link.Set(depl, elemName)
			}
		}
	}

	err = ioutil.WriteFile(manifestFile, manifest.BytesIndent("", "  "), filePerm)
	if err != nil {
		logger.Fatal("Temporary manifest file writing failed: " + err.Error())
		return
	}

	return
}
