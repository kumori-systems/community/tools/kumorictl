/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// registerDomainCmd represents the register Domain command
var registerDomainCmd = &cobra.Command{
	Use:     "domain <name>",
	Aliases: []string{"domains", "dom", "doms"},
	Short:   "Registers a Domain resource in the platform",
	Long:    `Registers a domain in the platform as a Domain resource.`,
	Args:    checkArgsRegisterDomain,
	Run:     func(cmd *cobra.Command, args []string) { runRegisterDomain(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerDomainCmd)

	registerDomainCmd.PersistentFlags().StringP(
		"domain", "d", "",
		"[required] Domain name",
	)
	registerDomainCmd.MarkPersistentFlagRequired("domain")
	registerDomainCmd.MarkFlagRequired("domain")
}

func checkArgsRegisterDomain(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	return nil
}

func runRegisterDomain(cmd *cobra.Command, args []string) {
	meth := "runRegisterDomain"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("Domain name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	domain, err := cmd.Flags().GetString("domain")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering Domain",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"domain", domain,
		"output", output,
		"meth", meth,
	)

	err = admission.CreateDomain(
		userDomain, name, owner, domain,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Resource Domain " + userDomain + "/" + name + " created")
	}
}
