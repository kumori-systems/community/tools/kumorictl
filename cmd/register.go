/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"regexp"

	"github.com/spf13/cobra"
)

// Names must:
// - start with a letter
// - end with a letter or number
// - contain any character except: / and spaces
// - be at most 63 characters long (not checked in regex, checked elsewhere)
var elementNameRegExpStr = `^[a-z]([^\/\s]*[a-z0-9])?$`
var elementNameRegExp = regexp.MustCompile(elementNameRegExpStr)

// registerCmd represents the register command
// Is just a container for several subcommands
var registerCmd = &cobra.Command{
	Use: "register",
	// Register command is executed only if an invalid subcommand is provided or a subcommand is not
	// provided at all. Hence, it will fail either asking for a subcommand (if none is included)
	// or indicating that the provided subcommand is invalid.
	Short: "Register an element in the platform",
	Args:  cobra.MatchAll(cobra.NoArgs, cobra.MinimumNArgs(1)),
	Run:   func(cmd *cobra.Command, args []string) {},
}

func init() {
	rootCmd.AddCommand(registerCmd)

	registerCmd.PersistentFlags().StringP(
		"owner", "", "",
		"owner of the new resource (only for administrators)",
	)
}

// Kumori element (v3deployments, inbounds, secrets...) names are used as label
// values and label names (more restricted syntax) in Kubernetes objects
//
// So: element names must meet the restrictions of the kubernetes label names
//
// Kubernetes labels names :
// << DNS Label: an alphanumeric (a-z, and 0-9) string, with a maximum length
// of 63 characters, with the - character allowed anywhere except the first
// or last character, suitable for use as a hostname or segment in a domain
// name. There are two variations:
// - rfc1035: based on regexp [a-z]([-a-z0-9]*[a-z0-9])?
// - rfc1123: based on regexp [a-z0-9]([-a-z0-9]*[a-z0-9])?  >>
func validateName(name string) bool {
	if len(name) > 63 {
		return false
	}
	return elementNameRegExp.MatchString(name)
}
