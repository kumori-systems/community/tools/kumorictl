/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// unregisterSolutionCmd represents the unregister solution command
var unregisterSolutionCmd = &cobra.Command{
	Use:     "deployment <user>/<name>",
	Aliases: []string{"deployments"},
	Short:   "Unregister existing deployment from platform",
	Long: `Unregister existing deployment from platform.
<user> can be ommited and defaults to the logged user.
Linked deployments won't be unregistered, unless ` + "`--force`" + ` flag is set.`,
	Args: checkArgsUnregisterSolution,
	Run:  func(cmd *cobra.Command, args []string) { runUnregisterSolution(cmd, args) },
}

// undeployCmd is just an alias to the "unregister solution" subcommand
var undeployCmd = &cobra.Command{
	Use:   "undeploy <user>/<name>",
	Short: unregisterSolutionCmd.Short + " (alias of register deployment)",
	Long:  unregisterSolutionCmd.Long,
	Args:  unregisterSolutionCmd.Args,
	Run:   unregisterSolutionCmd.Run,
}

func init() {
	// Set the same flags for "undeploy" and "unregister deployment" commands
	unregisterCmd.AddCommand(unregisterSolutionCmd)
	setUndeployFlags(unregisterSolutionCmd)
	rootCmd.AddCommand(undeployCmd)
	setUndeployFlags(undeployCmd)

	// The "undeploy" command doesn't depends directly on "unregister" command,
	// so it must inherit its flags too.
	setUnregisterFlags(undeployCmd)
}

func checkArgsUnregisterSolution(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runUnregisterSolution(cmd *cobra.Command, args []string) {
	meth := "runUnregisterSolution()"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	force, err := cmd.Flags().GetBool("force")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Unregistering deployment",
		"name", name,
		"userDomain", userDomain,
		"force", force,
		"wait", waitDuration,
		"output", output,
		"meth", meth,
	)

	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !exists {
		err = errors.New("deployment " + userDomain + "/" + name + " not found")
		logger.Fatal(err.Error())
	}

	err = admission.DeleteSolution(userDomain, name, force, waitDuration)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Deployment " + userDomain + "/" + name + " deleted")
	}
}

func setUndeployFlags(cmd *cobra.Command) {
	cmd.Flags().BoolP(
		"force", "f", false,
		"removes links as needed to allow unregistration",
	)
}
