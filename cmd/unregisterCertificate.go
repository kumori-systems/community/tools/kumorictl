/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// unregisterCertificateCmd represents the unregisterCertificate command
var unregisterCertificateCmd = &cobra.Command{
	Use:     "certificate <user>/name>",
	Aliases: []string{"cert"},
	Short:   "Unregister existing certificate from platform",
	Long: `Unregister existing certificate from platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsUnregisterCertificate,
	Run:  func(cmd *cobra.Command, args []string) { runUnregisterCertificate(cmd, args) },
}

func init() {
	unregisterCmd.AddCommand(unregisterCertificateCmd)
}

func checkArgsUnregisterCertificate(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runUnregisterCertificate(cmd *cobra.Command, args []string) {
	meth := "runUnregisterCertificate"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Unregistering certificate",
		"name", name,
		"userDomain", userDomain,
		"wait", waitDuration,
		"output", output,
		"meth", meth,
	)

	err = admission.DeleteCertificate(userDomain, name, waitDuration)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Resource certificate " + userDomain + "/" + name + " deleted")
	}
}
