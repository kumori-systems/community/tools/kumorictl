/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/Jeffail/gabs/v2"
)

// This package works with JSON like this:
// {
// 	"p": {  // Name of subservice: p
// 		"label": 3, // Subservice suffix: 003
// 		"subservice": {}
// 	},
// 	"s": { // Name of subservice: s
// 		"label": 1, // Subservice suffix: 001
// 		"subservice": {
// 			"s": { // Name of subservice: s
// 				"label": 2, // Subservice suffix: 002
// 				"subservice": {}
// 			}
// 		}
// 	}
// }

const suffixLength = 3

type Subservice struct {
	Label       int            `json:"label"`
	Subservices SubserviceInfo `json:"subservice"`
}

type SubserviceInfo map[string]Subservice

func NewSubserviceInfo(content string) (subd *SubserviceInfo, err error) {
	subd = &SubserviceInfo{}
	err = json.Unmarshal([]byte(content), subd)
	return
}

// Print just prints the content of SubserviceInfo (debug purposes)
func (s *SubserviceInfo) Print(depth int) {
	for k, v := range *s {
		fmt.Println(strings.Repeat(" ", depth), k, v.Label)
		v.Subservices.Print(depth + 1)
	}
}

// DecodeElementName basically returns the same deploymentName if the deployment
// is the top deployment in the solution or the deployment name minus the top. This is used
// when solutions include subservices. For example, if a solution called `calculator` includes
// a subdeployment assigned to the role `hazel` of the top deployment, the subdeployment name
// will be `calculator.hazel`. In this case, the method reutrn `hazel`.
func (s *SubserviceInfo) DecodeElementName(
	manifest *gabs.Container,
	deploymentName string,
) (decodedRole string, err error) {

	top := manifest.Search("top").Data().(string)

	if deploymentName == top {
		return fmt.Sprintf("Deployment"), nil
	}

	return fmt.Sprintf("Role %s", deploymentName[len(top):]), nil
}

// DecodeRole converts, using the subservices, a role name like "myrole001"
// into a human-readable format.
// For example:
// myrole000 => myrole
// myrole001 => subd1.myrole, being mysubd1 the subservice labeled as "1"
// myrole003 => subd2.subd1.myrole, being mysubd2 a subservice of mysubd1,
//
//	labeled (respectively) as "2" and "1"
//
// Role names contains a suffix of exactly three characters (like "002"), which
// identifies the subservice
func (s *SubserviceInfo) DecodeRole(
	roleName string,
) (decodedRole string, err error) {

	length := len(roleName)
	if length < suffixLength+1 {
		err = fmt.Errorf("Invalid role name: %s", roleName)
		return
	}
	name := roleName[0 : length-suffixLength]
	suffix := roleName[length-suffixLength : length]

	subd, found := s.getSubdNameBySuffix(suffix, "")
	if !found {
		err = fmt.Errorf("Subservice not found for role name: %s", roleName)
		return
	}
	decodedRole = subd + name
	return
}

// EncodeRole converts, using the subservices, a human-readame rolename into
// something like "myrole001"
// For example:
// myrole => myrole000
// subd1.myrole => myrole001, being mysubd1 the subservice labeled as "1"
// subd2.subd1.myrole => myrole003, being mysubd2 a subservice of mysubd1,
//
//	labeled (respectively) as "2" and "1"
func (s *SubserviceInfo) EncodeRole(roleName string) (encodedRole string, err error) {

	length := len(roleName)
	dotPosition := strings.LastIndex(roleName, ".")

	// This role doesnt belongs to any subservice (deployment "000")
	if dotPosition == -1 {
		encodedRole = roleName + strings.Repeat("0", suffixLength)
		return
	}

	subd := roleName[0:dotPosition]
	name := roleName[dotPosition+1 : length]

	suffix, found := s.getSubdSuffixByName(subd)
	if !found {
		err = fmt.Errorf("Subservice not found for role name: %s", roleName)
		return
	}
	encodedRole = name + suffix
	return
}

// getSubdNameBySuffix converts something like "003" to "subd2.subd1"
func (s *SubserviceInfo) getSubdNameBySuffix(
	suffix string, current string,
) (
	subd string, found bool,
) {
	// suffix is something like "001", "002"...
	// subdName is something like "subd1" or "subd2.subd1"

	// Special suffix "000" -> any subservice
	if suffix == strings.Repeat("0", suffixLength) {
		subd = ""
		found = true
		return
	}

	for k, v := range *s {
		if s.intToSuffix(v.Label) == suffix {
			subd = k + "." + current
			found = true
			return
		} else {
			subd, found = v.Subservices.getSubdNameBySuffix(suffix, k+"."+current)
			if found {
				return
			}
		}
	}
	return
}

// getSubdSuffixByName converts something like "subd2.subd1" to "003"
func (s *SubserviceInfo) getSubdSuffixByName(
	subdName string,
) (
	suffix string, found bool,
) {
	// subdName is something like "subd1" or "subd2.subd1"
	// suffix is something like "001", "002"...
	parts := strings.Split(subdName, ".")
	current := *s
	found = true
	for i := len(parts) - 1; i >= 0; i-- {
		aux, ok := current[parts[i]]
		if ok {
			suffix = s.intToSuffix(aux.Label)
			current = aux.Subservices
		} else {
			suffix = ""
			found = false
			break
		}
	}
	return
}

// intToSuffix converts 2 to "002"
func (s *SubserviceInfo) intToSuffix(value int) string {
	str := strconv.Itoa(value)
	for {
		if len(str) == suffixLength {
			return str
		} else {
			str = "0" + str
		}
	}
}

// suffixToInt converts "002" to 2
func (s *SubserviceInfo) suffixToInt(suffix string) int {
	value, _ := strconv.Atoi(suffix)
	return value
}
