/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// setupBashCompletionCmd represents the completion command
var setupBashCompletionCmd = &cobra.Command{
	Use:   "setup-bash-completion",
	Short: "Setup kumorictl bash completion for current user",
	Run: func(cmd *cobra.Command, args []string) {
		bashCompletionScriptsDir := os.Getenv("BASH_COMPLETION_USER_DIR")
		if bashCompletionScriptsDir == "" {
			bashCompletionScriptsDir = os.Getenv("XDG_DATA_HOME")
			if bashCompletionScriptsDir == "" {
				bashCompletionScriptsDir = os.Getenv("HOME") + "/.local/share"
			}
			bashCompletionScriptsDir += "/bash-completion"
		}
		bashCompletionScriptsDir += "/completions"
		err := os.MkdirAll(bashCompletionScriptsDir, 0755)
		if err != nil {
			logger.Fatal(err.Error())
		}
		kumorictlPath, err := os.Executable()
		if err != nil {
			logger.Fatal(err.Error())
		}
		bashCompletionScript := []byte("#!/bin/bash\n. <(" + kumorictlPath + " bash-completion)")
		err = ioutil.WriteFile(bashCompletionScriptsDir+"/kumorictl", bashCompletionScript, 0755)
		if err != nil {
			logger.Fatal(err.Error())
		}
	},
}

func init() {
	// Hidden because it is not working
	setupBashCompletionCmd.Hidden = true
	// rootCmd.AddCommand(setupBashCompletionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setupBashCompletionCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setupBashCompletionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
