/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getAllCmd represents the getAll command
var getAllCmd = &cobra.Command{
	Use:   "all",
	Short: "Get a list of all element",
	Args:  checkArgsGetAll,
	Run:   func(cmd *cobra.Command, args []string) { runGetAll(cmd, args) },
}

func init() {
	getCmd.AddCommand(getAllCmd)
}

func checkArgsGetAll(cmd *cobra.Command, args []string) error {
	if len(args) > 0 {
		return errors.New("too many arguments")
	}
	return nil
}

func runGetAll(cmd *cobra.Command, args []string) {
	output, _ := cmd.Flags().GetString("output")
	if output == JSONOutput {
		data := gabs.New()
		response.SetData(data)
		listSolutions(output)
		listCertificates(false, output)
		listSecrets(false, output)
		listPorts(false, output)
		listVolumes(false, output)
		listDomains(false, output)
		listCAs(false, output)
	} else {
		fmt.Println(toBold("Deployments:"))
		listSolutions(output)
		// fmt.Println(toBold("HTTP Inbounds:"))
		// listHTTPInbound()
		// fmt.Println(toBold("TCP Inbounds:"))
		// listTCPInbound()
		fmt.Println(toBold("Certificates:"))
		listCertificates(false, output)
		fmt.Println(toBold("Secrets:"))
		listSecrets(false, output)
		fmt.Println(toBold("Ports:"))
		listPorts(false, output)
		fmt.Println(toBold("Volumes:"))
		listVolumes(false, output)
		fmt.Println(toBold("Domains:"))
		listDomains(false, output)
		fmt.Println(toBold("CAs:"))
		listCAs(false, output)
	}
}
