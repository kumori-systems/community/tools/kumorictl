/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// unlinkCmd represents the unlink command
var unlinkCmd = &cobra.Command{
	Use:   "unlink <user>/<name>:<channel> <user>/<name>:<channel>",
	Short: "Unlink a previously linked pair of deployments' service channels",
	Long: `Unlink a previously linked pair of deployments' service channels.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsUnlink,
	Run:  func(cmd *cobra.Command, args []string) { runUnlink(cmd, args) },
}

func init() {
	rootCmd.AddCommand(unlinkCmd)

	unlinkCmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time link to be deleted before returning.",
	)
}

func checkArgsUnlink(cmd *cobra.Command, args []string) error {

	// TBD: is the same code than "link"

	err := cobra.ExactArgs(2)(cmd, args)
	if err != nil {
		return err
	}

	err = checkUserDomainAndNameAndChannel(args[0])
	if err != nil {
		return err
	}
	channel1, err := getChannel(args[0])
	if err != nil {
		return err
	}

	err = checkUserDomainAndNameAndChannel(args[1])
	if err != nil {
		return err
	}
	channel2, err := getChannel(args[1])
	if err != nil {
		return err
	}

	if channel1 == "" && channel2 == "" {
		return errors.New("channel missing in at least one argument. Arguments must match <userdomain/name:channel> format")
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runUnlink(cmd *cobra.Command, args []string) {
	meth := "runUnink"

	// Function checkArgsLink ensured that there is exactly 1 deployment and 1 inbound

	userDomain1, name1, channel1, err := getUserDomainAndNameAndChannel(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain2, name2, channel2, err := getUserDomainAndNameAndChannel(args[1], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Unlinking deployments",
		"userDomain1", userDomain1,
		"name1", name1,
		"channel1", channel1,
		"userDomain2", userDomain2,
		"name2", name2,
		"channel2", channel2,
		"wait", waitDuration,
		"output", output,
		"meth", meth,
	)

	err = admission.DeleteLink(
		userDomain1, name1, channel1,
		userDomain2, name2, channel2,
		"",
		waitDuration,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Link " + args[0] + " <=> " + args[1] + " deleted")
	}
}
