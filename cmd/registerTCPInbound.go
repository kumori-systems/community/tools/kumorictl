/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// registerTCPInboundCmd represents the register certificate command
var registerTCPInboundCmd = &cobra.Command{
	Use:     "tcp-inbound <name>",
	Aliases: []string{"tcpinbound"},
	Short:   "Create a TCP Inbound deployment and register it in the platform",
	// Long:    `Create a TCP Inbound deployment and register it in the platform.`,
	Args: checkArgsRegisterTCPInbound,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterTCPInbound(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerTCPInboundCmd)
	setRegisterTCPInboundFlags(registerTCPInboundCmd)
}

func checkArgsRegisterTCPInbound(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain of the TCPInbound can be omitted when user-domain is set in
	// configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	return nil
}

func runRegisterTCPInbound(cmd *cobra.Command, args []string) {
	meth := "runRegisterTCPInbound"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !validateName(name) {
		err = fmt.Errorf("TCP inbound name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if exists {
		err = errors.New("TCP inbound " + userDomain + "/" + name + " already exists")
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering TCP inbound",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"output", output,
		"meth", meth,
	)
	applyTCPInbound(cmd, args, owner)
	if output != JSONOutput {
		fmt.Println("TCP inbound " + userDomain + "/" + name + " created")
	}
}

func applyTCPInbound(cmd *cobra.Command, args []string, owner string) {
	meth := "applyTCPInbound"

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	// domain, err := cmd.Flags().GetString("domain")
	// if err != nil {
	// 	logger.Fatal(err.Error())
	// }

	port, err := cmd.Flags().GetString("port")
	if err != nil {
		logger.Fatal(err.Error())
	}
	portUserDomain, portName, err := getUserDomainAndName(port, owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	comment, err := cmd.Flags().GetString("comment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Applying TCP inbound",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"portName", portName,
		"portUserDomain", portUserDomain,
		"comment", comment,
		"output", output,
		"meth", meth,
	)

	err = admission.CreateTCPInbound(
		userDomain, name, owner, portName, portUserDomain, comment,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"TCP inbound applied",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"portName", portName,
		"portUserDomain", portUserDomain,
		"comment", comment,
		"meth", meth,
	)
}

func setRegisterTCPInboundFlags(cmd *cobra.Command) {
	// cmd.PersistentFlags().StringP(
	// 	"domain", "d", "",
	// 	"[required] tcp-inbound domain",
	// )
	// cmd.MarkPersistentFlagRequired("domain")
	// cmd.MarkFlagRequired("domain")

	cmd.Flags().StringP(
		"port", "p", "",
		"port <user>/<name> resource assigned to this tcp-inbound. <user> can be ommited and defaults to the logged user",
	)

	// cmd.Flags().StringP(
	// 	"owner", "", "",
	// 	"owner of the new deployment (only for administrators)",
	// )

	cmd.Flags().StringP(
		"comment", "m", "",
		"message describing the action",
	)
}
