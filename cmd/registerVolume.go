/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// registerVolumeCmd represents the registerVolume command
var registerVolumeCmd = &cobra.Command{
	Use:   "volume <name>",
	Short: "Create a volume resource and register it in the platform",
	// Long:  `Create a volume resource and register it in the platform.`,
	Args: checkArgsRegisterVolume,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterVolume(cmd, args) },
}

// Allowed units: M,G,T,P,E and Mi,Gi,Ti,Pi,Ti  (K/Ki is not allowed)
var volumeSizeRegexp = regexp.MustCompile(`^([0-9]+)(?:M|G|T|E|P|Mi|Gi|Ti|Pi|Ei)$`)

func init() {
	registerCmd.AddCommand(registerVolumeCmd)

	registerVolumeCmd.Flags().Uint32P(
		"items", "i", 0,
		"maximum number of volume items for this volume. If not set or set to 0, volume items won't be limited",
	)

	registerVolumeCmd.Flags().StringP(
		"size", "s", "",
		"size of each volume item, including the unit. Allowed units are: M,G,T,P,E,Mi,Gi,Ti,Pi,Ei",
	)

	registerVolumeCmd.Flags().StringP(
		"type", "t", "",
		"type of the volume to be created, for example 'persistent'. Allowed values will depend on the cluster configuration",
	)

	registerVolumeCmd.Flags().StringP(
		"parameters", "p", "",
		"additional parameters for the volume creation (currently ignored)",
	)
}

func checkArgsRegisterVolume(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return registerError(response.ErrorSeverityType, err)
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetUint32("items")
	if err != nil {
		return err
	}

	volSize, err := cmd.Flags().GetString("size")
	if err != nil {
		return err
	}
	err = checkVolSize(volSize)
	if err != nil {
		return err
	}

	volType, err := cmd.Flags().GetString("type")
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("parameters")
	if err != nil {
		return err
	}

	if volType == "" {
		return errors.New("Volume type cannot be empty")
	}

	return nil
}

func runRegisterVolume(cmd *cobra.Command, args []string) {
	meth := "runRegisterVolume"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("Volume name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	volItems, err := cmd.Flags().GetUint32("items")
	if err != nil {
		logger.Fatal(err.Error())
	}

	volSize, err := cmd.Flags().GetString("size")
	if err != nil {
		return
	}
	err = checkVolSize(volSize)
	if err != nil {
		logger.Fatal(err.Error())
	}

	volType, err := cmd.Flags().GetString("type")
	if err != nil {
		logger.Fatal(err.Error())
	}

	volParameters, err := cmd.Flags().GetString("parameters")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering volume",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"items", volItems,
		"size", volSize,
		"type", volType,
		"parameters", volParameters,
		"output", output,
		"meth", meth,
	)

	err = admission.CreateVolume(userDomain, name, owner, volItems, volSize, volType, volParameters)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Resource volume " + userDomain + "/" + name + " created")
	}
}

func checkVolSize(volSize string) (err error) {
	// Remove initial "zeros" before apply regular expression
	volSize = strings.TrimLeft(volSize, "0")

	// Check the size using a regular expression
	if volumeSizeRegexp.FindString(volSize) != volSize {
		err = fmt.Errorf("'%s' is not a valid size", volSize)
		return
	}
	return
}
