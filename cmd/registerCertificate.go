/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
)

// registerCertificateCmd represents the register certificate command
var registerCertificateCmd = &cobra.Command{
	Use:     "certificate <name>",
	Aliases: []string{"cert"},
	Short:   "Create a certificate resource and register it in the platform",
	// Long:    `Create a certificate resource and register it in the platform.`,
	Args: checkArgsRegisterCertificate,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterCertificate(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerCertificateCmd)

	registerCertificateCmd.PersistentFlags().StringP(
		"domain", "d", "",
		"[required] certificate domain (wildcard domains allowed)",
	)
	registerCertificateCmd.MarkPersistentFlagRequired("domain")
	registerCertificateCmd.MarkFlagRequired("domain")

	registerCertificateCmd.PersistentFlags().StringP(
		"cert-file", "c", "",
		"[required] certificate file",
	)
	registerCertificateCmd.MarkPersistentFlagRequired("cert-file")
	registerCertificateCmd.MarkFlagRequired("cert-file")

	registerCertificateCmd.PersistentFlags().StringP(
		"key-file", "k", "",
		"[required] private key file",
	)
	registerCertificateCmd.MarkPersistentFlagRequired("key-file")
	registerCertificateCmd.MarkFlagRequired("key-file")

	registerCertificateCmd.PersistentFlags().BoolP(
		"public", "p", false,
		"make the certificate available to all cluster users",
	)
}

func checkArgsRegisterCertificate(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])

	if err != nil {
		return err
	}

	return nil
}

func runRegisterCertificate(cmd *cobra.Command, args []string) {
	meth := "runRegisterCertificate"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("Certificate name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	domain, err := cmd.Flags().GetString("domain")
	if err != nil {
		logger.Fatal(err.Error())
	}

	certFile, err := cmd.Flags().GetString("cert-file")
	if err != nil {
		logger.Fatal(err.Error())
	}

	keyFile, err := cmd.Flags().GetString("key-file")
	if err != nil {
		logger.Fatal(err.Error())
	}

	public, err := cmd.Flags().GetBool("public")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering certificate",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"domain", domain,
		"certFile", certFile,
		"keyFile", keyFile,
		"public", public,
		"output", output,
		"meth", meth,
	)

	// Ambassador/Envoy crash if corrupted certificates are used (see ticket90)
	// err = x509Validator.ValidateCertificate(certFile)
	// if err != nil {
	// 	logger.Fatal(err.Error())
	// }
	// err = x509Validator.ValidatePrivateKey(keyFile)
	// if err != nil {
	// 	logger.Fatal(err.Error())
	// }
	err = x509Validator.ValidateKeyPair(certFile, keyFile)
	if err != nil {
		message := fmt.Sprintf("Error validating key pair: %s", err.Error())
		logger.Fatal(message)
	}

	err = admission.CreateCertificate(userDomain, name, owner, domain, certFile, keyFile, public)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Resource certificate " + userDomain + "/" + name + " created")
	}
}
