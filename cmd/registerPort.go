/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// registerPortCmd represents the registerPort command
var registerPortCmd = &cobra.Command{
	Use:   "port <name>",
	Short: "Register a Port in the platform",
	// Long: `Register a Port in the platform.`,
	Args: checkArgsRegisterPort,
	Run:  func(cmd *cobra.Command, args []string) { runRegisterPort(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerPortCmd)

	registerPortCmd.Flags().IntP(
		"port", "p", 0,
		"port number to be used",
	)

}

func checkArgsRegisterPort(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	port, err := cmd.Flags().GetInt("port")
	if err != nil {
		return err
	}

	if port == 0 {
		return errors.New("port flag must be used")
	}

	return nil
}

func runRegisterPort(cmd *cobra.Command, args []string) {
	meth := "runRegisterPort"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("Port name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	port, err := cmd.Flags().GetInt("port")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering port",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"port", port,
		"output", output,
		"meth", meth,
	)

	err = admission.CreatePort(userDomain, name, owner, port)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Resource Port " + userDomain + "/" + name + " created")
	}
}
