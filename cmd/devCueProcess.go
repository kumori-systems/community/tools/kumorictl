/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/ldez/go-git-cmd-wrapper/checkout"
	"github.com/ldez/go-git-cmd-wrapper/clone"
	"github.com/ldez/go-git-cmd-wrapper/git"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/cue2json"
	"gitlab.com/kumori/kumorictl/pkg/giturl"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// devCueProcessCmd represents the dev cue process command
var devCueProcessCmd = &cobra.Command{
	Use: "process <path/to/module | git URI> [output dir]",
	Example: `- Git URI: <git URL>[#<version>[/<src path>]]
  <git URL>: https://github.com/user/repo
             git@gitlab.com:user/repo
  <version>: branch, tag, or first-level directory.
  <src path>: (sub)directory where the CUE module is located.
  Full example:
    git@gitlab.com:user/repo#v1/deployment`,
	Short: "Validate and process given CUE module",
	Long: `Validate and process given CUE module.

This command outputs in JSON format the result of processing the given CUE module.

Specifying output directory is optional in case providing a local path, otherwise mandatory.

If no output directory is specified, the result is written in the source CUE module directory.`,
	Args: cobra.RangeArgs(1, 2),
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("devCueProcess called")
		if len(args) == 1 && (giturl.MatchesScheme(args[0]) || giturl.MatchesScpLike(args[0])) {
			logger.Fatal("Output directory is required when providing Git URI")
		}
		src := args[0]
		dst := args[len(args)-1]
		deploymentPackage, err := cmd.Flags().GetString("package")
		if err != nil {
			logger.Fatal(err.Error())
		}
		timestamp := time.Now().Format("2006-01-02_15.04.05")
		devCueProcess(src, dst, deploymentPackage, timestamp)
		fmt.Println("Done.")
	},
}

func devCueProcess(inputDir string, outputDir string, deploymentPackage string, timestamp string) {

	cuecmd := viper.Global.GetString("cue-command")
	if cuecmd == "" {
		logger.Fatal("CUE command not found")
	}

	if giturl.MatchesScheme(inputDir) || giturl.MatchesScpLike(inputDir) {
		gitTmpDir := "./.tmp/" + timestamp + "/git"
		defer func() {
			if !keepTmpFiles {
				err := os.RemoveAll("./.tmp/")
				if err != nil {
					logger.Fatal(err.Error())
				}
			}
		}()
		split := strings.Split(inputDir, "#")
		repository := split[0]
		_, err := git.Clone(clone.Repository(repository), clone.Directory(gitTmpDir))
		if err != nil {
			logger.Fatal("Cannot clone given repository")
		}
		originalDir, err := os.Getwd()
		if err != nil {
			logger.Fatal(err.Error())
		}
		err = os.Chdir(gitTmpDir)
		if err != nil {
			logger.Fatal(err.Error())
		}
		if len(split) == 2 {
			split = strings.SplitN(split[1], "/", 2)
			version := split[0]
			_, err := git.Checkout(checkout.Branch(version), checkout.Detach)
			if err != nil {
				_, err := git.Checkout(checkout.Branch(version))
				if err != nil {
					err = os.Chdir(version)
					if err != nil {
						logger.Fatal("Cannot find given version in repository")
					}
				}
			}
			if len(split) > 1 {
				srcPath := split[1]
				err = os.Chdir(srcPath)
				if err != nil {
					logger.Fatal("Given source path does not exist in repository")
				}
			}
		} else if len(split) > 2 {
			logger.Fatal("Malformed Git URI")
		}
		inputDir, err = os.Getwd()
		if err != nil {
			logger.Fatal(err.Error())
		}
		resolveModule(inputDir, false)
		err = os.Chdir(originalDir)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}
	err := os.MkdirAll(outputDir, 0755)
	if err != nil {
		logger.Fatal(err.Error())
	}
	err = cue2json.Export(cuecmd, inputDir, outputDir, deploymentPackage)
	if err != nil {
		logger.Fatal("CUE processing failed (please use kumorictl dev cue vet for further validation details): " + err.Error())
	}
}

func init() {
	devCueCmd.AddCommand(devCueProcessCmd)

	devCueCmd.Flags().StringP("package", "p", "deployment", "Deployment package")
}
