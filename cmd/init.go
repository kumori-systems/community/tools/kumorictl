/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize workspace",
	Long: `Initialize workspace.

This command initializes the current directory as a Kumori workspace, creating the config file required to use kumorictl. In order to create config file in your HOME directory, set ` + "`--global`" + ` flag. You can also use ` + "`--config`" + ` flag to specify a custom location.

The config file is created using global config or, if missing, defaults. To customize configuration parameters, then use ` + "`kumorictl config`" + `.

Please note this command will purposely fail if the current directory has already been initialized as a workspace.`,
	Args: cobra.NoArgs,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("init called")
		if cfgFile == "" {
			global, _ := cmd.Flags().GetBool("global")
			if global {
				viper.Global.SetConfigFile(globalConfigDir + "/" + configName + "." + configExt)
			} else {
				viper.Global.SetConfigFile(workspaceConfigDir + "/" + configName + "." + configExt)
			}
		}
		force, _ := cmd.Flags().GetBool("force")
		ignore, _ := cmd.Flags().GetBool("ignore")

		// Abort if force and ignore flags are set. An existing file cannot be kept as it is
		// and overwrited at the same time
		if ignore && force {
			logger.Fatal("Cannot ignore the existing file and overwrite it (force) at the same time")
		}

		logger.Info("Writing config file: " + viper.Global.ConfigFileUsed())

		// Creates the config file path if not exists
		_ = os.MkdirAll(filepath.Dir(viper.Global.ConfigFileUsed()), 0700)

		// If force flag is not set, checks if the config file already exists. If exists and
		// the ignore flag is set, exists (code 0). If exists but the ignore flag is not set,
		// failes. Otherwise, keeps going.
		if !force {
			_, err := os.Stat(viper.Global.ConfigFileUsed())
			if err == nil {
				if ignore {
					return
				}
				logger.Fatal(fmt.Sprintf("Config File %q already Exists", viper.Global.ConfigFileUsed()))
			}
			if !os.IsNotExist(err) {
				logger.Fatal(err.Error())
			}
		}

		// To avoid wiping tokens when running init on already initialized workspace.
		viper.Global.Set("access-token", "")
		viper.Global.Set("access-token-expiry-date", "")
		viper.Global.Set("refresh-token", "")
		viper.Global.Set("refresh-token-expiry-date", "")
		viper.Global.Set("docker-server", "https://index.docker.io/v1/")
		viper.Global.Set("admission", "")
		viper.Global.Set("admission-protocol", "https")
		viper.Global.Set("log-level", "warn")
		viper.Global.Set("config-version", configCmd.Version)
		viper.Global.Set("diff-tool", "internal")
		viper.Global.Set("user-domain", "anonymous")
		viper.Global.Set("cue-command", "cue")
		err := viper.Global.WriteConfigAs(viper.Global.ConfigFileUsed())
		if err != nil {
			logger.Fatal(err.Error())
		}

		output, _ := cmd.Flags().GetString("output")
		if output != JSONOutput {
			fmt.Println("Workspace initialized. You may want to use kumorictl config to customize configuration parameters.")
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().BoolP("global", "g", false, "write config file in HOME directory")
	initCmd.Flags().BoolP("ignore", "i", false, "keep the current config file if exists and exist without failing (returns code 0)")
	initCmd.Flags().BoolP("force", "f", false, "overwrites the current config file if exists")
}
