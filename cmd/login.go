/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login <user>",
	Short: "Login to Kumori Platform",
	Long: `Login to Kumori Platform.

This command interactively asks for a password and authenticates against Admission service to obtain access and refresh tokens, and their expiry dates. This is saved in config file and is used for later interactions with the Platform.

Tokens are dynamically renewed during kumorictl usage. Once refresh token expires, user will be instructed to login again.`,
	Args: cobra.ExactArgs(1),
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
		CheckConfigFile(cmd)
		SetAuthMethod(cmd)
		CheckTokenAuthenticationRequired(cmd)
		SetAdmissionURL(cmd)
		CheckClusterCompatibility(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) { runLogin(cmd, args) },
}

func init() {
	rootCmd.AddCommand(loginCmd)
}

func CheckTokenAuthenticationRequired(cmd *cobra.Command) {

	// Login and password and used when token-based mechanism is used. In that case, the user can get a
	// token by using his/her login and password. After that, he/she gets an access-token and a
	// refresh-token, which will be stored in kumorictl configuration.
	authMethod := admission.GetAuthMethod()

	if authMethod != admission.TokenAuthMethod {
		logger.Fatal(fmt.Sprintf("Login command cannot be used with %s authentication method", authMethod))
	}
}

func runLogin(cmd *cobra.Command, args []string) {
	meth := "runLogin"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Login",
		"output", output,
		"meth", meth,
	)

	prompt := ""
	if output != JSONOutput {
		prompt = "Enter Password: "
	}
	password, err := util.GetPasswd(prompt, true)
	if err != nil {
		logger.Fatal(err.Error())
	}
	username := args[0]

	accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err := admission.Login(username, password)
	if err != nil {
		logger.Fatal(err.Error())
	}
	viper.Global.Set("access-token", accessToken)
	viper.Global.Set("access-token-expiry-date", accessTokenExpiryDate)
	viper.Global.Set("refresh-token", refreshToken)
	viper.Global.Set("refresh-token-expiry-date", refreshTokenExpiryDate)
	viper.Global.Set("user-domain", username)
	err = viper.Global.WriteConfig()
	if err != nil {
		logger.Fatal("Workspace initialization is mandatory to use this command. " + err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Login OK.")
	}
}
