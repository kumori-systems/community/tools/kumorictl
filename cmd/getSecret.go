/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getSecretCmd represents the getSecret command
var getSecretCmd = &cobra.Command{
	Use:     "secret [<user>/<name>]",
	Aliases: []string{"secrets"},
	Short:   "Get secret resource from platform",
	Long: `Get secret resource from platform
If <user>/<name> is not provided, all secrets are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetSecret,
	Run:  func(cmd *cobra.Command, args []string) { runGetSecret(cmd, args) },
}

func init() {
	getCmd.AddCommand(getSecretCmd)
	getSecretCmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetSecret(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetSecret(cmd *cobra.Command, args []string) {
	meth := "runGetSecret"

	output, _ := cmd.Flags().GetString("output")
	if len(args) == 0 {
		// Get secret list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}

		logger.Debug(
			"Getting secret",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listSecrets(onlyInUse, output)

	} else if len(args) == 1 {
		// Get secret manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting secret",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getSecret(userDomain, name, output)
	}

	return
}

func listSecrets(onlyInUse bool, output string) {
	secrets, err := admission.GetSecrets(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {
		// If the data has not been set yet, then the secrets are included directly
		// in the root part. If a Data object already exists, the secrets are added
		// in the "secrets" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing secrets in JSON format", "error", err.Error())
		}
		for _, secret := range secrets {
			data.ArrayAppend(secret.LongName, "secrets")
		}

		if !dataExists {
			data = data.Search("secrets")
		}
		response.SetData(data)

	} else {

		for _, secret := range secrets {
			fmt.Println(secret.LongName)
		}
	}
	return
}

func getSecret(userDomain, name string, output string) {
	secret, err := admission.GetSecret(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(secret)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}
		} else {
			fmt.Println(secret)
		}
	}
	return
}
