/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"
	"hash/fnv"
	"strconv"
	"strings"

	"github.com/disiqueira/gotree/v3"
	"github.com/fatih/color"
)

func pad(level int) string {
	if level == 0 {
		return ""
	}
	return " " + strings.Repeat("   ", level-1) + " "
}

func treePrint(tree gotree.Tree, level int, a ...string) gotree.Tree {
	if level == 0 {
		return gotree.New(strings.Join(a, " "))
	}
	return tree.Add(strings.Join(a, " "))
}

// Colour a value if it is greater than the thresholds (thresholdYellow must be lower than thresholdRed).
// Example: for colouring percent of Memory usage.
func colourPercentageHigh(value int, thresholdYellow int, thresholdRed int) (percentage string) {
	percentage = strconv.Itoa(value) + "%"
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	yellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if value < thresholdYellow {
		percentage = green(percentage)
	} else if value < thresholdRed {
		percentage = yellow(percentage)
	} else {
		percentage = red(percentage)
	}
	return
}

// Colour a value if it is lower than the thresholds (thresholdRed must be lower than thresholdYellow).
// Example: for colouring percent of free disk space.
func colourPercentageLow(value int, thresholdYellow int, thresholdRed int) (percentage string) {
	percentage = strconv.Itoa(value) + "%"
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	yellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if value > thresholdYellow {
		percentage = green(percentage)
	} else if value > thresholdRed {
		percentage = yellow(percentage)
	} else {
		percentage = red(percentage)
	}
	return
}

// kumoriTableMerge merges de values of two consecutive columns if they represent the same
// element considering only the columns in groupedColumns. For example, if groupedColumns={0,1}
// and considering the following rows:
//
//	frontend    instance-0   value1
//	frontend    instance-0   value2
//	worker      instance-0   value3
//	worker      instance-1   value4
//
// The result will be:
//
//	frontend    instance-0   value1
//	                         value2
//	worker      instance-0   value3
//	            instance-1   value4
func kumoriTableMerge(groupedColums *[]int, table *[][]string) *[][]string {
	if groupedColums == nil || table == nil || len(*groupedColums) <= 0 || len(*table) <= 0 {
		return table
	}
	var lastRow []string
	for _, row := range *table {
		if lastRow == nil || len(lastRow) <= 0 {
			lastRow = make([]string, len(row))
			copy(lastRow, row)
			continue
		}
		same := true
		for j, col := range row {
			for _, m := range *groupedColums {
				if m == j {
					if (len(lastRow) > j) && (lastRow[j] != col) {
						same = false
						lastRow[j] = col
					} else {
						if same {
							row[j] = ""
						}
					}
				}
			}
		}
	}

	return table
}

// Hash returns a hashed version of a given string
func Hash(source string) string {
	hf := fnv.New32()
	hf.Write([]byte(source))
	return string(fmt.Sprintf("%08x", hf.Sum32()))
	// return rand.SafeEncodeString(fmt.Sprint(hf.Sum32()))
}
