/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// updateTCPInboundCmd represents the updateTCPInbound command
//
// Internally, this command is almost an alias to registerTCPInboundCmd,
// an the only difference is that the tcpinbound element must exist previously
var updateTCPInboundCmd = &cobra.Command{
	Use:     "tcp-inbound  <name>",
	Aliases: []string{"tcpinbound"},
	Short:   "Update a TCP Inbound deployment in the platform",
	// Long:    `Update a TCP Inbound deployment in the platform.`,
	Args: checkArgsUpdateTCPInbound,
	Run:  func(cmd *cobra.Command, args []string) { runUpdateTCPInbound(cmd, args) },
}

func init() {
	updateCmd.AddCommand(updateTCPInboundCmd)
	setRegisterTCPInboundFlags(updateTCPInboundCmd)

	updateTCPInboundCmd.Flags().Bool(
		"create-if-not-exists", false,
		"Create the TCP inbound if not exists instead of returning an error",
	)

}

func checkArgsUpdateTCPInbound(cmd *cobra.Command, args []string) error {
	return checkArgsRegisterTCPInbound(cmd, args)
}

func runUpdateTCPInbound(cmd *cobra.Command, args []string) {
	meth := "runUpdateTCPInbound"
	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	owner := userDomain

	createIfNotExists, err := cmd.Flags().GetBool("create-if-not-exists")
	if err != nil {
		logger.Fatal(err.Error())
	}
	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !exists && !createIfNotExists {
		err = errors.New("TCP inbound " + userDomain + "/" + name + " not found")
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Updating TCP inbound",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"output", output,
		"meth", meth,
	)

	// err = errors.New("Currently not implemented")
	// logger.Fatal(err.Error())
	applyTCPInbound(cmd, args, owner)

	if output != JSONOutput {
		fmt.Println("TCP inbound " + userDomain + "/" + name + " updated")
	}
}
