/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getDomainCmd represents the getDomain command
var getDomainCmd = &cobra.Command{
	Use:     "domain [<user>/<name>]",
	Aliases: []string{"domains", "dom", "doms"},
	Short:   "Get domain resource from platform",
	Long: `Get domain resource from platform
If <user>/<name> is not provided, all domains are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetDomain,
	Run:  func(cmd *cobra.Command, args []string) { runGetDomain(cmd, args) },
}

func init() {
	getCmd.AddCommand(getDomainCmd)
	getDomainCmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetDomain(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetDomain(cmd *cobra.Command, args []string) {
	meth := "runGetDomain"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	if len(args) == 0 {
		// Get domain list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting domain",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listDomains(onlyInUse, output)

	} else if len(args) == 1 {
		// Get domain manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting domain",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getDomain(userDomain, name, output)
	}

	return
}

func listDomains(onlyInUse bool, output string) {
	domains, err := admission.GetDomains(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {
		// If the data has not been set yet, then the domains are included directly
		// in the root part. If a Data object already exists, the domains is added
		// in the "domains" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing domains in JSON format", "error", err.Error())
		}
		for _, domain := range domains {
			data.ArrayAppend(domain.LongName, "domains")
		}

		if !dataExists {
			data = data.Search("domains")
		}
		response.SetData(data)

	} else {

		for _, domain := range domains {
			fmt.Println(domain.LongName)
		}
	}
	return
}

func getDomain(userDomain, name string, output string) {
	domain, err := admission.GetDomain(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(domain)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}

		} else {
			fmt.Println(domain)
		}
	}
	return
}
