/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// describeDomainCmd represents the describe Domain command
var describeDomainCmd = &cobra.Command{
	Use:     "domain <user>/<name>",
	Aliases: []string{"domains"},
	Short:   "Describe Domain on platform",
	Long: `Describe Domain on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribeDomain,
	Run:  func(cmd *cobra.Command, args []string) { runDescribeDomain(cmd, args) },
}

func init() {
	describeDomainCmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-domain")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describeDomainCmd)
}

func checkArgsDescribeDomain(cmd *cobra.Command, args []string) error {
	// Domain name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// Domain name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribeDomain(cmd *cobra.Command, args []string) {
	meth := "runDescribeDomain"

	userDomain := ""
	name := ""
	var err error

	// Calculates the user domain and name. If arguments are not provided, then
	// the user domain
	userDomain, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing Domain",
		"name", name,
		"userDomain", userDomain,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribeDomainJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribeDomainTable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribeDomainJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getDomainData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribeDomainTable(cmd *cobra.Command, args []string) {
	_, domainDetailsJSON, err := getDomainData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderDomain(domainDetailsJSON)
	fmt.Println("")
}

func getDomainData(fullName string) (string, *gabs.Container, error) {
	userDomain, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	domainManifest, domainJSON, err := admission.DescribeDomain(userDomain, name)
	if err != nil {
		return "", nil, err
	}
	if domainJSON == nil {
		return "", nil, fmt.Errorf("Domain not found")
	}

	return domainManifest, domainJSON, nil
}

func printHeaderDomain(json *gabs.Container) {

	domainName, _ := admission.DomainURNToName(json.Path("urn").Data().(string))

	fmt.Println(toBold("Domain"), ":", domainName)
	fmt.Println(toBold("Public"), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)
	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")
	fmt.Println(toBoldMaintainCase("Domain name"), ":", json.Path("description.domain").Data().(string))
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("Domain not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("Domain is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}
}

func getDomainNameAndDomain(urn string) (
	domain string,
	err error,
) {
	userDomain, name, err := admission.DomainDecomposeUrn(urn)
	domain = fmt.Sprintf("%s/%s", userDomain, name)
	return
}
