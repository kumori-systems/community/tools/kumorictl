/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/sergi/go-diff/diffmatchpatch"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// diffSolutionCmd represents the diff solution command
var diffSolutionCmd = &cobra.Command{
	Use:     "deployment <user>/<name>:<revision> [<user>/<name>:<revision>]...",
	Aliases: []string{"deployments"},
	Short:   "Shows differences between differents versions of a deployment",
	Long: `Calculates the differences between two deployments or between the workspace and a given deployment.
<user> can be ommited and defaults to the logged user.
<revision> can be omitted if the current revision is the one being compared.
The -d flag is required if the second deployment revision is not provided.
The -d flag is forbidden if the second deployment revision is provided.
If an external diff tool is used setting the -t flag, the error code returned by that tool is preserved.`,
	Args: checkArgsDiffSolution,
	Run:  func(cmd *cobra.Command, args []string) { runDiffSolution(cmd, args) },
}

var keysToDelete = []string{"urn", "name", "owner", "creationTimestamp", "lastModification"}

func init() {
	diffCmd.AddCommand(diffSolutionCmd)
	setDiffSolutionFlags(diffSolutionCmd)
}

func checkArgsDiffSolution(cmd *cobra.Command, args []string) error {

	if len(args) <= 0 {
		return errors.New("missing required argument")
	}

	if len(args) > 2 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	// revision can be omitted
	err := checkUserdDomainAndNameAndRevision(args[0])
	if err != nil {
		return err
	}

	if len(args) == 2 {
		// user-domain can be omitted when user-domain is set in configuration
		// revision can be omitted
		err := checkUserdDomainAndNameAndRevision(args[1])
		if err != nil {
			return err
		}
	}

	solution, err := cmd.Flags().GetString("deployment")
	if err != nil {
		return err
	}
	if len(args) == 2 && solution != "" {
		return errors.New("The -d flag is forbidden if two arguments are provided")
	} else if len(args) == 1 && solution == "" {
		return errors.New("The -d flag is required if one argument is provided")
	}

	_, err = cmd.Flags().GetString("package")
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("tool")
	if err != nil {
		return err
	}
	// if !util.ContainsString(validTools, tool) {
	// 	return errors.New("Invalid tool " + tool)
	// }
	return nil
}

func runDiffSolution(cmd *cobra.Command, args []string) {
	meth := "runDiffSolution"

	var sourceManifest, targetManifest *gabs.Container
	var err error
	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// The comparison is made with a solution manifets in the local workspace
	if len(args) == 1 {
		targetUserDomain, targetName, targetRevision, _ := getUserdDomainAndNameAndRevision(args[0], "")
		localSolution, _ := cmd.Flags().GetString("deployment")

		cuecmd := viper.Global.GetString("cue-command")
		if cuecmd == "" {
			logger.Fatal("CUE command not found")
		}

		allowJSONManifest, err := cmd.Flags().GetBool("allow-json-manifest")
		if err != nil {
			logger.Fatal(err.Error())
		}

		logger.Debug(
			"Getting deployments",
			"output", output,
			"meth", meth,
			"sourceWorkspace", localSolution,
			"targetName", targetName,
			"targetDomain", targetUserDomain,
			"targetRevision", targetRevision,
			"cueCommand", cuecmd,
			"allowJSONManifest", allowJSONManifest,
		)
		targetManifest, err = getSolutionManifest(targetUserDomain, targetName, targetRevision)
		if err != nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: %s", args[0], err.Error()))
		}
		if targetManifest == nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: the manifest is empty", args[0]))
		}

		owner := ""
		if targetManifest.Exists("owner") {
			owner = targetManifest.Search("owner").Data().(string)
		}

		comment := ""
		if targetManifest.Exists("comment") {
			comment = targetManifest.Search("comment").Data().(string)
		}
		deploymentPackage, _ := cmd.Flags().GetString("package")
		_, sourceManifest, err = generateSolutionManifest(
			targetUserDomain, targetName, owner, comment, localSolution, deploymentPackage,
			manifestsDir, allowJSONManifest, cuecmd,
		)
		// Two versions of registered services are being compared
	} else {
		// Get manifest
		sourceUserDomain, sourceName, sourceRevision, _ := getUserdDomainAndNameAndRevision(args[0], "")
		targetUserDomain, targetName, targetRevision, _ := getUserdDomainAndNameAndRevision(args[1], "")
		logger.Debug(
			"Getting deployments",
			"output", output,
			"meth", meth,
			"sourceName", sourceName,
			"sourceDomain", sourceUserDomain,
			"sourceRevision", sourceRevision,
			"targetName", targetName,
			"targetDomain", targetUserDomain,
			"targetRevision", targetRevision,
		)
		sourceManifest, err = getSolutionManifest(sourceUserDomain, sourceName, sourceRevision)
		if err != nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: %s", args[0], err.Error()))
		}
		if sourceManifest == nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: the manifest is empty", args[0]))
		}
		targetManifest, err = getSolutionManifest(targetUserDomain, targetName, targetRevision)
		if err != nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: %s", args[0], err.Error()))
		}
		if targetManifest == nil {
			logger.Fatal(fmt.Sprintf("Error getting deployment and revision %s: the manifest is empty", args[0]))
		}
	}

	tool, err := cmd.Flags().GetString("tool")
	if err != nil {
		logger.Fatal(fmt.Sprintf("Error getting tool: %s", err.Error()))
	}

	if tool == "" {
		tool = viper.Global.GetString("diff-tool")
	}

	compareManifests(sourceManifest, targetManifest, tool, output)
	return
}

func setDiffSolutionFlags(cmd *cobra.Command) {
	cmd.Flags().StringP(
		"deployment", "d", "",
		"path/to/module or git URI containing the CUE model",
	)

	cmd.Flags().StringP("package", "p", "deployment", "Deployment package")

	// diffTool := viper.Global.GetString("diff-tool")

	cmd.Flags().StringP(
		"tool", "t", "",
		"tool used to compare the deployment versions. If not specified, the default value set in diff-tool flag is used",
	)

	cmd.Flags().Bool(
		"allow-json-manifest", false,
		`(experimental) Allow to provide a pre-generated JSON manifest as deployment
input parameter (filename or - for stdin), instead of a path to CUE module`,
	)
}

func getSolutionManifest(
	userDomain string,
	name string,
	revision string,
) (manifest *gabs.Container, err error) {
	revisionInt := int(-1)
	if revision != "" {
		revisionInt, err = strconv.Atoi(revision)
		if err != nil {
			return nil, fmt.Errorf("The revison '%s' is not a valid revision: %s", revision, err.Error())
		}
	}
	manifest, err = getSolutionRevisionData(userDomain, name, revisionInt)
	if err != nil {
		return nil, err
	}

	return
}

func compareManifests(sourceManifest, targetManifest *gabs.Container, tool string, output string) error {
	elems := []*gabs.Container{sourceManifest, targetManifest}
	for _, elem := range elems {
		for _, apath := range keysToDelete {
			if elem.ExistsP(apath) {
				elem.DeleteP(apath)
			}
		}
	}

	switch tool {
	case "internal":
		compareUsingInternalDiff(sourceManifest, targetManifest, output)
	default:
		compareUsingExternalTool(sourceManifest, targetManifest, tool)
	}
	// manifest1 := sourceManifest.StringIndent("", "  ")
	// manifest2 := targetManifest.StringIndent("", "  ")
	// fmt.Printf("\n\n%s\n\n", manifest1)
	// fmt.Printf("\n\n%s\n\n", manifest2)

	return nil
}

func compareUsingExternalTool(sourceManifest, targetManifest *gabs.Container, tool string) {

	manifest1File := manifestsDir + "/Source.json"
	manifest2File := manifestsDir + "/Target.json"

	err := ioutil.WriteFile(manifest1File, sourceManifest.BytesIndent("", "  "), filePerm)
	if err != nil {
		logger.Fatal("Temporary source manifest file writing failed: " + err.Error())
		return
	}

	err = ioutil.WriteFile(manifest2File, targetManifest.BytesIndent("", "  "), filePerm)
	if err != nil {
		logger.Fatal("Temporary target manifest file writing failed: " + err.Error())
		return
	}

	parts := strings.Split(tool, " ")
	args := make([]string, 0, len(parts)+1)
	args = append(args, parts[1:]...)
	args = append(args, manifest1File, manifest2File)

	cmd := exec.Command(parts[0], args...)
	log.Printf("Running %s %s %s and waiting for it to finish...", tool, manifest1File, manifest2File)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {

		if _, ok := err.(*exec.ExitError); ok {
			panic(err)
			// os.Exit(exiterr.ExitCode())
		} else {
			logger.Fatal(fmt.Sprintf("Command finished with error: %v", err))
		}
	}

}

type Diff struct {
	Operation string `json:"operation"`
	Text      string `json:"text"`
}

func compareUsingInternalDiff(sourceManifest, targetManifest *gabs.Container, output string) {
	sourceJSON := sourceManifest.StringIndent("", "  ")
	targetJSON := targetManifest.StringIndent("", "  ")
	dmp := diffmatchpatch.New()
	diffs := dmp.DiffMain(sourceJSON, targetJSON, true)
	if output == JSONOutput {
		dataObj, err := gabs.New().Array()
		if err != nil {
			logger.Fatal("Cannot create JSON response data", "error", err.Error())
		}

		for _, diff := range diffs {
			diffobj := Diff{
				Text: diff.Text,
			}
			switch diff.Type {
			case diffmatchpatch.DiffDelete:
				diffobj.Operation = "delete"
			case diffmatchpatch.DiffInsert:
				diffobj.Operation = "insert"
			case diffmatchpatch.DiffEqual:
				diffobj.Operation = "equal"
			default:
				diffobj.Operation = "unknown"
			}
			dataObj.ArrayAppend(diffobj)
		}
		response.SetData(dataObj)

	} else {
		prettyDiff := dmp.DiffPrettyText(diffs)
		if prettyDiff != sourceJSON {
			fmt.Println(prettyDiff)
		} else {
			fmt.Println("No changes")
		}
	}
}
