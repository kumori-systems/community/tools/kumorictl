/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// unregisterDomainCmd represents the unregisterDomain command
var unregisterDomainCmd = &cobra.Command{
	Use:     "domain <user>/name>",
	Aliases: []string{"domains", "dom", "doms"},
	Short:   "Unregister existing domain from platform",
	Long: `Unregister existing domain from platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsUnregisterDomain,
	Run:  func(cmd *cobra.Command, args []string) { runUnregisterDomain(cmd, args) },
}

func init() {
	unregisterCmd.AddCommand(unregisterDomainCmd)
}

func checkArgsUnregisterDomain(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}
	return nil
}

func runUnregisterDomain(cmd *cobra.Command, args []string) {
	meth := "runUnregisterDomain"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	wait, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Unregistering domain",
		"name", name,
		"userDomain", userDomain,
		"wait", wait,
		"output", output,
		"meth", meth,
	)

	err = admission.DeleteDomain(userDomain, name, wait)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Resource domain " + userDomain + "/" + name + " deleted")
	}
}
