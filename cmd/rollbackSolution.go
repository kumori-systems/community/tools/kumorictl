/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// rollbackSolutionCmd represents the rollbackSolution command
var rollbackSolutionCmd = &cobra.Command{
	Use:   "deployment <name>",
	Short: "Roll back to a previous version a deployment in the platform.",
	// Long:  `Roll back to a previous version a deployment in the platform.`,
	Args: checkArgsRollbackSolution,
	Run:  func(cmd *cobra.Command, args []string) { runRollbackSolutionCmd(cmd, args) },
}

func init() {
	rollbackCmd.AddCommand(rollbackSolutionCmd)

	rollbackSolutionCmd.Flags().IntP(
		"revision", "r", 1,
		"revision number of the deployment to roll back to (default: last version)",
	)

	rollbackSolutionCmd.Flags().StringP(
		"comment", "m", "",
		"message describing the rollback action",
	)

	rollbackSolutionCmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time for the service to be ready before returning",
	)
}

func checkArgsRollbackSolution(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

// getSolutionRevisionData returns information of a given revision of a given solution.
// If the revision parameter is set to -1, the last revision is returned
func getSolutionRevisionData(userDomain string, name string, revision int) (manifest *gabs.Container, err error) {

	history := calculateSolutionRevisionHistory(userDomain, name)

	realRevision := history.CurrentCommit
	if revision >= 0 {

		// Figures out the revision number expected by admission, which is a commit number,
		// form the revision number provided by the user
		realRevision = -1
		for _, commit := range history.Commits {
			itemRevision, _ := strconv.Atoi(commit.Revision)
			itemCommit := commit.Commit
			if itemRevision == revision {
				// This will return the last commit for the selected revision
				realRevision = itemCommit
			}
		}
	}

	if realRevision == -1 {
		return nil, fmt.Errorf("Revision %d not found", revision)
	}

	manifest, err = admission.GetSolutionRevisionManifest(userDomain, name, realRevision)
	if err != nil {
		return nil, err
	}
	return
}

func runRollbackSolutionCmd(cmd *cobra.Command, args []string) {
	meth := "runRollbackSolutionCmd()"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	revision, err := cmd.Flags().GetInt("revision")
	if err != nil {
		logger.Fatal(err.Error())
	}

	comment, err := cmd.Flags().GetString("comment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Rollback deployment",
		"name", name,
		"userDomain", userDomain,
		"revision", revision,
		"comment", comment,
		"waitDuration", waitDuration,
		"output", output,
		"meth", meth,
	)

	manifestJSON, err := getSolutionRevisionData(userDomain, name, revision)

	if err != nil {
		logger.Fatal(err.Error())
	}
	if manifestJSON == nil {
		logger.Error("Revision not found", "meth", meth)
	}

	manifestFile := manifestsDir + "/Manifest.json"

	// Update the comment in the manifest
	manifestJSON.Set(comment, "comment")

	// Create the manifest file
	manifestStr := manifestJSON.StringIndent("", "  ")
	err = ioutil.WriteFile(manifestFile, []byte(manifestStr), filePerm)
	if err != nil {
		logger.Fatal("Temporary manifest file writing failed: " + err.Error())
	}

	err = admission.RollbackSolution(manifestsDir, tmpDir)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if waitDuration != util.ZERO_DURATION {
		// In an "rollback" operation, the service is already "running".
		// Therefore, we wait a while for the changes to begin to take place.
		time.Sleep(20 * time.Second)
		err = admission.WaitSolutionToBeReady(userDomain, name, waitDuration)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}
}
