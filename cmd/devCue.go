/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"os"
	"os/exec"
	"syscall"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// devCueCmd represents the dev cue command
var devCueCmd = &cobra.Command{
	Use:   "cue",
	Short: "Wrapper for CUE with extended features",
	Long: `Wrapper for CUE with extended features.

Calls CUE binary with the especified parameters, acting as a CUE wrapper, except for the available subcommands described below, which provide custom features oriented to Kumori elements development.`,
	ValidArgs: []string{"process", "cmd", "def", "eval", "export", "fix", "fmt", "get", "help", "import", "mod", "trim", "version", "vet"},
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("devCue called")
		env := os.Environ()
		cueBinary, err := exec.LookPath("cue")
		if err != nil {
			logger.Fatal("Getting CUE path: " + err.Error())
		}
		err = syscall.Exec(cueBinary, append([]string{"cue"}, args...), env)
		if err != nil {
			logger.Fatal("Executing CUE: " + err.Error())
		}
	},
}

func init() {
	devCmd.AddCommand(devCueCmd)
	devCueCmd.Flags().SetInterspersed(false)
}
