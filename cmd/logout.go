/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// logoutCmd represents the logout command
var logoutCmd = &cobra.Command{
	Use:   "logout",
	Short: "Logout from Kumori Platform",
	Long: `Logout from Kumori Platform.

This command invalidates and removes tokens from config file.`,
	Args: cobra.NoArgs,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
		CheckConfigFile(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) { runLogout(cmd, args) },
}

func init() {
	rootCmd.AddCommand(logoutCmd)
}

func runLogout(cmd *cobra.Command, args []string) {
	meth := "runLogout"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Login",
		"output", output,
		"meth", meth,
	)

	viper.Global.Set("access-token", "")
	viper.Global.Set("access-token-expiry-date", "")
	viper.Global.Set("refresh-token", "")
	viper.Global.Set("refresh-token-expiry-date", "")
	viper.Global.Set("user-domain", "anonymous")
	err = viper.Global.WriteConfig()
	if err != nil {
		logger.Fatal("Workspace initialization is mandatory to use this command. " + err.Error())
	}
	if output != JSONOutput {
		fmt.Println("Logged out.")
	}
}
