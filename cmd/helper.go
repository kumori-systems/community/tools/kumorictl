/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/viper"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
)

// getDefaultUserDomain returns the default user domain. It depends on the authentication method:
//   - Token based: the default user domain is the logged user, which is stored in "user-domain" configuration parameter
//   - MTLS based: the default user domain is the first section of the less significative part of the client certificate CN.
//     The client certificate subject CN has the following format: <user>.<axebow-id>.<base-domain> (example: juanjo.axebow1.axebow.com).
//   - None: the default user-domain is "anonymous", which is stored in "user-domain" configuration parameter
func getDefaultUserDomain() (defaultUserDomain string, err error) {
	authMethod := admission.GetAuthMethod()

	// If a token-based authentication method is required by admission, the default user domain is stored in "user-domain"
	// configuration parameter
	if authMethod == admission.TokenAuthMethod {
		return viper.Global.GetString("user-domain"), nil
	}

	// If a MTLS-based authentication method is required by admission, the default user domain is stored in the client certificate.
	// More precisely, is stored in the less significative part of the client certificate CN. The subject CN has the following format:
	// <user>.<axebow-id>.<base-domain>
	if authMethod == admission.MTLSAuthMethod {

		clientCert, _ := admission.GetClientCert()

		// Gets the subject CN
		subjectCN, err := x509Validator.GetCertificateSubjectCN(string(clientCert))
		if err != nil {
			return "", err
		}

		// The subject CN must follow the format <user>.<axebow-id>.<base-domain>. We are interested in the <user> part, which is also
		// the user domain
		subjectCNParts := strings.Split(subjectCN, ".")
		return subjectCNParts[0], nil
	}

	if authMethod == admission.NoneAuthMethod {
		// Will be "anonymous"
		return viper.Global.GetString("user-domain"), nil
	}

	return "", fmt.Errorf("unknown authentication method '%s'", authMethod)
}

func checkUserDomainAndName(param string) error {
	if param == "" {
		return errors.New("required parameter [domain/]name not provided")
	}

	// Valid formats:
	// - name
	// - domain/name
	parts := strings.Split(param, "/")
	if (len(parts) > 2) || (len(parts) < 1) {
		return errors.New("wrong format in required parameter [domain/]name")
	}

	return nil
}

func checkUserDomainAndNameAndChannel(param string) error {
	if param == "" {
		return errors.New("required parameter [domain/]name[:channel] not provided")
	}

	var name string

	// Valid formats:
	// - name
	// - domain/name
	// - name:channel
	// - domain/name:channel
	parts := strings.Split(param, ":")
	if len(parts) > 2 {
		return errors.New("wrong format in required parameter [domain/]name[:channel]")
	}
	name = parts[0]
	parts = strings.Split(name, "/")
	if (len(parts) > 2) || (len(parts) < 1) {
		return errors.New("wrong format in required parameter [domain/]name[:channel]")
	}

	return nil
}

func getChannel(param string) (channel string, err error) {
	parts := strings.Split(param, ":")
	if len(parts) > 2 {
		return "", errors.New("wrong format in required parameter [domain/]name[:channel]")
	}

	return parts[0], nil
}

func getUserDomainAndName(param, owner string) (userDomain, name string, err error) {

	// If an owner is set using --owner flag, the userDomain is that owner by default
	// instead of the user-domain configuration parameter
	var defaultUserDomain string
	if owner != "" {
		defaultUserDomain = owner
	} else {
		defaultUserDomain, err = getDefaultUserDomain()
		if err != nil {
			return "", "", err
		}
	}

	// If param is "userDomain/name", then returns "userDomain" and "name"
	// If param is "name", then the default user domain is set
	// Otherwise returns error
	if param != "" {
		parts := strings.Split(param, "/")
		if len(parts) == 2 {
			userDomain = parts[0]
			name = parts[1]
		} else if len(parts) == 1 {
			name = parts[0]
			userDomain = defaultUserDomain
		}
	}

	if name == "" && userDomain == "" {
		err = errors.New("required parameters user-domain/name not provided")
	} else if name == "" {
		err = errors.New("required parameter name not provided")
	} else if userDomain == "" {
		err = errors.New("required parameter user-domain not provided and is not set in configuration")
	}
	return
}

func getUserDomainAndNameAndChannel(param, owner string) (userDomain, name, channel string, err error) {
	// If param is "userDomain/name:channel", then returns "userDomain" and "name" and "channel"
	// If param is "name:channel", then "userDomain" must be setted in the configuration
	// Channel can be nul (httpinbound deployments)
	// Otherwise returns error

	//
	// TBD : use regular expressions
	//

	if param != "" {

		userDomain = ""
		name = ""
		channel = ""
		userDomainAndName := ""

		parts := strings.Split(param, ":")
		if len(parts) == 1 {
			userDomainAndName = parts[0]
		} else if len(parts) == 2 {
			userDomainAndName = parts[0]
			channel = parts[1]
		}

		userDomain, name, err = getUserDomainAndName(userDomainAndName, owner)
	}

	if name == "" || userDomain == "" {
		err = errors.New("required parameter in the form user-domain/name[:channel] not provided")
	}
	return
}

func getUserdDomainAndNameAndRevision(param, owner string) (userDomain, name, revision string, err error) {

	if param != "" {

		userDomain = ""
		name = ""
		revision = ""
		userDomainAndName := ""

		parts := strings.Split(param, ":")
		if len(parts) == 1 {
			userDomainAndName = parts[0]
		} else if len(parts) == 2 {
			userDomainAndName = parts[0]
			revision = parts[1]
		}

		userDomain, name, err = getUserDomainAndName(userDomainAndName, owner)
	}

	if name == "" || userDomain == "" {
		err = errors.New("required parameter in the form user-domain/name[:revision] not provided")
	}
	return
}

func checkUserdDomainAndNameAndRevision(param string) error {

	if param != "" {
		userDomainAndName := ""

		parts := strings.Split(param, ":")
		if len(parts) == 1 {
			userDomainAndName = parts[0]
		} else if len(parts) == 2 {
			userDomainAndName = parts[0]
		} else {
			return errors.New("required parameter in the form user-domain/name[:revision] not provided")
		}

		err := checkUserDomainAndName(userDomainAndName)
		if err != nil {
			return err
		}
	}
	return nil
}
