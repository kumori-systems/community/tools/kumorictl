/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// updatePasswordCmd represents the updatePassword command
var updatePasswordCmd = &cobra.Command{
	Use:   "password <user>",
	Short: "Change user password",
	Long: `Change user password

This command interactively asks for a password. Only administrators can change the password of other users.
If <user> is not provided, current user is used.`,
	Args: RangeArgs(0, 1),
	Run:  func(cmd *cobra.Command, args []string) { runUpdatePassword(cmd, args) },
}

func init() {
	updateCmd.AddCommand(updatePasswordCmd)
}

func runUpdatePassword(cmd *cobra.Command, args []string) {
	meth := "runUpdateDeployment"
	user, err := getUser(args)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Updating password",
		"user", user,
		"output", output,
		"meth", meth,
	)

	oldPassword, err := util.GetPasswd("Enter current password: ", false)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if oldPassword == "" {
		logger.Fatal("Password can not be empty")
	}

	newPassword, err := util.GetPasswd("Enter new password: ", false)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if newPassword == "" {
		logger.Fatal("Password can not be empty")
	}
	if newPassword == oldPassword {
		logger.Fatal("The new and current passwords must be different")
	}

	retypeNewPassword, err := util.GetPasswd("Retype new password: ", false)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if newPassword != retypeNewPassword {
		logger.Fatal("Retyped pasword does not match new password")
	}

	err = admission.ChangePassword(user, oldPassword, newPassword)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Password updated")
	}

}

func getUser(args []string) (user string, err error) {
	if len(args) > 0 {
		user = args[0]
		return
	}
	token := viper.Global.Get("access-token").(string)
	if token != "" {
		user, err = util.GetUserFromToken(token)
	}
	return
}
