/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getCertificateCmd represents the getCertificate command
var getCertificateCmd = &cobra.Command{
	Use:     "certificate [<user>/<name>]",
	Aliases: []string{"cert", "certs", "certificates"},
	Short:   "Get certificate resource from platform",
	Long: `Get certificate resource from platform
If <user>/<name> is not provided, all certificaters are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetCertificate,
	Run:  func(cmd *cobra.Command, args []string) { runGetCertificate(cmd, args) },
}

func init() {
	getCmd.AddCommand(getCertificateCmd)
	getCertificateCmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetCertificate(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetCertificate(cmd *cobra.Command, args []string) {
	meth := "runGetCertificate"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	if len(args) == 0 {
		// Get certificate list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting certificate",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listCertificates(onlyInUse, output)

	} else if len(args) == 1 {
		// Get certificate manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting certificate",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getCertificate(userDomain, name, output)
	}

	return
}

func listCertificates(onlyInUse bool, output string) {
	certs, err := admission.GetCertificates(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {
		// If the data has not been set yet, then the certificates are included directly
		// in the root part. If a Data object already exists, the certificates are added
		// in the "certificates" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing certificates in JSON format", "error", err.Error())
		}
		for _, cert := range certs {
			data.ArrayAppend(cert.LongName, "certificates")
		}

		if !dataExists {
			data = data.Search("certificates")
		}
		response.SetData(data)

	} else {
		for _, cert := range certs {
			fmt.Println(cert.LongName)
		}
	}
	return
}

func getCertificate(userDomain, name string, output string) {
	certificate, err := admission.GetCertificate(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(certificate)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}

		} else {
			fmt.Println(certificate)
		}
	}
	return
}
