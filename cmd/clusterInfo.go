/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// clusterInfo represents the getCA command
var clusterInfoCmd = &cobra.Command{
	Use:     "cluster-info",
	Aliases: []string{"cluster-info"},
	Short:   "Show cluster information",
	Args:    cobra.NoArgs,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
		SetAuthMethod(cmd)
		SetCertificates(cmd)
		// SetAdmissionURL(cmd) : This function can't be used, because "clusterinfo"
		// command allow use a different Admission url. So "runClusterInfo" function
		// must perform a similar work as "SetAdmissionURL" function.
	},
	Run: func(cmd *cobra.Command, args []string) { runClusterInfo(cmd, args) },
}

func init() {
	rootCmd.AddCommand(clusterInfoCmd)

	clusterInfoCmd.Flags().StringP(
		"admission-url", "a", "",
		"Admission URL",
	)
}

func runClusterInfo(cmd *cobra.Command, args []string) {
	meth := "runClusterInfo"

	output, _ := cmd.Flags().GetString("output")

	admissionURL, err := cmd.Flags().GetString("admission-url")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Cluster info",
		"admission-url", admissionURL,
		"output", output,
		"meth", meth,
	)

	// If Admission URL was provided as param, use it and ignore any previously configured one
	if admissionURL == "" {
		admissionURL = viper.Global.GetString("admission-protocol") + "://" + viper.Global.GetString("admission")
	}
	admission.SetAdmissionURL(admissionURL)

	// Set admission-timeout
	err = admission.SetAdmissionTimeout(admissionTimeout)
	if err != nil {
		logger.Fatal("Error setting Admission timeout: %s", err.Error())
	}

	admission.SetInsecure(insecure)

	err = admission.CheckAdmission()
	if err != nil {
		// If clientcertificate authentication is used, admission service requires a client certificate.
		if output != JSONOutput {
			fmt.Println()
			fmt.Println("Unable to contact Admission server. Error: " + err.Error())
			fmt.Println("Please, make sure you have properly configured Admission domain and protocol.")
		}
		logger.Fatal("Unable to contact Admission server. Error: " + err.Error())

	}

	// Get cluster config
	clusterConfig, err := admission.GetClusterConfig()
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output == JSONOutput {
		dataObj := gabs.New()
		dataObj.Set(clusterConfig.ClusterVersion, "version")
		dataObj.Set(clusterConfig.ClusterName, "name")
		dataObj.Set(clusterConfig.ReferenceDomain, "domain")
		dataObj.Set(clusterConfig.IngressDomain, "ingress", "domain")
		dataObj.Set(clusterConfig.AdmissionDomain, "admission", "domain")
		for _, version := range clusterConfig.KumorictlSupportedVersions {
			dataObj.ArrayAppend(version, "kumorictl", "versions")
		}
		for volName, volType := range *clusterConfig.VolumeTypes {
			dataObj.Set(volType.Properties, "volumeTypes", volName, "properties")
		}
		response.SetData(dataObj)
	} else {
		fmt.Println(toBold("Cluster version:"), clusterConfig.ClusterVersion)
		fmt.Println(toBold("Cluster name:"), clusterConfig.ClusterName)
		fmt.Println(toBold("Reference domain:"), clusterConfig.ReferenceDomain)
		fmt.Println(toBold("Ingress domain:"), clusterConfig.IngressDomain)
		fmt.Println(toBold("Admission domain:"), clusterConfig.AdmissionDomain)
		fmt.Println(toBold("KumoriCtl supported versions:"), clusterConfig.KumorictlSupportedVersions)
		fmt.Println(toBold("VolumeTypes:"))
		if clusterConfig.VolumeTypes == nil {
			fmt.Println("   None")
		} else {
			for volName, volType := range *clusterConfig.VolumeTypes {
				fmt.Println(" - " + toBoldMaintainCase(volName))
				fmt.Println("   Properties: " + volType.Properties)
			}
		}
	}
}
