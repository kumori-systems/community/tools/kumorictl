/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

const timeFormat = "2006.01.02 15:04:05"

// RevisionCommit contains information about a given revision commit
type RevisionCommit struct {
	Revision  string    `json:"revision"`
	Commit    int       `json:"commit"`
	Timestamp time.Time `json:"timestamp"`
	Comment   string    `json:"comment"`
}

// SolutionRevisionHistory contains information about a given solution
// revisions
type SolutionRevisionHistory struct {
	CurrentRevision string           `json:"currentRevison"`
	CurrentCommit   int              `json:"currentCommit"`
	Commits         []RevisionCommit `json:"commits"`
}

// historySolutionCmdCmd represents the historySolutionCmd command
var historySolutionCmdCmd = &cobra.Command{
	Use:   "deployment <user>/<name>",
	Short: "Show the history of a deployment in the platform",
	Long: `Show the history of a deployment in the platform.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsHistorySolution,
	Run:  func(cmd *cobra.Command, args []string) { runHistorySolutionCmd(cmd, args) },
}

func init() {
	historyCmd.AddCommand(historySolutionCmdCmd)
}

func checkArgsHistorySolution(cmd *cobra.Command, args []string) error {
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// Checks the output format is a valid value
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(historyValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runHistorySolutionCmd(cmd *cobra.Command, args []string) {
	meth := "runHistorySolutionCmd()"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"History deployment",
		"name", name,
		"userDomain", userDomain,
		"format", format,
		"meth", meth,
	)

	history := calculateSolutionRevisionHistory(userDomain, name)

	if format == "json" {
		jsonPrintRevisionHistoryAsJsonfunc(history)
	} else {
		tablePrintRevisionHistory(history, userDomain+"/"+name)
	}

}

func jsonPrintRevisionHistoryAsJsonfunc(history *SolutionRevisionHistory) {
	if history != nil {
		data := gabs.New()
		data.Set(history)
		response.SetData(data)
	}
}

// +------------+---------------------+--------------------------------+
// | Revision   | Timestamp           | Comment                        |
// +------------+---------------------+--------------------------------+
// | * 1        | 2020.11.19 07:37:58 |                                |
// |            | 2020.11.19 07:45:19 | Back to initial value          |
// |   2        | 2020.11.19 07:42:06 | Changed environment variable   |
// |   3        | 2020.11.19 07:43:56 | Changed environment variable   |
// |            |                     | again                          |
// +------------+---------------------+--------------------------------+
func tablePrintRevisionHistory(history *SolutionRevisionHistory, depName string) {
	revisionsTable := tablewriter.NewWriter(os.Stdout)
	revisionsTable.SetHeader([]string{"Revision", "Timestamp", "Comment"})

	fmt.Println(toBold("Deployment"), ":", depName)
	fmt.Println(toBold("Revision history :"))

	// revisionsTable.AppendBulk(revisionList)
	revisionsTable.SetAutoFormatHeaders(false)
	revisionsTable.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	revisionsTable.SetAlignment(tablewriter.ALIGN_LEFT)
	revisionsTable.SetCenterSeparator("+")
	revisionsTable.SetColumnSeparator("|")
	revisionsTable.SetRowSeparator("-")
	revisionsTable.SetHeaderLine(true)
	revisionsTable.SetBorder(true)
	revisionsTable.SetNoWhiteSpace(false)
	revisionsTable.SetAutoMergeCellsByColumnIndex([]int{0, 1})

	for _, commit := range history.Commits {
		row := []string{
			fmt.Sprintf("  %s", commit.Revision),
			commit.Timestamp.Format(timeFormat),
			commit.Comment,
		}
		if commit.Revision == history.CurrentRevision {
			row[0] = fmt.Sprintf("* %s", commit.Revision)
			currentColor := tablewriter.Colors{tablewriter.Normal, tablewriter.FgHiGreenColor}
			defaultColor := tablewriter.Colors{}
			// item[0] = fmt.Sprintf("%s (*)", item[0])
			if commit.Commit == history.CurrentCommit {
				revisionsTable.Rich(row, []tablewriter.Colors{currentColor, currentColor, currentColor})
				continue
			}
			revisionsTable.Rich(row, []tablewriter.Colors{currentColor, defaultColor, defaultColor})
			continue
		}
		revisionsTable.Append(row)
	}
	revisionsTable.Render()
}

func calculateSolutionRevisionHistory(
	userDomain string,
	name string,
) *SolutionRevisionHistory {

	meth := "runHistorySolutionCmd()"
	revisionListJSON, err := admission.GetSolutionRevisionList(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if revisionListJSON == nil {
		logger.Error("Revision list not found", "meth", meth)
	}

	revisionList := []RevisionCommit{}
	var currentRevision string
	var currentCommit int

	for _, item := range revisionListJSON.Children() {
		// Which fields are used for each table column:
		// - Revision: the `name` field
		// - Commit: the `revision` field
		// - Comment: the `comment` field
		// - Timestamp: the `timestamp` field
		revision := item.Path("name").Data().(string)
		comment := item.Path("comment").Data().(string)
		commit := int(item.Path("revision").Data().(float64))
		timestamp := int64(item.Path("timestamp").Data().(float64))
		row := RevisionCommit{
			Revision:  revision,
			Commit:    commit,
			Timestamp: time.Unix(timestamp, 0),
			Comment:   comment,
		}
		revisionList = append(revisionList, row)
	}

	// Calculate each revision timestamp and the current revison and commit.
	// - Revision timestamp: the moment this revision was created, which is the
	//   timestamp of this revision first commit (older timestamp)
	// - Current commit: the commit with the greater value.
	// - Current revision: the revision containing the current commit.
	timestampPerRevision := map[string]time.Time{}
	currentCommit = -1
	for _, commit := range revisionList {
		if _, ok := timestampPerRevision[commit.Revision]; ok {
			if commit.Timestamp.Before(timestampPerRevision[commit.Revision]) {
				timestampPerRevision[commit.Revision] = commit.Timestamp
			}
		} else {
			timestampPerRevision[commit.Revision] = commit.Timestamp
		}
		if currentCommit < commit.Commit {
			currentCommit = commit.Commit
			currentRevision = commit.Revision
		}
	}

	// Admission doesn't return an ordered list. The items are ordered per
	// revision (using its timestamp) and, inside a revision, per commit.
	sort.Slice(
		revisionList,
		func(i, j int) bool {
			revisioni := revisionList[i].Revision
			commiti := revisionList[i].Commit
			timestampi, _ := timestampPerRevision[revisioni]
			revisionj := revisionList[j].Revision
			commitj := revisionList[j].Commit
			timestampj, _ := timestampPerRevision[revisionj]

			if revisioni == revisionj {
				return commiti < commitj
			}

			return timestampi.Before(timestampj)

			// Comparison before ticket261
			// revi, _ := strconv.Atoi(revisionList[i][0])
			// revj, _ := strconv.Atoi(revisionList[j][0])
			// return revi < revj
		},
	)

	lastRevisionName := ""
	index := 1
	for i, commit := range revisionList {
		if lastRevisionName != "" && lastRevisionName != commit.Revision {
			index++
		}
		lastRevisionName = commit.Revision
		commit.Revision = strconv.Itoa(index)
		if lastRevisionName == currentRevision {
			currentRevision = commit.Revision
		}

		revisionList[i] = commit
	}

	history := SolutionRevisionHistory{
		CurrentRevision: currentRevision,
		CurrentCommit:   currentCommit,
		Commits:         revisionList,
	}

	return &history
}
