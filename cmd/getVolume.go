/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getVolumeCmd represents the getVolume command
var getVolumeCmd = &cobra.Command{
	Use:     "volume [<user>/<name>]",
	Aliases: []string{"volumes"},
	Short:   "Get volume resource from platform",
	Long: `Get volume resource from platform
If <user>/<name> is not provided, all volumes are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetVolume,
	Run:  func(cmd *cobra.Command, args []string) { runGetVolume(cmd, args) },
}

func init() {
	getCmd.AddCommand(getVolumeCmd)
	getVolumeCmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetVolume(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetVolume(cmd *cobra.Command, args []string) {
	meth := "runGetVolume"

	output, _ := cmd.Flags().GetString("output")
	if len(args) == 0 {
		// Get volume list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}

		logger.Debug(
			"Getting volume",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listVolumes(onlyInUse, output)

	} else if len(args) == 1 {
		// Get volume manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting volume",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getVolume(userDomain, name, output)
	}

	return
}

func listVolumes(onlyInUse bool, output string) {
	volumes, err := admission.GetVolumes(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}

	if output == JSONOutput {
		// If the data has not been set yet, then the volumes are included directly
		// in the root part. If a Data object already exists, the volumes are added
		// in the "volumes" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing volumes in JSON format", "error", err.Error())
		}
		for _, volume := range volumes {
			data.ArrayAppend(volume.LongName, "volumes")
		}

		if !dataExists {
			data = data.Search("volumes")
		}
		response.SetData(data)

	} else {
		for _, volume := range volumes {
			fmt.Println(volume.LongName)
		}
	}
	return
}

func getVolume(userDomain, name string, output string) {
	volume, err := admission.GetVolume(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(volume)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}
		} else {

			fmt.Println(volume)
		}
	}
	return
}
