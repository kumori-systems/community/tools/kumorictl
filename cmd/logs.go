/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// logsCmd represents the logs command
var logsCmd = &cobra.Command{
	Use:   "logs <user>/<deployment> <role> <instance> [container]",
	Short: "Fetch the logs of an instance container",
	Long: `Fetch the logs of an instance container.
Specifying a container is optional unless the instance has several containers.`,
	Args: checkArgsLogs,
	Run:  func(cmd *cobra.Command, args []string) { runLogs(cmd, args) },
}

func init() {
	rootCmd.AddCommand(logsCmd)
	logsCmd.Flags().BoolP(
		"follow", "f", false,
		"continue printing logs as they keep coming",
	)
	logsCmd.Flags().DurationP(
		"since", "s", 0,
		"omit logs older than duration (e.g. 1s, 1m, 1h, 1h1m1s)",
	)
	logsCmd.Flags().UintP(
		"tail", "t", 0,
		"output last uint log lines",
	)
	logsCmd.Flags().BoolP(
		"print-date", "d", false,
		"prefix log lines with local print date (requires follow)",
	)
	logsCmd.Flags().BoolP(
		"previous", "p", false,
		"print logs of the previous instance of the container, if it exists",
	)
}

func checkArgsLogs(cmd *cobra.Command, args []string) error {

	err := cobra.RangeArgs(3, 4)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	return nil
}

func runLogs(cmd *cobra.Command, args []string) {
	meth := "runLogs"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}
	role := args[1]
	instance := args[2]
	container := ""
	if len(args) > 3 {
		container = args[3]
	}

	follow, _ := cmd.Flags().GetBool("follow")
	since, _ := cmd.Flags().GetDuration("since")
	sinceSeconds := int(since.Truncate(time.Second).Seconds())
	tail, _ := cmd.Flags().GetUint("tail")
	printDate, _ := cmd.Flags().GetBool("print-date")
	previous, _ := cmd.Flags().GetBool("previous")
	if printDate && !follow {
		logger.Fatal("-d / --print-date flag must be used along with -f / --follow flag")
	}

	logger.Debug(
		"Getting log",
		"name", name,
		"userDomain", userDomain,
		"role", role,
		"instance", instance,
		"container", container,
		"follow", follow,
		"since", uint64(since),
		"sinceSeconds", sinceSeconds,
		"tail", tail,
		"printDate", printDate,
		"previous", previous,
		"meth", meth,
	)

	kubeInstance := strings.ReplaceAll(instance, "instance-", "")

	admissionStream, err := admission.ContainerLogsStream(
		userDomain, name, role, kubeInstance, container, follow, sinceSeconds, tail, previous,
	)
	if err != nil {
		logger.Fatal(err.Error())
	}

	defer admissionStream.Close()
	reader := bufio.NewReader(admissionStream)
	log.SetOutput(os.Stdout)
	firstLine := true
	for {
		line, err := reader.ReadBytes('\n')

		if firstLine {
			firstLine = false
			isControl, cmerr := checkControlMessage(line)
			// If first line is an error control message, log it and exit
			if cmerr != nil {
				logger.Fatal(cmerr.Error())
			}
			// If first line is a non-error control message, skip it and continue reading the stream
			if isControl {
				continue
			}
		}

		if printDate {
			log.Print(string(line))
		} else {
			fmt.Print(string(line))
		}
		if err != nil {
			if err.Error() == "EOF" {
				break
			}
			logger.Fatal(err.Error())
		}
	}
}

func checkControlMessage(line []byte) (bool, error) {
	lineJSONParsed, err := gabs.ParseJSON(line)
	if err != nil {
		// Not an Admission response
		return false, nil
	}
	if lineJSONParsed.Exists("success") && lineJSONParsed.Exists("message") {
		// It's most likely an Admission error response
		success := lineJSONParsed.Path("success").Data().(bool)
		message := lineJSONParsed.Path("message").Data().(string)
		if !success {
			err = errors.New("Error reading logs: " + message)
			return true, err
		}
	} else {
		// Not an Admission response
		return false, nil
	}
	return false, nil
}
