/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

var historyValidFormats = []string{"table", "json"}

// historyCmd represents the historyCmd command
// Is just a container for several subcommands
var historyCmd = &cobra.Command{
	Use:   "history",
	Short: "Show the history of an element in the platform",
	// History command is executed only if an invalid subcommand is provided or a subcommand is not
	// provided at all. Hence, it will fail either asking for a subcommand (if none is included)
	// or indicating that the provided subcommand is invalid.
	Args: cobra.MatchAll(cobra.NoArgs, cobra.MinimumNArgs(1)),
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// logger.Debug("Root.PersistentPreRun - perform common validations")
		InitLog(cmd)
		InitResponse(cmd)
		CheckConfigFile(cmd)
		SetAuthMethod(cmd)
		SetCertificates(cmd)
		SetAdmissionURL(cmd)
		// The following two lines order has been switched due to backward compatible issues.
		// In older clusters, the "clusterconfig" call requires a refreshed token. In v1.0.1 clusters
		// this call can be executed by anonymous and authentication users. The later means that
		// a refreshed token is attached. The "clusterconfig" call is performed during the
		// "CheckClusterCompatibility". If this function is executed before getting a valid token
		// ("RefreshAdmissionCredentials" function), hence the "clusterconfig" call is executed always
		// anonymously. This is fine for v1.0.1 clusters but fails in older clusters. To keep
		// backwards compatibility, we have switched the execution order for this two functions in
		// root, describe and history commands (see ticket 896).
		RefreshAdmissionCredentials(cmd)
		CheckClusterCompatibility(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) {},
}

func init() {
	rootCmd.AddCommand(historyCmd)

	historyCmd.PersistentFlags().StringP(
		"output", "o", "table",
		"output format: table (default), json",
	)
	viper.Global.BindPFlag("output", historyCmd.PersistentFlags().Lookup("output"))

}
