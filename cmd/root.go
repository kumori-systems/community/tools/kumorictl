/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"gitlab.com/kumori/kumorictl/pkg/viper"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"

	homedir "github.com/mitchellh/go-homedir"
)

const tmpDirBase = "./.tmp/"

var cfgFile string
var keepTmpFiles bool
var admissionURL string
var admissionTimeout string
var tmpDir string
var manifestsDir string
var insecure bool
var configFileLoaded bool
var output string

// This variables are used in root.go and init.go --> ugly
var home string
var workspaceConfigDir string
var globalConfigDir string
var configName string
var configExt string
var logFileName string

const dirPerm = 0755
const filePerm = 0644

const version = "v1.5.0"

const (
	JSONOutput string = "json"
	TextOutput string = "text"
)

var commandsWithoutOutputFlag = []string{"exec", "logs", "help"}

var rootOutputValidFormats = []string{TextOutput, JSONOutput}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "kumorictl",
	Short: "A command-line interface for Kumori",
	Long: `A command-line interface for Kumori.

kumorictl is a tool for managing Kumori elements and interacting with Kumori Platform.`,
	Version: version,
	// Root command is executed only if an invalid subcommand is provided or a subcommand is not
	// provided at all. Hence, it will fail either asking for a subcommand (if none is included)
	// or indicating that the provided subcommand is invalid.
	Args: cobra.MatchAll(cobra.NoArgs, cobra.MinimumNArgs(1)),
	CompletionOptions: cobra.CompletionOptions{
		DisableDefaultCmd: true,
	},
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// logger.Debug("Root.PersistentPreRun - perform common validations")
		CreateTmp()
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
		CheckConfigFile(cmd)
		SetAuthMethod(cmd)
		SetCertificates(cmd)
		SetAdmissionURL(cmd)
		// The following two lines order has been switched due to backward compatible issues.
		// In older clusters, the "clusterconfig" call requires a refreshed token. In v1.0.1 clusters
		// this call can be executed by anonymous and authentication users. The later means that
		// a refreshed token is attached. The "clusterconfig" call is performed during the
		// "CheckClusterCompatibility". If this function is executed before getting a valid token
		// ("RefreshAdmissionCredentials" function), hence the "clusterconfig" call is executed always
		// anonymously. This is fine for v1.0.1 clusters but fails in older clusters. To keep
		// backwards compatibility, we have switched the execution order for this two functions in
		// root, describe and history commands (see ticket 896).
		RefreshAdmissionCredentials(cmd)
		CheckClusterCompatibility(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) {},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	defer func() {
		cleanTmp(keepTmpFiles)
		if err := recover(); err != nil {
			if exiterr, ok := err.(*exec.ExitError); ok {
				// If this happens is because an external command has been executed. In that
				// case, we just preserve the error code
				response.AddError(response.FatalSeverityType, exiterr.Error())
				response.Print()
				os.Exit(exiterr.ExitCode())
			} else {
				if exiterr != nil {
					response.AddError(response.FatalSeverityType, exiterr.Error())
				}
				if logger.IsInit() {
					logger.Fatal("Panic occurred: " + fmt.Sprintf("%v", err) + " (at " + logger.IdentifyPanic() + ")")
				} else {
					fmt.Fprintln(os.Stderr, "Panic occurred: "+fmt.Sprintf("%v", err)+" (at "+logger.IdentifyPanic()+")")
					response.Print()
					os.Exit(1)
				}
			}

		}
		response.Print()
	}()
	if err := rootCmd.Execute(); err != nil {
		if response.Output != response.JSONOutputType {
			fmt.Fprintln(os.Stderr, "Error:", err)
		} else {
			response.AddError(response.FatalSeverityType, err.Error())
			response.Print()
		}
		cleanTmp(keepTmpFiles)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Checks if the flags --version and --output json are set
	isversion := false
	isjson := false
	for i, arg := range os.Args {
		if (arg == "--version") || (arg == "-v") {
			isversion = true
			continue
		}
		if (arg == "--output") || (arg == "-o") {
			if i < (len(os.Args)-1) && os.Args[i+1] == "json" {
				isjson = true
			}
		}
	}

	// Overwrites the --help flag if the flag `-o json` is also set
	if isversion && isjson {
		rootCmd.SetVersionTemplate(`{{printf "{\n  \"errors\": []\n  \"success\": true\n  \"data\": {\n    \"version\": \"%s\"\n  }\n}\n" .Version}}`)
	}

	// Overwrites the usage and help functions to:
	// * Show a JSON output if the -o json flag is set
	// * Hide the -o json flag in commands included in commandsWithoutOutputFlag list
	rootCmd.SilenceErrors = true
	rootUsageFunc := rootCmd.UsageFunc()
	rootCmd.SetUsageFunc(func(cmd *cobra.Command) error {
		output, _ := cmd.Flags().GetString("output")
		if output == JSONOutput {
			InitLog(cmd)
			InitResponse(cmd)
			return nil
		}
		cmdname := cmd.Name()
		for _, hide := range commandsWithoutOutputFlag {
			if hide == cmdname {
				cmd.Flags().MarkHidden("output")
				break
			}
		}
		return rootUsageFunc(cmd)
	})

	rootHelpFunc := rootCmd.HelpFunc()

	rootCmd.SetHelpFunc(func(cmd *cobra.Command, strings []string) {
		output, _ := cmd.Flags().GetString("output")
		if output == JSONOutput {
			InitLog(cmd)
			InitResponse(cmd)
			return
		}
		cmdname := cmd.Name()
		for _, hide := range commandsWithoutOutputFlag {
			if hide == cmdname {
				cmd.Flags().MarkHidden("output")
				break
			}
		}
		rootHelpFunc(cmd, strings)
	})

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (by default looks for ./.kumori/kumori.yaml or $HOME/.kumori/kumori.yaml)")
	rootCmd.PersistentFlags().BoolVar(&keepTmpFiles, "keep-tmp-files", false, "keeps temporary files used during operation")
	rootCmd.PersistentFlags().StringVar(&admissionTimeout, "timeout", "2m", "Timeout for Admission requests")
	rootCmd.PersistentFlags().String("access-token", "", "Admission access token (overrides config one). Please use kumorictl login instead of this.")
	viper.Global.BindPFlag("access-token", rootCmd.PersistentFlags().Lookup("access-token"))
	rootCmd.PersistentFlags().MarkHidden("access-token")
	rootCmd.PersistentFlags().BoolVar(&insecure, "insecure", false, "allow insecure connections to Admission server when using SSL")
	rootCmd.PersistentFlags().StringVarP(&output, "output", "o", "text", "output format: table (default), details, json")

	// Configuration related to mTLS authentication:
	// * admission-ca-file: path to the file containing the CA signing the certificate presented by admission.
	// * admission-ca: the CA signing the certificate presented by admission coded in base64.
	// * client-cert-file: path to the file containing the certificate presented by kumorictl.
	// * client-cert: the certificate presented by kumorictl coded in base64.
	// * client-key-file: path to the file containing the private key used by kumorictl.
	// * client-key: the private key used by kumorictl coded in base64.
	// The previous configuration parameters will be used only if the target admission service uses MTLS authentication.
	// The admission-ca-file and admission-ca are optional. If missing, the CA must be installed in the OS.
	// The client certificate and private key must be provided either pointing to the file containing the data or including the data itself in base64.
	// For each certificate or key, two configuration parameters are available. If both of them are set, the one without the `-file` suffix has precedence.
	// Those parameters can be also set through environment variables. For example, "client-key" can be
	// set by using KUMORICTL_CLIENT_KEY environment variable
	viper.Global.SetEnvPrefix("kumorictl")
	viper.Global.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	rootCmd.PersistentFlags().String("admission-ca-file", "", "Path to the CA to validate admission certificates when mTLS-based auhentication is used. It is preceeded by admission-ca.")
	viper.Global.BindPFlag("admission-ca-file", rootCmd.PersistentFlags().Lookup("admission-ca-file"))
	rootCmd.PersistentFlags().MarkHidden("admission-ca-file")
	rootCmd.PersistentFlags().String("client-cert-file", "", "Path to the client certificate used when mTLS-based authentication is used. It is preceeded by client-cert.")
	viper.Global.BindPFlag("client-cert-file", rootCmd.PersistentFlags().Lookup("client-cert-file"))
	rootCmd.PersistentFlags().MarkHidden("client-cert-file")
	rootCmd.PersistentFlags().String("client-key-file", "", "Path to the private key used by client when mTLS-based authentication is used. It is preceeded by client-key.")
	viper.Global.BindPFlag("client-key-file", rootCmd.PersistentFlags().Lookup("client-key-file"))
	rootCmd.PersistentFlags().MarkHidden("client-key-file")
	rootCmd.PersistentFlags().String("admission-ca", "", "CA to validate admission certificates when mTLS-based autgentication is used. It must be encoded using base64. Preceeds admission-ca-file.")
	viper.Global.BindPFlag("admission-ca", rootCmd.PersistentFlags().Lookup("admission-ca"))
	viper.Global.BindEnv("admission-ca")
	rootCmd.PersistentFlags().MarkHidden("admission-ca")
	rootCmd.PersistentFlags().String("client-cert", "", "Client certificate used when mTLS-based authentication is used. It must be encoded using base64. Preceeds client-cert-file.")
	viper.Global.BindPFlag("client-cert", rootCmd.PersistentFlags().Lookup("client-cert"))
	viper.Global.BindEnv("client-cert")
	rootCmd.PersistentFlags().MarkHidden("client-cert")
	rootCmd.PersistentFlags().String("client-key", "", "Private key used by kumorictl when mTLS-based authentication is used. It must be encoded using base64. Preceeds client-key-file.")
	viper.Global.BindPFlag("client-key", rootCmd.PersistentFlags().Lookup("client-key"))
	viper.Global.BindEnv("client-key")
	rootCmd.PersistentFlags().MarkHidden("client-key")

	rootCmd.PersistentFlags().String("admission-authentication-type", "", "Authentication type required to communicate with admission. Values: clientcertificate (default), token, none.")
	viper.Global.BindPFlag("admission-authentication-type", rootCmd.PersistentFlags().Lookup("admission-authentication-type"))
	viper.Global.BindEnv("admission-authentication-type")
	rootCmd.PersistentFlags().MarkHidden("admission-authentication-type")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	// Note that this values are forced in kumorictl init when the --force flag
	// is set in this command
	viper.Global.SetDefault("admission", "")
	viper.Global.SetDefault("admission-protocol", "https")
	viper.Global.SetDefault("log-level", "warn")
	viper.Global.SetDefault("config-version", configCmd.Version)
	viper.Global.SetDefault("diff-tool", "internal")
	viper.Global.SetDefault("user-domain", "anonymous")
	viper.Global.SetDefault("admission-ca-file", "")
	viper.Global.SetDefault("client-cert-file", "")
	viper.Global.SetDefault("client-key-file", "")
	viper.Global.SetDefault("admission-ca", "")
	viper.Global.SetDefault("client-cert", "")
	viper.Global.SetDefault("client-key", "")
	viper.Global.SetDefault("admission-authentication-type", "clientcertificate")

	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		logger.Fatal("Error getting home directory: %s", err.Error())
		os.Exit(1)
	}

	// Not configurable variables
	tmpDir = tmpDirBase + time.Now().Format("2006-01-02_15.04.05")
	manifestsDir = tmpDir + "/manifests"
	workspaceConfigDir = "./.kumori"
	globalConfigDir = home + "/.kumori"
	configName = "kumori"
	configExt = "yaml"
	logFileName = "kumorictl.log"

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.Global.SetConfigFile(cfgFile)
	} else {
		// Search config in home directory with name ".kumori" (without extension).
		viper.Global.AddConfigPath(workspaceConfigDir)
		viper.Global.AddConfigPath(globalConfigDir)
		viper.Global.SetConfigName(configName)
	}

	viper.Global.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in and reconfig logger
	// UGLY! :
	err := viper.Global.ReadInConfig()
	if err != nil {
		configFileLoaded = false
		if strings.Contains(err.Error(), "Not Found in") {
			return
		}
		fmt.Println("Unexpected error reading kumorictl config file:" + err.Error())
	}

	configFileLoaded = true
}

func CheckConfigFile(cmd *cobra.Command) {
	if !configFileLoaded {
		logger.Warn("Workspace not yet initialized. Use 'kumorictl init' or 'kumorictl init --global' to initialize it.")
		return
	}
	if !strings.HasPrefix(viper.Global.GetString("config-version"), strings.Split(configCmd.Version, ".")[0]+".") {
		currentVersion := viper.Global.GetString("config-version")
		expectedVersion := strings.Split(configCmd.Version, ".")[0] + ".x"

		message := fmt.Sprintf("Current workspace version (%s) is not compatible with this kumorictl version"+
			" (expects %s)", currentVersion, expectedVersion)

		output, _ := cmd.Flags().GetString("output")
		if output != JSONOutput {
			fmt.Println()
			fmt.Println(message)
			fmt.Println("Please, make sure you are using the correct workspace and kumorictl version.")
		}

		logger.Fatal(message)
	}
}

func InitLog(cmd *cobra.Command) {
	logFile := getLogFile()
	loglevel := viper.Global.GetString("log-level")
	output, _ := cmd.Flags().GetString("output")
	if output == JSONOutput {
		logger.Init(logFile, "debug", nil, loglevel) // TBD: ¿always debug?
	} else {
		logger.Init(logFile, "debug", os.Stderr, loglevel) // TBD: ¿always debug?
	}
	defer logger.End()
}

func InitResponse(cmd *cobra.Command) {
	output, _ := cmd.Flags().GetString("output")
	var outputType response.OutputType
	switch output {
	case string(response.JSONOutputType):
		outputType = response.JSONOutputType
	default:
		outputType = response.SilentOutputType
	}
	logger.Debug("Setting output type", "output", outputType)

	response.Init(outputType)
}

func SetAdmissionURL(cmd *cobra.Command) {

	if viper.Global.GetString("admission") == "" {
		logger.Fatal("Admission server is not configured. Please use config command to configure it.")
	}

	admissionURL = viper.Global.GetString("admission-protocol") + "://" + viper.Global.GetString("admission")

	err := admission.SetAdmissionTimeout(admissionTimeout)
	if err != nil {
		logger.Fatal("Error setting Admission timeout: %s", err.Error())
	}

	admission.SetInsecure(insecure)

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	admission.SetAdmissionURL(admissionURL)

	err = admission.CheckAdmission()
	if err != nil {
		// If clientcertificate authentication is used, admission service requires a client certificate.
		if output != JSONOutput {
			fmt.Println()
			fmt.Println("Unable to contact Admission server. Error: " + err.Error())
			fmt.Println("Please, make sure you have properly configured Admission domain and protocol.")
		}
		logger.Fatal("Unable to contact Admission server. Error: " + err.Error())

	}
}

func SetAuthMethod(cmd *cobra.Command) {
	// Validates the authentication type
	authenticationType := viper.Global.GetString("admission-authentication-type")
	var authMethod admission.AuthMethod
	switch authenticationType {
	case "clientcertificate":
		authMethod = admission.MTLSAuthMethod
	case "token":
		authMethod = admission.TokenAuthMethod
	case "none":
		authMethod = admission.NoneAuthMethod
	default:
		admission.SetAuthMethod(admission.UnknownAuthMethod)
		errorMessage := fmt.Sprintf("Admission authentication type '%s' unknown", authenticationType)
		logger.Fatal(errorMessage)
	}
	admission.SetAuthMethod(authMethod)

}

func SetCertificates(cmd *cobra.Command) {

	authMethod := admission.GetAuthMethod()

	if authMethod == admission.MTLSAuthMethod {

		logger.Debug("Using MTLS-based authentication")

		// Validates the admission CA if it has been set
		logger.Debug("Getting admission CA")
		admissionCA := []byte(viper.Global.GetString("admission-ca"))
		if len(admissionCA) > 0 {
			decodedAdmissionCA, err := util.DecodeBase64(string(admissionCA))
			if err != nil {
				logger.Fatal("Error decoding admission CA. Error: " + err.Error())
			}
			admissionCA = []byte(decodedAdmissionCA)
		} else {
			if admissionCAFile := viper.Global.GetString("admission-ca-file"); len(admissionCAFile) > 0 {
				var err error
				admissionCA, err = os.ReadFile(admissionCAFile)
				if err != nil {
					logger.Fatal(fmt.Sprintf("error getting the admission CA: %v", err))
				}
			}
		}
		if len(admissionCA) > 0 {
			logger.Debug("Validaitng admission CA")
			if err := x509Validator.ValidateCertificateRaw(admissionCA); err != nil {
				logger.Fatal("Error validating admission CA. Error: " + err.Error())
			}

			// Stores the admission CA and private key in admission package configuration
			admission.SetAdmissionCA(admissionCA)
		} else {
			logger.Debug("Admission CA not set")
		}

		// Reads the client certificate
		logger.Debug("Getting client certificate")
		clientCert := []byte(viper.Global.GetString("client-cert"))
		if len(clientCert) > 0 {
			decodedClientCert, err := util.DecodeBase64(string(clientCert))
			if err != nil {
				logger.Fatal("Error decoding client certificate. Error: " + err.Error())
			}
			clientCert = []byte(decodedClientCert)
		} else {
			if clientCertFile := viper.Global.GetString("client-cert-file"); len(clientCertFile) > 0 {
				var err error
				clientCert, err = os.ReadFile(clientCertFile)
				if err != nil {
					logger.Fatal(fmt.Sprintf("error getting the client certificate: %v", err))
				}
			} else {
				logger.Fatal("Missing client certificate")
			}
		}

		// Reads the client private key
		logger.Debug("Getting client private key")
		clientKey := []byte(viper.Global.GetString("client-key"))
		if len(clientKey) > 0 {
			decodedClientKey, err := util.DecodeBase64(string(clientKey))
			if err != nil {
				logger.Fatal("Error decoding client private key. Error: " + err.Error())
			}
			clientKey = []byte(decodedClientKey)
		} else {
			if clientKeyFile := viper.Global.GetString("client-key-file"); len(clientKeyFile) > 0 {
				var err error
				clientKey, err = os.ReadFile(clientKeyFile)
				if err != nil {
					logger.Fatal(fmt.Sprintf("error getting the client key: %v", err))
				}
			} else {
				logger.Fatal("Missing client private key")
			}
		}

		// Validates the client certificate and key pairs
		logger.Debug("Validaitng client certificate")
		if err := x509Validator.ValidateKeyPairRaw(clientCert, clientKey); err != nil {
			logger.Fatal("Error validating client certificate. Error: " + err.Error())
		}

		// Stores the client certificate and private key in admission package configuration
		admission.SetClientCert(clientCert, clientKey)

	}
}

func RefreshAdmissionCredentials(cmd *cobra.Command) {

	authMethod := admission.GetAuthMethod()

	// If the configured Admission has authentication disabled, skip this checks
	if authMethod == admission.NoneAuthMethod {
		// Skip the checks
		logger.Debug("Admission authentication is disabled, skipping credentials checks.")
		return
	}
	// authEnabled, err := admission.IsAuthEnabled()
	// if err != nil {
	// 	logger.Warn("Unable to determine if Admission authentication is enabled. Will assume it is.")
	// } else {
	// 	if !authEnabled {
	// 		// Skip the checks
	// 		logger.Debug("Admission authentication is disabled, skipping credentials checks.")
	// 		return
	// 	}
	// }

	if authMethod == admission.TokenAuthMethod {
		logger.Debug("Using token-based authentication")

		// Get access token info from configuration file
		accessToken := viper.Global.GetString("access-token")
		accessTokenExpiryDate := time.Time{}
		accessTokenExpiryDate.UnmarshalText([]byte(viper.Global.GetString("access-token-expiry-date")))

		// Get refresh token info from configuration file
		refreshToken := viper.Global.GetString("refresh-token")
		refreshTokenExpiryDate := time.Time{}
		refreshTokenExpiryDate.UnmarshalText([]byte(viper.Global.GetString("refresh-token-expiry-date")))

		// Check if access token has expired
		if accessTokenExpiryDate.Before(time.Now()) {
			logger.Debug("Access token is expired.")

			// Try to get a new access token with the refresh token
			if refreshToken == "" {
				logger.Debug("No refresh token found.")
				logger.Fatal("Access token has expired and no refresh token found. Please use kumorictl login again.")
			} else {
				// Check if refresh token has expired
				if refreshTokenExpiryDate.Before(time.Now()) {
					logger.Debug("Refresh token is expired.")
					logger.Fatal("Both access and refresh tokens have expired. Please use kumorictl login again.")
				} else {
					logger.Debug("Refreshing tokens.")
					var accessTokenExpiryDate, refreshTokenExpiryDate string
					var err error
					accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err = admission.RefreshTokens(refreshToken)
					if err != nil {
						logger.Fatal("Error refreshing tokens. Error: " + err.Error())
					}
					viper.Global.Set("access-token", accessToken)
					viper.Global.Set("access-token-expiry-date", accessTokenExpiryDate)
					viper.Global.Set("refresh-token", refreshToken)
					viper.Global.Set("refresh-token-expiry-date", refreshTokenExpiryDate)
					err = viper.Global.WriteConfig()
					if err != nil {
						logger.Error("Error writing refreshed tokens. " + err.Error())
					}
				}
			}
		}

		if accessToken != "" {
			logger.Debug("Using access token")
			admission.SetAccessToken(accessToken)
		}
	}

}

// Verify CLI compatibility with the cluster
func CheckClusterCompatibility(cmd *cobra.Command) {

	// Get Cluster configuration
	clusterConfig, err := admission.GetClusterConfig()
	if err != nil {
		logger.Fatal("Error getting cluster configuration. " + err.Error())
	}

	// Check current CLI version is one of the supported ones

	// Remove the initial 'v' from the version
	_, i := utf8.DecodeRuneInString(version)
	myVersion := version[i:]

	compatible := false
	for _, supVer := range clusterConfig.KumorictlSupportedVersions {
		if supVer == myVersion {
			compatible = true
			break
		}
	}

	// If CLI is not compatible with Cluster, print an error and exit
	if !compatible {
		output, _ := cmd.Flags().GetString("output")
		message := fmt.Sprintf("This version of KumoriCtl (%s) is not supported by the cluster you are trying to access. Supported versions: %v", myVersion, clusterConfig.KumorictlSupportedVersions)
		if output == JSONOutput {
			response.AddError(response.WarnSeverityType, message)
		} else {
			fmt.Println()
			fmt.Println(message)
			logger.Fatal("CLI version is not supported by the Cluster.")
		}
	}
}

func CheckFlags(cmd *cobra.Command) {
	// format is not allowed when role/instance/container is specified
	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal("Error getting output flag", "error", err.Error())
	}
	if !util.ContainsString(rootOutputValidFormats, output) {
		logger.Fatal("Invalid output flag " + output)
	}
}

func CreateTmp() {
	err := os.MkdirAll(manifestsDir, dirPerm)
	if err != nil {
		logger.Fatal("Error creating tmp folder '%s': %s", manifestsDir, err.Error())
		os.Exit(1)
	}
}

func cleanTmp(keepTmpFiles bool) {

	if tmpDir == "" {
		return
	}

	tmpDirContainsFiles, err := util.DirectoryContainFiles(tmpDir)
	if err != nil {
		logger.Fatal(err.Error())
	}

	// If tmp files must not be kept or there are no files to be kept (there
	// are only directories), then clean directory
	if !keepTmpFiles || !tmpDirContainsFiles {
		err := os.RemoveAll(tmpDir)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}

	// Tmp files must be keeped and there are files to be keeped, so inform the
	// user
	if keepTmpFiles && tmpDirContainsFiles && (output != JSONOutput) {
		fmt.Printf("\nTemporary files stored in %s\n", tmpDir)
	}
}

func getLogFile() string {

	// If a config file is defined using the --config flag
	configFileUsed := viper.Global.ConfigFileUsed()
	if configFileUsed != "" {
		configDirUsed := filepath.Dir(configFileUsed)
		return configDirUsed + "/" + logFileName
	}

	// Find out which configuration directory is being used
	configDirs := []string{workspaceConfigDir, globalConfigDir}
	for _, configDir := range configDirs {
		if util.FileExists(configDir + "/" + configName + "." + configExt) {
			return configDir + "/" + logFileName
		}
	}

	return ""
}

func registerError(severity response.SeverityType, err error) error {
	response.AddError(severity, err.Error())
	return err
}

func createAndRegisterError(severity response.SeverityType, message string) error {
	return registerError(severity, fmt.Errorf(message))
}

// NoArgs returns an error if any args are included.
func NoArgs(cmd *cobra.Command, args []string) error {
	if len(args) > 0 {
		return createAndRegisterError(response.FatalSeverityType, fmt.Sprintf("unknown command %q for %q", args[0], cmd.CommandPath()))
	}
	return nil
}

// RangeArgs returns an error if the number of args is not within the expected range.
func RangeArgs(min int, max int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) < min || len(args) > max {
			return createAndRegisterError(response.FatalSeverityType, fmt.Sprintf("accepts between %d and %d arg(s), received %d", min, max, len(args)))
		}
		return nil
	}
}

// ExactArgs returns an error if there are not exactly n args.
func ExactArgs(n int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) != n {
			return createAndRegisterError(response.FatalSeverityType, fmt.Sprintf("accepts %d arg(s), received %d", n, len(args)))
		}
		return nil
	}
}
