/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"github.com/spf13/cobra"
)

// getCmd represents the get command
// Is just a container for several subcommands
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get element manifest from platform",
	// Get command is executed only if an invalid subcommand is provided or a subcommand is not
	// provided at all. Hence, it will fail either asking for a subcommand (if none is included)
	// or indicating that the provided subcommand is invalid.
	Args: cobra.MatchAll(cobra.NoArgs, cobra.MinimumNArgs(1)),
	Run:  func(cmd *cobra.Command, args []string) {},
}

func init() {
	rootCmd.AddCommand(getCmd)
}
