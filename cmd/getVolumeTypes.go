/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence isº
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getVolumeTypesCmd represents the getVolumeTypes command
var getVolumeTypesCmd = &cobra.Command{
	Use:   "volume-types",
	Short: "Get a list of persistent volume types supported by the cluster",
	Args:  checkArgsGetVolumeTypes,
	Run:   func(cmd *cobra.Command, args []string) { runGetVolumeTypes(cmd, args) },
}

func init() {
	getCmd.AddCommand(getVolumeTypesCmd)
}

func checkArgsGetVolumeTypes(cmd *cobra.Command, args []string) error {
	if len(args) > 0 {
		return errors.New("too many arguments")
	}
	return nil
}

func runGetVolumeTypes(cmd *cobra.Command, args []string) {
	clusterConfig, err := admission.GetClusterConfig()
	if err != nil {
		logger.Fatal("Error getting cluster configuration. " + err.Error())
	}

	// Only show volume types that include the 'persistent' property
	persistentVolumeTypes := map[string]admission.ClusterConfigVolumeType{}
	for volName, volType := range *clusterConfig.VolumeTypes {
		isPersistent := false
		if volType.Properties != "" {
			volTypeProps := strings.Split(volType.Properties, ",")
			for _, volTypeProp := range volTypeProps {
				if strings.TrimSpace(volTypeProp) == "persistent" {
					isPersistent = true
					break
				}
			}
		}
		if isPersistent {
			persistentVolumeTypes[volName] = volType
		}
	}

	output, _ := cmd.Flags().GetString("output")
	if output == JSONOutput {
		data := gabs.New()
		for volName, volType := range persistentVolumeTypes {
			data.Set(volType.Properties, "volumeTypes", volName, "properties")
		}
		response.SetData(data)
	} else {
		fmt.Println(toBold("VolumeTypes:"))
		for volName, volType := range persistentVolumeTypes {
			fmt.Println(" - " + toBoldMaintainCase(volName))
			fmt.Println("   Properties: " + volType.Properties)
		}
	}
}
