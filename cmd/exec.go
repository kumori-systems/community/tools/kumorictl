/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// execCmd represents the exec command
var execCmd = &cobra.Command{
	Use:   "exec <user>/<deployment> <role> <instance> <container | -- > <command [params]...>",
	Short: "Execute command on an instance container",
	Long: `Execute command on an instance container.
Specifying a container is optional unless the instance has several containers.
Please note ` + "`--`" + ` should be used as separator between instance and command if no container is specified.`,
	Args: checkArgsExec,
	Run:  func(cmd *cobra.Command, args []string) { runExec(cmd, args) },
}

func init() {
	rootCmd.AddCommand(execCmd)
	execCmd.Flags().BoolP("tty", "t", false, "interactive TTY session mode")
	execCmd.Flags().SetInterspersed(false)
}

func checkArgsExec(cmd *cobra.Command, args []string) error {
	if len(args) < 5 {
		return errors.New("missing required argument")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	return nil
}

func runExec(cmd *cobra.Command, args []string) {
	meth := "runLogs"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}
	role := args[1]
	instance := args[2]
	container := ""
	if args[3] != "--" {
		container = args[3]
	}
	command := args[4:]

	tty, _ := cmd.Flags().GetBool("tty")

	logger.Debug(
		"Executing command",
		"name", name,
		"userDomain", userDomain,
		"role", role,
		"instance", instance,
		"container", container,
		"command", command,
		"tty", tty,
		"meth", meth,
	)

	kubeInstance := strings.ReplaceAll(instance, "instance-", "")

	err = admission.Exec(userDomain, name, role, kubeInstance, container, tty, command...)
	if err != nil {
		logger.Fatal(err.Error())
	}
}
