/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

// getCACmd represents the getCA command
var getCACmd = &cobra.Command{
	Use:     "ca [<user>/<name>]",
	Aliases: []string{"ca", "cas"},
	Short:   "Get CA resource from platform",
	Long: `Get CA from platform
If <user>/<name> is not provided, all CAs are listed.
<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsGetCA,
	Run:  func(cmd *cobra.Command, args []string) { runGetCA(cmd, args) },
}

func init() {
	getCmd.AddCommand(getCACmd)
	getCACmd.Flags().Bool(
		"in-use", false,
		"show only resources in use",
	)
}

func checkArgsGetCA(cmd *cobra.Command, args []string) error {
	if len(args) > 1 {
		return errors.New("too many arguments")
	}
	if len(args) == 1 {
		// user-domain can be omitted when user-domain is set in configuration
		err := checkUserDomainAndName(args[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func runGetCA(cmd *cobra.Command, args []string) {
	meth := "runGetCA"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	if len(args) == 0 {
		// Get CA list
		onlyInUse, err := cmd.Flags().GetBool("in-use")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting CA",
			"onlyInUse", onlyInUse,
			"output", output,
			"meth", meth,
		)
		listCAs(onlyInUse, output)

	} else if len(args) == 1 {
		// Get CA manifest
		userDomain, name, err := getUserDomainAndName(args[0], "")
		if err != nil {
			logger.Fatal(err.Error())
		}
		logger.Debug(
			"Getting CA",
			"name", name,
			"userDomain", userDomain,
			"output", output,
			"meth", meth,
		)
		getCA(userDomain, name, output)
	}

	return
}

func listCAs(onlyInUse bool, output string) {
	cas, err := admission.GetCAs(onlyInUse)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Fatal(err.Error())
	}
	if output == JSONOutput {
		// If the data has not been set yet, then the cas are included directly
		// in the root part. If a Data object already exists, the cas are added
		// in the "cas" key
		data := response.GetData()
		dataExists := (data != nil)
		if !dataExists {
			data = gabs.New()
		}
		var err error
		if err != nil {
			logger.Fatal("Error printing CAs in JSON format", "error", err.Error())
		}
		for _, ca := range cas {
			data.ArrayAppend(ca.LongName, "cas")
		}

		if !dataExists {
			data = data.Search("cas")
		}
		response.SetData(data)

	} else {
		for _, ca := range cas {
			fmt.Println(ca.LongName)
		}
	}
	return
}

func getCA(userDomain, name string, output string) {
	ca, err := admission.GetCA(userDomain, name)
	if err != nil {
		response.AddError(response.ErrorSeverityType, err.Error())
		logger.Error(err.Error())
	} else {
		if output == JSONOutput {
			if data, err := gabs.ParseJSON([]byte(ca)); err != nil {
				logger.Fatal("Error writing the JSON response", "error", err.Error())
			} else {
				response.SetData(data)
			}
		} else {
			fmt.Println(ca)
		}
	}
	return
}
