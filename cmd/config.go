/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"

	"cuelang.org/go/pkg/strings"
	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/viper"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Set workspace configuration",
	Long: `Set workspace configuration.

This command sets configuration parameters passed as flags on config file.

Only the specified configuration parameters with be replaced, others will be kept.

When using ` + "`--show`" + ` flag, configuration will be printed instead of being modified on file.`,
	Args:    cobra.NoArgs,
	Version: "1.1",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		InitLog(cmd)
		InitResponse(cmd)
		CheckFlags(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("config called")
		show, _ := cmd.Flags().GetBool("show")
		output, _ := cmd.Flags().GetString("output")
		if show {
			if output == JSONOutput {
				dataObj := gabs.New()
				for key, value := range viper.Global.AllSettings() {
					dataObj.Set(fmt.Sprintf("%v", value), key)
				}
				response.SetData(dataObj)

			} else {
				for key, value := range viper.Global.AllSettings() {
					if strings.HasSuffix(key, "-token") {
						fmt.Println(key+":", "<hidden>")
					} else {
						fmt.Println(key+":", value)
					}
				}
			}
		} else {
			currentAdmissionURL := viper.Global.GetString("admission")

			// Bind command flags to viper configuration
			viper.Global.BindPFlag("admission", cmd.Flags().Lookup("admission"))
			viper.Global.BindPFlag("admission-protocol", cmd.Flags().Lookup("admission-protocol"))
			viper.Global.BindPFlag("docker-server", cmd.Flags().Lookup("docker-server"))
			viper.Global.BindPFlag("log-level", cmd.Flags().Lookup("log-level"))
			viper.Global.BindPFlag("cue-command", cmd.Flags().Lookup("cue-command"))
			viper.Global.BindPFlag("diff-tool", cmd.Flags().Lookup("diff-tool"))
			viper.Global.BindPFlag("admission-ca-file", cmd.Flags().Lookup("admission-ca-file"))
			viper.Global.BindPFlag("client-cert-file", cmd.Flags().Lookup("client-cert-file"))
			viper.Global.BindPFlag("client-key-file", cmd.Flags().Lookup("client-key-file"))
			viper.Global.BindPFlag("admission-ca", cmd.Flags().Lookup("admission-ca"))
			viper.Global.BindPFlag("client-cert", cmd.Flags().Lookup("client-cert"))
			viper.Global.BindPFlag("client-key", cmd.Flags().Lookup("client-key"))
			viper.Global.BindPFlag("admission-authentication-type", cmd.Flags().Lookup("admission-authentication-type"))

			// If admission URL has changed, then current user must be removed
			// (by default, it is the anonymous user)
			providedAdmissionURL, err := cmd.Flags().GetString("admission")
			if err != nil {
				logger.Fatal("Error reading admission configuration key. " + err.Error())
			}
			if (providedAdmissionURL != "") && (providedAdmissionURL != currentAdmissionURL) {
				logger.Debug("User has been reset to anonymous")
				viper.Global.Set("access-token", "")
				viper.Global.Set("access-token-expiry-date", "")
				viper.Global.Set("refresh-token", "")
				viper.Global.Set("refresh-token-expiry-date", "")
				viper.Global.Set("user-domain", "anonymous")
				// viper.Global.Set("admission-ca-file", "")
				// viper.Global.Set("client-cert-file", "")
				// viper.Global.Set("client-key-file", "")
				// viper.Global.Set("admission-ca", "")
				// viper.Global.Set("client-cert", "")
				// viper.Global.Set("client-key", "")
				// viper.Global.Set("admission-authentication-type", "")
			}

			err = viper.Global.WriteConfig()
			if err != nil {
				logger.Fatal("Workspace initialization is mandatory to use this command. " + err.Error())
			}
			if output != JSONOutput {
				fmt.Println("Workspace configuration updated.")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	configCmd.Flags().StringP("admission", "a", "", "Admission bare URL (e.g. admission.test.kumori.cloud)")
	configCmd.Flags().String("admission-protocol", "", "Admission protocol (http/https)")
	configCmd.Flags().StringP("log-level", "l", "", "kumorictl log level")
	configCmd.Flags().String("docker-server", "", "Docker server URL (e.g. https://index.docker.io/v1/) used by default when docker credentials are registered as secrets")
	configCmd.Flags().String("cue-command", "", "Command used to compile CUE files (e.g. cue)")
	configCmd.Flags().Bool("show", false, "prints configuration instead of saving it (dry-run)")
	// "user-domain" flag is disabled.
	// The user domain MUST be the user name. I've decided not to keep it as a given
	// flag in case someone is using a script setting the domain. We want them to
	// change thaa and the one way of forcing it is to completely remove the flag
	// configCmd.Flags().StringP("user-domain", "d", viper.Global.GetString("user-domain"), "domain used for composing long URN when short URN is provided")
	// configCmd.Flags().MarkHidden("user-domain")
	configCmd.Flags().String("diff-tool", "", "Tool used by default to compare two deployment versions (default: internal)")
	// Flags to set MTLS certificates and keys
	configCmd.Flags().String("admission-ca-file", "", "Path to the CA to validate admission certificates when mTLS-based autgentication is used.")
	configCmd.Flags().String("client-cert-file", "", "Path to the client certificate used when mTLS-based authentication is used.")
	configCmd.Flags().String("client-key-file", "", "Path to the private key used by client when mTLS-based authentication is used.")
	configCmd.Flags().String("admission-ca", "", "CA (in base64) to validate admission certificates when mTLS-based autgentication is used.")
	configCmd.Flags().String("client-cert", "", "Client certificate (in base64) used when mTLS-based authentication is used.")
	configCmd.Flags().String("client-key", "", "Private key (in base64)  used by client when mTLS-based authentication is used.")
	configCmd.Flags().String("admission-authentication-type", "", "Authentication type required to communicate with admission. Values: clientcertificate, token, none.")
}

func unset(key string) error {
	configMap := viper.Global.AllSettings()
	delete(configMap, key)
	encodedConfig, _ := json.MarshalIndent(configMap, "", " ")
	err := viper.Global.ReadConfig(bytes.NewReader(encodedConfig))
	if err != nil {
		return err
	}
	return nil
}
