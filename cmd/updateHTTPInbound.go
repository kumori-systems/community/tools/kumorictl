/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// updateHTTPInboundCmd represents the updateHTTPInbound command
//
// Internally, this command is almost an alias to registerHTTPInboundCmd,
// an the only difference is that the httpinbound element must exist previously
var updateHTTPInboundCmd = &cobra.Command{
	Use:     "http-inbound  <name>",
	Aliases: []string{"inbound", "httpinbound"},
	Short:   "Update a HTTP(S) Inbound deployment in the platform",
	// Long:    `Update a HTTP(S) Inbound deployment in the platform.`,
	Args: checkArgsUpdateHTTPInbound,
	Run:  func(cmd *cobra.Command, args []string) { runUpdateHTTPInbound(cmd, args) },
}

func init() {
	updateCmd.AddCommand(updateHTTPInboundCmd)
	setRegisterHTTPInboundFlags(updateHTTPInboundCmd)

	updateHTTPInboundCmd.Flags().Bool(
		"create-if-not-exists", false,
		"Create the HTTP inbound if not exists instead of returning an error",
	)
}

func checkArgsUpdateHTTPInbound(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain of the HTTPInbound can be omitted when user-domain is set in
	// configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// Idem, for the user-domain of the certificate
	cert, err := cmd.Flags().GetString("cert")
	if err != nil {
		return err
	}
	err = checkUserDomainAndName(cert)
	if err != nil {
		return err
	}

	// Idem, for the user-domain of the CA
	ca, err := cmd.Flags().GetString("ca")
	if err != nil {
		return err
	}
	if ca != "" {
		err = checkUserDomainAndName(ca)
		if err != nil {
			return err
		}
	}

	// client-cert-required and ca flags are allowed only if client-cert is enabled
	clientCert, err := cmd.Flags().GetBool("client-cert")
	if err != nil {
		return err
	}
	clientCertRequired, err := cmd.Flags().GetBool("client-cert-required")
	if err != nil {
		return err
	}
	if !clientCert {
		if ca != "" {
			err = fmt.Errorf("ca (certificate authority) flag can only be used with client-cert")
			return err
		}
		if clientCertRequired {
			err = fmt.Errorf("client-cert-required flag can only be used with client-cert")
			return err
		}
	}

	domain, err := cmd.Flags().GetString("domain")
	if err != nil {
		return err
	}
	err = checkUserDomainAndName(domain)
	if err != nil {
		return err
	}

	return nil
}

func runUpdateHTTPInbound(cmd *cobra.Command, args []string) {
	meth := "runUpdateHTTPInbound"
	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	owner := userDomain

	createIfNotExists, err := cmd.Flags().GetBool("create-if-not-exists")
	if err != nil {
		logger.Fatal(err.Error())
	}
	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !exists && !createIfNotExists {
		err = errors.New("http-inbound " + userDomain + "/" + name + " not found")
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Updating http-inbound",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"output", output,
		"meth", meth,
	)

	// err = errors.New("Currently not implemented")
	// logger.Fatal(err.Error())
	applyHTTPInbound(cmd, args, owner)

	if output != JSONOutput {
		fmt.Println("HttpInbound " + userDomain + "/" + name + " updated")
	}
}
