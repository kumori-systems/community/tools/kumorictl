/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// getLegacyRegistryCmd represents the get legacyRegistry command
var getLegacyRegistryCmd = &cobra.Command{
	Use:     "legacy-registry [URN]...",
	Aliases: []string{"legacy-registries"},
	Short:   "Get manifest from platform's legacy registry",
	Long: `Get manifest from platform's legacy registry.

If no URN is provided, all legacy registries are listed.

This command requires using long URNs.`,
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		args = util.FixArgs(args) // FIXING BROKEN COMPLETION
		suggestions := []string{}
		for _, registry := range admission.GetLegacyRegistries() {
			if !util.ContainsString(args, registry) {
				suggestions = append(suggestions, registry)
			}
		}
		util.FixCompletion(suggestions, toComplete) // FIXING BROKEN COMPLETION
		return suggestions, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("getLegacyRegistry called")
		if len(args) == 0 {
			for _, deployment := range admission.GetLegacyRegistries() {
				fmt.Println(deployment)
			}
		} else {
			for _, urn := range args {
				registry, err := admission.GetLegacyRegistry(urn)
				if err != nil {
					logger.Error(err.Error())
				} else {
					fmt.Println(registry)
				}
			}
		}
	},
}

func init() {
	// Hidden because seems to be useless
	getLegacyRegistryCmd.Hidden = true
	// getCmd.AddCommand(getLegacyRegistryCmd)
}
