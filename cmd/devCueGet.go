/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// devCueGetCmd represents the dev cue get command
var devCueGetCmd = &cobra.Command{
	Use:   "get [path/to/module]...",
	Short: "Fetch dependencies of CUE modules in workspace",
	Long: `Fetch dependencies of CUE modules in workspace.

If no path to module is provided, the dependencies of all CUE modules in workspace are fetched.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger.Debug("devCueGet called")
		if len(args) == 0 {
			src := "."
			err := filepath.Walk(src, func(target string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if info.IsDir() && strings.HasSuffix(target, "cue.mod") {
					return filepath.SkipDir
				}
				if info.IsDir() {
					resolveModule(target, true)
				}
				return nil
			})
			if err != nil {
				logger.Fatal(err.Error())
			}
		} else {
			for _, modulePath := range args {
				resolveModule(modulePath, false)
			}
		}
		output, _ := cmd.Flags().GetString("output")
		if output != JSONOutput {
			fmt.Println("Done.")
		}
	},
}

func init() {
	devCueCmd.AddCommand(devCueGetCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// devCueGetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// devCueGetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func resolveModule(modulePath string, ignoreNotAModuleError bool) {
	ctx, err := cuedm.Resolve(modulePath)
	if ctx != nil {
		for _, mod := range ctx.CUEModules {
			logger.Info("Module: ", "name", mod.Name, "resolved", mod.Resolved)
			for _, dep := range mod.Dependencies {
				logger.Info("Dependency: ", "name", dep.VersionedName, "repo", dep.Repository, "resolved", dep.Resolved)
			}
		}
	}
	if err != nil && (err.Error() != "Context is not a CUE module" || !ignoreNotAModuleError) {
		logger.Fatal("Cannot fetch dependencies: " + err.Error())
	}
}
