/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// updateSolutionCmd represents the updateSolution command
var updateSolutionCmd = &cobra.Command{
	Use:   "deployment <name>",
	Short: "Update a deployment in the platform",
	// Long:  `Update a deployment in the platform.`,
	Args: checkArgsUpdateSolution,
	Run:  func(cmd *cobra.Command, args []string) { runUpdateSolution(cmd, args) },
}

func init() {
	updateCmd.AddCommand(updateSolutionCmd)

	updateSolutionCmd.PersistentFlags().StringP(
		"deployment", "d", "",
		"[required] path/to/module or git URI containing the CUE module",
	)
	updateSolutionCmd.MarkPersistentFlagRequired("deployment")
	updateSolutionCmd.MarkFlagRequired("deployment")

	updateSolutionCmd.Flags().StringP("package", "p", "deployment", "Deployment package")

	updateSolutionCmd.Flags().StringP(
		"comment", "m", "",
		"message describing the update action",
	)

	updateSolutionCmd.Flags().Bool(
		"create-if-not-exists", false,
		"Create the deployment if not exists instead of returning an error",
	)

	updateSolutionCmd.Flags().Bool(
		"dry-run", false,
		"Perform all local operations but do not update in the cluster",
	)

	updateSolutionCmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time for the service to be ready before returning",
	)

	updateSolutionCmd.Flags().Bool(
		"allow-json-manifest", false,
		`(experimental) Allow to provide a pre-generated JSON manifest as deployment
input parameter (filename or - for stdin), instead of a path to CUE module`,
	)
}

func checkArgsUpdateSolution(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runUpdateSolution(cmd *cobra.Command, args []string) {
	meth := "runUpdateSolution"
	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	owner := userDomain

	createIfNotExists, err := cmd.Flags().GetBool("create-if-not-exists")
	if err != nil {
		logger.Fatal(err.Error())
	}
	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !exists && !createIfNotExists {
		err = errors.New("solution " + userDomain + "/" + name + " not found")
		logger.Fatal(err.Error())
	}

	deploymentPackage, err := cmd.Flags().GetString("package")
	if err != nil {
		logger.Fatal(err.Error())
	}

	assumeYes, err := cmd.Flags().GetBool("yes")
	if err != nil {
		logger.Fatal(err.Error())
	}

	dryRun, err := cmd.Flags().GetBool("dry-run")
	if err != nil {
		logger.Fatal(err.Error())
	}

	allowJSONManifest, err := cmd.Flags().GetBool("allow-json-manifest")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Updating solution",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"package", deploymentPackage,
		"assumeYes", assumeYes,
		"createIfNotExists", createIfNotExists,
		"dryRun", dryRun,
		"allowJSONManifest", allowJSONManifest,
		"waitDuration", waitDuration,
		"output", output,
		"meth", meth,
	)

	applySolution(cmd, args, owner, assumeYes, dryRun, allowJSONManifest, waitDuration, true)
	if dryRun {
		if output != JSONOutput {
			fmt.Println("Dry-run: solution " + userDomain + "/" + name + " was not updated in the cluster")
		}
	} else {
		if output != JSONOutput {
			fmt.Println("Solution " + userDomain + "/" + name + " updated")
		}
	}
}
