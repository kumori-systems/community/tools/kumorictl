/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// disableSolutionCmd represents the disableSolution command
var disableSolutionCmd = &cobra.Command{
	Use:   "solution <user>/<name>",
	Short: "Disable a solution in the platform",
	Long: `Disable a solution in the platform.
	<user> can be ommited and defaults to the logged user`,
	Args: checkArgsDisableSolution,
	Run:  func(cmd *cobra.Command, args []string) { runDisableSolution(cmd, args) },
}

func init() {
	// Hidden because it is not working
	disableSolutionCmd.Hidden = true
	// disableCmd.AddCommand(disableSolutionCmd)

	disableSolutionCmd.Flags().StringP(
		"comment", "m", "",
		"message describing the disable action",
	)

	disableSolutionCmd.PersistentFlags().StringP(
		"wait", "w", "0s",
		"Wait for some time for solution to be disabled before returning",
	)
}

func checkArgsDisableSolution(cmd *cobra.Command, args []string) error {
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-domain can be omitted when user-domain is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// "wait" parameter must be a valid duration ("5m", "30s",...)
	_, err = util.GetTimeFlag("wait", cmd)
	if err != nil {
		return err
	}

	return nil
}

func runDisableSolution(cmd *cobra.Command, args []string) {
	meth := "runDisableSolution()"

	userDomain, name, err := getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	comment, err := cmd.Flags().GetString("comment")
	if err != nil {
		logger.Fatal(err.Error())
	}

	waitDuration, err := util.GetTimeFlag("wait", cmd)
	if err != nil {
		logger.Fatal(err.Error())
	}

	exists, err := admission.ExistsSolution(userDomain, name)
	if err != nil {
		logger.Fatal(err.Error())
	}
	if !exists {
		err = errors.New("solution " + userDomain + "/" + name + " not found")
		logger.Fatal(err.Error())
	}

	format, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Disabling solution",
		"name", name,
		"userDomain", userDomain,
		"comment", comment,
		"waitDuration", waitDuration,
		"format", format,
		"meth", meth,
	)

	//
	//
	// https://gitlab.com/kumori/kv3/project-management/-/issues/721
	// There is a bug in the "disable" command implementation, that implies that
	// the coredns pods are stuck in a crash loop.
	// So, for now, the "disable solution" are not allowed
	//
	//
	err = errors.New("Currently not implemented")
	logger.Fatal(err.Error())

	manifestFile := manifestsDir + "/Manifest.json"

	manifestContent := `{
		"ref": {
			"domain": "` + userDomain + `",
			"kind": "solution",
			"name": "` + name + `"
		},
		"comment": "` + comment + `",
		"description": {}
	}
	`
	err = ioutil.WriteFile(manifestFile, []byte(manifestContent), filePerm)
	if err != nil {
		logger.Fatal("Temporary manifest file writing failed: " + err.Error())
	}

	err = admission.DisableSolution(manifestsDir, tmpDir)
	if err != nil {
		logger.Fatal(err.Error())
	}
	output, _ := cmd.Flags().GetString("output")

	if output != JSONOutput {
		fmt.Println("Solution " + userDomain + "/" + name + " disabled")
	}

	if waitDuration != util.ZERO_DURATION {
		// In an "disable" operation, the service is already "running".
		// Therefore, we wait a while for the changes to begin to take place.
		time.Sleep(20 * time.Second)
		err = admission.WaitSolutionToBeReady(userDomain, name, waitDuration)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}
}
