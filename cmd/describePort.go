/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"errors"
	"fmt"

	"github.com/Jeffail/gabs/v2"
	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/response"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// describePortCmd represents the describe Port command
var describePortCmd = &cobra.Command{
	Use:     "port <user>/<name>",
	Aliases: []string{"ports"},
	Short:   "Describe Port on platform",
	Long: `Describe Port on platform.
	<user> can be ommited and defaults to the logged user.`,
	Args: checkArgsDescribePort,
	Run:  func(cmd *cobra.Command, args []string) { runDescribePort(cmd, args) },
}

func init() {

	describePortCmd.SetHelpFunc(func(command *cobra.Command, strings []string) {
		// Hide flag for this command
		// command.Flags().MarkHidden("user-port")
		// Call parent help func
		command.Parent().HelpFunc()(command, strings)
	})
	describeCmd.AddCommand(describePortCmd)
}

func checkArgsDescribePort(cmd *cobra.Command, args []string) error {
	// Port name is mandatory
	if len(args) < 1 {
		return errors.New("missing required argument")
	}

	// Port name is the only allowed parameter
	if len(args) > 1 {
		return errors.New("too many arguments")
	}

	// user-port can be omitted when user-port is set in configuration
	err := checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	// format is not allowed when role/instance/container is specified
	format, err := cmd.Flags().GetString("output")
	if err != nil {
		return err
	}
	if !util.ContainsString(describeValidFormats, format) {
		return errors.New("Invalid output format " + format)
	}

	return nil
}

func runDescribePort(cmd *cobra.Command, args []string) {
	meth := "runDescribePort"

	userDomain := ""
	name := ""
	var err error

	// Calculates the user port and name. If arguments are not provided, then
	// the user port
	userDomain, name, err = getUserDomainAndName(args[0], "")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Describing Port",
		"name", name,
		"userDomain", userDomain,
		"output", output,
		"meth", meth,
	)

	if output == "json" {
		runDescribePortJSON(cmd, args)
		return
	}

	if output == "table" {
		runDescribePortTable(cmd, args)
		return
	}

	logger.Fatal("Incorrect arguments")
}

func runDescribePortJSON(cmd *cobra.Command, args []string) {
	_, manifest, err := getPortData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	response.SetData(manifest)
}

func runDescribePortTable(cmd *cobra.Command, args []string) {
	_, portDetailsJSON, err := getPortData(args[0])
	if err != nil {
		logger.Fatal(err.Error())
	}

	printHeaderPort(portDetailsJSON)
	fmt.Println("")
}

func getPortData(fullName string) (string, *gabs.Container, error) {
	userDomain, name, err := getUserDomainAndName(fullName, "")
	if err != nil {
		return "", nil, err
	}
	portManifest, portJSON, err := admission.DescribePort(userDomain, name)
	if err != nil {
		return "", nil, err
	}
	if portJSON == nil {
		return "", nil, fmt.Errorf("Port not found")
	}

	return portManifest, portJSON, nil
}

func printHeaderPort(json *gabs.Container) {

	portName, _ := admission.PortURNToName(json.Path("urn").Data().(string))

	fmt.Println(toBold("Port              "), ":", portName)
	fmt.Println(toBold("Public            "), ":", json.Path("public").Data().(bool))
	creationTimestamp := UnknownValue
	if json.Exists("creationTimestamp") {
		creationTimestamp = json.Search("creationTimestamp").Data().(string)
	}
	fmt.Println(
		toBold("Creation timestamp"),
		":",
		creationTimestamp,
	)

	lastModification := UnknownValue
	if json.Exists("lastModification") {
		lastModification = json.Search("lastModification").Data().(string)
	}
	fmt.Println(
		toBold("Last modification "),
		":",
		lastModification,
	)
	fmt.Println("")
	fmt.Println(toBoldMaintainCase("Port number"), ":", json.Path("description.port").Data().(float64))
	fmt.Println("")
	usingDeplList := json.Path("inUseBy").Children()
	if len(usingDeplList) < 1 {
		fmt.Println(toBoldMaintainCase("Port not currently in use."))
	} else {
		fmt.Println(toBoldMaintainCase("Port is currently in use by the following deployments:"))
		for _, usingDepl := range usingDeplList {
			deplName, _ := admission.SolutionURNToName(usingDepl.Data().(string))
			fmt.Println(" - ", deplName)
		}
	}
}

func getPortNameAndPort(urn string) (
	port string,
	err error,
) {
	userDomain, name, err := admission.PortDecomposeUrn(urn)
	port = fmt.Sprintf("%s/%s", userDomain, name)
	return
}
