/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kumori/kumorictl/pkg/admission"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	x509Validator "gitlab.com/kumori/kumorictl/pkg/x509-validator"
)

// registerCACmd represents the register CA command
var registerCACmd = &cobra.Command{
	Use:     "ca <name>",
	Aliases: []string{"ca", "cas"},
	Short:   "Create a certificate authority (CA) resource and register it in the platform",
	Long:    `Create a certificate authority (CA) resource and register it in the platform.`,
	Args:    checkArgsRegisterCA,
	Run:     func(cmd *cobra.Command, args []string) { runRegisterCA(cmd, args) },
}

func init() {
	registerCmd.AddCommand(registerCACmd)

	registerCACmd.Flags().StringP(
		"ca-file", "a", "",
		"Certificate authority file",
	)
	registerCACmd.MarkPersistentFlagRequired("ca-file")
	registerCACmd.MarkFlagRequired("ca-file")

}

func checkArgsRegisterCA(cmd *cobra.Command, args []string) error {
	err := cobra.ExactArgs(1)(cmd, args)
	if err != nil {
		return err
	}

	_, err = cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	// user-domain can be omitted when user-domain is set in configuration
	err = checkUserDomainAndName(args[0])
	if err != nil {
		return err
	}

	return nil
}

func runRegisterCA(cmd *cobra.Command, args []string) {
	meth := "runRegisterCA"

	owner, err := cmd.Flags().GetString("owner")
	if err != nil {
		logger.Fatal(err.Error())
	}

	userDomain, name, err := getUserDomainAndName(args[0], owner)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if !validateName(name) {
		err = fmt.Errorf("CA name '%s' is not a valid name", name)
		logger.Fatal(err.Error())
	}

	caFile, err := cmd.Flags().GetString("ca-file")
	if err != nil {
		logger.Fatal(err.Error())
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(
		"Registering CA",
		"name", name,
		"userDomain", userDomain,
		"owner", owner,
		"caFile", caFile,
		"output", output,
		"meth", meth,
	)

	// Ambassador/Envoy crash if corrupted certificates are used (see ticket90)
	err = x509Validator.ValidateCertificate(caFile)
	if err != nil {
		message := fmt.Sprintf("Error validating CA certificate: %s", err.Error())
		logger.Fatal(message)
	}

	err = admission.CreateCA(userDomain, name, owner, caFile)
	if err != nil {
		logger.Fatal(err.Error())
	}

	if output != JSONOutput {
		fmt.Println("Resource CA " + userDomain + "/" + name + " created")
	}
}
