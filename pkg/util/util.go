/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package util

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"syscall"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
)

const jwtTokenUsernameField = "preferred_username"

const ZERO_DURATION = time.Duration(0) * time.Millisecond

func ContainsString(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func FixArgs(args []string) (fixedArgs []string) {
	fixedArgs = []string{}
	for index, arg := range args {
		if len(args) > index+1 && args[index+1] == ":" || arg == ":" {
			continue
		}
		if index-1 >= 0 && args[index-1] == ":" {
			fixedArgs = append(fixedArgs, args[index-2]+args[index-1]+arg)
			continue
		}
		fixedArgs = append(fixedArgs, arg)
	}
	return
}

func FixCompletion(suggestions []string, toComplete string) {
	for _, suggestion := range suggestions {
		splittedSuggestion := strings.Split(suggestion, ":")
		if toComplete != "" && len(splittedSuggestion) == 2 {
			fmt.Println(splittedSuggestion[1])
		}
	}
}

func ReadAsBase64(filepath string) (base64str string, err error) {
	byteValue, err := os.ReadFile(filepath)
	if err != nil {
		return
	}
	base64str = base64.StdEncoding.EncodeToString(byteValue)
	return
}

func DecodeBase64(encoded string) (decoded string, err error) {
	decodedBytes, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		return "", err
	}

	return string(decodedBytes), nil
}

func StringAsBase64(value string) (base64str string) {
	base64str = base64.StdEncoding.EncodeToString([]byte(value))
	return
}

func GetPasswd(prompt string, allowStdin bool) (password string, err error) {
	fmt.Print(prompt)
	bytePassword, err := ReadPassword("", allowStdin)
	if err != nil {
		return
	}
	fmt.Println()
	password = strings.TrimSpace(string(bytePassword))
	return
}

// ReadPassword read password function that works for both for:
// - user types the password after being asked
// - password is passed via stdin without TTY (pipes, etc.)
//
// This is a known issue of this package and this method is taken from a
// proposed in the issue:
// See: https://github.com/golang/go/issues/19909
func ReadPassword(prompt string, allowStdin bool) ([]byte, error) {
	fmt.Fprint(os.Stderr, prompt)
	var fd int
	var pass []byte
	if terminal.IsTerminal(syscall.Stdin) {
		fd = syscall.Stdin
		inputPass, err := terminal.ReadPassword(fd)
		if err != nil {
			return nil, err
		}
		pass = inputPass
	} else if allowStdin {
		reader := bufio.NewReader(os.Stdin)
		s, err := reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		pass = []byte(s)
	}

	return pass, nil
}

// GetUserFromToken reads the username from the token.
// The "github.com/dgrijalva/jwt-go" is used, because is the mos popular in
// the https://jwt.io libraries list.
// In this case, we dont need verify the token, just read a field... so we can
// use the "dangerous" ParseUnverified function.
func GetUserFromToken(tokenstr string) (user string, err error) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenstr, jwt.MapClaims{})
	if err == nil {
		user = token.Claims.(jwt.MapClaims)[jwtTokenUsernameField].(string)
	}
	return
}

// Check if a directory contains any file (including subdirectories)
func DirectoryContainFiles(dirPath string) (bool, error) {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	for _, file := range files {
		if file.IsDir() {
			existFileInDir, err := DirectoryContainFiles(dirPath + "/" + file.Name())
			if err != nil {
				return false, err
			}
			if existFileInDir {
				return true, nil
			}
		} else {
			return true, nil
		}
	}
	return false, nil
}

// FileExists checks if a file exists and is not a directory
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// GetTimeFlag returns a string flag as a time.Duration value
func GetTimeFlag(flagName string, cmd *cobra.Command) (time.Duration, error) {
	wait, err := cmd.Flags().GetString(flagName)
	if err != nil {
		return 0, err
	}
	waitDuration, err := time.ParseDuration(wait)
	if err != nil {
		return 0, err
	}
	return waitDuration, nil
}

// CopyFile copies the file content form one path to another
func CopyFile(srcFilePath, dstFilePath string) error {

	srcFile, err := os.Open(srcFilePath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(dstFilePath)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return nil
}

// CreateFileFromBytes copies the bytes to the provided file path
func CreateFileFromBytes(srcBytes []byte, dstFilePath string) error {

	dstFile, err := os.Create(dstFilePath)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = dstFile.Write(srcBytes)
	if err != nil {
		return err
	}

	return nil
}

// CreateFileFromStdin copies the stdin input to the provided file path
func CreateFileFromStdin(dstFilePath string) error {

	if terminal.IsTerminal(syscall.Stdin) {
		return fmt.Errorf("no stdin")
	}

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return err
	}

	err = CreateFileFromBytes(bytes, dstFilePath)
	if err != nil {
		return err
	}

	return nil
}
