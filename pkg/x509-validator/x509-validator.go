/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package x509Validator

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

var regexBlankLines *regexp.Regexp

func init() {
	regexBlankLines, _ = regexp.Compile(`[\t\r\n]+`)
}

func removeBlankLines(source []byte) []byte {
	str := regexBlankLines.ReplaceAllString(strings.TrimSpace(string(source)), "\n")
	return []byte(str)
}

func ValidateCertificateOpenSSL(certificateFilePath string) error {
	certificateFileAbsolutePath, _ := filepath.Abs(certificateFilePath)
	cmd := exec.Command("openssl", "x509", "-in", certificateFileAbsolutePath, "-noout")
	if err := cmd.Run(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return errors.New("Invalid certificate")
		}
		return err
	}
	return nil
}

func ValidateCertificateRaw(cert []byte) error {

	// Remove blank lines
	cert = removeBlankLines(cert)

	// cert can contain multiple certs, and pem.Decode() function
	// extracts them one by one.
	// But... when pem.Decode() detects an invalid cert, it just ignore
	// it, but we want to detect the invalid cert!! So we calculate the
	// expected size of the sum of certs as a workaround to this problem.
	originalSize := len(cert)
	calculatedSize := 0
	var block *pem.Block
	for true {
		// pem.Decode extracts one block from the cert, and returns
		// the rest of the cert.
		block, cert = pem.Decode(cert)

		// End of certificate
		if block == nil && (len(cert) == 0 || cert == nil) {
			break
		}

		if block == nil {
			return fmt.Errorf("Invalid certificate (no PEM block detected)")
		}

		_, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return fmt.Errorf("Invalid certificate (%s)", err.Error())
		}

		calculatedSize = calculatedSize + len(pem.EncodeToMemory(block))
	}

	// Original size (after remove all new-lines) and calculated size must
	// be equal (well, the calculated size includes a "new-line" at the end).
	if originalSize+1 != calculatedSize {
		return fmt.Errorf("Invalid certificate (unexpected size of cert after validating)")
	}

	return nil
}

func ValidateCertificate(certificateFilePath string) error {

	cert, err := os.ReadFile(certificateFilePath)
	if err != nil {
		return err
	}

	err = ValidateCertificateRaw(cert)
	return err
}

func ValidatePrivateKeyOpenSSL(privateKeyFilePath string) error {
	privateKeyFileAbsolutePath, _ := filepath.Abs(privateKeyFilePath)
	cmd := exec.Command("openssl", "rsa", "-in", privateKeyFileAbsolutePath, "-check", "-noout")
	if err := cmd.Run(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return errors.New("Invalid private key")
		}
		return err
	}
	return nil
}

func ValidatePrivateKeyRaw(privateKey []byte) error {
	block, _ := pem.Decode([]byte(privateKey))
	if block == nil {
		return errors.New("Invalid private key")
	}
	_, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return errors.New("Invalid private key")
	}
	return err

}

func ValidatePrivateKey(privateKeyFilePath string) error {
	privateKeyString, err := os.ReadFile(privateKeyFilePath)
	if err != nil {
		return err
	}

	err = ValidatePrivateKeyRaw(privateKeyString)
	return err
}

// ValidateKeyPair reads and parses a public/private key pair from a pair of files.
// The files must contain PEM encoded data. The certificate file may contain
// intermediate certificates following the leaf certificate to form a certificate
// chain. On successful return, Certificate.Leaf will be nil because the parsed
// form of the certificate is not retained.
func ValidateKeyPair(certFile, keyFile string) error {
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return err
	}

	err = ValidateCertificate(certFile)
	if err != nil {
		return err
	}

	x509Cert, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return err
	}
	if len(x509Cert.Subject.CommonName) <= 0 {
		return fmt.Errorf("Common name (CN) not found in certificate '%s'", certFile)
	}

	return nil
}

func ValidateKeyPairRaw(certRaw, keyRaw []byte) error {
	cert, err := tls.X509KeyPair(certRaw, keyRaw)
	if err != nil {
		return err
	}

	err = ValidateCertificateRaw(certRaw)
	if err != nil {
		return err
	}

	x509Cert, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return err
	}
	if len(x509Cert.Subject.CommonName) <= 0 {
		return fmt.Errorf("Common name (CN) not found in certificate '%s'", string(certRaw))
	}

	return nil
}

// GetValidityBounds returns the validity interval for a given certificate
func GetValidityBounds(certificateString string) (notNefore, notAfter time.Time, err error) {
	block, _ := pem.Decode([]byte(certificateString))
	if block == nil {
		return time.Now(), time.Now(), errors.New("Invalid certificate")
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return time.Now(), time.Now(), errors.New("Invalid certificate")
	}
	return cert.NotBefore, cert.NotAfter, nil
}

// GetParsedCertificate returns a parsed version of the given certificate
func GetParsedCertificate(certificateString string) (parsedCertificate *x509.Certificate, err error) {
	block, _ := pem.Decode([]byte(certificateString))
	if block == nil {
		return nil, errors.New("Invalid certificate")
	}
	parsedCertificate, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, errors.New("Invalid certificate")
	}
	return parsedCertificate, nil
}

// GetCertificateSubjectCN returns the subject CN of a given certificate provided as a string
func GetCertificateSubjectCN(certificateString string) (cn string, err error) {
	cert, err := GetParsedCertificate(certificateString)
	if err != nil {
		return "", err
	}

	return cert.Subject.CommonName, nil
}
