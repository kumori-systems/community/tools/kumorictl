/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
)

// Volume format
//
//   {
//     "ref": {
//       "name": "myvolume",
//       "kind": "volume",
//       "domain": "kumori.examples"
//     },
//     "description": {
//       "items": 5,
//       "parameters":
//       [],
//       "size": "10M",
//       "type": "persistent"
//     }
//   }

func GetVolumes(onlyInUse bool) (volumes []Resource, err error) {
	return GetResources(onlyInUse, VolumeCheckUrn, VolumeDecomposeUrn)
}

func GetVolume(userDomain, name string) (manifest string, err error) {
	urn := VolumeComposeUrn(userDomain, name)
	if VolumeCheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("Volume resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a volume resource one")
	}
	return
}

func CreateVolume(
	userDomain, name, owner string,
	volItems uint32,
	volSize, volType, volParameters string,
) (err error) {
	meth := "CreateVolume"

	// Create a temporary file with the volume manifest
	manifest, err := volumeManifest(userDomain, name, owner, volItems, volSize, volType, volParameters)
	if err != nil {
		return
	}
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("Volume created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}
	return
}

func DeleteVolume(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeleteVolume"
	urn := VolumeComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("Volume deleted", "urn", urn, "meth", meth)
	}
	return
}

func VolumeCheckUrn(urn string) bool {
	return urnregexp.CheckVolumeUrn(urn)
}

func VolumeComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/volume/" + name
	return
}

func VolumeDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/volume/<NAME>"
	if !urnregexp.CheckVolumeUrn(urn) {
		err = errors.New("Volume URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/volume/")
	userDomain = urn[8:pos] // here 8 is len("eslap://")
	name = urn[pos+8:]      // here 8 is len("/volume/")
	if userDomain == "" || name == "" {
		err = errors.New("Volume URN expected format:" + expectedFormat)
		return
	}
	return
}

func VolumeURNToName(urn string) (longName string, err error) {
	domain, name, err := VolumeDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

func volumeManifest(
	domain, name, owner string,
	volItems uint32,
	volSize, volType, parameters string,
) (manifest string, err error) {

	items := strconv.FormatUint(uint64(volItems), 10)

	volobj := Volume{
		Name:       name,
		UserDomain: domain,
		Owner:      owner,
		Items:      items,
		Size:       volSize,
		Type:       volType,
	}

	tmpl, err := template.New("volume").Parse(volumeTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, volobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribeVolume(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := VolumeComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	volDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if volDetails == nil {
		err = fmt.Errorf("Volume \"" + urn + "\" not found")
		return
	}

	manifest = volDetails.StringIndent("", "  ")
	manifestJSON = volDetails

	return
}

type Volume struct {
	Name       string
	UserDomain string
	Owner      string
	Items      string
	Size       string
	Type       string
}

const volumeTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "volume",
		"domain": "{{.UserDomain}}"
	},
	"description" : {
		"items": {{.Items}},
		"parameters":
		[],
		"size": "{{.Size}}",
		"type": "{{.Type}}"
	}
}`
