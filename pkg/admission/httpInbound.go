/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"fmt"
	"os"
	"text/template"

	"gitlab.com/kumori/kumorictl/pkg/bundle"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

func CreateHTTPInbound(
	userDomain, name, owner, vhostUserDomain, vhostName,
	certUserDomain, certName, caUserDomain, caName string,
	clientCert, clientCertRequired, websockets bool, comment string,
	allowedIpList, deniedIpList []string,
	remoteAddressHeader string, cleanXForwardedFor bool,
) (
	err error,
) {
	meth := "CreateHTTPInbound"
	certLongName := fmt.Sprintf("%s/%s", certUserDomain, certName)
	vhostLongName := fmt.Sprintf("%s/%s", vhostUserDomain, vhostName)
	caLongName := ""
	if caName != "" {
		caLongName = fmt.Sprintf("%s/%s", caUserDomain, caName)
	}

	manifest, err := HTTPInboundManifest(
		userDomain, name, owner, vhostLongName, certLongName, caLongName,
		clientCert, clientCertRequired, websockets, comment, allowedIpList,
		deniedIpList, remoteAddressHeader, cleanXForwardedFor,
	)

	if err != nil {
		return
	}

	// fmt.Println(manifest)

	tmpManifestFile, tmpDir := createTmpManifestFile(manifest)
	defer os.RemoveAll(tmpDir)

	err = bundle.ZipBundle(tmpManifestFile, tmpManifestFile+".zip")
	if err != nil {
		return
	}

	urn, err := RegisterSolutionBundle(tmpManifestFile + ".zip")
	if err == nil {
		logger.Debug("HttpInbound created", "urn", urn, "meth", meth)
	}
	return
}

func HTTPInboundManifest(
	userDomain, name, owner, vhostURN, certificateURN,
	caURN string, clientCert, clientCertRequired,
	websockets bool, comment string,
	allowedIpList, deniedIpList []string,
	remoteAddressHeader string, cleanXForwardedFor bool,
) (
	manifest string,
	err error,
) {

	httpInbound := HttpInbound{
		Name:                name,
		UserDomain:          userDomain,
		Owner:               owner,
		ClientCert:          clientCert,
		ClientCertRequired:  clientCertRequired,
		ClientCertCA:        caURN,
		WebSockets:          websockets,
		Certificate:         certificateURN,
		Domain:              vhostURN,
		Comment:             comment,
		AllowedIpList:       allowedIpList,
		DeniedIpList:        deniedIpList,
		RemoteAddressHeader: remoteAddressHeader,
		CleanXForwardedFor:  cleanXForwardedFor,
	}

	tmpl, err := template.New("httpinbound").Parse(HttpInboundTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, httpInbound)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

type HttpInbound struct {
	Name                string
	UserDomain          string
	Owner               string
	ClientCert          bool
	ClientCertRequired  bool
	ClientCertCA        string
	WebSockets          bool
	Certificate         string
	Domain              string
	Comment             string
	AllowedIpList       []string
	DeniedIpList        []string
	RemoteAddressHeader string
	CleanXForwardedFor  bool
}

func (h HttpInbound) AllowedIpListAsJsonArray() string {
	return h.ListAsJsonArray(h.AllowedIpList)
}

func (h HttpInbound) DeniedIpListAsJsonArray() string {
	return h.ListAsJsonArray(h.DeniedIpList)
}

// ListAsJsonArray converts a []string variable to a JSON array.
// For example:  []string{"value1", "value2"}  ==>  ["value1","value2"]
func (h HttpInbound) ListAsJsonArray(input []string) (output string) {
	output = "["
	for _, v := range input {
		if v == "" {
			continue
		}
		if output != "[" { // If it isnt the rirst element
			output += ","
		}
		output += "\"" + v + "\""
	}
	output += "]"
	return
}

const HttpInboundTemplate = `
{
	"cspec": "1.0",
	"comment": "{{.Comment}}",
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref": {
		"name": "{{.Name}}",
		"domain": "{{.UserDomain}}",
		"kind": "solution"
	},
	"links": [],
	"top": "{{.Name}}",
	"deployments": {
		"{{.Name}}": {
			"config": {
				"parameter": {
					"type": "https",
					{{if .AllowedIpList}}
					"accesspolicies": {
						"allowedIPList": {{ .AllowedIpListAsJsonArray }}
					},
					{{else if .DeniedIpList}}
					"accesspolicies": {
						"deniedIpList": {{ .DeniedIpListAsJsonArray }}
					},
					{{end}}
					"clientcert": {{.ClientCert}},
					"certrequired": {{.ClientCertRequired}},
					"websocket": {{.WebSockets}},
					"remoteaddressheader": "{{.RemoteAddressHeader}}",
					"cleanxforwardedfor": {{.CleanXForwardedFor}}
				},
				"resource": {
					{{if .ClientCert}}
					"clientcertca": {
						"ca": "{{.ClientCertCA}}"
					},
					{{end}}
					"servercert": {
						"certificate": "{{.Certificate}}"
					},
					"serverdomain": {
						"domain": "{{.Domain}}"
					}
				},
				"resilience": 0,
				"scale": {
					"detail": {}
				}
			},
			"name": "{{.Name}}",
			"meta": {},
			"up": null,
			"artifact": {
				"spec": [
					1,
					0
				],
				"ref": {
					"kind": "service",
					"domain": "kumori.systems",
					"module": "builtins/inbound",
					"name": "inbound",
					"version": [
						1,
						3,
						0
					]
				},
				"description": {
					"config": {
						"parameter": {
							"type": "https",
							{{if .AllowedIpList}}
							"accesspolicies": {
								"allowedIPList": {{ .AllowedIpListAsJsonArray }}
							},
							{{else if .DeniedIpList}}
							"accesspolicies": {
								"deniedIpList": {{ .DeniedIpListAsJsonArray }}
							},
							{{end}}
							"clientcert": {{.ClientCert}},
							"certrequired": {{.ClientCertRequired}},
							"websocket": {{.WebSockets}},
							"remoteaddressheader": "{{.RemoteAddressHeader}}",
							"cleanxforwardedfor": {{.CleanXForwardedFor}}
						},
						"resource": {
							{{if .ClientCert}}
							"clientcertca": {
								"ca": "{{.ClientCertCA}}"
							},
							{{end}}
							"servercert": {
								"certificate": "{{.Certificate}}"
							},
							"serverdomain": {
								"domain": "{{.Domain}}"
							}
						}
					},
					"srv": {
						"client": {
							"inbound": {
								"inherited": false,
								"protocol": "http"
							}
						},
						"server": {},
						"duplex": {}
					},
					"role": {},
					"builtin": true,
					"connector": {}
				}
			}
		}
	}
}
`
