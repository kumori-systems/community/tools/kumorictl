/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"fmt"
	"os"
	"text/template"

	"gitlab.com/kumori/kumorictl/pkg/bundle"
	"gitlab.com/kumori/kumorictl/pkg/logger"
)

// CreateTCPInbound creates a new TCP inbound from a given domain, name and TCP port name.
func CreateTCPInbound(
	userDomain, name, owner, portName, portUserDomain, comment string,
) (
	err error,
) {
	meth := "CreateTCPInbound"

	portLongName := fmt.Sprintf("%s/%s", portUserDomain, portName)
	manifest, err := TCPInboundManifest(userDomain, name, owner, portLongName, comment)
	if err != nil {
		return
	}

	// fmt.Println(manifest)

	tmpManifestFile, tmpDir := createTmpManifestFile(manifest)
	defer os.RemoveAll(tmpDir)

	err = bundle.ZipBundle(tmpManifestFile, tmpManifestFile+".zip")
	if err != nil {
		return
	}

	urn, err := RegisterSolutionBundle(tmpManifestFile + ".zip")
	if err == nil {
		logger.Debug("TCP inbound created", "urn", urn, "meth", meth)
	}
	return
}

// TCPInboundManifest returns a manifest to register a new TCP inbound
func TCPInboundManifest(
	userDomain, name, owner, port, comment string,
) (
	manifest string,
	err error,
) {

	tcpInbound := TcpInbound{
		Name:       name,
		UserDomain: userDomain,
		Owner:      owner,
		Comment:    comment,
		Port:       port,
	}

	tmpl, err := template.New("tcpinbound").Parse(TcpInboundTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, tcpInbound)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

type TcpInbound struct {
	Name       string
	UserDomain string
	Owner      string
	Comment    string
	Port       string
}

const TcpInboundTemplate = `
{
	"cspec": "1.0",
	"comment": "{{.Comment}}",
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref": {
		"name": "{{.Name}}",
		"domain": "{{.UserDomain}}",
		"kind": "solution"
	},
	"links": [],
	"top": "{{.Name}}",
	"deployments": {
		"{{.Name}}": {
			"config": {
				"parameter": {
					"type": "tcp"
				},
				"resource": {
					"port": {
						"port": "{{.Port}}"
					}
				},
				"resilience": 0,
				"scale": {
					"detail": {}
				}
			},
			"name": "{{.Name}}",
			"meta": {},
			"up": null,
			"artifact": {
				"spec": [
					1,
					0
				],
				"ref": {
					"kind": "service",
					"domain": "kumori.systems",
					"module": "builtins/inbound",
					"name": "inbound",
					"version": [
						1,
						1,
						0
					]
				},
				"description": {
					"config": {
						"parameter": {
							"type": "tcp"
						},
						"resource": {
							"port": {
								"port": "{{.Port}}"
							}
						},
						"resilience": 0,
						"scale": {
							"detail": {}
						}
					},
					"srv": {
						"client": {
							"inbound": {
								"inherited": false,
								"protocol": "tcp"
							}
						},
						"server": {},
						"duplex": {}
					},
					"role": {},
					"builtin": true,
					"connector": {}
				}
			}
		}
	}
}
`
