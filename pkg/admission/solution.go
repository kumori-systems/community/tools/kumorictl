/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"errors"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/bundle"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

func GetSolutions() (solutions []string, err error) {
	solutions = []string{}
	list := admission("GET", "admission/solutions", nil, nil, nil, false).Path("data").ChildrenMap()
	for urn := range list {
		if SolutionCheckUrn(urn) {
			var domain, name string
			domain, name, err = SolutionDecomposeUrn(urn)
			if err != nil {
				return
			}
			solutions = append(solutions, domain+"/"+name)
		} else {
			logger.Warn("Solution URN check failed. URN: " + urn)
		}

	}
	sort.Strings(solutions)
	return
}

func GetSolution(userDomain, name string) (manifest string, err error) {
	urn := SolutionComposeUrn(userDomain, name)
	manifest, err = GetLegacyRegistry(urn)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			err = errors.New("Solution " + userDomain + "/" + name + " not found")
		}
	}
	return
}

func CreateSolution(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "CreateSolution"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterSolutionBundle(bundleZip)
	if err == nil {
		logger.Debug("Solution created", "urn", urn, "meth", meth)
	}
	return
}

func DeleteSolution(userDomain, name string, force bool, waitDuration time.Duration) (err error) {
	meth := "DeleteSolution"
	urn := SolutionComposeUrn(userDomain, name)
	deletedUrn, err := UndeploySolution(urn, force, waitDuration)
	// "deleteUrn" must be the same as "urn"
	if err == nil {
		logger.Debug("Solution deleted", "urn", deletedUrn, "meth", meth)
	}
	return
}

func DisableSolution(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "DisableSolution"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterSolutionBundle(bundleZip)
	if err == nil {
		logger.Debug("Solution disabled", "urn", urn, "meth", meth)
	}
	return
}

func DescribeSolution(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := SolutionComposeUrn(userDomain, name)
	return DescribeSolutionByUrn(urn)
}

func DescribeSolutionByUrn(
	urn string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	for _, child := range admission("GET", "admission/solutions", nil, nil, nil, false, "urn", urn).Path("data").ChildrenMap() {
		manifest = child.StringIndent("", "  ")
		manifestJSON = child
		return
	}
	err = fmt.Errorf("Solution \"" + urn + "\" not found")
	return
}

func DescribeSolutions(userDomain string) (solutions []*gabs.Container, err error) {
	meth := "DescribeSolutions"
	list := admission("GET", "admission/solutions", nil, nil, nil, false, "show", "extended").Path("data").ChildrenMap()
	solutions = make([]*gabs.Container, 0, len(list))
	for urn, solution := range list {
		if SolutionCheckUrn(urn) {
			continue
		}
		if userDomain == "" {
			solutions = append(solutions, solution)
			continue
		}
		if !solution.Exists("ref.domain") || solution.Search("ref.domain").Data() == nil {
			logger.Debug("Domain not found for solution", urn, "meth", meth)
			continue
		}

		domain := solution.Search("ref.domain").Data().(string)

		if domain == userDomain {
			solutions = append(solutions, solution)
		}

	}
	return solutions, nil
}

func GetSolutionRevisionList(
	userDomain, name string,
) (
	revisionListJSON *gabs.Container, err error,
) {
	/*
		List all revisions of a solution: GET /solutions/<urn>/revisions
		{
			"success": true,
			"message": "Solution revisions retrieved successfully.",
			"data": [
				{
					"comment": "My deploy - change value a",
					"timestamp": 1600336105136109300,
					"revision": 10,
					"name": "kd-091539-4cfb3eb5-559f856ff5"
				},
				{
					"comment": "My deploy - value a back to previous value",
					"timestamp": 1600336405088347100,
					"revision": 12,
					"name": "kd-091539-4cfb3eb5-559f856ff5"
				},
				{
					"comment": "My deploy - change value a again",
					"timestamp": 1600336105514005200,
					"revision": 11,
					"name": "kd-091539-4cfb3eb5-74c745f55b"
				}
			]
		}
	*/
	urn := SolutionComposeUrn(userDomain, name)
	url := "admission/solutions/" + url.QueryEscape(urn) + "/revisions"
	admissionResponse := admission("GET", url, nil, nil, nil, false)
	if admissionResponse.Path("success").Data().(bool) {
		revisionListJSON = admissionResponse.Path("data")
	} else {
		err = errors.New("Solution \"" + urn + "\" not found")
	}
	return
}

// revisionNumber = 0 means "get the previous revision"
func GetSolutionRevisionManifest(
	userDomain, name string, revisionNumber int,
) (
	manifestJSON *gabs.Container, err error,
) {
	/*
		Get a specific revisions of a solution: GET /solutions/<urn>/revisions/<revision_name>
		{
			"success": true,
			"message": "Solution revision retrieved successfully.",
			"data": {
				"name": "kd-091539-4cfb3eb5",
				"urn": "eslap://mydomain/solution/mysolution2",
				"comment": "My deploy - change value a"
				"owner": "juanjo@kumori.systems",
				"ref": {
					"name": "mysolution2",
					"kind": "solution",
					"domain": "mydomain"
				},
				"description": {
					[...]
				}
			}
		}
	*/

	// First, get the revision name related to the revision number... so we need
	// retrieve the list
	revisionListJSON, err := GetSolutionRevisionList(userDomain, name)
	if err != nil {
		return
	}
	if revisionListJSON == nil {
		err = fmt.Errorf("Revision list found")
		return
	}

	// If revisionNumber is 0, then the previous version must be used
	if revisionNumber == 0 {
		currentVersion := 0
		for _, item := range revisionListJSON.Children() {
			aux := int(item.Path("revision").Data().(float64))
			if aux > currentVersion {
				currentVersion = aux
			}
		}
		revisionNumber = currentVersion - 1
	}

	revisionName := ""
	for _, item := range revisionListJSON.Children() {
		if revisionNumber == int(item.Path("revision").Data().(float64)) {
			revisionName = item.Path("name").Data().(string)
			break
		}
	}

	if revisionName == "" {
		err = fmt.Errorf("Revision number not found")
		return
	}

	urn := SolutionComposeUrn(userDomain, name)
	url := "admission/solutions/" + url.QueryEscape(urn) + "/revisions/" + revisionName
	// fmt.Println(url)
	admissionResponse := admission("GET", url, nil, nil, nil, false)
	if admissionResponse.Path("success").Data().(bool) {
		manifestJSON = admissionResponse.Path("data")
	} else {
		err = fmt.Errorf("Revision %s:%d (%s) not found", urn, revisionNumber, revisionName)
	}
	return
}

func RollbackSolution(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "RollbackSolution"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterSolutionBundle(bundleZip)
	if err == nil {
		logger.Debug("Rollback done", "urn", urn, "meth", meth)
	}
	return
}

func ExistsSolution(userDomain, name string) (exists bool, err error) {
	_, err2 := GetSolution(userDomain, name)
	if err2 != nil {
		if strings.Contains(err2.Error(), "not found") {
			exists = false
		} else {
			err = err2
		}
	} else {
		exists = true
	}
	return
}

func SolutionCheckUrn(urn string) bool {
	return urnregexp.CheckSolutionUrn(urn)
}

func SolutionComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/solution/" + name
	return
}

func SolutionDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/solution/<NAME>"
	if !urnregexp.CheckSolutionUrn(urn) {
		err = errors.New("Solution URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/solution/")
	userDomain = urn[8:pos] // 8 is len("eslap://")
	name = urn[pos+10:]     // 10 is len("/solution/")
	if userDomain == "" || name == "" {
		err = errors.New("Solution URN expected format:" + expectedFormat)
		return
	}
	return
}

func SolutionURNToName(urn string) (longName string, err error) {
	domain, name, err := SolutionDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

// SolutionIsRunning return true if all the expected instances are running
// and ready.
//
// NOTE:
// We are currently not taking into account (in the case of UPDATE, ROLLBACK and
// DISABLE operations) that there are roles that are no longer part of the
// service.
// This function is not taking into account whether such instances have
// actually been removed.
// To achieve this level of detail, it is necessary that the platform returns
// information about them (it does not currently do so).
func SolutionIsReady(userDomain, name string) (bool, error) {
	meth := "SolutionIsReady"
	logger.Info("Checking solution", "userDomain", userDomain, "name", name, "meth", meth)

	// Retrieve the Solution description
	_, depJSON, err := DescribeSolution(userDomain, name)
	if err != nil {
		return false, err
	}

	// Get the list of deployments in the solution: if it is empty, then it is the
	// special case of a disabled solution.
	deployments := depJSON.Search("deployments")
	if deployments == nil {
		return true, nil
	}

	// Check if all deployments included in the solution are ready
	for deploymentName, deployment := range deployments.ChildrenMap() {
		logger.Info("Checking deployment", "Deployment", deploymentName, "meth", meth)
		ready, err := DeploymentIsReady(userDomain, deploymentName, deployment)
		if err != nil {
			return false, err
		}
		if !ready {
			return false, nil
		}
	}
	return true, nil
}

// waitSolutionToBeReady waits for solution (being created, updated, rollbacked,
// or disabled) to be ready: all its instances are ready
// Only v3dep are supported (nor httpinbound nor tcpinbound solutions)
func WaitSolutionToBeReady(userDomain, name string, waitDuration time.Duration) error {

	for start := time.Now(); time.Since(start) < waitDuration; {
		running, err := SolutionIsReady(userDomain, name)
		if err != nil {
			return err
		}
		if running {
			return nil
		}
		time.Sleep(5 * time.Second)
	}

	return fmt.Errorf("Timeout waiting solution to be ready")
}

// RestartSolutionRoleInstance restarts an instance of a given role. If forceDelete is set to true,
// the instance is deleted and created again instead of restarted. If an instance is not specified, all
// instances are restarted.
func RestartSolutionRoleInstance(
	userDomain string,
	name string,
	role string,
	instance string,
	forceDelete bool,
	waitDuration time.Duration,
) (err error) {
	meth := "RestartSolutionRole"

	// Path format: admission/solutions/:urn/roles/:role/innstances/:instance?
	urn := SolutionComposeUrn(userDomain, name)
	apiPath := "admission/solutions/" + url.QueryEscape(urn) + "/roles/" + url.QueryEscape(role) + "/instances"
	if instance != "" {
		apiPath = apiPath + "/" + instance
	}

	forceParam := "false"
	if forceDelete {
		forceParam = "yes"
	}

	params := []string{
		"forceDelete", forceParam,
	}

	admissionResponseJSON := admission("DELETE", apiPath, nil, nil, nil, false, params...)
	if admissionResponseJSON.Path("success").Data().(bool) {
		if waitDuration != util.ZERO_DURATION {
			// In an "restart" operation, the service is already "running".
			// Therefore, we wait a while for the changes to begin to take place.
			time.Sleep(20 * time.Second)
			err = WaitSolutionToBeReady(userDomain, name, waitDuration)
		}
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	// "deleteUrn" must be the same as "urn"
	if err == nil {
		if instance == "" {
			logger.Debug("Role restarted", "urn", urn, "role", role, "meth", meth)
		} else {
			logger.Debug("Role restarted", "urn", urn, "role", role, "instance", instance, "meth", meth)
		}
	}
	return
}
