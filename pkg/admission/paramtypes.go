/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/url"
	"os"
	"strings"
)

type FormData struct {
	Name     string
	FileName string
	FilePath string
}

type RequestParams struct {
	UseFormData    bool
	FormData       FormData
	UseFormParams  bool
	FormParams     map[string]string
	UseQueryParams bool
	QueryParams    map[string]string
	UseJSONPayload bool
	JSONPayload    []byte
}

func NewRequestParamsUsingFormData(
	name string, fileName string, filePath string,
) (
	requestParams RequestParams, err error,
) {
	requestParams = RequestParams{
		UseFormData: true,
		FormData: FormData{
			Name:     name,
			FileName: fileName,
			FilePath: filePath,
		},
		UseFormParams:  false,
		FormParams:     nil,
		UseQueryParams: false,
		QueryParams:    nil,
		UseJSONPayload: false,
		JSONPayload:    nil,
	}
	return
}

func NewRequestParamsUsingFormParams(
	params ...string,
) (
	requestParams RequestParams, err error,
) {
	formParams, err := arrayToStringMap(params)
	if err != nil {
		return
	}
	requestParams = RequestParams{
		UseFormData:    false,
		FormData:       FormData{},
		UseFormParams:  true,
		FormParams:     formParams,
		UseQueryParams: false,
		QueryParams:    nil,
		UseJSONPayload: false,
		JSONPayload:    nil,
	}
	return
}

func NewRequestParamsUsingQueryParams(
	params ...string,
) (
	requestParams RequestParams, err error,
) {
	queryParams, err := arrayToStringMap(params)
	if err != nil {
		return
	}
	requestParams = RequestParams{
		UseFormData:    false,
		FormData:       FormData{},
		UseFormParams:  false,
		FormParams:     nil,
		UseQueryParams: true,
		QueryParams:    queryParams,
		UseJSONPayload: false,
		JSONPayload:    nil,
	}
	return
}

func NewRequestParamsUsingJSONPayload(
	payload []byte,
) (
	requestParams RequestParams, err error,
) {
	requestParams = RequestParams{
		UseFormData:    false,
		FormData:       FormData{},
		UseFormParams:  false,
		FormParams:     nil,
		UseQueryParams: false,
		QueryParams:    nil,
		UseJSONPayload: true,
		JSONPayload:    payload,
	}
	return
}

func (rp RequestParams) GetBody() (body io.Reader, contentType string, err error) {
	if rp.UseFormData {
		reader, writer := io.Pipe()
		multipartWriter := multipart.NewWriter(writer)
		go func() { // Why goroutine? To avoid ram consumption in case of big files
			defer writer.Close()
			defer multipartWriter.Close()
			part, err := multipartWriter.CreateFormFile(rp.FormData.Name, rp.FormData.FileName)
			if err != nil {
				return
			}
			file, err := os.Open(rp.FormData.FilePath)
			if err != nil {
				return
			}
			defer file.Close()
			if _, err = io.Copy(part, file); err != nil {
				return
			}
		}()
		body = reader
		contentType = multipartWriter.FormDataContentType()
		return
	} else if rp.UseFormParams {
		formValue := url.Values{}
		for k, v := range rp.FormParams {
			formValue.Set(k, v)
		}
		body = strings.NewReader(formValue.Encode())
		contentType = "application/x-www-form-urlencoded"
	} else if rp.UseQueryParams {
		body = nil
		contentType = ""
	} else if rp.UseJSONPayload {
		body = strings.NewReader(string(rp.JSONPayload))
		contentType = "application/json"
	}
	return
}

func arrayToStringMap(paramsArray []string) (paramsMap map[string]string, err error) {
	if len(paramsArray)%2 != 0 {
		err = fmt.Errorf("Invalid number of parameteres")
		return
	}
	paramsMap = map[string]string{}
	for i := 0; i < len(paramsArray); i = i + 2 {
		paramsMap[paramsArray[i]] = paramsArray[i+1]
	}
	return
}
