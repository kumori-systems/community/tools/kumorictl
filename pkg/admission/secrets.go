/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
)

// Secret format
// {
//   "ref": {
//     "name": "mysecret",
//     "kind": "secret",
//     "domain": "juanjo.examples"
//   },
//   "description": {
//     "data": "..." // base64
//   }
// }

func GetSecrets(onlyInUse bool) (secrets []Resource, err error) {
	return GetResources(onlyInUse, SecretCheckUrn, SecretDecomposeUrn)
}

func GetSecret(userDomain, name string) (manifest string, err error) {
	urn := SecretComposeUrn(userDomain, name)
	if SecretCheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("Secret resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a secret resource one")
	}
	return
}

func CreateSecret(userDomain, name, owner, base64Content string) (err error) {
	meth := "CreateSecret"

	// Create a temporary file with the secret manifest
	manifest, err := secretManifest(userDomain, name, owner, base64Content)
	if err != nil {
		return
	}
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("Secret created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}
	return
}

func DeleteSecret(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeleteSecret"
	urn := SecretComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("Secret deleted", "urn", urn, "meth", meth)
	}
	return
}

func SecretCheckUrn(urn string) bool {
	return urnregexp.CheckSecretUrn(urn)
}

func SecretComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/secret/" + name
	return
}

func SecretDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/secret/<NAME>"
	if !urnregexp.CheckSecretUrn(urn) {
		err = errors.New("Secret URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/secret/")
	userDomain = urn[8:pos] // 8 is len("eslap://")
	name = urn[pos+8:]      // 8 is len("/secret/")
	if userDomain == "" || name == "" {
		err = errors.New("Secret URN expected format:" + expectedFormat)
		return
	}
	return
}

func SecretURNToName(urn string) (longName string, err error) {
	domain, name, err := SecretDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

func secretManifest(
	domain, name, owner string,
	secret string,
) (manifest string, err error) {
	secretobj := Secret{
		Name:       name,
		UserDomain: domain,
		Owner:      owner,
		Secret:     secret,
	}

	tmpl, err := template.New("secret").Parse(secretTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, secretobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribeSecret(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := SecretComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	secretDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if secretDetails == nil {
		err = fmt.Errorf("Secret \"" + urn + "\" not found")
		return
	}

	manifest = secretDetails.StringIndent("", "  ")
	manifestJSON = secretDetails

	return
}

type Secret struct {
	Name       string
	UserDomain string
	Owner      string
	Secret     string
}

const secretTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "secret",
		"domain": "{{.UserDomain}}"
	},
	"description" : {
		"data": "{{.Secret}}"
	}
}`
