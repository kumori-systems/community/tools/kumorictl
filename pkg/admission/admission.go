/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/sys/unix"
)

var admissionTimeout time.Duration
var admissionURL string
var admissionWSSchema string
var admissionWSHost string
var admissionCA []byte
var clientCert []byte
var clientKey []byte
var accessToken *string
var basicAuth *string
var insecure bool = false

func SetAdmissionTimeout(timeout string) (err error) {
	admissionTimeout, err = time.ParseDuration(timeout)
	return
}

func GetAdmissionTimeout() time.Duration {
	return admissionTimeout
}

func SetAdmissionURL(url string) {
	admissionURL = url
	if strings.HasPrefix(url, "https://") {
		admissionWSSchema = "wss"
		admissionWSHost = strings.TrimPrefix(url, "https://")
	} else {
		admissionWSSchema = "ws"
		admissionWSHost = strings.TrimPrefix(url, "http://")
	}
}

func GetAdmissionURL() string {
	return admissionURL
}

func SetAccessToken(token string) {
	accessToken = &token
}

func SetInsecure(insecureValue bool) {
	insecure = insecureValue
}

func GetAdmissionCA() []byte {
	return admissionCA
}

func SetAdmissionCA(ca []byte) {
	admissionCA = ca
}

func SetClientCert(cert []byte, key []byte) {
	clientCert = cert
	clientKey = key
}

func GetClientCert() (cert []byte, key []byte) {
	return clientCert, clientKey
}

// admission function will be deprecated. Use doRequest() function instead.
// admission() parameters are confusing, and currently doesnt allow a request
// with a JSON payload.
func admission(
	httpMethod, api string, formDataName, formDataFilename, formDataFilePath *string,
	paramsAsForm bool, params ...string,
) *gabs.Container {

	meth := "admission.admission"

	authMethod := GetAuthMethod()

	logger.Debug("Authentication", "authentication method", authMethod, "meth", meth)

	var httpClient *http.Client
	if insecure {
		// Create an HTTP client that doesn't verify SSL certificates
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient = &http.Client{
			Timeout:   admissionTimeout,
			Transport: tr,
		}
	} else {
		// Create a "normal" HTTP client
		httpClient = &http.Client{
			Timeout: admissionTimeout,
		}
	}

	// httpClient = connspy.NewClient(nil, nil)
	req, err := http.NewRequest(httpMethod, admissionURL+"/"+api, nil)
	if formDataFilePath != nil {
		reader, writer := io.Pipe()
		multipartWriter := multipart.NewWriter(writer)
		go func() {
			defer writer.Close()
			defer multipartWriter.Close()
			part, err := multipartWriter.CreateFormFile(*formDataName, *formDataFilename)
			if err != nil {
				logger.Fatal(err.Error())
			}
			file, err := os.Open(*formDataFilePath)
			if err != nil {
				logger.Fatal(err.Error())
			}
			defer file.Close()
			if _, err = io.Copy(part, file); err != nil {
				logger.Fatal(err.Error())
			}
		}()
		req, err = http.NewRequest(httpMethod, admissionURL+"/"+api, reader)
		req.Header.Set("Content-Type", multipartWriter.FormDataContentType())
	}
	if paramsAsForm {
		formData := url.Values{}
		for i := 0; i < len(params); {
			formData.Set(params[i], params[i+1])
			i += 2
		}
		req, err = http.NewRequest(httpMethod, admissionURL+"/"+api, strings.NewReader(formData.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	if err != nil {
		logger.Fatal(err.Error())
	}

	if (authMethod == TokenAuthMethod) && (accessToken != nil) {
		req.Header.Set("Authorization", "Bearer "+*accessToken)
	}

	// If admission is using MTLS based authentication mechanism, admisson CA and the client certificate and
	// key must be included in the TLS configuration of the http client transport.
	if authMethod == MTLSAuthMethod {
		transport, ok := httpClient.Transport.(*http.Transport)
		if !ok {
			transport = &http.Transport{}
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
		if len(admissionCA) > 0 {
			logger.Debug("Authentication", "Admission CA", admissionCA, "Method", meth)
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(admissionCA)
			transport.TLSClientConfig.RootCAs = caCertPool
		}

		logger.Debug("Authentication", "Certificate", clientCert, "Method", meth)

		certificate, err := tls.X509KeyPair(clientCert, clientKey)
		if err != nil {
			logger.Fatal(err.Error())
		}

		transport.TLSClientConfig.Certificates = []tls.Certificate{certificate}
		httpClient.Transport = transport
	}

	if basicAuth != nil {
		req.Header.Set("Authorization", "Basic "+*basicAuth)
	}

	if !paramsAsForm {
		q := req.URL.Query()
		for i := 0; i < len(params); {
			q.Add(params[i], params[i+1])
			i += 2
		}
		req.URL.RawQuery = q.Encode()
	}
	logger.Debug(httpMethod + " " + req.URL.String())
	resp, err := httpClient.Do(req)
	if err != nil {
		logger.Fatal("Admission request error: " + err.Error())
	}
	defer resp.Body.Close()
	responseRaw, err := io.ReadAll(resp.Body)
	if err != nil {
		logger.Fatal(err.Error())
	}
	logger.Debug(string(responseRaw))
	if resp.StatusCode != 200 {
		logger.Fatal(string(responseRaw))
	}

	responseJSONParsed, err := gabs.ParseJSON(responseRaw)
	if err != nil {
		logger.Fatal(err.Error())
	}
	return responseJSONParsed
}

// doRequest function will replace de old admission() function (with very complex
// parameters). doRequest() is used in kumorimgr project.
func doRequest(
	httpMethod string, path string, requestParams RequestParams,
) (
	resJSON *gabs.Container, err error,
) {
	meth := "admission.doRequest"
	logger.Debug("Requesting", "method", httpMethod, "path", path, "meth", meth)

	authMethod := GetAuthMethod()

	logger.Debug("Authentication", "authentication method", authMethod, "meth", meth)

	body, contentType, err := requestParams.GetBody()
	if err != nil {
		return
	}

	req, err := http.NewRequest(httpMethod, admissionURL+"/"+path, body)
	if err != nil {
		return
	}
	if contentType != "" {
		req.Header.Set("Content-Type", contentType)
	}

	var httpClient *http.Client
	if insecure {
		// Create an HTTP client that doesn't verify SSL certificates
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient = &http.Client{
			Timeout:   admissionTimeout,
			Transport: tr,
		}
	} else {
		// Create a "normal" HTTP client
		httpClient = &http.Client{
			Timeout: admissionTimeout,
		}
	}

	if (authMethod == TokenAuthMethod) && (accessToken != nil) {
		req.Header.Set("Authorization", "Bearer "+*accessToken)
	}

	// If admission is using MTLS based authentication mechanism, admisson CA and the client certificate and key
	// must be included in the TLS configuration of the http client transport.
	if authMethod == MTLSAuthMethod {
		transport, ok := httpClient.Transport.(*http.Transport)
		if !ok {
			transport = &http.Transport{}
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
		if len(admissionCA) > 0 {
			logger.Debug("Authentication", "Admission CA", admissionCA, "Method", meth)
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(admissionCA)
			transport.TLSClientConfig.RootCAs = caCertPool
		}

		logger.Debug("Authentication", "Certificate", clientCert, "Method", meth)

		certificate, err := tls.X509KeyPair(clientCert, clientKey)
		if err != nil {
			logger.Fatal(err.Error())
		}
		transport.TLSClientConfig.Certificates = []tls.Certificate{certificate}
		httpClient.Transport = transport
	}

	if basicAuth != nil {
		req.Header.Set("Authorization", "Basic "+*basicAuth)
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return
	}
	defer res.Body.Close()

	resRaw, err := io.ReadAll(res.Body)
	if err != nil {
		return
	}
	if res.StatusCode != 200 {
		err = fmt.Errorf(string(resRaw))
		return
	}
	resJSON, err = gabs.ParseJSON(resRaw)
	if err != nil {
		err = fmt.Errorf(string(resRaw))
		return
	}

	return
}

// analyzeResponseError analyzes response errors when response is like:
// { message: "bla bla bla", success: false, [...] }
func analyzeResponseError(response *gabs.Container) (err error) {
	success, found := response.Path("success").Data().(bool)
	if !found {
		err = fmt.Errorf("Unexpected result. Original: %s", response.String())
		return
	}
	if !success {
		message, found := response.Path("message").Data().(string)
		if !found {
			err = fmt.Errorf("Unexpected result. Original: %s", response.String())
			return
		}
		err = fmt.Errorf(message)
		return
	}
	return // No error!
}

func admissionStream(httpMethod, api string, paramsAsForm bool, params ...string) (stream io.ReadCloser, err error) {
	var httpClient *http.Client
	if insecure {
		// Create an HTTP client that doesn't verify SSL certificates
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient = &http.Client{
			Transport: tr,
		}
	} else {
		// Create a "normal" HTTP client
		httpClient = &http.Client{}
	}
	// httpClient = connspy.NewClient(nil, nil)
	req, err := http.NewRequest(httpMethod, admissionURL+"/"+api, nil)
	if paramsAsForm {
		formData := url.Values{}
		for i := 0; i < len(params); {
			formData.Set(params[i], params[i+1])
			i += 2
		}
		req, err = http.NewRequest(httpMethod, admissionURL+"/"+api, strings.NewReader(formData.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	if err != nil {
		logger.Fatal(err.Error())
	}
	/*
		req, err = http.NewRequest(httpMethod, "https://postman-echo.com/post", reader)
		if err != nil {
			logger.Fatal(err.Error())
		}
	*/
	authMethod := GetAuthMethod()
	if (authMethod == TokenAuthMethod) && (accessToken != nil) {
		req.Header.Set("Authorization", "Bearer "+*accessToken)
	}

	// If admission is using MTLS based authentication mechanism, admisson CA and the client certificate and key
	// must be included in the TLS configuration of the http client transport.
	if authMethod == MTLSAuthMethod {
		transport, ok := httpClient.Transport.(*http.Transport)
		if !ok {
			transport = &http.Transport{}
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
		if len(admissionCA) > 0 {
			logger.Debug("Authentication", "Admission CA", admissionCA)
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(admissionCA)
			transport.TLSClientConfig.RootCAs = caCertPool
		}

		logger.Debug("Authentication", "Certificate", clientCert)

		certificate, err := tls.X509KeyPair(clientCert, clientKey)
		if err != nil {
			logger.Fatal(err.Error())
		}
		transport.TLSClientConfig.Certificates = []tls.Certificate{certificate}
		httpClient.Transport = transport
	}

	if basicAuth != nil {
		req.Header.Set("Authorization", "Basic "+*basicAuth)
	}
	if !paramsAsForm {
		q := req.URL.Query()
		for i := 0; i < len(params); {
			q.Add(params[i], params[i+1])
			i += 2
		}
		req.URL.RawQuery = q.Encode()
	}
	logger.Debug(httpMethod + " " + req.URL.String())
	resp, err := httpClient.Do(req)
	if err != nil {
		logger.Debug(err.Error())
		logger.Fatal("Cannot contact Admission service. Please check network connectivity and/or configuration. If problem persists, please contact support.")
	}
	if resp.StatusCode != 200 {
		err = errors.New(strconv.Itoa(resp.StatusCode))
	}
	stream = resp.Body
	return
}

type ClusterConfiguration struct {
	ClusterVersion             string                              `json:"clusterVersion"`
	ClusterName                string                              `json:"clusterName"`
	ReferenceDomain            string                              `json:"referenceDomain"`
	IngressDomain              string                              `json:"ingressDomain"`
	AdmissionDomain            string                              `json:"admissionDomain"`
	KumorictlSupportedVersions []string                            `json:"kumorictlSupportedVersions"`
	KumorimgrSupportedVersions []string                            `json:"kumorimgrSupportedVersions"`
	VolumeTypes                *map[string]ClusterConfigVolumeType `json:"volumeTypes,omitempty"`
}

// ClusterConfigVolumeType contains the configuration of a volume type as provided in the cluster configuration
type ClusterConfigVolumeType struct {
	StorageClass string `json:"storageClass" yaml:"storageClass"`
	Properties   string `json:"properties" yaml:"properties"`
}

func GetClusterConfig() (clusterConfig *ClusterConfiguration, err error) {

	// The cluster configuration call not requires any authentication type.
	authMethod := admissionAuthMethod

	if authMethod != MTLSAuthMethod {
		SetAuthMethod(NoneAuthMethod)
	}

	reqParams, _ := NewRequestParamsUsingQueryParams()
	response, err := doRequest("GET", "admission/clusterconfig", reqParams)

	if authMethod != MTLSAuthMethod {
		// The original authentication system is restored
		SetAuthMethod(authMethod)
	}

	// If the request to Admission fails, the error is returned
	if err != nil {
		return nil, err
	}

	// If the request Admission doesnt fail, but its response describes an error,
	// then an error is returned
	if !response.Path("success").Data().(bool) {
		strError := "unknown"
		if response.Exists("message") {
			strError = response.Path("message").Data().(string)
		}
		err = errors.New(strError)
		return nil, err
	}

	clusterConfigStr, err := response.Path("data").MarshalJSON()
	if err != nil {
		return nil, err
	}
	clusterConfig = &ClusterConfiguration{}
	err = json.Unmarshal(clusterConfigStr, clusterConfig)

	return clusterConfig, err
}

func RegisterGenericBundle(bundleFilePath string) (admissionResponse string, admissionResponseJSON *gabs.Container) {
	bundleFilePath, err := filepath.Abs(bundleFilePath)
	if err != nil {
		logger.Fatal(err.Error())
	}
	formDataName := "bundlesZip"
	formDataFilename := "bundle.zip"
	admissionResponseJSON = admission("POST", "admission/bundles", &formDataName, &formDataFilename, &bundleFilePath, false)
	admissionResponse = admissionResponseJSON.StringIndent("", "  ")
	return
}

func RegisterResource(resourceFilePath string) (admissionResponse string, admissionResponseJSON *gabs.Container) {
	resourceFilePath, err := filepath.Abs(resourceFilePath)
	if err != nil {
		logger.Fatal(err.Error())
	}
	formDataName := "resourceManifest"
	formDataFilename := "resourceManifest.json"
	admissionResponseJSON = admission(
		"POST", "admission/resources",
		&formDataName, &formDataFilename, &resourceFilePath, false,
	)
	admissionResponse = admissionResponseJSON.StringIndent("", "  ")
	return
}

func UnregisterResource(urn string, waitDuration time.Duration) (err error) {
	apiPath := "admission/resources"
	url := apiPath + "/" + url.QueryEscape(urn)
	admissionResponseJSON := admission("DELETE", url, nil, nil, nil, false)
	if admissionResponseJSON.Path("success").Data().(bool) {
		if waitDuration != util.ZERO_DURATION {
			err = waitToBeRemoved(urn, waitDuration)
		}
		return
	}
	strError := "unknown"
	if admissionResponseJSON.Exists("message") {
		strError = admissionResponseJSON.Path("message").Data().(string)
	}
	err = errors.New(strError)
	return
}

func RegisterSolutionBundle(
	bundleFilePath string,
) (
	deploymentURN string, err error,
) {
	admissionResponse, admissionResponseJSON := RegisterGenericBundle(bundleFilePath)
	if admissionResponseJSON.Exists("data", "solutions") {
		if admissionResponseJSON.Exists("data", "solutions", "successful", "0") {
			deploymentURN = admissionResponseJSON.Search("data", "solutions", "successful", "0", "solutionURN").Data().(string)
			return
		}
		err = errors.New(admissionResponseJSON.Search("data", "solutions", "errors", "0", "message").Data().(string))
		return
	}
	if admissionResponseJSON.Path("success").Data().(bool) {
		fmt.Fprintln(os.Stderr, admissionResponse)
		err = errors.New("Registered bundle does not contain a solution. Unparsed Admission response written in stderr.")
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

func RegisterDeploymentBundle(
	bundleFilePath string,
) (
	deploymentURN string, err error,
) {
	admissionResponse, admissionResponseJSON := RegisterGenericBundle(bundleFilePath)
	if admissionResponseJSON.Exists("data", "deployments") {
		if admissionResponseJSON.Exists("data", "deployments", "successful", "0") {
			deploymentURN = admissionResponseJSON.Search("data", "deployments", "successful", "0", "deploymentURN").Data().(string)
			return
		}
		err = errors.New(admissionResponseJSON.Search("data", "deployments", "errors", "0", "message").Data().(string))
		return
	}
	if admissionResponseJSON.Path("success").Data().(bool) {
		fmt.Fprintln(os.Stderr, admissionResponse)
		err = errors.New("Registered bundle does not contain a deployment. Unparsed Admission response written in stderr.")
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

func Undeploy(urn string, force bool, waitDuration time.Duration) (deploymentURN string, err error) {
	forceParam := "false"
	if force {
		forceParam = "yes"
	}
	apiPath := "admission/deployments"
	admissionResponseJSON := admission("DELETE", apiPath, nil, nil, nil, false, "urn", urn, "force", forceParam)
	if admissionResponseJSON.Path("success").Data().(bool) {
		deploymentURN = admissionResponseJSON.Path("data").Data().(string)
		if waitDuration != util.ZERO_DURATION {
			err = waitToBeRemoved(urn, waitDuration)
		}
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

func UndeploySolution(urn string, force bool, waitDuration time.Duration) (solutionURN string, err error) {
	forceParam := "false"
	if force {
		forceParam = "yes"
	}
	apiPath := "admission/solutions"
	admissionResponseJSON := admission("DELETE", apiPath, nil, nil, nil, false, "urn", urn, "force", forceParam)
	if admissionResponseJSON.Path("success").Data().(bool) {
		solutionURN = admissionResponseJSON.Path("data").Data().(string)
		if waitDuration != util.ZERO_DURATION {
			err = waitToBeRemoved(urn, waitDuration)
		}
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

func Unregister(urn string, waitDuration time.Duration) (unregisteredURN string, err error) {
	apiPath := "admission/registries"
	admissionResponseJSON := admission("DELETE", apiPath, nil, nil, nil, false, "urn", urn)
	if admissionResponseJSON.Path("success").Data().(bool) {
		unregisteredURN = urn
		if waitDuration != util.ZERO_DURATION {
			err = waitToBeRemoved(urn, waitDuration)
		}
		return
	}
	if strings.Contains(admissionResponseJSON.Path("message").Data().(string), "unknown type") {
		err = errors.New("URN \"" + urn + "\" is not a valid one")
		return
	}
	if strings.Contains(admissionResponseJSON.Path("message").Data().(string), "not found") {
		err = errors.New("Element \"" + urn + "\" not found")
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

func GetResourceInUse(urn string) (state string, err error) {
	for key, resource := range admission("GET", "admission/resources", nil, nil, nil, false).Path("data").ChildrenMap() {
		if key == urn {
			return resource.StringIndent("", "  "), nil
		}
	}
	return "", errors.New("Resource \"" + urn + "\" not found")
}

func GetLegacyRegistries() (URNs []string) {
	registries := []string{}
	for urn := range admission("GET", "admission/registries", nil, nil, nil, false).Path("data").ChildrenMap() {
		if strings.HasPrefix(urn, "eslap://") {
			registries = append(registries, urn)
		}
	}
	sort.Strings(registries)
	return registries
}

func GetLegacyRegistry(urn string) (manifest string, err error) {
	admissionResponse := admission("GET", "admission/registries/"+url.QueryEscape(urn), nil, nil, nil, false)
	if admissionResponse.Path("success").Data().(bool) {
		return admissionResponse.Path("data").StringIndent("", "  "), nil
	}
	return "", errors.New("Registry \"" + urn + "\" not found")
}

func ContainerLogsStream(userDomain, name, role, instance, container string, follow bool, since int, tail uint, previous bool) (stream io.ReadCloser, err error) {
	params := []string{}
	if container != "" {
		params = append(params, "container", container)
	}
	if follow {
		params = append(params, "follow", "yes")
	}
	if previous {
		params = append(params, "previous", "yes")
	}
	if since > 0 {
		params = append(params, "since", strconv.Itoa(since))
	}
	if tail > 0 {
		params = append(params, "tail", strconv.Itoa(int(tail)))
	}

	urn := DeploymentComposeUrn(userDomain, name)

	apiPath := "admission/solutions/" + url.QueryEscape(urn) + "/roles/" + url.QueryEscape(role) + "/instances/" + url.QueryEscape(instance) + "/logs"
	stream, err = admissionStream("GET", apiPath, false, params...)

	if err != nil {
		if err.Error() == "504" {
			err = errors.New("Cannot fetch container logs: container not found")
		} else {
			err = errors.New("Cannot fetch container logs: error " + err.Error())
		}
	}
	return
}

func Exec(userDomain, name, role, instance, container string, tty bool, command ...string) error {
	params := []string{}
	if container != "" {
		params = append(params, "container", container)
	}
	if tty {
		params = append(params, "tty", "yes")
	}
	columns, rows, err := terminal.GetSize(int(os.Stdin.Fd()))
	if err != nil {
		return err
	}
	params = append(params, "rows", strconv.Itoa(rows), "columns", strconv.Itoa(columns))
	for _, commandParam := range command {
		params = append(params, "command", commandParam)
	}

	urn := DeploymentComposeUrn(userDomain, name)
	apiPath := "admission/solutions/" + url.QueryEscape(urn) + "/roles/" + url.QueryEscape(role) + "/instances/" + url.QueryEscape(instance) + "/exec"
	admissionResponse := admission("GET", apiPath, nil, nil, nil, false, params...)

	if admissionResponse.Path("success").Data().(bool) {
		params = append(params, "session_token", admissionResponse.Search("data", "wsSessionToken").Data().(string))
		if accessToken != nil {
			params = append(params, "access_token", *accessToken)
		}

		url := url.URL{Scheme: admissionWSSchema, Host: admissionWSHost, Path: "/" + apiPath}
		q := url.Query()
		for i := 0; i < len(params); {
			q.Add(params[i], params[i+1])
			i += 2
		}
		url.RawQuery = q.Encode()

		// If admission is using MTLS based authentication mechanism, admisson CA and the client certificate and key
		// must be included in the TLS configuration of the http client transport.
		authMethod := GetAuthMethod()
		if authMethod == MTLSAuthMethod {

			if websocket.DefaultDialer.TLSClientConfig == nil {
				websocket.DefaultDialer.TLSClientConfig = &tls.Config{}
			}

			// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
			if len(admissionCA) > 0 {
				caCertPool := x509.NewCertPool()
				caCertPool.AppendCertsFromPEM(admissionCA)
				websocket.DefaultDialer.TLSClientConfig.RootCAs = caCertPool
			}

			certificate, err := tls.X509KeyPair(clientCert, clientKey)
			if err != nil {
				logger.Fatal(err.Error())
			}
			websocket.DefaultDialer.TLSClientConfig.Certificates = []tls.Certificate{certificate}

		}

		wsConn, _, err := websocket.DefaultDialer.Dial(url.String(), nil)
		if err != nil {
			return errors.New("WS dial: " + err.Error())
		}
		defer wsConn.Close()

		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt)
		done := make(chan struct{})
		stdinReadChannel := make(chan []byte)

		if tty {
			oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
			if err != nil {
				return err
			}
			defer terminal.Restore(int(os.Stdin.Fd()), oldState)

			go func() {
				defer close(stdinReadChannel)
				var b []byte = make([]byte, 1)
				for {
					_, err := os.Stdin.Read(b)
					if err != nil {
						logger.Error(err.Error())
						return
					}
					stdinReadChannel <- []byte(string(b))
				}
			}()
		}

		go func() {
			defer close(done)
			for {
				_, message, err := wsConn.ReadMessage()
				if err != nil {
					logger.Debug("WS read: " + err.Error())
					return
				}
				if strings.HasPrefix(string(message), "OUT:") {
					os.Stdout.Write(message[4:])
				} else if strings.HasPrefix(string(message), "ERR:") {
					os.Stderr.Write(message[4:])
				} else {
					os.Stdout.Write(message)
				}
			}
		}()

		winch := make(chan os.Signal, 1)
		signal.Notify(winch, unix.SIGWINCH)
		defer signal.Stop(winch)

		for {
			select {
			case <-done:
				return nil
			case b, ok := <-stdinReadChannel:
				if !ok {
					return nil
				}
				err := wsConn.WriteMessage(websocket.TextMessage, b)
				if err != nil {
					return errors.New("WS write:" + err.Error())
				}
			case <-winch:
				columnsNew, rowsNew, err := terminal.GetSize(int(os.Stdin.Fd()))
				if err != nil {
					return err
				}
				if columnsNew != columns || rowsNew != rows {
					columns = columnsNew
					rows = rowsNew
					err = wsConn.WriteMessage(websocket.TextMessage, []byte("RESIZE:"+strconv.Itoa(rows)+","+strconv.Itoa(columns)))
					if err != nil {
						return errors.New("WS write:" + err.Error())
					}
				}
			case <-interrupt:
				// Cleanly close the connection by sending a close message and then
				// waiting (with timeout) for the server to close the connection.
				err := wsConn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
				if err != nil {
					return errors.New("WS write close:" + err.Error())
				}
				select {
				case <-done:
				case <-stdinReadChannel:
				case <-time.After(time.Second):
				}
				fmt.Println()
				return nil
			}
		}
	} else {
		return errors.New(admissionResponse.Path("message").Data().(string))
	}
}

func CheckAdmission() error {
	// This step assumes that the admission URL with an empty path can be accessed anonymously (with a simple HTTP or HTTPS connection).
	var httpClient *http.Client
	// Create an HTTP client that doesn't verify server certificates.
	if insecure {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient = &http.Client{
			Timeout:   admissionTimeout,
			Transport: tr,
		}
	} else {
		// Create a "normal" HTTP client
		httpClient = &http.Client{
			Timeout: admissionTimeout,
		}
	}

	authType := GetAuthMethod()

	if authType == MTLSAuthMethod {
		transport, ok := httpClient.Transport.(*http.Transport)
		if !ok {
			transport = &http.Transport{}
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		admissionCA := GetAdmissionCA()
		clientCert, clientKey := GetClientCert()

		// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
		if len(admissionCA) > 0 {
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(admissionCA)
			transport.TLSClientConfig.RootCAs = caCertPool
		}

		certificate, err := tls.X509KeyPair(clientCert, clientKey)
		if err != nil {
			logger.Fatal(err.Error())
		}
		transport.TLSClientConfig.Certificates = []tls.Certificate{certificate}
		httpClient.Transport = transport
	}

	_, err := httpClient.Get(admissionURL)
	if err != nil {
		return err
	}

	return nil
}

func createTmpManifestFile(
	contentStr string, optionalFileName ...string,
) (
	tmpManifestFile string, tmpDir string,
) {

	fileName := "Manifest.json" // default value
	if len(optionalFileName) > 0 {
		fileName = optionalFileName[0]
	}
	// fmt.Printf("\nManifest:\n%s\n", contentStr)

	content := []byte(contentStr)

	tmpDir, err := os.MkdirTemp("", "tmp")
	if err != nil {
		logger.Fatal(err.Error())
	}

	tmpManifestFile = filepath.Join(tmpDir, fileName)
	err = os.WriteFile(tmpManifestFile, content, 0644)
	if err != nil {
		logger.Fatal(err.Error())
	}

	return
}

// waitToBeRemoved waits for the object specified in the "urn" parameter to
// disappear, checking the admission/registries REST API path.
func waitToBeRemoved(urn string, waitDuration time.Duration) error {
	time.Sleep(1 * time.Second)
	for start := time.Now(); time.Since(start) < waitDuration; {
		_, err := GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				return nil
			} else {
				return err
			}
		}
		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("Timeout waiting element to be removed")
}
