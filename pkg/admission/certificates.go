/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// Certificate format (updated for v1.0.0):
// {
//   "ref": {
//     "name": "calccert.wildcard",
//     "kind": "certificate",
//     "domain": "juanjo.examples"
//   },
//   "description": {
//     "domain": "*.test.kumori.cloud",
//     "cert": "...",  // base64
//     "key": "..."    // base64
//   }
// }

func GetCertificates(onlyInUse bool) (certificates []Resource, err error) {

	return GetResources(onlyInUse, CertificateCheckUrn, CertificateDecomposeUrn)
}

func GetCertificate(userDomain, name string) (manifest string, err error) {
	urn := CertificateComposeUrn(userDomain, name)
	if CertificateCheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("Certificate resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a certificate resource one")
	}
	return
}

func CreateCertificate(
	userDomain, name, owner string,
	domain, certificateFilePath, privateKeyFilePath string,
	public bool,
) (
	err error,
) {
	meth := "CreateCertificate"

	certificate, err := util.ReadAsBase64(certificateFilePath)
	if err != nil {
		logger.Fatal(err.Error())
	}
	privateKey, err := util.ReadAsBase64(privateKeyFilePath)
	if err != nil {
		logger.Fatal(err.Error())
	}

	// Create a temporary file with the certificate manifest
	manifest, err := certificateManifest(userDomain, name, owner, domain, certificate, privateKey, public)
	if err != nil {
		return
	}
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("Certificate created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}
	return

}

func DeleteCertificate(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeleteCertificate"

	urn := CertificateComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("Certificate deleted", "urn", urn, "meth", meth)
	}
	return
}

func CertificateCheckUrn(urn string) bool {
	return urnregexp.CheckCertUrn(urn)
}

func CertificateComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/certificate/" + name
	return
}

func CertificateDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/certificate/<NAME>"
	if !urnregexp.CheckCertUrn(urn) {
		err = errors.New("Certificate URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/certificate/")
	userDomain = urn[8:pos] // 8 is len("eslap://"")
	name = urn[pos+13:]     // 13 is len("/certificate/")
	if userDomain == "" || name == "" {
		err = errors.New("Certificate URN expected format:" + expectedFormat)
		return
	}
	return
}

func CertificateURNToName(urn string) (longName string, err error) {
	domain, name, err := CertificateDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

func certificateManifest(
	userDomain, name, owner string,
	domain, certificate, privateKey string,
	public bool,
) (manifest string, err error) {

	certobj := Certificate{
		Name:        name,
		UserDomain:  userDomain,
		Domain:      domain,
		Owner:       owner,
		Certificate: certificate,
		PrivateKey:  privateKey,
		Public:      public,
	}

	tmpl, err := template.New("certificate").Parse(certificateTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, certobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribeCertificate(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := CertificateComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	certificateDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if certificateDetails == nil {
		err = fmt.Errorf("Certificate \"" + urn + "\" not found")
		return
	}

	manifest = certificateDetails.StringIndent("", "  ")
	manifestJSON = certificateDetails

	return
}

type Certificate struct {
	Name        string
	UserDomain  string
	Owner       string
	Domain      string
	Certificate string
	PrivateKey  string
	Public      bool
}

const certificateTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "certificate",
		"domain": "{{.UserDomain}}"
	},
	{{if .Public}}
	"public": {{.Public}},
	{{end}}
	"description" : {
		"domain": "{{.Domain}}",
		"cert": "{{.Certificate}}",
		"key": "{{.PrivateKey}}"
	}
}`
