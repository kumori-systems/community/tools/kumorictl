/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
)

// DOMAIn format:
// {
//   "ref": {
//     "name": "mydomain",
//     "kind": "domain",
//     "domain": "juanjo.examples"
//   },
//   "description": {
//     "domain": "mydomain.com"
//   }
// }

func GetDomains(onlyInUse bool) (domains []Resource, err error) {

	return GetResources(onlyInUse, DomainCheckUrn, DomainDecomposeUrn)
}

func GetDomain(userDomain, name string) (manifest string, err error) {
	urn := DomainComposeUrn(userDomain, name)
	if DomainCheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("Domain resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a domain resource one")
	}
	return
}

func CreateDomain(
	userDomain, name, owner, domain string,
) (
	err error,
) {
	meth := "CreateDomain"

	// Create a temporary file with the domain manifest
	manifest, err := domainManifest(userDomain, name, owner, domain)
	if err != nil {
		return
	}
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("Domain created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}
	return

}

func DeleteDomain(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeleteDomain"
	urn := DomainComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("Domain deleted", "urn", urn, "meth", meth)
	}
	return
}

func DomainCheckUrn(urn string) bool {
	return urnregexp.CheckDomainUrn(urn)
}

func DomainComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/domain/" + name
	return
}

func DomainDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/domain/<NAME>"
	if !urnregexp.CheckDomainUrn(urn) {
		err = errors.New("Domain URN expected format: " + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/domain/")
	userDomain = urn[8:pos] // 8 is len("eslap://"")
	name = urn[pos+8:]      // 8 is len("/domain/")
	if userDomain == "" || name == "" {
		err = errors.New("Domain URN expected format:" + expectedFormat)
		return
	}
	return
}

func DomainURNToName(urn string) (longName string, err error) {
	domain, name, err := DomainDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

func domainManifest(
	domain, name, owner string,
	domainName string,
) (manifest string, err error) {
	domainobj := Domain{
		Name:       name,
		UserDomain: domain,
		Domain:     domainName,
		Owner:      owner,
	}

	tmpl, err := template.New("domain").Parse(domainTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, domainobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribeDomain(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := DomainComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	domainDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if domainDetails == nil {
		err = fmt.Errorf("Domain \"" + urn + "\" not found")
		return
	}

	manifest = domainDetails.StringIndent("", "  ")
	manifestJSON = domainDetails

	return
}

type Domain struct {
	Name       string
	UserDomain string
	Owner      string
	Domain     string
}

const domainTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "domain",
		"domain": "{{.UserDomain}}"
	},
	"description" : {
		"domain": "{{.Domain}}"
	}
}`
