/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

func GetCAs(onlyInUse bool) (cas []Resource, err error) {

	return GetResources(onlyInUse, CACheckUrn, CADecomposeUrn)
}

func GetCA(userDomain, name string) (manifest string, err error) {
	urn := CAComposeUrn(userDomain, name)
	if CACheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("CA resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a ca resource one")
	}
	return
}

func CreateCA(
	userDomain, name, owner, caFilePath string,
) (
	err error,
) {
	meth := "CreateCA"

	caContent, err := util.ReadAsBase64(caFilePath)
	if err != nil {
		return
	}

	// Create a temporary file with the ca manifest
	manifest, err := caManifest(userDomain, name, owner, caContent)
	if err != nil {
		return
	}
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("CA created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}

	return
}

func DeleteCA(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeleteCA"
	urn := CAComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("CA deleted", "urn", urn, "meth", meth)
	}
	return
}

func CACheckUrn(urn string) bool {
	return urnregexp.CheckCAUrn(urn)
}

func CAComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/ca/" + name
	return
}

func CADecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/ca/<NAME>"
	if !urnregexp.CheckCAUrn(urn) {
		err = errors.New("CA URN expected format: " + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/ca/")
	userDomain = urn[8:pos] // 8 is len("eslap://"")
	name = urn[pos+4:]      // 7 is len("/ca/")
	if userDomain == "" || name == "" {
		err = errors.New("CA URN expected format:" + expectedFormat)
		return
	}
	return
}

func CAURNToName(urn string) (longName string, err error) {
	domain, name, err := CADecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

func caManifest(
	domain, name, owner string,
	ca string,
) (manifest string, err error) {
	caobj := CA{
		Name:       name,
		UserDomain: domain,
		Owner:      owner,
		CA:         ca,
	}

	tmpl, err := template.New("ca").Parse(caTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, caobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribeCA(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := CAComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	caDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if caDetails == nil {
		err = fmt.Errorf("CA \"" + urn + "\" not found")
		return
	}

	manifest = caDetails.StringIndent("", "  ")
	manifestJSON = caDetails

	return
}

type CA struct {
	Name       string
	UserDomain string
	Owner      string
	CA         string
}

const caTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "ca",
		"domain": "{{.UserDomain}}"
	},
	"description" : {
		"ca": "{{.CA}}"
	}
}`
