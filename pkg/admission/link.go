/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"strconv"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// CreateLink links a deployment with a http/tcp inbound
func CreateLink(
	userDomain1, name1, channel1 string,
	userDomain2, name2, channel2 string,
	owner string,
	meta string,
) (
	err error,
) {
	meth := "CreateLink"

	// Creates the link manifest
	manifest, err := linkManifest(userDomain1, name1, channel1, userDomain2, name2, channel2, owner, meta)
	if err != nil {
		return err
	}

	logger.Debug("Link manifest created", "manifest", manifest, "meth", meth)

	// Saves the link manifest in a temporary folder removed after finishing the operation
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest)
	defer os.RemoveAll(tmpDir)

	// Sends the link operation to admission
	formDataName := "linkManifest"
	formDataFilename := "Manifest.json"
	urn := ""
	admissionResponseJSON := admission(
		"POST", "admission/links", &formDataName, &formDataFilename, &tmpManifestFile, false,
	)

	// Checks admission result
	if admissionResponseJSON.Path("success").Data().(bool) {
		urn = admissionResponseJSON.Path("data").Data().(string)
	} else {
		err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	}
	if err == nil {
		logger.Debug("Link created", "urn", urn, "meth", meth)
	}

	return
}

// DeleteLink removes a previously created link
func DeleteLink(
	userDomain1, name1, channel1 string,
	userDomain2, name2, channel2 string,
	owner string,
	waitDuration time.Duration,
) (
	err error,
) {
	meth := "DeleteLink"

	// Creates the unlink manifest in a temporary folder removed after finishing the operation
	manifest, err := linkManifest(userDomain1, name1, channel1, userDomain2, name2, channel2, owner, "")

	// Stores the manifest in a temporary folder removed after finishing the operation
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest)
	defer os.RemoveAll(tmpDir)

	// Sends the unlink opration to admission
	formDataName := "linkManifest"
	formDataFilename := "Manifest.json"
	urn := ""
	admissionResponseJSON := admission(
		"DELETE", "admission/links", &formDataName, &formDataFilename, &tmpManifestFile, false,
	)

	// Checks admission result
	if admissionResponseJSON.Path("success").Data().(bool) {
		urn = admissionResponseJSON.Path("data").Data().(string)
		if waitDuration != util.ZERO_DURATION {
			err = waitLinkToBeRemoved(manifest, waitDuration)
		}
		if err == nil {
			logger.Debug("Link deleted", "urn", urn, "meth", meth)
		}
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	return
}

// LinkManifest creates a manifest to link/unlink a deployment
func linkManifest(
	userDomain1, name1, channel1 string,
	userDomain2, name2, channel2 string,
	owner string,
	meta string,
) (
	manifest string, err error,
) {

	link := Link{
		Owner:       owner,
		Meta:        meta,
		Name1:       name1,
		Kind1:       "solution",
		UserDomain1: userDomain1,
		Channel1:    channel1,
		Name2:       name2,
		Kind2:       "solution",
		UserDomain2: userDomain2,
		Channel2:    channel2,
	}

	tmpl, err := template.New("link").Parse(LinkTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, link)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

type Link struct {
	Owner       string
	Meta        string
	Name1       string
	Kind1       string
	UserDomain1 string
	Channel1    string
	Name2       string
	Kind2       string
	UserDomain2 string
	Channel2    string
}

const LinkTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	{{if .Meta}}
	"meta": {{.Meta}},
	{{end}}
	"endpoints": [
		{
			"name": "{{.Name1}}",
			"kind": "{{.Kind1}}",
			"domain": "{{.UserDomain1}}",
			"channel": "{{.Channel1}}"
		},
		{
			"name": "{{.Name2}}",
			"kind": "{{.Kind2}}",
			"domain": "{{.UserDomain2}}",
			"channel": "{{.Channel2}}"
		}
	]
}
`

// compareEndpoints compares two JSON (gabs) links
// Each link contains something like:
//
//	[
//	  {
//	    "name": "calcdep",
//	    "kind": "deployment",
//	    "domain": "kumori.examples",
//	    "channel": "service",
//	  },
//	  {
//	    "name": "calcinb",
//	    "kind": "httpinbound",
//	    "domain": "kumori.examples",
//	    "channel": "",
//	  }
//	]
//
// Returns "true" is the manifest and the JSON represents the same link.
// Returns an error if the JSON hasn't the expected format
func compareLinks(link1, link2 *gabs.Container) (equal bool, err error) {

	link1Endpoint1, err := generateLinkEndpointName(link1, 0) // 0: array index
	if err != nil {
		return
	}
	link1Endpoint2, err := generateLinkEndpointName(link1, 1) // 1: array index
	if err != nil {
		return
	}

	link2Endpoint1, err := generateLinkEndpointName(link2, 0)
	if err != nil {
		return
	}
	link2Endpoint2, err := generateLinkEndpointName(link2, 1)
	if err != nil {
		return
	}

	// Link order is not important (there is no "link direction")
	if (link1Endpoint1 == link2Endpoint1) && (link1Endpoint2 == link2Endpoint2) {
		return true, nil
	}
	if (link1Endpoint1 == link2Endpoint2) && (link1Endpoint2 == link2Endpoint1) {
		return true, nil
	}

	return false, nil
}

// generateLinkEndpointName generates a deterministic name using data of an
// endpoint of a link.
// Just: kind/domain/name/channel
// This name is not related with internal names in the platform: it is just used
// to compare link manifests in an easy way
func generateLinkEndpointName(link *gabs.Container, index int) (generatedName string, err error) {
	endpoint := link.Path(strconv.Itoa(index))
	if endpoint == nil {
		err = fmt.Errorf("Unexpected link endpoint content: %s", link.String())
		return
	}

	kind, found := endpoint.Path("kind").Data().(string)
	if !found {
		err = fmt.Errorf("Kind field not found in link endpoint content: %s", link.String())
		return
	}
	domain, found := endpoint.Path("domain").Data().(string)
	if !found {
		err = fmt.Errorf("Domain field not found in link endpoint content: %s", link.String())
		return
	}
	name, found := endpoint.Path("name").Data().(string)
	if !found {
		err = fmt.Errorf("Name field not found in link endpoint content: %s", link.String())
		return
	}
	channel, found := endpoint.Path("channel").Data().(string)
	if !found {
		err = fmt.Errorf("Channel field not found in link endpoint content: %s", link.String())
		return
	}

	generatedName = kind + "/" + domain + "/" + name + "/" + channel
	return
}

// waitLinkToBeRemoved is equivalent to waitToBeremoved, but with an specific
// implementation for links. Links are not stored "as a manifest", so it is not
// available in the legacy registry
//
// The admission RESTAPI "admission/links" returns something like:
//
//	{
//	  "data": {
//	    "kl-xxx": {
//	      "endpoints": [{
//	        "domain": "my.examples",
//	        "name": "helloworlddep"
//	        "channel": "hello",
//	        "kind": "deployment",
//	        [...]
//	      },
//	      {
//	        "domain": "my.examples",
//	        "name": "helloworldinb"
//	        "channel": "",
//	        "kind": "httpinbound", (or "tcpinbound")
//	        [...]
//	      }],
//	      [...]
//	    }
//	    "kl-yyy": {
//	      [...]
//	    }
//	  },
//	  "message": "Successfully got list of links.",
//	  "success": true
//	}
func waitLinkToBeRemoved(manifest string, waitDuration time.Duration) error {

	// Convert received manifest to gabs JSON and get its endpoints
	manifestJson, err := gabs.ParseJSON([]byte(manifest))
	if err != nil {
		return err
	}
	receivedEndpoints := manifestJson.Path("endpoints")
	if receivedEndpoints == nil {
		return fmt.Errorf("Unexpected received link manifest: %s", manifestJson.String())
	}

	time.Sleep(1 * time.Second)
	for start := time.Now(); time.Since(start) < waitDuration; {

		// Retrieve current links from Admission
		reqParams, _ := NewRequestParamsUsingQueryParams() // No params
		jsonResponse, err := doRequest("GET", "admission/links", reqParams)
		if err != nil {
			return err
		}
		err = analyzeResponseError(jsonResponse) // checks "message" and "success" fields
		if err != nil {
			return err
		}
		currentLinks := jsonResponse.Path("data")
		if currentLinks == nil {
			return fmt.Errorf("Unexpected response link manifest: %s", jsonResponse.String())
		}

		// Iterate over current links
		removed := true
		for _, link := range currentLinks.ChildrenMap() {
			retrievedEndpoints := link.Path("endpoints")
			if retrievedEndpoints == nil {
				continue
			}
			equal, err := compareLinks(receivedEndpoints, retrievedEndpoints)
			if err != nil {
				return err
			}
			if equal {
				removed = false
				break
			}
		}
		if removed {
			return nil
		}

		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("Timeout waiting element to be removed")
}
