/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"bytes"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
)

// GetPorts gets the use ports
// Port format
//
//	{
//	  "ref": {
//	    "name": "myport",
//	    "kind": "port",
//	    "domain": "juanjo.examples"
//	  },
//	  "description": {
//	    "data": "..." // base64
//	  }
//	}
func GetPorts(onlyInUse bool) (ports []Resource, err error) {
	return GetResources(onlyInUse, PortCheckUrn, PortDecomposeUrn)
}

// GetPort gets a user port given a domain and name
func GetPort(userDomain, name string) (manifest string, err error) {
	urn := PortComposeUrn(userDomain, name)
	if PortCheckUrn(urn) {
		manifest, err = GetLegacyRegistry(urn)
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				err = errors.New("Port resource \"" + urn + "\" not found")
			}
		}
	} else {
		err = errors.New("URN \"" + urn + "\" is not a port resource one")
	}
	return
}

// CreatePort creates a port given a domain, name and port number
func CreatePort(
	userDomain,
	name string,
	owner string,
	port int,
) (err error) {
	meth := "CreatePort"

	// Create a temporary file with the port manifest
	manifest, err := portManifest(userDomain, name, owner, port)
	tmpManifestFile, tmpDir := createTmpManifestFile(manifest, "resourceManifest.json")
	if err != nil {
		return
	}
	defer os.RemoveAll(tmpDir)

	// Send it to to Admission
	_, jsonRes := RegisterResource(tmpManifestFile)

	if jsonRes.Path("success").Data().(bool) {
		createdUrn := ""
		if jsonRes.Exists("data") {
			createdUrn = jsonRes.Path("data").Data().(string)
		}
		logger.Debug("Port created", "urn", createdUrn, "meth", meth)
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}
	return
}

// DeletePort deletes an exising port
func DeletePort(userDomain, name string, waitDuration time.Duration) (err error) {
	meth := "DeletePort"
	urn := PortComposeUrn(userDomain, name)
	err = UnregisterResource(urn, waitDuration)
	if err == nil {
		logger.Debug("Port deleted", "urn", urn, "meth", meth)
	}
	return
}

// PortCheckUrn checks if the given URN is a valid port URN.
func PortCheckUrn(urn string) bool {
	return urnregexp.CheckPortUrn(urn)
}

// PortComposeUrn creates a valid port URN for a given port domain and name
func PortComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/port/" + name
	return
}

// PortDecomposeUrn extracts a port domain and name from a given URN
func PortDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/port/<NAME>"
	if !urnregexp.CheckPortUrn(urn) {
		err = errors.New("Port URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/port/")
	userDomain = urn[8:pos] // 8 is len("eslap://")
	name = urn[pos+6:]      // 6 is len("/port/")
	if userDomain == "" || name == "" {
		err = errors.New("Port URN expected format:" + expectedFormat)
		return
	}
	return
}

// PortURNToName gets the port long name (domain/name) from a given URN
func PortURNToName(urn string) (longName string, err error) {
	domain, name, err := PortDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

// portManifest creates the KukuPort manifest to register a Port
func portManifest(
	domain, name, owner string,
	port int,
) (manifest string, err error) {
	stringPort := strconv.Itoa(port)

	portobj := Port{
		Name:       name,
		UserDomain: domain,
		Owner:      owner,
		Port:       stringPort,
	}

	tmpl, err := template.New("port").Parse(portTemplate)
	if err != nil {
		return
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, portobj)
	if err != nil {
		return
	}

	manifest = buf.String()

	return
}

func DescribePort(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := PortComposeUrn(userDomain, name)
	path := "admission/resources/" + url.QueryEscape(urn) + "/details"

	portDetails := admission("GET", path, nil, nil, nil, false).Path("data")

	if portDetails == nil {
		err = fmt.Errorf("Port \"" + urn + "\" not found")
		return
	}

	manifest = portDetails.StringIndent("", "  ")
	manifestJSON = portDetails

	return
}

type Port struct {
	Name       string
	UserDomain string
	Owner      string
	Port       string
}

const portTemplate = `
{
	{{if .Owner}}
	"owner": "{{.Owner}}",
	{{end}}
	"ref" : {
		"name": "{{.Name}}",
		"kind": "port",
		"domain": "{{.UserDomain}}"
	},
	"description" : {
		"port": {{.Port}}
	}
}`
