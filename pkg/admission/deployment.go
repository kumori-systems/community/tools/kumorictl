/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"errors"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"gitlab.com/kumori/kumorictl/pkg/bundle"
	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/urnregexp"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

func GetDeployments() (deployments []string, err error) {
	deployments = []string{}
	list := admission("GET", "admission/deployments", nil, nil, nil, false).Path("data").ChildrenMap()
	for urn := range list {
		if DeploymentCheckUrn(urn) {
			var domain, name string
			domain, name, err = DeploymentDecomposeUrn(urn)
			if err != nil {
				return
			}
			deployments = append(deployments, domain+"/"+name)
		} else {
			logger.Warn("Deployment URN check failed. URN: " + urn)
		}

	}
	sort.Strings(deployments)
	return
}

func GetDeployment(userDomain, name string) (manifest string, err error) {
	urn := DeploymentComposeUrn(userDomain, name)
	manifest, err = GetLegacyRegistry(urn)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			err = errors.New("Deployment " + userDomain + "/" + name + " not found")
		}
	}
	return
}

func CreateDeployment(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "CreateDeployment"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterDeploymentBundle(bundleZip)
	if err == nil {
		logger.Debug("Deployment created", "urn", urn, "meth", meth)
	}
	return
}

func DeleteDeployment(userDomain, name string, force bool, waitDuration time.Duration) (err error) {
	meth := "DeleteDeployment"
	urn := DeploymentComposeUrn(userDomain, name)
	deletedUrn, err := Undeploy(urn, force, waitDuration)
	// "deleteUrn" must be the same as "urn"
	if err == nil {
		logger.Debug("Deployment deleted", "urn", deletedUrn, "meth", meth)
	}
	return
}

func DisableDeployment(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "DisableDeployment"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterDeploymentBundle(bundleZip)
	if err == nil {
		logger.Debug("Deployment disabled", "urn", urn, "meth", meth)
	}
	return
}

func DescribeDeployment(
	userDomain, name string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	urn := DeploymentComposeUrn(userDomain, name)
	return DescribeDeploymentByUrn(urn)
}

func DescribeDeploymentByUrn(
	urn string,
) (
	manifest string, manifestJSON *gabs.Container, err error,
) {
	for _, child := range admission("GET", "admission/deployments", nil, nil, nil, false, "urn", urn).Path("data").ChildrenMap() {
		manifest = child.StringIndent("", "  ")
		manifestJSON = child
		return
	}
	err = fmt.Errorf("Deployment \"" + urn + "\" not found")
	return
}

func DescribeDeployments(userDomain string) (deployments []*gabs.Container, err error) {
	meth := "DescribeDeployments"
	list := admission("GET", "admission/deployments", nil, nil, nil, false, "show", "extended").Path("data").ChildrenMap()
	deployments = make([]*gabs.Container, 0, len(list))
	for urn, deployment := range list {
		if DeploymentCheckUrn(urn) {
			continue
		}
		if userDomain == "" {
			deployments = append(deployments, deployment)
			continue
		}
		if !deployment.Exists("ref.domain") || deployment.Search("ref.domain").Data() == nil {
			logger.Debug("Domain not found for deployment", urn, "meth", meth)
			continue
		}

		domain := deployment.Search("ref.domain").Data().(string)

		if domain == userDomain {
			deployments = append(deployments, deployment)
		}

	}
	return deployments, nil
}

func GetDeploymentRevisionList(
	userDomain, name string,
) (
	revisionListJSON *gabs.Container, err error,
) {
	/*
		List all revisions of a deployment: GET /deployments/<urn>/revisions
		{
			"success": true,
			"message": "Deployment revisions retrieved successfully.",
			"data": [
				{
					"comment": "My deploy - change value a",
					"timestamp": 1600336105136109300,
					"revision": 10,
					"name": "kd-091539-4cfb3eb5-559f856ff5"
				},
				{
					"comment": "My deploy - value a back to previous value",
					"timestamp": 1600336405088347100,
					"revision": 12,
					"name": "kd-091539-4cfb3eb5-559f856ff5"
				},
				{
					"comment": "My deploy - change value a again",
					"timestamp": 1600336105514005200,
					"revision": 11,
					"name": "kd-091539-4cfb3eb5-74c745f55b"
				}
			]
		}
	*/
	urn := DeploymentComposeUrn(userDomain, name)
	url := "admission/deployments/" + url.QueryEscape(urn) + "/revisions"
	admissionResponse := admission("GET", url, nil, nil, nil, false)
	if admissionResponse.Path("success").Data().(bool) {
		revisionListJSON = admissionResponse.Path("data")
	} else {
		err = errors.New("Deployment \"" + urn + "\" not found")
	}
	return
}

// revisionNumber = 0 means "get the previous revision"
func GetDeploymentRevisionManifest(
	userDomain, name string, revisionNumber int,
) (
	manifestJSON *gabs.Container, err error,
) {
	/*
		Get a specific revisions of a deployment: GET /deployments/<urn>/revisions/<revision_name>
		{
			"success": true,
			"message": "Deployment revision retrieved successfully.",
			"data": {
				"name": "kd-091539-4cfb3eb5",
				"urn": "eslap://mydomain/deployment/mydeployment2",
				"comment": "My deploy - change value a"
				"owner": "juanjo@kumori.systems",
				"ref": {
					"name": "mydeployment2",
					"kind": "deployment",
					"domain": "mydomain"
				},
				"description": {
					[...]
				}
			}
		}
	*/

	// First, get the revision name related to the revision number... so we need
	// retrieve the list
	revisionListJSON, err := GetDeploymentRevisionList(userDomain, name)
	if err != nil {
		return
	}
	if revisionListJSON == nil {
		err = fmt.Errorf("Revision list found")
		return
	}

	// If revisionNumber is 0, then the previous version must be used
	if revisionNumber == 0 {
		currentVersion := 0
		for _, item := range revisionListJSON.Children() {
			aux := int(item.Path("revision").Data().(float64))
			if aux > currentVersion {
				currentVersion = aux
			}
		}
		revisionNumber = currentVersion - 1
	}

	revisionName := ""
	for _, item := range revisionListJSON.Children() {
		if revisionNumber == int(item.Path("revision").Data().(float64)) {
			revisionName = item.Path("name").Data().(string)
			break
		}
	}

	if revisionName == "" {
		err = fmt.Errorf("Revision number not found")
		return
	}

	urn := DeploymentComposeUrn(userDomain, name)
	url := "admission/deployments/" + url.QueryEscape(urn) + "/revisions/" + revisionName
	// fmt.Println(url)
	admissionResponse := admission("GET", url, nil, nil, nil, false)
	if admissionResponse.Path("success").Data().(bool) {
		manifestJSON = admissionResponse.Path("data")
	} else {
		err = fmt.Errorf("Revision %s:%d (%s) not found", urn, revisionNumber, revisionName)
	}
	return
}

func RollbackDeployment(
	manifestsDir string,
	tmpDir string,
) (
	err error,
) {
	meth := "RollbackDeployment"

	bundleZip := tmpDir + "/bundle.zip"

	err = bundle.ZipBundle(manifestsDir, bundleZip)
	if err != nil {
		return
	}

	urn, err := RegisterDeploymentBundle(bundleZip)
	if err == nil {
		logger.Debug("Rollback done", "urn", urn, "meth", meth)
	}
	return
}

func ExistsDeployment(userDomain, name string) (exists bool, err error) {
	_, err2 := GetDeployment(userDomain, name)
	if err2 != nil {
		if strings.Contains(err2.Error(), "not found") {
			exists = false
		} else {
			err = err2
		}
	} else {
		exists = true
	}
	return
}

func DeploymentCheckUrn(urn string) bool {
	return urnregexp.CheckDeploymentUrn(urn)
}

func DeploymentComposeUrn(userDomain, name string) (urn string) {
	urn = "eslap://" + userDomain + "/deployment/" + name
	return
}

func DeploymentDecomposeUrn(urn string) (userDomain, name string, err error) {
	expectedFormat := "eslap://<USERDOMAIN>/deployment/<NAME>"
	if !urnregexp.CheckDeploymentUrn(urn) {
		err = errors.New("Deployment URN expected format:" + expectedFormat)
		return
	}
	pos := strings.Index(urn, "/deployment/")
	userDomain = urn[8:pos] // 8 is len("eslap://")
	name = urn[pos+12:]     // 12 is len("/deployment/")
	if userDomain == "" || name == "" {
		err = errors.New("Deployment URN expected format:" + expectedFormat)
		return
	}
	return
}

func DeploymentURNToName(urn string) (longName string, err error) {
	domain, name, err := DeploymentDecomposeUrn(urn)
	if err == nil {
		longName = domain + "/" + name
	}
	return
}

// DeploymentIsRunning return true if all the expected instances are running
// and ready.
//
// NOTE:
// We are currently not taking into account (in the case of UPDATE, ROLLBACK and
// DISABLE operations) that there are roles that are no longer part of the
// service.
// This function is not taking into account whether such instances have
// actually been removed.
// To achieve this level of detail, it is necessary that the platform returns
// information about them (it does not currently do so).
func DeploymentIsReady(userDomain, name string, depJSON *gabs.Container) (bool, error) {
	meth := "DeploymentIsReady"
	logger.Info("Checking deployment", "userDomain", userDomain, "name", name, "meth", meth)

	// Roles map of the service
	roles := depJSON.Path("artifact.description.role")
	if roles != nil {
		// Iterate over the roles of the service
		for roleName, role := range roles.ChildrenMap() {

			logger.Info("Checking role", "roleName", roleName, "meth", meth)

			// Expected instances
			expectedInstances, found := role.Path("config.scale.hsize").Data().(float64)
			if !found {
				err := fmt.Errorf("Number of instances of role %s not found", roleName)
				return false, err
			}

			// Instances map of the role
			instances := role.Path("instances")
			// At the beggining of the deployment being created, role instances doesnt
			// exist
			if instances == nil {
				return false, nil
			}

			// Iterate over instances
			var readyInstances float64 // gabs-json works using float64 numbers
			readyInstances = 0
			notReadyInstances := 0
			terminatingInstances := 0
			for _, instance := range instances.ChildrenMap() {

				status, statusFound := instance.Path("status").Data().(string)
				ready, readyFound := instance.Path("ready").Data().(bool)
				deletionTimestamp, deletionTimestampFound := instance.Path("deletionTimestamp").Data().(string)

				if deletionTimestampFound && deletionTimestamp != "" {
					terminatingInstances++
				} else if statusFound && (status == "Running") &&
					readyFound && ready {
					readyInstances++
				} else {
					notReadyInstances++
				}
			}

			summary := fmt.Sprintf(
				"Expected:%f, ready:%f, notReady:%d, terminating:%d",
				expectedInstances, readyInstances, notReadyInstances, terminatingInstances,
			)
			logger.Info(summary, "userDomain", userDomain, "name", name, "meth", meth)

			if (terminatingInstances > 0) || (notReadyInstances > 0) || (expectedInstances > readyInstances) {
				return false, nil
			}
		}
	} else {
		logger.Info("Deployment has no roles", "userDomain", userDomain, "name", name, "meth", meth)
	}

	logger.Info("Deployment is ready", "userDomain", userDomain, "name", name, "meth", meth)
	return true, nil
}

// waitDeploymentToBeReady waits for deployment (being created, updated, rollbacked,
// or disabled) to be ready: all its instances are ready
// Only v3dep are supported (nor httpinbound nor tcpinbound deployments)
func WaitDeploymentToBeReady(userDomain, name string, waitDuration time.Duration) error {

	for start := time.Now(); time.Since(start) < waitDuration; {
		// Retrieve the Deployment description
		_, depJSON, err := DescribeDeployment(userDomain, name)
		if err != nil {
			return err
		}
		// Check if Deployment is ready
		running, err := DeploymentIsReady(userDomain, name, depJSON)
		if err != nil {
			return err
		}
		if running {
			return nil
		}
		time.Sleep(5 * time.Second)
	}

	return fmt.Errorf("Timeout waiting deployment to be ready")
}

// RestartDeploymentRoleInstance restarts an instance of a given role. If forceDelete is set to true,
// the instance is deleted and created again instead of restarted. If an instance is not specified, all
// instances are restarted.
func RestartDeploymentRoleInstance(
	userDomain string,
	name string,
	role string,
	instance string,
	forceDelete bool,
	waitDuration time.Duration,
) (err error) {
	meth := "RestartDeploymentRole"

	// Path format: admission/deployments/:urn/roles/:role/innstances/:instance?
	urn := DeploymentComposeUrn(userDomain, name)
	apiPath := "admission/solutions/" + url.QueryEscape(urn) + "/roles/" + url.QueryEscape(role) + "/instances"
	if instance != "" {
		apiPath = apiPath + "/" + instance
	}

	forceParam := "false"
	if forceDelete {
		forceParam = "yes"
	}

	params := []string{
		"forceDelete", forceParam,
	}

	admissionResponseJSON := admission("DELETE", apiPath, nil, nil, nil, false, params...)
	if admissionResponseJSON.Path("success").Data().(bool) {
		if waitDuration != util.ZERO_DURATION {
			// In an "restart" operation, the service is already "running".
			// Therefore, we wait a while for the changes to begin to take place.
			time.Sleep(20 * time.Second)
			err = WaitDeploymentToBeReady(userDomain, name, waitDuration)
		}
		return
	}
	err = errors.New(admissionResponseJSON.Path("message").Data().(string))
	// "deleteUrn" must be the same as "urn"
	if err == nil {
		if instance == "" {
			logger.Debug("Role restarted", "urn", urn, "role", role, "meth", meth)
		} else {
			logger.Debug("Role restarted", "urn", urn, "role", role, "instance", instance, "meth", meth)
		}
	}
	return
}
