/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"encoding/base64"
	"errors"
	"strings"
	"time"

	"gitlab.com/kumori/kumorictl/pkg/logger"
)

type AuthMethod string

const (
	MTLSAuthMethod    AuthMethod = "clientcertificate"
	TokenAuthMethod   AuthMethod = "token"
	NoneAuthMethod    AuthMethod = "none"
	UnknownAuthMethod AuthMethod = "unknown"
)

var admissionAuthMethod AuthMethod = UnknownAuthMethod

func IsAuthEnabled() (enabled bool, err error) {

	reqParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}

	response, err := doRequest("GET", "auth/login", reqParams)

	// If Admission authentication is disabled, Admission's response will be:
	//   {
	//      "message":"Authentication is currently disabled in Admission."
	//   }

	if err != nil {
		logger.Debug("Unable to verify if Admission authentication is enabled. Assuming it is enabled.", "error", err.Error())
		enabled = true
		return
	}

	if response.Exists("message") {
		msg := response.Path("message").Data().(string)
		if strings.Contains(msg, "currently disabled") {
			logger.Debug("Admission authentication is disabled")
			enabled = false
		} else {
			logger.Debug("Admission authentication is enabled")
			enabled = true
		}
	} else {
		logger.Debug("Unable to verify if Admission authentication is enabled. Assuming it is enabled. (No 'message' property)")
		enabled = true
	}

	return
}

func SetAuthMethod(authMethod AuthMethod) {
	admissionAuthMethod = authMethod
}

// GetAuthMethod returns the authentication method required by the configured admission server.
// Since currently admission is not declaring its authentication method, this funcion returns always MTLS.
func GetAuthMethod() AuthMethod {
	return admissionAuthMethod
}

func Login(
	user string, password string,
) (
	accessToken, accessTokenExpiryDate,
	refreshToken, refreshTokenExpiryDate string,
	err error,
) {

	encoded := base64.StdEncoding.EncodeToString([]byte(user + ":" + password))
	basicAuth = &encoded

	reqParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}

	// The login call not requires any previous authentication mechanism. Hence, none authentication
	// method is set
	authMethod := GetAuthMethod()
	SetAuthMethod(NoneAuthMethod)

	response, err := doRequest("GET", "auth/login", reqParams)

	// Sets the original authentication method
	SetAuthMethod(authMethod)

	if err != nil {
		return
	}
	if response.Exists("message") {
		err = errors.New(response.Path("message").Data().(string))
		return
	}

	// Values to be returned:

	accessToken = response.Path("access_token").Data().(string)

	expiresIn := int(response.Path("expires_in").Data().(float64))
	accessTokenExpiryDateBytes, _ := time.Now().Add(time.Duration(expiresIn) * time.Second).MarshalText()
	accessTokenExpiryDate = string(accessTokenExpiryDateBytes)

	refreshToken = response.Path("refresh_token").Data().(string)
	refreshExpiresIn := int(response.Path("refresh_expires_in").Data().(float64))
	if refreshExpiresIn == 0 {
		// Expiration date is unknown, use a date very far in the future (10 years)
		refreshTokenExpiryDateBytes, _ := time.Now().Add(10 * 365 * 24 * time.Hour).MarshalText()
		refreshTokenExpiryDate = string(refreshTokenExpiryDateBytes)
	} else {
		refreshTokenExpiryDateBytes, _ := time.Now().Add(time.Duration(refreshExpiresIn) * time.Second).MarshalText()
		refreshTokenExpiryDate = string(refreshTokenExpiryDateBytes)
	}

	return
}

func RefreshTokens(
	refreshToken string,
) (
	accessToken, accessTokenExpiryDate,
	newRefreshToken, refreshTokenExpiryDate string,
	err error,
) {
	reqParams, err := NewRequestParamsUsingFormParams(
		"grant_type", "refresh_token",
		"refresh_token", refreshToken,
	)
	if err != nil {
		return
	}

	response, err := doRequest("POST", "auth/tokens/refresh", reqParams)
	if err != nil {
		return
	}
	if response.Exists("message") {
		err = errors.New(response.Path("message").Data().(string))
		return
	}

	// Values to be returned:

	accessToken = response.Path("access_token").Data().(string)

	expiresIn := int(response.Path("expires_in").Data().(float64))
	accessTokenExpiryDateBytes, _ := time.Now().Add(time.Duration(expiresIn) * time.Second).MarshalText()
	accessTokenExpiryDate = string(accessTokenExpiryDateBytes)

	newRefreshToken = response.Path("refresh_token").Data().(string)

	refreshExpiresIn := int(response.Path("refresh_expires_in").Data().(float64))
	if refreshExpiresIn == 0 {
		// Expiration date is unknown, use a date very far in the future (10 years)
		refreshTokenExpiryDateBytes, _ := time.Now().Add(10 * 365 * 24 * time.Hour).MarshalText()
		refreshTokenExpiryDate = string(refreshTokenExpiryDateBytes)
	} else {
		refreshTokenExpiryDateBytes, _ := time.Now().Add(time.Duration(refreshExpiresIn) * time.Second).MarshalText()
		refreshTokenExpiryDate = string(refreshTokenExpiryDateBytes)
	}

	return
}

func ChangePassword(user string, oldpassword string, newpassword string) (err error) {

	jsonDataBytes := []byte(`{
	  "oldPassword": "` + oldpassword + `",
		"newPassword": "` + newpassword + `"
	}`)

	requestParams, err := NewRequestParamsUsingJSONPayload(jsonDataBytes)
	if err != nil {
		return
	}
	response, err := doRequest("PATCH", "admission/users/"+user+"/password", requestParams)
	if err != nil {
		return
	}

	err = analyzeResponseError(response)
	if err != nil {
		return
	}

	return
}
