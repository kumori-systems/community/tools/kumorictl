/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"errors"
	"fmt"
	"sort"
)

type Resource struct {
	URN      string
	LongName string
	Name     string
	Domain   string
	Owner    string
	Public   bool
}

func GetResources(
	inuse bool,
	checkURN func(urn string) bool,
	decomposeURN func(urn string) (string, string, error),
) (resources []Resource, err error) {
	resources = []Resource{}

	api := "admission/resources"

	if inuse {
		api = "admission/resources/inuse"
	}

	jsonRes := admission("GET", api, nil, nil, nil, false)
	if jsonRes.Path("success").Data().(bool) {
		// Admission returns an array of URNs
		for urn, value := range jsonRes.Path("data").ChildrenMap() {
			if checkURN(urn) {
				var domain, name string
				domain, name, err = decomposeURN(urn)
				if err != nil {
					return
				}
				ca := Resource{
					URN:      urn,
					Name:     name,
					Domain:   domain,
					LongName: fmt.Sprintf("%s/%s", domain, name),
				}
				if value.Exists("owner") {
					ca.Owner = value.Search("owner").Data().(string)
				}
				if value.Exists("public") {
					ca.Public = value.Search("public").Data().(bool)
				}
				resources = append(resources, ca)
			}
		}
		sort.SliceStable(resources, func(i, j int) bool {
			return resources[i].LongName < resources[j].LongName
		})
	} else {
		strError := "unknown"
		if jsonRes.Exists("message") {
			strError = jsonRes.Path("message").Data().(string)
		}
		err = errors.New(strError)
	}

	return

}
