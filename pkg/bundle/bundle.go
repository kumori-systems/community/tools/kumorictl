/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
package bundle

import (
	"archive/zip"
	"io/ioutil"
	"os"
	"path"
)

// func ZipBundle(fileOrDirToZip string, outputZipFilePath string) error {
// 	outputZipFileAbsolutePath, _ := filepath.Abs(outputZipFilePath)
// 	cmd := exec.Command("zip", "-FSr", outputZipFileAbsolutePath, filepath.Base(fileOrDirToZip))
// 	cmd.Dir = filepath.Dir(fileOrDirToZip)
// 	cmd.Stdout = os.Stderr
// 	cmd.Stderr = os.Stderr
// 	return cmd.Run()
// }

func ZipBundle(fileOrDirToZip string, outputZipFilePath string) error {
	outFile, err := os.Create(outputZipFilePath)
	if err != nil {
		return err
	}
	defer outFile.Close()

	writer := zip.NewWriter(outFile)
	defer writer.Close()

	fileInfo, err := os.Stat(fileOrDirToZip)
	if err != nil {
		return err
	}

	if fileInfo.IsDir() {
		pending := []string{""}
		for len(pending) > 0 {
			basepath := pending[0]
			dirpath := path.Join(fileOrDirToZip, basepath)
			pending = pending[1:]
			files, err := ioutil.ReadDir(dirpath)
			if err != nil {
				return err
			}
			for _, file := range files {
				newbasepath := path.Join(basepath, file.Name())
				if file.IsDir() {
					pending = append(pending, newbasepath)
				} else {
					srcpath := path.Join(fileOrDirToZip, newbasepath)
					err = writeFile(writer, srcpath, newbasepath)
					if err != nil {
						return err
					}
				}
			}
		}
	} else {
		err = writeFile(writer, fileOrDirToZip, fileInfo.Name())
		if err != nil {
			return err
		}
	}

	return nil
}

func writeFile(writer *zip.Writer, src string, dst string) (err error) {
	data, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}
	filewriter, err := writer.Create(dst)
	if err != nil {
		return err
	}
	_, err = filewriter.Write(data)
	if err != nil {
		return err
	}
	return
}
