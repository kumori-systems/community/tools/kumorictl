package model

Manifest :: {
  version: string,
	domain:  string,
	name:    string,
  kind:    string,
  fullname: domain + "/" + kind + "/" + name + "/" + version
  spec: _
  ...
}

Component :: Manifest & {
  kind: "component",
  spec: {
    image: string
  }
}

Service :: Manifest & {
  kind: "service",
  spec: {
    components: [...string]
  }
  internal: [...Component]
}

Deployment :: Manifest & {
  kind: "deployment",
  spec: {
    service: string
  },
  $service: Service
  $components: [...Component]
}