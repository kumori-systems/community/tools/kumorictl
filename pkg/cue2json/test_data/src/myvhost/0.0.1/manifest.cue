package myresource

import k "kumori.systems/kumori/model"

Manifest :: k.Manifest & {
  version: [0,0,1]
	domain:  "mydomain",
  kind: "resource/vhost"
	name:    "myresource",
  things: {
    domain: "users.mycompany.com"
  }
}

Manifest

