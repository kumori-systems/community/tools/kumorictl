package mybackend

import k "kumori.systems/kumori/model"

Manifest :: k.Component & {
  version: "0_0_1",
	domain:  "mydomain",
	name:    "mybackend",
  spec: {
    image: "dockerhub/mybackend"
  }
}

Manifest
