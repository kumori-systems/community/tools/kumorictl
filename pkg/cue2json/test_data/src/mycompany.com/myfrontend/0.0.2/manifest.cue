package myfrontend

import k "kumori.systems/kumori/model"

Manifest :: k.Component & {
  version: "0_0_2",
	domain:  "mydomain",
	name:    "myfrontend",
  spec: {
    image: "dockerhub/myfrontend"
  }
}

Manifest
