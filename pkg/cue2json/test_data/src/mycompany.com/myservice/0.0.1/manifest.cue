package myservice

import k "kumori.systems/kumori/model"
import fe "mycompany.com/myfrontend/0.0.2:myfrontend"
import be "mycompany.com/mybackend/0.0.1:mybackend"

Manifest :: k.Service & {
  version: "0_0_1",
	domain:  "mydomain",
	name:    "myservice",
  spec: {
    components: [
      "mydomain/component/myfrontend/0.0.2",
      "mydomain/component/mybackend/0.0.1"
    ]
  }
  internal: [fe.Manifest, be.Manifest]
}

Manifest

