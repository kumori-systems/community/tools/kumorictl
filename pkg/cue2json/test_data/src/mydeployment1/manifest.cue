package mydeployment

import k "kumori.systems/kumori/model"
import s "mycompany.com/myservice/0.0.1:myservice"

Manifest :: k.Deployment & {
  version: "0_0_1",
	domain:  "mydomain",
	name:    "mydeployment",
  spec: {
    service: "mydomain/service/myservice/0_0_1"
  }
  $service: s.Manifest
  $components: s.Manifest.internal
}

Manifest
