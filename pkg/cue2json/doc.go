/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package cue2json transform a cue source into a json destination.
//
// IMPORTANT: FOR NOW, cue tool must installed to use this package. In the future
// it will not be necessary.
//
// IMPORTANT: FOR NOW, cue module dependencies are not resolved; this package
// assumes that dependencies are already resolved. See notes below.
//
// EXAMPLE:
//
// Let's assume this source directory (with the dependencies resolved) and
// kumori.systems schemas:
//
// kumori.systems
// └── kumori
//
//	├── cue.mod
//	│   └── module.cue
//	└── model
//	    └── manifest.cue
//
// └── src
//    ├── mycompany.com
//    │   ├── mybackend
//    │   │   └── 0.0.1
//    │   │       ├── cue.mod
//    │   │       │   └── module.cue
//    │   │       └── manifest.cue
//    │   ├── myfrontend
//    │   │   └── 0.0.2
//    │   │       ├── cue.mod
//    │   │       │   └── module.cue
//    │   │       └── manifest.cue
//    │   └── myservice
//    │       └── 0.0.1
//    │           ├── cue.mod
//    │           │   └── module.cue
//    │           └── manifest.cue
//    ├── mydeployment1
//    │   ├── cue.mod
//    │   │   ├── module.cue
//    │   │   └── pkg
//    │   │       ├── kumori.systems -> /[...link...]/kumori.systems
//    │   │       └── mycompany.com -> /[...link...]/src/mycompany.com
//    │   └── manifest.cue
//    ├── mydeployment2
//    │   ├── cue.mod
//    │   │   ├── module.cue
//    │   │   └── pkg
//    │   │       ├── kumori.systems -> /[...link...]/kumori.systems
//    │   │       └── mycompany.com -> /[...link...]/src/mycompany.com
//    │   └── manifest.cue
//    └── myvhost
//        └── 0.0.1
//            ├── cue.mod
//            │   ├── module.cue
//            │   └── pkg
//            │       ├── kumori.systems -> /[...link...]/kumori.systems
//            │       └── mycompany.com -> /[...link...]/src/mycompany.com
//            └── manifest.cue
//
// We can use cuejson package to validate ("vet") the source, and to generate
// ("export") the json files.
// The destination directory will be created, if not exists.
//
// For example:
//
//	err = cue2json.Vet("./src")
//	if err != nil {
//		...
//	}
//
//	err = cue2json.Export("./src/mydeployment1", "./dst")
//	if err != nil {
//		...
//	}
//
// And the result will be:
//
//	└── dst
//			└── mydomain
//					├── component
//					│   ├── mybackend
//					│   │   └── 0_0_1
//					│   │       └── manifest.json
//					│   └── myfrontend
//					│       └── 0_0_2
//					│           └── manifest.json
//					├── deployment
//					│   └── mydeployment
//					│       └── 0_0_1
//					│           └── manifest.json
//					└── service
//							└── myservice
//									└── 0_0_1
//											└── manifest.json
//
// NOTE: about dependencies.
// this package assumes (FOR NOW) that dependencies are already resolved.
// Dependencies can be "resolved by hand", using symbolic links. For example:
// $ rm -rf ./src/mydeployment1/cue.mod/pkg
// $ mkdir ./src/mydeployment1/cue.mod/pkg
// $ ln -s $(pwd)/kumori.systems $(pwd)/src/mydeployment1/cue.mod/pkg/kumori.systems
// $ ln -s $(pwd)/src/mycompany.com $(pwd)/src/mydeployment1/cue.mod/pkg/mycompany.com
package cue2json
