/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cue2json

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/kumori/kumorictl/pkg/logger"
	"gitlab.com/kumori/kumorictl/pkg/util"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

// File permission
const filePerm = 0644
const dirPerm = 0755

// Current Kumori "official" CUE version is v0.4.2.
// But any patch (v0.4.x) is allowed when kumorictl is executed, assuming the
// risk of non-retrocompability
const supportedCueVersion = "v0.4"

// Command definitions
var versionCommand, vetCommand, exportCommand, importCommand string
var versionParams, vetParams, exportParams, importParams []string
var importValidExtension []string

func init() {
	versionParams = []string{"version"}
	vetParams = []string{"vet", "-c"}
	exportParams = []string{"export", "-e", "generateSolution", "."}
	importParams = []string{"import", "-f"}
	importValidExtension = []string{".json", ".jsonl", ".ldjson", ".yaml", ".yml"}
}

// Checks the version of CUE. Just the major-and-minor components of the
// (semantic) version are checked.
func CheckCueVersion(cuecmd string) (err error) {
	logger.Debug("Checking CUE version...")

	// The command returns the following format: "cue version v0.4.1 linux/amd64"
	cmd := exec.Command(cuecmd, versionParams...)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		logger.Debug("Unable to get CUE version", "err", err.Error())
		return
	}
	outputStr := string(output)
	outputParts := strings.Split(outputStr, " ")
	if len(outputParts) < 3 {
		err = fmt.Errorf("Unable to determine CUE version. (%s)", string(output))
		logger.Debug("CUE Version check failed: ", "err", err.Error())
		return
	}
	var currentCueVersion = outputParts[2]

	// Check if current CUE version (for example v0.4.2) is compatible with the
	// supported version (for example v0.4)
	if !strings.HasPrefix(currentCueVersion, supportedCueVersion) {
		err = fmt.Errorf("Wrong CUE version: %s (expected %s)", currentCueVersion, supportedCueVersion)
		logger.Debug("CUE Version check failed: ", "err", err.Error())
		return
	}

	logger.Debug("CUE version is OK.", "cueversion", currentCueVersion)
	return

}

// Vet executes the "cue vet" command in the source directory (see see doc.go).
// If an error occur, its description is included in the text of the error.
func Vet(cuecmd string, src string) (err error) {
	logger.Debug("Validating")

	logger.Debug("Changing workdir", "path", src)
	currentWorkDir, err := changeWorkDir(src)
	if err != nil {
		return
	}

	defer func() {
		logger.Debug("Returning to workdir", "path", currentWorkDir)
		_, err2 := changeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	cmd := exec.Command(cuecmd, vetParams...)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		logger.Debug("Validating failed", "err", err.Error())
		return
	}

	logger.Debug("Validating succesful", "output", string(output))
	return
}

// Export executes the "cue export" command in the source directory, process
// the result (json manifests for each element; see doc.go), and stores it in the
// destination directory.
// If destination directory not exists, it will be created.
// If an error occur, its description is included in the text of the error.
// deploymentPackage is the package name for the deployment module
func Export(cuecmd string, src string, dst string, deploymentPackage string) (err error) {
	logger.Debug("Exporting")

	err = CheckCueVersion(cuecmd)
	if err != nil {
		return
	}

	logger.Debug("Changing workdir", "path", src)
	currentWorkDir, err := changeWorkDir(src)
	if err != nil {
		return
	}

	// Temporary file to complete the user module with "generateDeployment" function
	// and more
	tmpFile, err := filepath.Abs("./tmp.cue")
	if err != nil {
		return
	}

	defer func() {
		logger.Debug("Removing temporary file")
		err2 := removeTemporaryFile(tmpFile)
		logger.Debug("Returning to workdir", "path", currentWorkDir)
		_, err2 = changeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	logger.Debug("Processing non CUE files")
	err = processNonCUEfiles(cuecmd, deploymentPackage)
	if err != nil {
		logger.Error("Processing non CUE files failed", "err", err.Error())
		return
	}

	// Temporary file to complete the user module with "generateDeployment" function
	// and more
	logger.Debug("Creating temporary file")
	err = createTemporaryFile(tmpFile, deploymentPackage)
	if err != nil {
		logger.Error("Creating temporary file", "err", err.Error())
		return
	}

	logger.Debug("Executing cue command:", exportCommand, exportParams)
	cmd := exec.Command(cuecmd, exportParams...)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		logger.Error("Exporting failed", "err", err.Error())
		return
	}

	logger.Debug("Returning to workdir", "path", currentWorkDir)
	_, err = changeWorkDir(currentWorkDir)
	if err != nil {
		return
	}

	// Makes sure the destination directory exists
	logger.Debug("Ensuring destination directory exists", "path", dst)
	err = os.MkdirAll(dst, os.ModeDir|dirPerm)
	if err != nil {
		logger.Error("Making destination directory", "err", err.Error())
		return
	}

	err = createFile(dst, output)
	if err != nil {
		logger.Error("Creating destination directory", "err", err.Error())
		return
	}

	logger.Debug("Exporting succesful")
	return
}

// changeWorkDir changes the workdir, and returns the current workdir
func changeWorkDir(newWorkDir string) (oldWorkDir string, err error) {
	oldWorkDir, err = os.Getwd()
	if err != nil {
		return
	}
	err = os.Chdir(newWorkDir)
	return
}

// createFile create a file (ensuring that the directory is created, if it does
// not exist, with the provided content)
func createFile(basePath string, content []byte) (err error) {
	err = os.MkdirAll(basePath, os.ModeDir|dirPerm)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(basePath+"/Manifest.json", content, filePerm)
	if err != nil {
		return
	}
	return
}

func processNonCUEfiles(cuecmd string, deploymentPackage string) (err error) {
	// About the "auto, *" parameters: we have found a strange behavior in the command
	// cue import, described in:
	// - https://gitlab.com/kumori/kv3/project-management/-/issues/489#note_535695952
	// - https://github.com/cuelang/cue/issues/850
	// A workaround to avoid this behaviour is specify "files" instead of
	// "directory" as source for cue import.

	// List of files in the current directory. Note: we cant use "*" with
	// os/exec commands.
	// See https://stackoverflow.com/questions/31467153/golang-failed-exec-command-that-works-in-terminal

	var filesToImport []string
	filesToImport, err = filepath.Glob("*")
	if err != nil {
		logger.Error("Importing failed", "err", err.Error())
		return
	}

	importParams = append(importParams, "-p", deploymentPackage)
	for _, file := range filesToImport {
		extension := filepath.Ext(file)
		if util.ContainsString(importValidExtension, extension) {
			importParams = append(importParams, file)
		}
	}

	logger.Debug("Executing cue import command:", importCommand, importParams)
	cmd := exec.Command(cuecmd, importParams...)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		logger.Error("Importing failed", "err", err.Error())
		return
	}
	return
}

func removeTemporaryFile(tmpFile string) error {
	return os.Remove(tmpFile)
}

func createTemporaryFile(tmpFile string, deploymentPackage string) error {
	content := "package " + deploymentPackage + "\n"
	content = content +
		`import k "kumori.systems/kumori"
#Deployment: k.#Deployment
generateSolution: {
(k.#Extract & { #p: deployment: #Deployment }) & k.#Solution
}`
	return os.WriteFile(tmpFile, []byte(content), filePerm)
}
