package diff

import (
	"bufio"
	"fmt"
	"os"
	"sort"

	"github.com/Jeffail/gabs/v2"
	diff "gitlab.com/kumori-systems/community/libraries/manifest-diff"
	"gitlab.com/kumori/kumorictl/pkg/response"
)

type convertFunction func(*gabs.Container, string) (string, error)
type keyElement struct {
	Description string
	Parameters  []int
	Path        []string
	Functions   map[int]convertFunction
	ShowValues  bool
	ShowUpdated bool
	ShowAdded   bool
	ShowRemoved bool
}

// Sets the pats we are interested in. Each element of this structure contains the following keys:
//   - Description: the description shown to user if this element is changed, added or removed.
//     Can include parameters as strings using "%s"
//   - Parameters: the elements in the path to be used as parameters in description. This is used
//     when the element in path is variable.
//   - Path: the path to check if it is updated, added or removes. The path is an arrat of keys
//     representing a key in the JSON structure. If it is an array, the element in the path is the
//     index in the array. If the empty string then any possible key will match.
//   - Functions: functions to be used to process a parameter before setting it in the description.
//     it is a map where the key is the index in the path of the element to be transformed and the
//     value is a transformation function with the signature `func(string) (string, error)`.
var simpleKeys = []keyElement{

	///////////////////////
	// SOLUTION ELEMENTS //
	///////////////////////

	// Configuration //
	{
		Description: "Owner",
		Parameters:  []int{},
		Path:        []string{"owner"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s configuration parameter '%s'",
		Parameters:  []int{1, 4},
		Path:        []string{"deployments", "", "config", "parameter", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s resource '%s'",
		Parameters:  []int{1, 4},
		Path:        []string{"deployments", "", "config", "resource", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s resilience",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "config", "resilience"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	///////////////////////
	// SERVICE ELEMENTS //
	///////////////////////

	// Service reference //
	{
		Description: "%s reference domain",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "ref", "domain"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s reference name",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "ref", "name"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s reference kind",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "ref", "kind"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s reference pre",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "ref", "pre"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s reference version",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "ref", "version"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Service builtin //
	{
		Description: "%s builtin",
		Parameters:  []int{1},
		Path:        []string{"deployments", "", "artifact", "description", "builtin"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Service connectors //
	{
		Description: "%s connector '%s'",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "connector", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Service channels //
	{
		Description: "%s server channel '%s' port",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "server", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s server channel '%s' number of ports",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "server", "", "portnum"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s server channel '%s' protocol",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "server", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s server channel '%s'",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "server", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s client channel '%s' protocol",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "client", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s client channel '%s'",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "client", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s duplex channel '%s' port",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "duplex", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s duplex channel '%s' number of ports",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "duplex", "", "portnum"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s duplex channel '%s' protocol",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "duplex", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s duplex channel '%s'",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "srv", "duplex", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	////////////////////
	// ROLES ELEMENTS //
	////////////////////

	// Role reference //
	{
		Description: "%s.%s reference domain",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "ref", "domain"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s reference name",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "ref", "name"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s reference kind",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "ref", "kind"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s reference pre",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "ref", "pre"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s reference version",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "ref", "version"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Role metadata //
	{
		Description: "%s.%s metadata",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "meta"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Role builtin //
	{
		Description: "%s.%s builtin",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "builtin"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Role channels //
	{
		Description: "%s.%s server channel '%s' port",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "server", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s server channel '%s' number of ports",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "server", "", "portnum"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s server channel '%s' protocol",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "server", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s server channel '%s'",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "server", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s client channel '%s' protocol",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "client", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s client channel '%s'",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "client", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s duplex channel '%s' port",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "duplex", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s duplex channel '%s' number of ports",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "duplex", "", "portnum"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s duplex channel '%s' protocol",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "duplex", "", "protocol"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s duplex channel '%s'",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "srv", "duplex", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Role configuration //
	{
		Description: "%s.%s configuration parameter '%s'",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "config", "parameter", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s resource '%s'",
		Parameters:  []int{1, 5, 10},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "config", "resource", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s resilience",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "config", "resilience"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s horizontal size",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "config", "scale", "hsize"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Role probes //
	{
		Description: "%s.%s container '%s' liveness probe",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "probe", "", "liveness"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' readiness probe",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "probe", "", "readiness"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' prometheus metrics probe",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "probe", "", "pmetrics"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' probes",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "probe", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s probes",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "probe"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},

	// Role size //
	{
		Description: "%s.%s bandwidth limit",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "size", "bandwidth"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s required bandwidth",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "size", "minbandwidth"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	// Removed to avoid duplicating changes. Currently, the role mincpu is automatically calculated by the service model from
	// containers mincpu. If a container mincpu is updates, the list of updates will contain two entries: the change related
	// to the container mincpu and the change related to role mincpu. This second change is not obvious for CUE writers.
	// {
	// 	Description: "%s.%s required cpu",
	// 	Parameters:  []int{1, 5},
	// 	Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "size", "mincpu"},
	// 	Functions:   map[int]convertFunction{},
	// 	ShowValues:  true,
	// 	ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	// },

	// Role profile //
	{
		Description: "%s.%s assigned iops intensive flag",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "profile", "iopsintensive"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s threadability",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "profile", "threadability"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	////////////////////////
	// CONTAINER ELEMENTS //
	////////////////////////

	// Init container //
	{
		Description: "%s.%s container '%s' init",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "init"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Conteiner image //
	{
		Description: "%s.%s container '%s' image hub",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "image", "hub", "name"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' image secret",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "image", "hub", "secret"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' image tag",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "image", "tag"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Size //
	{
		Description: "%s.%s container '%s' cpu limit",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "size", "cpu"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' required cpu",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "size", "mincpu"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' memory limit",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "size", "memory"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	// Mapping //
	{
		Description: "%s.%s container '%s' environment variable '%s' value",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "value"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s' secret",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "secret"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s' port",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s' domain",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "domain"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s' certificate",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "certificate"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s' CA",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", "", "ca"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' environment variable '%s'",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "env", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping secret",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "secret"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping port",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "port"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping domain",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "domain"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping certificate",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "certificate"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping CA",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "ca"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping mode",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "mode"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping reboot on update",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "rebootOnUpdate"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping data",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "data"},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping format",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "format"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping volume",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", "", "volumeref"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' file '%s' mapping",
		Parameters:  []int{1, 5, 9, 12},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "mapping", "filesystem", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' cmd",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "cmd"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' entrypoint",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "entrypoint"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s container '%s' user",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", "", "user"},
		Functions:   map[int]convertFunction{},
		ShowValues:  true,
		ShowUpdated: true, ShowAdded: true, ShowRemoved: true,
	},

	///////////////
	// SAFEGUARD //
	///////////////
	{
		Description: "%s.%s container '%s'",
		Parameters:  []int{1, 5, 9},
		Path:        []string{"deployments", "", "artifact", "description", "role", "", "artifact", "description", "code", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s.%s role",
		Parameters:  []int{1, 5},
		Path:        []string{"deployments", "", "artifact", "description", "role", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},
	{
		Description: "%s role",
		Parameters:  []int{1},
		Path:        []string{"deployments", ""},
		Functions:   map[int]convertFunction{},
		ShowValues:  false,
		ShowUpdated: false, ShowAdded: true, ShowRemoved: true,
	},
}

var skip = map[string]interface{}{
	"inherited": true,
}

type diffElement struct {
	Description string          `json:"description"`
	Current     *gabs.Container `json:"current,omitempty"`
	New         *gabs.Container `json:"new,omitempty"`
	Path        []string        `json:"path"`
}

// printUpdatedElements prints the changes to be applied if the solution is updated
// and waits for confirmation.
func PrintUpdatedElements(
	currentManifest *gabs.Container,
	newManifest *gabs.Container,
	assumeYes bool,
	output string,
) error {

	// This object is used to convert the role names from the manifest format to
	// the user format.
	// subdInfo, err := NewSubserviceInfo(
	// 	manifest.Path("description.service.description.subservice").String(),
	// )
	// if err != nil {
	// 	return err
	// }

	// Calculates the differences between the current revision of this service and
	// the new one we are going to set.
	upd, add, rm := diff.Diff(currentManifest, newManifest, skip)

	// Travels the elements modified, added and removed and checks if affect any of the
	// paths of interest. In that case, calculates the message to be shown to the user.
	updated := [][]string{}
	added := []string{}
	removed := []string{}

	// Stores the data in the response
	data := gabs.New()

	// Elements of interest updated
	if len(upd) > 0 {
		// content, _ := json.MarshalIndent(upd, "", "  ")
		// fmt.Printf("\n\n------->UPD:\n%s\n", string(content))
		for _, elem := range upd {
			for _, simpleKey := range simpleKeys {
				match := true
				realPath := make([]string, 0, len(simpleKey.Path))
				for i, elemPath := range simpleKey.Path {
					if (i >= len(elem)) || (elemPath != "" && elemPath != elem[i]) {
						match = false
						break
					}
					realPath = append(realPath, elem[i])
				}
				if match {
					var description string
					if (simpleKey.Parameters != nil) && (len(simpleKey.Parameters) > 0) {
						params := []interface{}{}
						for _, index := range simpleKey.Parameters {
							if convertFn, ok := simpleKey.Functions[index]; ok {
								converted, err := convertFn(currentManifest, elem[index])
								if err != nil {
									params = append(params, elem[index])
									continue
								} else {

								}
								params = append(params, converted)
							} else {
								params = append(params, elem[index])
							}
						}
						description = fmt.Sprintf(simpleKey.Description, params...)
					} else {
						description = simpleKey.Description
					}
					var lines []string
					diffelem := diffElement{
						Description: description,
						Path:        realPath,
					}
					if simpleKey.ShowValues {
						basePath := elem[0:len(simpleKey.Path)]
						currentData := currentManifest.Search(basePath...)
						newData := newManifest.Search(basePath...)
						currentValue := truncText(currentData.String(), 100)
						newValue := truncText(newData.String(), 100)
						diffelem.Current = currentData
						diffelem.New = newData
						lines = []string{
							description,
							fmt.Sprintf("Current: %s", currentValue),
							fmt.Sprintf("New: %s", newValue),
						}
					} else {
						lines = []string{description}
					}
					if simpleKey.ShowUpdated && !existsInList(data.Search("updated"), &diffelem) {
						updated = append(updated, lines)
						data.ArrayAppend(diffelem, "updated")
					}

					break
				}
			}
		}
	}

	// Elements of interest added. If the path of the element added fits perfectly in a
	// path of interest this path of interest is considered as "added". If a path of interest is
	// a prefix of the element added, then this path of interest is considered "updated". For
	// example, if ["description","service","role",""] is a path of interest and
	// ["description","service","role","worker"] is added, then ["description","service","role","worker"]
	// is  considered as "added". However, if the added element is
	// ["description","service","role","worker","description","configuration","parameter","myparam"] then
	// ["description","service","role","worker"] is considered "updated"
	if len(add) > 0 {
		for _, elem := range add {
			for _, simpleKey := range simpleKeys {
				match := true
				complete := false
				realPath := make([]string, 0, len(simpleKey.Path))
				for i, elemPath := range simpleKey.Path {
					if (i >= len(elem)) || (elemPath != "" && elemPath != elem[i]) {
						match = false
						break
					}
					realPath = append(realPath, elem[i])
					if i == (len(elem) - 1) {
						complete = true
					}
				}
				if match {
					if complete {
						if (simpleKey.Parameters != nil) && (len(simpleKey.Parameters) > 0) {
							params := []interface{}{}
							for _, index := range simpleKey.Parameters {
								if convertFn, ok := simpleKey.Functions[index]; ok {
									converted, err := convertFn(currentManifest, elem[index])
									if err != nil {
										params = append(params, elem[index])
										continue
									} else {

									}
									params = append(params, converted)
								} else {
									if len(elem) > index {
										params = append(params, elem[index])
									}
								}
							}
							if simpleKey.ShowAdded {
								diffelem := diffElement{
									Description: fmt.Sprintf(simpleKey.Description, params...),
									Path:        realPath,
								}
								if !existsInList(data.Search("added"), &diffelem) {
									added = append(added, diffelem.Description)
									data.ArrayAppend(diffelem, "added")
								}
							}
						} else {
							if simpleKey.ShowAdded {
								diffelem := diffElement{
									Description: simpleKey.Description,
									Path:        realPath,
								}
								if !existsInList(data.Search("added"), &diffelem) {
									added = append(added, simpleKey.Description)
									data.ArrayAppend(diffelem, "added")
								}
							}
						}
					} else {
						var description string
						if (simpleKey.Parameters != nil) && (len(simpleKey.Parameters) > 0) {
							params := []interface{}{}
							for _, index := range simpleKey.Parameters {
								if convertFn, ok := simpleKey.Functions[index]; ok {
									converted, err := convertFn(currentManifest, elem[index])
									if err != nil {
										params = append(params, elem[index])
										continue
									} else {

									}
									params = append(params, converted)
								} else {
									params = append(params, elem[index])
								}
							}
							description = fmt.Sprintf(simpleKey.Description, params...)
						} else {
							description = simpleKey.Description
						}
						var lines []string
						diffelem := diffElement{
							Description: description,
							Path:        realPath,
						}
						if simpleKey.ShowValues {
							basePath := elem[0:len(simpleKey.Path)]
							currentData := currentManifest.Search(basePath...)
							newData := newManifest.Search(basePath...)
							currentValue := truncText(currentData.String(), 100)
							newValue := truncText(newData.String(), 100)
							diffelem.Current = currentData
							diffelem.New = newData
							lines = []string{
								description,
								fmt.Sprintf("Current: %s", currentValue),
								fmt.Sprintf("New: %s", newValue),
							}

						} else {
							lines = []string{description}
						}
						if simpleKey.ShowUpdated && !existsInList(data.Search("updated"), &diffelem) {
							updated = append(updated, lines)
							data.ArrayAppend(diffelem, "updated")
						}
						break
					}
					break
				}
			}
		}
	}

	// Elements of interest removed. If the path of the element removed fits perfectly in a
	// path of interest this path of interest is considered as "removed". If a path of interest is
	// a prefix of the element removed, then this path of interest is considered "updated". For
	// example, if ["description","service","role",""] is a path of interest and
	// ["description","service","role","worker"] is removed, then ["description","service","role","worker"]
	// is  considered as "removed". However, if the removed element is
	// ["description","service","role","worker","description","configuration","parameter","myparam"] then
	// ["description","service","role","worker"] is considered "updated"
	if len(rm) > 0 {
		for _, elem := range rm {
			for _, simpleKey := range simpleKeys {
				match := true
				complete := false
				realPath := make([]string, 0, len(simpleKey.Path))
				for i, elemPath := range simpleKey.Path {
					if (i >= len(elem)) || (elemPath != "" && elemPath != elem[i]) {
						match = false
						break
					}
					realPath = append(realPath, elem[i])
					if i == (len(elem) - 1) {
						complete = true
					}
				}
				if match {
					if complete {
						if (simpleKey.Parameters != nil) && (len(simpleKey.Parameters) > 0) {
							params := []interface{}{}
							for _, index := range simpleKey.Parameters {
								if convertFn, ok := simpleKey.Functions[index]; ok {
									converted, err := convertFn(currentManifest, elem[index])
									if err != nil {
										params = append(params, elem[index])
										continue
									} else {

									}
									params = append(params, converted)
								} else {
									params = append(params, elem[index])
								}
							}
							if simpleKey.ShowRemoved {
								diffelem := diffElement{
									Description: fmt.Sprintf(simpleKey.Description, params...),
									Path:        realPath,
								}
								if !existsInList(data.Search("removed"), &diffelem) {
									removed = append(removed, diffelem.Description)
									data.ArrayAppend(diffelem, "removed")
								}
							}
						} else {
							if simpleKey.ShowRemoved {
								diffelem := diffElement{
									Description: simpleKey.Description,
									Path:        realPath,
								}
								if !existsInList(data.Search("removed"), &diffelem) {
									removed = append(removed, simpleKey.Description)
									data.ArrayAppend(diffelem, "removed")
								}
							}
						}
					} else {
						var description string
						if (simpleKey.Parameters != nil) && (len(simpleKey.Parameters) > 0) {
							params := []interface{}{}
							for _, index := range simpleKey.Parameters {
								if convertFn, ok := simpleKey.Functions[index]; ok {
									converted, err := convertFn(currentManifest, elem[index])
									if err != nil {
										params = append(params, elem[index])
										continue
									} else {

									}
									params = append(params, converted)
								} else {
									params = append(params, elem[index])
								}
							}
							description = fmt.Sprintf(simpleKey.Description, params...)
						} else {
							description = simpleKey.Description
						}
						var lines []string
						diffelem := diffElement{
							Description: description,
							Path:        realPath,
						}
						if simpleKey.ShowValues {
							basePath := elem[0:len(simpleKey.Path)]
							currentData := currentManifest.Search(basePath...)
							newData := newManifest.Search(basePath...)
							currentValue := truncText(currentData.String(), 100)
							newValue := truncText(newData.String(), 100)
							diffelem.Current = currentData
							diffelem.New = newData
							lines = []string{
								description,
								fmt.Sprintf("Current: %s", currentValue),
								fmt.Sprintf("New: %s", newValue),
							}

						} else {
							lines = []string{description}
						}
						if simpleKey.ShowUpdated && !existsInList(data.Search("updated"), &diffelem) {
							updated = append(updated, lines)
							data.ArrayAppend(diffelem, "updated")
						}
						break

					}
					break
				}
			}
		}
	}
	response.SetData(data)

	// Prints the elements updated, added and removed in a table format
	if output != "json" {
		if (len(updated) > 0) || (len(added) > 0) || (len(removed) > 0) {
			// sort.Strings(updated)
			sort.SliceStable(updated, func(i, j int) bool {
				return updated[i][0] < updated[j][0]
			})
			sort.Strings(added)
			sort.Strings(removed)
			fmt.Println("")
			if len(updated) > 0 {
				fmt.Println("+---------------------------------------------------------------------")
				fmt.Println("| The following elements will be updated:")
				// fmt.Println("+---------------------------------------------------------------------")
				shown := []string{}
				for _, elem := range updated {
					printed := false
					for _, alreadyPrinted := range shown {
						if alreadyPrinted == elem[0] {
							printed = true
						}
					}
					if !printed {
						fmt.Println("| * " + elem[0])
						if len(elem) > 1 {
							for _, line := range elem[1:] {
								fmt.Println("|     " + line)
							}
						}
						shown = append(shown, elem[0])
					}
				}
			}

			if len(added) > 0 {
				fmt.Println("+---------------------------------------------------------------------")
				fmt.Println("| The following elements will be added:")
				// fmt.Println("+---------------------------------------------------------------------")
				shown := []string{}
				for _, elem := range added {
					printed := false
					for _, alreadyPrinted := range shown {
						if alreadyPrinted == elem {
							printed = true
						}
					}
					if !printed {
						fmt.Println("| * " + elem)
						shown = append(shown, elem)
					}
				}
			}

			if len(removed) > 0 {
				fmt.Println("+---------------------------------------------------------------------")
				fmt.Println("| The following elements will be removed:")
				// fmt.Println("+---------------------------------------------------------------------")
				shown := []string{}
				for _, elem := range removed {
					printed := false
					for _, alreadyPrinted := range shown {
						if alreadyPrinted == elem {
							printed = true
						}
					}
					if !printed {
						fmt.Println("| * " + elem)
						shown = append(shown, elem)
					}
				}
			}

			fmt.Println("+---------------------------------------------------------------------")
			if !assumeYes {
				reader := bufio.NewReader(os.Stdin)
				fmt.Println("")
				fmt.Print("Press <ENTER> to continue or <CTRL-c> to abort")
				_, _ = reader.ReadString('\n')
			}
		} else {
			fmt.Println("")

			fmt.Println("No changes detected. Exiting")
			fmt.Println("")
			os.Exit(0)
		}

	}

	return nil
}

// truncText truncs the text and ads "..." if its length is bigger than maxlength
func truncText(text string, maxlength int) string {
	if len(text) < maxlength {
		return text
	}

	return fmt.Sprintf("%s...", text[0:(maxlength-3)])
}

func existsInList(data *gabs.Container, diffelem *diffElement) bool {

	if (data == nil) || (diffelem == nil) {
		return false
	}

	for _, elem := range data.Children() {

		delem := elem.Data().(diffElement)

		if delem.Description != diffelem.Description {
			continue
		}

		if len(delem.Path) != len(diffelem.Path) {
			continue
		}

		areEqual := true
		for i, step := range delem.Path {
			if step != diffelem.Path[i] {
				areEqual = false
				break
			}
		}

		if areEqual {
			return true
		}
	}

	return false
}
