/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package logger

import (
	"fmt"
	"os"
	"runtime"
	"strings"

	"gitlab.com/kumori/kumorictl/pkg/response"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	rotatory "gopkg.in/natefinch/lumberjack.v2" // Adds rotatory log feature
)

//
// EXAMPLE OF USE
//
//  import (
//  	"mymodule/pkg/logger"
//  )
//
//  func main() {
//
//    logger.Init(
//      "./test.log",  // filepath to write logs (rotatory style)
//      "debug",       // loglevel to use to write to file
//      os.Stderr,     // os.File to write logs (console)
//      "info"         // loglevel to use to write to console
//    )
//    defer logger.End()
//
//    [...]
//    logger.Info("Hello!")
//    [...]
//  }
//

var logger *zap.Logger
var sugarLogger *zap.SugaredLogger

// Module initialization
func init() {
	logger = nil
	sugarLogger = nil
}

//
// PUBLIC FUNCTIONS
//

// Init initializes the logger
// - file: filepath to write logs (rotatory style)
// - fileLevel: loglevel to use to write to file
// - console: os.Stderr or os.Stdout
// - consoleLevel: loglevel to use to write to console
func Init(file string, fileLevel string, console *os.File, consoleLevel string) {

	writerSyncer := getLogWriter(file)

	var core zapcore.Core
	if console != nil {
		core = zapcore.NewTee(
			zapcore.NewCore(getFileEncoder(false), writerSyncer, getLogLevel(fileLevel)),
			zapcore.NewCore(getConsoleEncoder(), console, getLogLevel(consoleLevel)),
		)
	} else {
		core = zapcore.NewTee(
			zapcore.NewCore(getFileEncoder(false), writerSyncer, getLogLevel(fileLevel)),
		)
	}

	logger = zap.New(core)
	sugarLogger = logger.Sugar()
}

func End() {
	logger.Sync()
}

func IsInit() bool {
	return sugarLogger != nil
}

// Special use from kumorictl
func IdentifyPanic() string {
	var name, file string
	var line int
	var pc [16]uintptr

	n := runtime.Callers(3, pc[:])
	for _, pc := range pc[:n] {
		fn := runtime.FuncForPC(pc)
		if fn == nil {
			continue
		}
		file, line = fn.FileLine(pc)
		name = fn.Name()
		if !strings.HasPrefix(name, "runtime.") {
			break
		}
	}

	switch {
	case name != "":
		return fmt.Sprintf("%v:%v", name, line)
	case file != "":
		return fmt.Sprintf("%v:%v", file, line)
	}

	return fmt.Sprintf("pc:%x", pc)
}

//
// Just renaming zap logger functions
//

func Info(message string, fields ...interface{}) {
	sugarLogger.Infow(message, fields...)
}

func Debug(message string, fields ...interface{}) {
	sugarLogger.Debugw(message, fields...)
}

func Warn(message string, fields ...interface{}) {
	sugarLogger.Warnw(message, fields...)
}

func Error(message string, fields ...interface{}) {
	sugarLogger.Errorw(message, fields...)
}

func Fatal(message string, fields ...interface{}) {
	response.AddError(response.FatalSeverityType, message)
	response.Print()
	sugarLogger.Fatalw(message, fields...)
}

//
// PRIVATE FUNCTIONS
//

func getConsoleEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = nil
	encoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getFileEncoder(useJSON bool) zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	if useJSON {
		return zapcore.NewJSONEncoder(encoderConfig)
	} else {
		return zapcore.NewConsoleEncoder(encoderConfig)
	}
}

func getLogWriter(logFile string) zapcore.WriteSyncer {
	// TBD: ups... configuration is hardcoded :-(
	rotatoryLogger := &rotatory.Logger{
		Filename:   logFile,
		MaxSize:    10, // MB
		MaxBackups: 5,
		MaxAge:     30,
		Compress:   true,
	}
	return zapcore.AddSync(rotatoryLogger)
}

// Converts a string ("debug", "info"...) to a zap loglevel constant
func getLogLevel(level string) zapcore.Level {
	switch level {
	case "debug":
		return zap.DebugLevel
	case "info":
		return zap.InfoLevel
	case "warn":
		return zap.WarnLevel
	case "error":
		return zap.ErrorLevel
	case "fatal":
		return zap.FatalLevel
	default:
		return zap.InfoLevel
	}
}
