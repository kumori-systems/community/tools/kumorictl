/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package urnregexp

import (
	"regexp"
)

// var domainRegExpStr = `(?:[_a-z0-9](?:[_a-z0-9-]{0,61}[a-z0-9]\.)|(?:[0-9]+/[0-9]{2})\.)+(?:[a-z](?:[a-z0-9-]{0,61}[a-z0-9])?)?`
var urnDomainRegExpStr = `(?:[_a-z0-9](?:[_a-z0-9-@.]{0,61}[a-z0-9]))?`
var wordRegExpStr = `(.+)`

var urnDomainRegExp = regexp.MustCompile(`^` + urnDomainRegExpStr + `$`)

var certRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/certificate/` + wordRegExpStr + `$`)
var caRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/ca/` + wordRegExpStr + `$`)
var deploymentRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/deployment/` + wordRegExpStr + `$`)
var domainRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/domain/` + wordRegExpStr + `$`)
var httpinboundRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/httpinbound/` + wordRegExpStr + `$`)
var portRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/port/` + wordRegExpStr + `$`)
var solutionRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/solution/` + wordRegExpStr + `$`)
var secretRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/secret/` + wordRegExpStr + `$`)
var tcpinboundRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/tcpinbound/` + wordRegExpStr + `$`)
var volumeRegExp = regexp.MustCompile(`^eslap://` + urnDomainRegExpStr + `/volume/` + wordRegExpStr + `$`)

func CheckSecretUrn(urn string) bool {
	return secretRegExp.MatchString(urn)
}

func CheckCertUrn(urn string) bool {
	return certRegExp.MatchString(urn)
}

func CheckDomainUrn(urn string) bool {
	return domainRegExp.MatchString(urn)
}

func CheckCAUrn(urn string) bool {
	return caRegExp.MatchString(urn)
}

func CheckPortUrn(urn string) bool {
	return portRegExp.MatchString(urn)
}

func CheckSolutionUrn(urn string) bool {
	return solutionRegExp.MatchString(urn)
}

func CheckDeploymentUrn(urn string) bool {
	return deploymentRegExp.MatchString(urn)
}

func CheckHTTPInboundUrn(urn string) bool {
	return httpinboundRegExp.MatchString(urn)
}

func CheckTCPInboundUrn(urn string) bool {
	return tcpinboundRegExp.MatchString(urn)
}

func CheckVolumeUrn(urn string) bool {
	return volumeRegExp.MatchString(urn)
}
