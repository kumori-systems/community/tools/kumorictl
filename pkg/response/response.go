/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package response

import (
	"fmt"

	"github.com/Jeffail/gabs/v2"
)

type Response struct {
	Success bool            `json:"success"`
	Data    *gabs.Container `json:"data"`
	Errors  []ResponseError `json:"errors"`
	Spec    []int           `json:"ctlspec,omitempty"`
}

type ResponseError struct {
	Severity SeverityType `json:"severity"`
	Message  string       `json:"message"`
}

type SeverityType string

const (
	WarnSeverityType  SeverityType = "warning"
	ErrorSeverityType SeverityType = "error"
	FatalSeverityType SeverityType = "fatal"
)

type OutputType string

const (
	JSONOutputType   OutputType = "json"
	SilentOutputType OutputType = "silent"
)

var response Response = Response{
	Success: true,
	Errors:  []ResponseError{},
}
var Output OutputType = SilentOutputType

// Init initializes the json printer if the `-o json` flag is set
func Init(output OutputType) {

	// Initializes the response message
	Output = output
}

func GetData() *gabs.Container {
	return response.Data
}

// SetData sets the data of the response message
func SetData(data *gabs.Container) {
	response.Data = data
}

// SetSuccess sets the success flag of the response
func SetSuccess(success bool) {
	response.Success = success
}

// SetSpec sets the specification used to generate the response
func SetSpec(spec []int) {
	response.Spec = spec
}

// AddError adds a new error to the errors list
func AddError(severity SeverityType, message string) {
	response.Errors = append(response.Errors, ResponseError{
		Severity: severity,
		Message:  message,
	})

	if severity == ErrorSeverityType || severity == FatalSeverityType {
		response.Success = false
	}
}

// Print prints the response in a given format
func Print() {
	jsonObj := JSON()
	switch Output {
	case JSONOutputType:
		{
			bytes := jsonObj.BytesIndent("", " ")
			fmt.Println(string(bytes))
		}
	default:
		// Do nothing. Cannot log because to avoid a cyclic dependency with
		// logger module.
	}
}

// JSONPrint prints the result as a JSON document
func JSON() *gabs.Container {
	jsonObj := gabs.New()
	jsonObj.Set(response.Success, "success")
	if response.Data != nil {
		jsonObj.Set(response.Data.Data(), "data")
	}
	jsonObj.Array("errors")
	for _, err := range response.Errors {
		jsonObj.ArrayAppend(err, "errors")
	}
	if len(response.Spec) > 0 {
		for _, specValue := range response.Spec {
			jsonObj.ArrayAppend(specValue, "ctlspec")
		}
	}
	return jsonObj
}
