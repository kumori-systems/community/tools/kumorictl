/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processRole(id string, data *gabs.Container) (role *Role, err error) {

	// Checks if there is an artifact for this deployment
	if !data.Exists("artifact") || (data.Search("artifact").Data() == nil) {
		err = fmt.Errorf("missing artifact")
		return
	}

	// Gets the role name
	if !data.Exists("name") || (data.Search("name").Data() == nil) {
		err = fmt.Errorf("role name not found")
		return
	}
	roleName := data.Search("name").Data().(string)

	// Calculates the artifact
	var artifact Artifact
	artifact, err = g.processArtifact(id, data.Search("artifact"))
	if err != nil {
		return
	}

	// Gets the artifact configuration (if any)
	var config *Configuration
	if data.Exists("artifact", "description", "config") && (data.Search("artifact", "description", "config").Data() != nil) {
		roleConfig := data.Search("artifact", "description", "config")
		config, err = g.processConfiguration(roleConfig)
		if err != nil {
			return
		}
	}

	// Gets the role instances (if any)
	var instances map[string]Instance
	if data.Exists("instances") && (data.Search("instances").Data() != nil) {
		instancesData := data.Search("instances")
		instances, err = g.processInstances(id, roleName, instancesData)
		if err != nil {
			return
		}
	}

	// Gets the role metadata (if any)
	var meta *json.RawMessage
	if data.Exists("meta") && (data.Search("meta").Data() != nil) {
		metaData := json.RawMessage(data.Search("meta").String())
		meta = &metaData
	}

	// Creates the role object
	role = &Role{
		Artifact:  artifact,
		Config:    config,
		Instances: instances,
		Meta:      meta,
	}

	return
}
