/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) getEvents() (events []Event, err error) {
	return g.processEvents(g.root)
}

func (g *generator) processEvents(data *gabs.Container) (events []Event, err error) {
	// Skips if the event lists has not been found or is empty
	if !data.Exists("events") || (data.Search("events").Data() == nil) {
		return
	}
	eventsData := data.Search("events").Children()
	if len(eventsData) <= 0 {
		return
	}
	if len(eventsData) <= 0 {
		return
	}

	events = make([]Event, 0, len(events))

	for _, eventData := range eventsData {
		// Gets the counter
		counter := int(1)
		if eventData.Exists("counter") && (eventData.Search("counter").Data() != nil) {
			counterData := eventData.Search("counter").String()
			counter, err = strconv.Atoi(counterData)
			if err != nil {
				err = fmt.Errorf("wrong counter format '%s'", counterData)
			}
		}

		// Gets the first appearance timestamp
		var firstTimestamp time.Time
		if eventData.Exists("firstTimestamp") && (eventData.Search("firstTimestamp").Data() != nil) {
			firstTimestamp, err = g.calculateTimestamp("firstTimestamp", eventData)
		} else if eventData.Exists("time") && (eventData.Search("time").Data() != nil) {
			firstTimestamp, err = g.calculateTimestamp("time", eventData)
		}

		// Gets the last appearance timestamp
		lastTimestamp := firstTimestamp
		if eventData.Exists("lastTimestamp") && (eventData.Search("lastTimestamp").Data() != nil) {
			firstTimestamp, err = g.calculateTimestamp("lastTimestamp", eventData)
		}

		// Gets the source (if any)
		objectKind := ""
		if eventData.Exists("objectKind") && (eventData.Search("objectKind").Data() != nil) {
			objectKind = eventData.Search("objectKind").Data().(string)
		}
		objectName := ""
		objectInternalName := ""
		if eventData.Exists("objectName") && (eventData.Search("objectName").Data() != nil) {
			objectInternalName = eventData.Search("objectName").Data().(string)
			objectName = g.calculateKumoriName(objectInternalName, objectKind)
		}
		var source *Source
		if (objectKind != "") || (objectName != "") {
			source = &Source{
				Kind: objectKind,
				Name: objectName,
				Id:   objectInternalName,
			}
		}

		// Gets the message
		var message string
		if eventData.Exists("message") && (eventData.Search("message").Data() != nil) {
			message = eventData.Search("message").Data().(string)

			// Some messages need some processing for cleaning hashed names
			message = g.cleanEventMessage(objectKind, message)
		}

		// Gets the reason
		var reason string
		if eventData.Exists("reason") && (eventData.Search("reason").Data() != nil) {
			reason = eventData.Search("reason").Data().(string)
		}

		// Gets the event type
		var mtype string
		if eventData.Exists("type") && (eventData.Search("type").Data() != nil) {
			mtype = eventData.Search("type").Data().(string)
		}

		// Appends the event
		event := Event{
			Counter:        counter,
			FirstTimestamp: firstTimestamp,
			LastTimestamp:  lastTimestamp,
			Message:        message,
			Source:         source,
			Reason:         reason,
			Type:           mtype,
		}
		events = append(events, event)

	}

	return
}

func (g *generator) cleanEventMessage(objectKind string, message string) (cleanMessage string) {

	switch objectKind {
	case "Pod":
		// For Pod events messages, we try to detect container names hashes and replace them with clear names
		cleanMessage = message
		if strings.Contains(cleanMessage, " container k-") {
			for hashedCont, clearCont := range g.containerHashToName {
				cleanMessage = strings.ReplaceAll(cleanMessage, "ontainer k-"+hashedCont, "ontainer "+clearCont)
			}
		}
		return cleanMessage
	case "":
		// If there is no Kind, it is typically an instance-related event, so we perform the same
		// process as for Pod events
		cleanMessage = message
		if strings.Contains(cleanMessage, " container k-") {
			for hashedCont, clearCont := range g.containerHashToName {
				cleanMessage = strings.ReplaceAll(cleanMessage, "ontainer k-"+hashedCont, "ontainer "+clearCont)
			}
		}
		return cleanMessage
	default:
		return message
	}
}
