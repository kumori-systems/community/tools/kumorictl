/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strconv"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processChannels(data *gabs.Container) (srv *Srv, err error) {

	// Initializes the SRV section
	srv = &Srv{}

	// Calculates the service client channels
	if data.Exists("client") && (data.Search("client").Data() != nil) {
		clientsData := data.Search("client").ChildrenMap()
		if len(clientsData) > 0 {
			srv.Clients = make(map[string]Client, len(clientsData))
			for clientName, clientData := range clientsData {

				// Checks if this is an inherited channel. If that's the case, skips it since
				// this channel has been added during the flattening process and must be removed
				// during the deflating process
				inherited := true
				if clientData.Exists("inherited") && (clientData.Search("inherited").Data() != nil) {
					inherited = clientData.Search("inherited").Data().(bool)
				}
				if inherited {
					continue
				}

				// Gets the channel protocol
				if !clientData.Exists("protocol") || (clientData.Search("protocol").Data() == nil) {
					err = fmt.Errorf("missing protocol in client channel '%s'", clientName)
					return
				}
				protocol := clientData.Search("protocol").Data().(string)

				// Adds the client channel
				srv.Clients[clientName] = Client{
					Protocol: protocol,
				}
			}
		}
	}

	// Calculates the service server channels
	if data.Exists("server") && (data.Search("server").Data() != nil) {
		serversData := data.Search("server").ChildrenMap()
		if len(serversData) > 0 {
			srv.Servers = make(map[string]Server, len(serversData))
			for serverName, serverData := range serversData {

				// Checks if this is an inherited channel. If that's the case, skips it since
				// this channel has been added during the flattening process and must be removed
				// during the deflating process
				inherited := true
				if serverData.Exists("inherited") && (serverData.Search("inherited").Data() != nil) {
					inherited = serverData.Search("inherited").Data().(bool)
				}
				if inherited {
					continue
				}

				// Gets the channel protocol
				if !serverData.Exists("protocol") || (serverData.Search("protocol").Data() == nil) {
					err = fmt.Errorf("missing protocol in server channel '%s'", serverName)
					return
				}
				protocol := serverData.Search("protocol").Data().(string)

				// Gets the channel port
				if !serverData.Exists("port") || (serverData.Search("port").Data() == nil) {
					err = fmt.Errorf("missing port in server channel '%s'", serverName)
					return
				}
				portData := serverData.Search("port").String()
				var portInt int
				portInt, err = strconv.Atoi(portData)
				if err != nil {
					err = fmt.Errorf("error getting server channel '%s' port: %v", serverName, err)
					return
				}
				port := int32(portInt)

				// Gets the channel portnum
				portnum := int32(1)
				if serverData.Exists("portnum") && (serverData.Search("portnum").Data() != nil) {
					portnumData := serverData.Search("portnum").String()
					var portnumInt int
					portnumInt, err = strconv.Atoi(portnumData)
					if err != nil {
						err = fmt.Errorf("error getting server channel '%s' portnum: %v", serverName, err)
						return
					}
					portnum = int32(portnumInt)
				}

				// Adds the new server channel
				srv.Servers[serverName] = Server{
					Protocol: protocol,
					Port:     port,
					PortNum:  portnum,
				}
			}
		}
	}

	// Calculates the service duplex channels
	if data.Exists("duplex") && (data.Search("duplex").Data() != nil) {
		duplexData := data.Search("duplex").ChildrenMap()
		if len(duplexData) > 0 {
			srv.Duplex = make(map[string]Duplex, len(duplexData))
			for dupName, dupData := range duplexData {

				// Checks if this is an inherited channel. If that's the case, skips it since
				// this channel has been added during the flattening process and must be removed
				// during the deflating process
				inherited := true
				if dupData.Exists("inherited") && (dupData.Search("inherited").Data() != nil) {
					inherited = dupData.Search("inherited").Data().(bool)
				}
				if inherited {
					continue
				}

				// Gets the channel protocol
				if !dupData.Exists("protocol") || (dupData.Search("protocol").Data() == nil) {
					err = fmt.Errorf("missing protocol in duplex channel '%s'", dupName)
					return
				}
				protocol := dupData.Search("protocol").Data().(string)

				// Gets the channel port
				if !dupData.Exists("port") || (dupData.Search("port").Data() == nil) {
					err = fmt.Errorf("missing port in duplex channel '%s'", dupName)
					return
				}
				portData := dupData.Search("port").String()
				var portInt int
				portInt, err = strconv.Atoi(portData)
				if err != nil {
					err = fmt.Errorf("error getting duplex channel '%s' port: %v", dupName, err)
					return
				}
				port := int32(portInt)

				// Gets the channel portnum
				portnum := int32(1)
				if dupData.Exists("portnum") && (dupData.Search("portnum").Data() != nil) {
					portnumData := dupData.Search("portnum").String()
					var portnumInt int
					portnumInt, err = strconv.Atoi(portnumData)
					if err != nil {
						err = fmt.Errorf("error getting duplex channel '%s' portnum: %v", dupName, err)
						return
					}
					portnum = int32(portnumInt)
				}

				// Adds the new duplex channel
				srv.Duplex[dupName] = Duplex{
					Protocol: protocol,
					Port:     port,
					PortNum:  portnum,
				}
			}
		}
	}

	return
}
