/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strconv"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processReference(data *gabs.Container) (ref Reference, err error) {

	// Checks if this JSON document includes a solution reference
	if !data.Exists("domain") || (data.Search("domain").Data() == nil) ||
		!data.Exists("name") || (data.Search("name").Data() == nil) {
		err = fmt.Errorf("wrong reference format")
		return
	}

	// Gets name and domain
	name := data.Search("name").Data().(string)
	domain := data.Search("domain").Data().(string)

	// Creates the reference
	ref = Reference{
		Name:   name,
		Domain: domain,
	}

	// Adds the module (if exists)
	if data.Exists("module") && (data.Search("module").Data() != nil) {
		module := data.Search("module").Data().(string)
		ref.Module = module
	}

	return
}

func (g *generator) processVersionedReference(data *gabs.Container) (ref VersionedReference, err error) {

	var reference Reference
	if reference, err = g.processReference(data); err != nil {
		return
	}

	if !data.Exists("version") || (data.Search("version").Data() == nil) {
		err = fmt.Errorf("missing version")
		return
	}

	versionData := data.Search("version").Children()
	var prerelease string
	if data.Exists("prerelease") && (data.Search("prerelease").Data() != nil) {
		prerelease = data.Search("prerelease").Data().(string)
	}

	version := [3]int32{}
	for i, versionElement := range versionData {
		vdata := versionElement.String()
		var vint int
		vint, err = strconv.Atoi(vdata)
		if err != nil {
			err = fmt.Errorf("error parsing version number '%s': %v", vdata, err)
			return
		}
		version[i] = int32(vint)
	}

	ref = VersionedReference{
		Reference:  reference,
		Version:    version,
		PreRelease: prerelease,
	}

	return
}

func (g *generator) processComponentReference(data *gabs.Container) (ref ComponentReference, err error) {
	// Gets the component reference
	var vref VersionedReference
	vref, err = g.processVersionedReference(data)
	if err != nil {
		return
	}
	ref = ComponentReference{
		VersionedReference: vref,
		Kind:               ComponentKindValue,
	}

	return
}

func (g *generator) processServiceReference(data *gabs.Container) (ref ServiceReference, err error) {
	// Gets the service reference
	var vref VersionedReference
	vref, err = g.processVersionedReference(data)
	if err != nil {
		return
	}
	ref = ServiceReference{
		VersionedReference: vref,
		Kind:               ServiceKindValue,
	}

	return
}
