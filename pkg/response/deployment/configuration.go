/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) getConfiguration() (config *Configuration, err error) {

	// Checks if exists a configuration in the top deployment of the solution. Note that the
	// configuration in the top deployment is the configuration provided to the solution by the
	// user
	if !g.root.Exists("top") || (g.root.Search("top").Data() == nil) {
		err = fmt.Errorf("top deployment not found")
		return
	}
	top := g.root.Search("top").Data().(string)
	if !g.root.Exists("deployments", top) || (g.root.Search("deployments", top).Data() == nil) {
		err = fmt.Errorf("top deployment '%s' not found", top)
		return
	}
	topDeployment := g.root.Search("deployments", top)
	if !topDeployment.Exists("config") || (topDeployment.Search("config").Data() == nil) {
		// The top deployment configuration not exists. Returns nil
		return
	}
	topConfig := topDeployment.Search("config")

	config, err = g.processConfiguration(topConfig)
	if err != nil {
		err = fmt.Errorf("error processing top deployment configuration: %v", err)
	}

	return
}

func (g *generator) processConfiguration(data *gabs.Container) (config *Configuration, err error) {

	// Initializes the deployment configuration
	config = &Configuration{}

	// Gets the resilience
	resilience := int32(0)
	if data.Exists("resilience") && (data.Search("resilience").Data() != nil) {
		topResilience := data.Search("resilience").String()
		var number int
		number, err = strconv.Atoi(topResilience)
		if err != nil {
			err = fmt.Errorf("reslience '%s' is not a number", topResilience)
			return
		}
		resilience = int32(number)
	}
	config.Resilience = resilience

	// Gets parameters
	if data.Exists("parameter") && (data.Search("parameter").Data() != nil) {
		topParams := data.Search("parameter").ChildrenMap()
		if len(topParams) > 0 {
			config.Parameters = make(map[string]json.RawMessage, len(topParams))
			for paramName, paramGabsData := range topParams {
				paramData := paramGabsData.String()
				config.Parameters[paramName] = json.RawMessage(paramData)
			}
		}
	}

	// Gets resources
	if data.Exists("resource") && (data.Search("resource").Data() != nil) {
		topResources := data.Search("resource").ChildrenMap()
		if len(topResources) > 0 {
			config.Resources = make(map[string]Resource, len(topResources))
			for resName, resGabsData := range topResources {
				if resGabsData.Exists("ca") && (resGabsData.Search("ca").Data() != nil) {
					resData := resGabsData.Search("ca").Data().(string)
					if resData != "" {
						config.Resources[resName] = Resource{
							CA: resData,
						}
						continue
					}
				}
				if resGabsData.Exists("certificate") && (resGabsData.Search("certificate").Data() != nil) {
					resData := resGabsData.Search("certificate").Data().(string)
					if resData != "" {
						config.Resources[resName] = Resource{
							Certificate: resData,
						}
						continue
					}
				}
				if resGabsData.Exists("domain") && (resGabsData.Search("domain").Data() != nil) {
					resData := resGabsData.Search("domain").Data().(string)
					if resData != "" {
						config.Resources[resName] = Resource{
							Domain: resData,
						}
						continue
					}
				}
				if resGabsData.Exists("port") && (resGabsData.Search("port").Data() != nil) {
					resData := resGabsData.Search("port").Data().(string)
					if resData != "" {
						config.Resources[resName] = Resource{
							Port: resData,
						}
						continue
					}
				}
				if resGabsData.Exists("secret") && (resGabsData.Search("secret").Data() != nil) {
					resData := resGabsData.Search("secret").Data().(string)
					if resData != "" {
						config.Resources[resName] = Resource{
							Secret: resData,
						}
						continue

					}
				}
				if resGabsData.Exists("volume") && (resGabsData.Search("volume").Data() != nil) {
					resData := resGabsData.Search("volume").String()
					if resData != "" {
						resBytes := json.RawMessage(resData)
						config.Resources[resName] = Resource{
							Volume: &resBytes,
						}
						continue
					}
				}
				err = fmt.Errorf("resource '%s' type unknown", resName)
				return
			}
		}
	}

	// Gets the scale (only for roles)
	if data.Exists("scale", "hsize") && (data.Search("scale", "hsize").Data() != nil) {
		hsizeData := data.Search("scale", "hsize").String()
		var hsize int
		hsize, err = strconv.Atoi(hsizeData)
		if err != nil {
			err = fmt.Errorf("wrong hsize '%s': %v", hsizeData, err)
			return
		}
		config.Scale = &Scale{
			HorizontalSize: int32(hsize),
		}
	}

	return
}
