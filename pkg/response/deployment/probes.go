/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processProbes(data *gabs.Container) (probes *map[string]Probes, err error) {

	probesMap := data.ChildrenMap()
	if len(probesMap) <= 0 {
		return
	}

	probesInit := make(map[string]Probes, len(probesMap))

	for containerName, probesData := range probesMap {
		var containerProbes Probes
		containerProbes, err = g.processContainerProbes(probesData)
		if err != nil {
			return
		}
		probesInit[containerName] = containerProbes
	}

	probes = &probesInit

	return
}

func (g *generator) processContainerProbes(data *gabs.Container) (probes Probes, err error) {
	probesMap := data.ChildrenMap()
	if len(probesMap) <= 0 {
		return
	}

	for probeType, probeData := range probesMap {
		switch probeType {
		case "liveness":
			var frequency *json.RawMessage
			frequency, err = g.processFrequency(probeData)
			if err != nil {
				return
			}
			var protocol ProbeProtocol
			protocol, err = g.processProbeProtocol(probeData)
			if err != nil {
				return
			}
			var timeout *int32
			timeout, err = g.processTimeout(probeData)
			if err != nil {
				return
			}
			var startupGraceWindow *StartupGraceWindow
			startupGraceWindow, err = g.processStartupGraceWindow(probeData)
			if err != nil {
				return
			}
			probes.Liveness = &LivenessProbeAttributes{
				Protocol:           protocol,
				StartupGraceWindow: startupGraceWindow,
				Frequency:          frequency,
				Timeout:            timeout,
			}
		case "readiness":
			var frequency *json.RawMessage
			frequency, err = g.processFrequency(probeData)
			if err != nil {
				return
			}
			var protocol ProbeProtocol
			protocol, err = g.processProbeProtocol(probeData)
			if err != nil {
				return
			}
			var timeout *int32
			timeout, err = g.processTimeout(probeData)
			if err != nil {
				return
			}
			probes.Readiness = &ReadinessProbeAttributes{
				Protocol:  protocol,
				Frequency: frequency,
				Timeout:   timeout,
			}
		case "pmetrics":
			var protocol ProbeProtocol
			protocol, err = g.processProbeProtocol(probeData)
			if err != nil {
				return
			}
			if protocol.HTTP == nil {
				err = fmt.Errorf("pmetrics requires an HTTP probe protocol")
				return
			}
			probes.PrometheusMetrics = &PrometheusMetricsProbeAttributes{
				Protocol: HTTPOnlyProbeProtocol{
					HTTP: protocol.HTTP,
				},
			}
		default:
			err = fmt.Errorf("unknown probe type '%s'", probeType)
			return
		}
	}

	return
}

func (g *generator) processFrequency(data *gabs.Container) (frequency *json.RawMessage, err error) {
	if !data.Exists("frequency") || (data.Search("frequency").Data() == nil) {
		return
	}

	freqData := []byte(data.Search("frequency").String())
	freqRaw := json.RawMessage(freqData)
	frequency = &freqRaw
	return
}

func (g *generator) processProbeProtocol(data *gabs.Container) (protocol ProbeProtocol, err error) {
	if !data.Exists("protocol") || (data.Search("protocol").Data() == nil) {
		err = fmt.Errorf("missing protocol section")
		return
	}

	protocolMap := data.Search("protocol").ChildrenMap()
	if len(protocolMap) <= 0 {
		return
	}

	for protoName, protoData := range protocolMap {
		switch protoName {
		case "http":
			if !protoData.Exists("path") || (protoData.Search("path").Data() == nil) {
				err = fmt.Errorf("cannot find http protocol path")
				return
			}
			if !protoData.Exists("port") || (protoData.Search("port").Data() == nil) {
				err = fmt.Errorf("cannot find http protocol port")
				return
			}
			protoPath := protoData.Search("path").Data().(string)
			protoPortData := protoData.Search("port").String()
			var protoPortInt int
			protoPortInt, err = strconv.Atoi(protoPortData)
			if err != nil {
				err = fmt.Errorf("port '%s' is not a number", protoPortData)
				return
			}
			protoPort := int32(protoPortInt)
			httpProbeProtocol := HTTPProbeProtocol{
				Port: protoPort,
				Path: protoPath,
			}
			protocol = ProbeProtocol{
				HTTP: &httpProbeProtocol,
			}
			return
		case "tcp":
			if !protoData.Exists("port") || (protoData.Search("port").Data() == nil) {
				err = fmt.Errorf("cannot find tcp protocol port")
				return
			}
			protoPortData := protoData.Search("port").String()
			var protoPortInt int
			protoPortInt, err = strconv.Atoi(protoPortData)
			if err != nil {
				err = fmt.Errorf("port '%s' is not a number", protoPortData)
				return
			}
			protoPort := int32(protoPortInt)
			tcpProbeProtocol := TCPProbeProtocol{
				Port: protoPort,
			}
			protocol = ProbeProtocol{
				TCP: &tcpProbeProtocol,
			}
			return
		case "exec":
			if !protoData.Exists("path") || (protoData.Search("path").Data() == nil) {
				err = fmt.Errorf("cannot find exec protocol path")
				return
			}
			protoPath := protoData.Search("path").Data().(string)
			execProbeProtocol := ExecProbeProtocol{
				Path: protoPath,
			}
			protocol = ProbeProtocol{
				Exec: &execProbeProtocol,
			}
			return
		default:
			err = fmt.Errorf("unknown protocol '%s'", protoName)
			return
		}
	}

	return
}

func (g *generator) processTimeout(data *gabs.Container) (timeout *int32, err error) {
	if !data.Exists("timeout") || (data.Search("timeout").Data() == nil) {
		return
	}
	timeoutData := data.Search("timeout").String()
	var timeoutInt int
	timeoutInt, err = strconv.Atoi(timeoutData)
	if err != nil {
		err = fmt.Errorf("timeout '%s' is not a number", timeoutData)
	}
	timeoutInt32 := int32(timeoutInt)
	timeout = &timeoutInt32
	return
}

func (g *generator) processStartupGraceWindow(data *gabs.Container) (
	startupGraceWindow *StartupGraceWindow,
	err error,
) {
	if !data.Exists("startupGraceWindow") || (data.Search("startupGraceWindow").Data() == nil) {
		return
	}
	if !data.Exists("startupGraceWindow", "duration") || (data.Search("startupGraceWindow", "duration").Data() == nil) {
		err = fmt.Errorf("missing duration in startup grace window")
		return
	}
	if !data.Exists("startupGraceWindow", "probe") || (data.Search("startupGraceWindow", "probe").Data() == nil) {
		err = fmt.Errorf("missing probe flag in startup grace window")
		return
	}
	if !data.Exists("startupGraceWindow", "unit") || (data.Search("startupGraceWindow", "unit").Data() == nil) {
		err = fmt.Errorf("missing unit in startup grace window")
		return
	}

	// Get duration
	durationData := data.Search("startupGraceWindow", "duration").String()
	var durationInt int
	durationInt, err = strconv.Atoi(durationData)
	if err != nil {
		err = fmt.Errorf("duration '%s' is not a number", durationData)
	}
	duration := uint(durationInt)

	// Get probe flag
	probe := data.Search("startupGraceWindow", "probe").Data().(bool)

	// Get unit
	unitStr := data.Search("startupGraceWindow", "unit").Data().(string)
	var unit StartupGraceWindowUnit
	switch unitStr {
	case "ms":
		unit = MillisStartupGraceWindowUnit
	case "attempts":
		unit = AttemptsStartupGraceWindowUnit
	default:
		err = fmt.Errorf("startup grace window unit '%s' unknown", unitStr)
		return
	}

	window := StartupGraceWindow{
		Unit:     unit,
		Duration: duration,
		Probe:    probe,
	}

	startupGraceWindow = &window
	return

}
