/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	"strings"
)

func (g *generator) getLinks() (links map[string][]Link, err error) {

	// Checks if this JSON document includes an externalLinks key. If that's not the case,
	// returns an error. Note that the JSON document returnes by admission also includes a
	// "links" section. However, this section contains the links between subdeployments and
	// are automatically generated during the flattening of the original CUE description. The
	// deployment in this section undoes this flattening and, hence, a link only connects
	// service channels of the top deployments of two different solutions.
	if !g.root.Exists("externalLinks") || (g.root.Search("externalLinks").Data() == nil) {
		err = fmt.Errorf("externalLinks not found")
		return
	}

	// Gets the links in external maps
	externalLinks := g.root.Search("externalLinks").ChildrenMap()

	if len(externalLinks) > 0 {

		links = make(map[string][]Link, len(externalLinks))

		// Generates the links sections of the deployment
		for channel, content := range externalLinks {

			// If this channel is not connected to anyone, skip it
			solutions := content.ChildrenMap()
			if len(solutions) <= 0 {
				continue
			}

			// Each channel can be connected to several solutions. The solution URN is provied
			// in the original manifest. For example: eslap://devel/solution/helloworldinb
			links[channel] = make([]Link, 0, len(solutions))
			for solutionURN, solutionData := range solutions {

				// Removes the eslap:// to get just the path
				solutionPath := solutionURN[8:]

				// Splits the path in three parts using a slash as a separator. If there are more
				// than three parts, all parts after the second are concatenated in a single part.
				parts := strings.SplitN(solutionPath, "/", 3)
				if len(parts) < 3 {
					err = fmt.Errorf("wrong urn format: '%s'", solutionURN)
					return
				}
				domain := parts[0]
				kind := parts[1]
				name := parts[2]

				// The linked element must be a solution since we are only considering external links
				if kind != SolutionReferenceKind {
					err = fmt.Errorf("channel '%s' is linked to '%s' but is not of kind 'solution'", channel, solutionURN)
					return
				}

				// Gets the linked channel
				channelsMap := solutionData.ChildrenMap()
				if len(channelsMap) <= 0 {
					err = fmt.Errorf("target channel not found")
					return
				}
				channels := g.getKeysOrderedByName(channelsMap)

				for _, targetChannel := range channels {
					target := channelsMap[targetChannel]

					// Creates and appends the link
					link := Link{
						Channels: []string{targetChannel},
						Domain:   domain,
						Name:     name,
					}
					if target.Exists("meta") {
						valueStr := target.Search("meta").String()
						meta := json.RawMessage([]byte(valueStr))
						link.Meta = &meta
					}
					links[channel] = append(links[channel], link)
				}

			}
		}
	}

	return
}
