/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processConnectors(data *gabs.Container) (connectors map[string]Connector, err error) {
	// Process connectors
	connectorsData := data.ChildrenMap()
	if len(connectorsData) > 0 {
		connectors = make(map[string]Connector, len(connectorsData))
		for conName, conData := range connectorsData {

			// Gets the connector kind
			if !conData.Exists("kind") || (conData.Search("kind").Data() == nil) {
				err = fmt.Errorf("wrong format in connector '%s'. Missing kind", conName)
				return
			}
			kindData := conData.Search("kind").Data().(string)
			var kind ConnectorKind
			switch kindData {
			case "lb":
				kind = LBConnectorKind
			case "full":
				kind = FullConnectorKind
			default:
				err = fmt.Errorf("unknown format kind '%s' connector '%s'", kindData, conName)
			}

			// Gets the connector client endpoints
			var clients []Endpoint
			if conData.Exists("clients") && (conData.Search("clients").Data() != nil) {
				clientsData := conData.Search("clients")
				clients, err = g.processConnectorEndpoints(clientsData)
				if err != nil {
					err = fmt.Errorf("error processing client endpoints of connector '%s': %v", conName, err)
					return
				}
			}

			// Gets the connector server endpoints
			var servers []Tag
			if conData.Exists("servers") && (conData.Search("servers").Data() != nil) {
				serversData := conData.Search("servers").Children()
				if len(serversData) > 0 {
					servers = make([]Tag, len(serversData))
					tag := Tag{}
					for serverIndex, serverData := range serversData {
						if serverData.Exists("links") && (serverData.Search("links").Data() != nil) {
							serverLinks := serverData.Search("links")
							var links []Endpoint
							links, err = g.processConnectorEndpoints(serverLinks)
							if err != nil {
								err = fmt.Errorf("error processing server endpoints of connector '%s' tag '%d': %v", conName, serverIndex, err)
								return
							}
							tag.Links = links
						}
						if serverData.Exists("meta") && (serverData.Search("meta").Data() != nil) {
							metasData := serverData.Search("meta").Children()
							if len(metasData) > 0 {
								tag.Meta = make([]json.RawMessage, len(metasData))
								for metaIndex, metaData := range metasData {
									if metaData.Exists("user") && (metaData.Search("user").Data() != nil) {
										tag.Meta[metaIndex] = json.RawMessage(metaData.Search("user").String())
									}
								}
							}
						}
						servers[serverIndex] = tag
					}
				}
			}

			// Adds the connector
			connectors[conName] = Connector{
				Clients: clients,
				Kind:    kind,
				Servers: servers,
			}
		}
	}

	return
}

func (g *generator) processConnectorEndpoints(data *gabs.Container) (endpoints []Endpoint, err error) {

	// Return nil if the endpoints list is empty
	endpointsData := data.Children()
	if len(endpointsData) <= 0 {
		return
	}

	// Initializes the endpoints array
	endpoints = make([]Endpoint, len(endpointsData))

	// Fills the endpoints array
	for epIndex, epData := range endpointsData {
		if !epData.Exists("channel") || (epData.Search("channel").Data() == nil) {
			err = fmt.Errorf("missing channel name in endpoint '%d'", epIndex)
			return
		}
		if !epData.Exists("role") || (epData.Search("role").Data() == nil) {
			err = fmt.Errorf("missing role name in endpoint '%d'", epIndex)
			return
		}

		channel := epData.Search("channel").Data().(string)
		role := epData.Search("role").Data().(string)
		endpoints[epIndex] = Endpoint{
			Channel: channel,
			Role:    role,
		}
	}

	return
}
