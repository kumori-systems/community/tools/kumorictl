/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strconv"
	"time"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processInstances(
	id string,
	role string,
	data *gabs.Container,
) (instances map[string]Instance, err error) {

	// Return an empty map if the instances map in the manifest is empty
	instancesData := data.ChildrenMap()
	if len(instancesData) <= 0 {
		return
	}

	instances = make(map[string]Instance, len(instancesData))

	// Process instancess
	for instanceName, instanceData := range instancesData {
		publicName := g.calculateInstanceKumoriName(id, role, instanceName)

		// Process information about containers
		var containers map[string]InstanceContainer
		if instanceData.Exists("containers") && (instanceData.Search("containers").Data() != nil) {
			instanceContainers := instanceData.Search("containers").ChildrenMap()
			if len(instanceContainers) > 0 {
				containers = make(map[string]InstanceContainer, len(instanceContainers))
				for containerName, containerData := range instanceContainers {
					var instanceContainer InstanceContainer
					instanceContainer, err = g.processInstanceContainer(role, publicName, containerName, containerData)
					if err != nil {
						err = fmt.Errorf("error processing container '%s' data in instance '%s' of role '%s'", containerName, instanceName, role)
						continue
					}
					containers[containerName] = instanceContainer
				}
			}
		}

		// Gets creation timestamp
		var creationTimestamp time.Time
		if instanceData.Exists("creationDate") && (instanceData.Search("creationDate").Data() != nil) {
			creationTimestamp, err = g.calculateTimestamp("creationDate", instanceData)
		}

		// Process information about events
		var events []Event
		if instanceData.Exists("events") && (instanceData.Search("events").Data() != nil) {
			if events, err = g.processEvents(instanceData); err != nil {
				err = fmt.Errorf("error getting events for instance '%s' in role '%s': %v", publicName, role, err)
			}
		}

		// Gathers instance metrics
		var metrics Metrics
		if instanceData.Exists("metrics") && (instanceData.Search("metrics").Data() != nil) {
			if metrics, err = g.processMetrics(instanceData.Search("metrics")); err != nil {
				err = fmt.Errorf("error getting metrics for instance '%s' in role '%s': %v", publicName, role, err)
			}
		}

		// Gets the name of the node hosting the instance
		node := "Unknown"
		if instanceData.Exists("node") && (instanceData.Search("node").Data() != nil) {
			node = instanceData.Search("node").Data().(string)
		}

		// Gets the instance Status
		status := "Unknown"
		if instanceData.Exists("status") && (instanceData.Search("status").Data() != nil) {
			status = instanceData.Search("status").Data().(string)
		}

		// Gets the instance Status reason
		var statusReason *string
		if instanceData.Exists("statusReason") && (instanceData.Search("statusReason").Data() != nil) {
			auxi := instanceData.Search("statusReason").Data().(string)
			statusReason = &auxi
		}

		// Gets the instance Status message
		var statusMessage *string
		if instanceData.Exists("statusMessage") && (instanceData.Search("statusMessage").Data() != nil) {
			auxi := instanceData.Search("statusMessage").Data().(string)
			statusMessage = &auxi
		}

		// Gets ready flag
		ready := false
		if instanceData.Exists("ready") && (instanceData.Search("ready").Data() != nil) {
			ready = instanceData.Search("ready").Data().(bool)
		}
		// Creates the instance object
		instances[publicName] = Instance{
			Containers:        containers,
			CreationTimestamp: creationTimestamp,
			Events:            events,
			Id:                instanceName,
			Metrics:           metrics,
			Node:              node,
			Ready:             ready,
			Status:            status,
			StatusReason:      statusReason,
			StatusMessage:     statusMessage,
		}
	}

	return
}

func (g *generator) processMetrics(data *gabs.Container) (metrics Metrics, err error) {

	// Gets the CPU usage
	var cpu float64
	if data.Exists("usage", "cpu") && (data.Search("usage", "cpu").Data() != nil) {
		cpu = data.Search("usage", "cpu").Data().(float64)
	}

	// Gets the memory usage
	var mem uint64
	if data.Exists("usage", "memory") && (data.Search("usage", "memory").Data() != nil) {
		memData := data.Search("usage", "memory").String()
		mem, err = strconv.ParseUint(memData, 0, 64)
		if err != nil {
			err = fmt.Errorf("wrong memory usage format '%s'", memData)
			return
		}
	}

	// Creates the metrics object
	metrics = Metrics{
		Usage: UsageMetrics{
			CPU:    cpu,
			Memory: mem,
		},
	}

	return
}

func (g *generator) processInstanceContainer(
	role string,
	instanceId string,
	containerId string,
	data *gabs.Container,
) (container InstanceContainer, err error) {

	// Gets metrics
	var metrics Metrics
	if data.Exists("metrics") && (data.Search("metrics").Data() != nil) {
		if metrics, err = g.processMetrics(data.Search("metrics")); err != nil {
			err = fmt.Errorf("error getting metrics for container '%s' of instance '%s' in role '%s': %v", containerId, instanceId, role, err)

		}
	}

	// Gets ready flag
	ready := false
	if data.Exists("ready") && (data.Search("ready").Data() != nil) {
		ready = data.Search("ready").Data().(bool)
	}

	// Gets number of restarts
	var restarts uint32
	if data.Exists("restarts") && (data.Search("restarts").Data() != nil) {
		restartsData := data.Search("restarts").String()
		var restartsInt int
		restartsInt, err = strconv.Atoi(restartsData)
		restarts = uint32(restartsInt)
		if err != nil {
			err = fmt.Errorf("wrong restarts format '%s'", restartsData)
		}
	}

	// Gets states list
	states := InstanceStates{}
	if data.Exists("state") && (data.Search("state").Data() != nil) {
		statesData := data.Search("state").ChildrenMap()
		if len(statesData) > 0 {
			for stateName, stateData := range statesData {
				switch stateName {
				case "running":
					states.Running = &InstanceRunningState{}
					if stateData.Exists("startedAt") && (stateData.Search("startedAt").Data() != nil) {
						var startedAt time.Time
						startedAt, err = g.calculateTimestamp("startedAt", stateData)
						if err != nil {
							err = fmt.Errorf("role/instance/container '%s/%s/%s'. Error processing startedAt field '%s': %v", role, instanceId, containerId, stateData.Search("startedAt").Data().(string), err)
							continue
						}
						states.Running.StartedAt = &startedAt
					}
				case "waiting":
					states.Waiting = &InstanceWaitingState{}
					if stateData.Exists("message") && (stateData.Search("message").Data() != nil) {
						message := stateData.Search("message").Data().(string)
						states.Waiting.Message = &message
					}
					if stateData.Exists("reason") && (stateData.Search("reason").Data() != nil) {
						reason := stateData.Search("reason").Data().(string)
						states.Waiting.Reason = &reason
					}

				case "terminated":

					// Only exit code is mandatory
					if !stateData.Exists("exitCode") || (stateData.Search("exitCode").Data() == nil) {
						err = fmt.Errorf("role/instance/container '%s/%s/%s'. Wrong terminated state format: missing exitCode", role, instanceId, containerId)
						continue
					}

					exitCodeStr := stateData.Search("exitCode").String()
					var exitCodeInt int
					exitCodeInt, err = strconv.Atoi(exitCodeStr)
					if err != nil {
						err = fmt.Errorf("role/instance/container '%s/%s/%s'. Error processing exitCode field '%s': %v", role, instanceId, containerId, exitCodeStr, err)
						continue
					}
					exitCode := int64(exitCodeInt)

					states.Terminated = &InstanceTerminatedState{
						ExitCode: exitCode,
					}

					if stateData.Exists("containerID") && (stateData.Search("containerID").Data() != nil) {
						containerId := stateData.Search("containerID").Data().(string)
						states.Terminated.ContainerID = &containerId
					}

					if stateData.Exists("finishedAt") && (stateData.Search("finishedAt").Data() != nil) {
						var finishedAt time.Time
						finishedAt, err = g.calculateTimestamp("finishedAt", stateData)
						if err != nil {
							err = fmt.Errorf("role/instance/container '%s/%s/%s'. Error processing finishedAt field '%s': %v", role, instanceId, containerId, stateData.Search("finishedAt").Data().(string), err)
						} else {
							states.Terminated.FinishedAt = &finishedAt
						}
					}

					if stateData.Exists("message") && (stateData.Search("message").Data() != nil) {
						message := stateData.Search("message").Data().(string)
						states.Terminated.Message = &message
					}

					if stateData.Exists("reason") && (stateData.Search("reason").Data() != nil) {
						reason := stateData.Search("reason").Data().(string)
						states.Terminated.Reason = &reason
					}

					if stateData.Exists("signal") && (stateData.Search("signal").Data() != nil) {

						signalStr := stateData.Search("signal").String()
						var signalInt int
						signalInt, err = strconv.Atoi(signalStr)
						if err != nil {
							err = fmt.Errorf("role/instance/container '%s/%s/%s'. Error processing signal field '%s': %v", role, instanceId, containerId, stateData.Search("signal").String(), err)
						} else {
							signal := int64(signalInt)
							states.Terminated.Signal = &signal
						}

					}

					if stateData.Exists("startedAt") && (stateData.Search("startedAt").Data() != nil) {
						var startedAt time.Time
						startedAt, err = g.calculateTimestamp("startedAt", stateData)
						if err != nil {
							err = fmt.Errorf("role/instance/container '%s/%s/%s'. Error processing startedAt field '%s': %v", role, instanceId, containerId, stateData.Search("startedAt").Data().(string), err)
						} else {
							states.Terminated.StartedAt = &startedAt
						}
					}

				default:
					err = fmt.Errorf("role/instance/container '%s/%s/%s'. Unknown state '%s'", role, instanceId, containerId, stateName)
					continue
				}
			}
		}
	}

	// Creates the instance container data object
	container = InstanceContainer{
		Metrics:  metrics,
		Ready:    ready,
		Restarts: restarts,
		States:   states,
	}

	return
}
