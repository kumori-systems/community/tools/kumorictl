/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"

	"github.com/Jeffail/gabs/v2"
)

type generator struct {
	root *gabs.Container

	// This map is used to store every deployment role name and its hash to be able to decode them.
	roleHashToName map[string]string
	// This map is used to store every container name and its hash to be able to decode them.
	containerHashToName map[string]string
}

// NewResponse creates a deployment using the information container in the Data section of admission
// response
func NewResponse(data *gabs.Container) (deployment *Deployment, err error) {
	g := &generator{
		root:                data,
		roleHashToName:      map[string]string{},
		containerHashToName: map[string]string{},
	}

	return g.generate()
}

func (g *generator) generate() (deployment *Deployment, err error) {

	// Checks cspec
	if !g.root.Exists("cspec") || (g.root.Search("cspec").Data() == nil) {
		err = fmt.Errorf("unsupported response version (cspec not found)")
		return
	}
	cspec := g.root.Search("cspec").Data().(string)
	if cspec != ExpectedCSpec {
		err = fmt.Errorf("unsupported response version '%s'", cspec)
		return
	}

	// Gets the solution internal id
	if !g.root.Exists("name") || (g.root.Search("name").Data() == nil) {
		err = fmt.Errorf("missing solution name (internal id)")
		return
	}

	// Initialize role and container hash-to-name dictionaries
	err = g.initializeHashDictionaries()
	if err != nil {
		err = fmt.Errorf("unable to initialize hash dictionaries")
		return
	}

	// Gets the deployment artifact
	artifact, err := g.getRootArtifact()
	if err != nil {
		return
	}

	// Gets the comment attached to the current version of the deployment
	comment, err := g.getComment()
	if err != nil {
		return
	}

	// Gets the deployment configuration
	config, err := g.getConfiguration()
	if err != nil {
		return
	}

	// Gets deployment creation timestamp
	creationTimestamp, err := g.calculateTimestamp("creationTimestamp", g.root)
	if err != nil {
		return
	}

	// Gets the list of existing events related to this deployment
	events, err := g.getEvents()
	if err != nil {
		return
	}

	// Gets the solution id
	id, err := g.getSolutionId()
	if err != nil {
		return
	}

	// Gets when this deployment was modified for the last time
	lastModification, err := g.calculateTimestamp("lastModification", g.root)
	if err != nil {
		return nil, err
	}

	// Gets the links affecting this deployment. Only the external links are included.
	// Internal links (links between the service roles and channels) are represented as
	// connectors in the artifact
	links, err := g.getLinks()
	if err != nil {
		return nil, err
	}

	// Gets who owns the deployment (the deployment creator)
	owner, err := g.getOwner()
	if err != nil {
		return nil, err
	}

	// Gets the artifact reference (name and domain)
	ref, err := g.getReference()
	if err != nil {
		return nil, err
	}

	// Generates the deployment response
	deployment = &Deployment{
		Comment:           comment,
		CreationTimestamp: creationTimestamp,
		Events:            events,
		Id:                id,
		LastModification:  lastModification,
		Links:             links,
		Owner:             owner,
		Ref:               ref,
	}

	if artifact != nil {
		deployment.Artifact = artifact
	}

	if config != nil {
		deployment.Config = config
	}

	return
}

func (g *generator) getComment() (comment string, err error) {

	// Checks if this JSON document includes a comment key. If that's not the case,
	// returns an error
	if !g.root.Exists("comment") || (g.root.Search("comment").Data() == nil) {
		err = fmt.Errorf("comment not found")
		return
	}

	// Gets and returns the comment
	comment = g.root.Search("comment").Data().(string)
	return

}

func (g *generator) initializeHashDictionaries() (err error) {

	for _, deploymentData := range g.root.Search("deployments").ChildrenMap() {
		depRoles := deploymentData.Path("artifact.description.role")
		if depRoles != nil {
			for roleName, roleData := range depRoles.ChildrenMap() {
				hashedRoleName := Hash(roleName)
				g.roleHashToName[hashedRoleName] = roleName

				roleConts := roleData.Path("artifact.description.code")
				if roleConts != nil {
					for contName, _ := range roleConts.ChildrenMap() {
						hashedContName := Hash(contName)
						g.containerHashToName[hashedContName] = contName
					}
				}
			}
		}
	}
	return
}

func (g *generator) getSolutionId() (id string, err error) {
	if !g.root.Exists("name") || (g.root.Search("name").Data() == nil) {
		err = fmt.Errorf("id not found")
		return
	}
	id = g.root.Search("name").Data().(string)
	return
}

func (g *generator) getOwner() (owner string, err error) {

	// Checks if this JSON document includes a owner key. If that's not the case,
	// returns an error
	if !g.root.Exists("owner") || (g.root.Search("owner").Data() == nil) {
		err = fmt.Errorf("owner not found")
		return
	}

	// Gets and returns the owner
	owner = g.root.Search("owner").Data().(string)
	return
}

func (g *generator) getReference() (ref Reference, err error) {

	// Checks if this JSON document includes a solution reference
	if !g.root.Exists("ref") || (g.root.Search("ref").Data() == nil) {
		err = fmt.Errorf("solution reference not found")
		return
	}
	if !g.root.Exists("ref", "domain") || (g.root.Search("ref", "domain").Data() == nil) ||
		!g.root.Exists("ref", "kind") || (g.root.Search("ref", "kind").Data() == nil) ||
		!g.root.Exists("ref", "name") || (g.root.Search("ref", "name").Data() == nil) {
		err = fmt.Errorf("wrong solution reference format")
		return
	}
	kind := g.root.Search("ref", "kind").Data().(string)
	if kind != SolutionReferenceKind {
		err = fmt.Errorf("expected a solution but found a '%s'", kind)
		return
	}

	// Gets the reference
	ref, err = g.processReference(g.root.Search("ref"))

	return
}
