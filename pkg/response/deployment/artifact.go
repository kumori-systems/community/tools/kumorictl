/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strings"

	"github.com/Jeffail/gabs/v2"
)

// getRootArtifact returns the artifact of the top deployment. The subdeployments are
// included as roles.
func (g *generator) getRootArtifact() (artifact Artifact, err error) {

	// Gets the top deployment
	if !g.root.Exists("top") || (g.root.Search("top").Data() == nil) {
		err = fmt.Errorf("top deployment not declared")
		return
	}
	top := g.root.Search("top").Data().(string)

	// If this manifest not includes a deployments section the artifact is nil
	if !g.root.Exists("deployments", top) || (g.root.Search("deployments").Data() == nil) {
		return
	}

	// Gets the deployments map. Directly returns if this map is empty
	deploymentsMap := g.root.Search("deployments").ChildrenMap()
	if len(deploymentsMap) <= 0 {
		err = fmt.Errorf("missing top deployment '%s'", top)
		return
	}

	// Gets the artifact after each deployment
	deployments := make(map[string]Artifact, len(deploymentsMap))
	for deploymentName, deploymentData := range deploymentsMap {

		// Checks if there is an artifact for this deployment
		if !deploymentData.Exists("artifact") || (deploymentData.Search("artifact").Data() == nil) {
			err = fmt.Errorf("missing artifact")
			return
		}

		// Gets the deployment id. If not exists is because this deployment has been
		// deleted or has not been created yet. In that case, leaves the id empty
		var id string
		if deploymentData.Exists("id") && (deploymentData.Search("id").Data() != nil) {
			id = deploymentData.Search("id").Data().(string)
		}

		// Gets the deployment artifact
		var deploymentArtifact Artifact
		deploymentArtifact, err = g.processArtifact(id, deploymentData.Search("artifact"))
		if err != nil {
			return
		}

		deployments[deploymentName] = deploymentArtifact
	}

	// Deflatens the original deployments section adding subdeployments as roles in their
	// parent deployment. This only makes sense if there are subdeployments.
	if len(deployments) > 1 {
		for deploymentName, deploymentData := range deploymentsMap {

			// Skips top deployment since it
			if deploymentName == top {
				continue
			}

			// Gets the deployment artifact
			deploymentArtifact, ok := deployments[deploymentName]
			if !ok {
				err = fmt.Errorf("artifact not found for deployment '%s'", deploymentName)
				return
			}

			// Calculates the deployment configuration
			var deploymentConfig *Configuration
			if deploymentData.Exists("config") && (deploymentData.Search("config").Data() != nil) {
				configData := deploymentData.Search("config")
				deploymentConfig, err = g.processConfiguration(configData)
				if err != nil {
					err = fmt.Errorf("error processing deployment '%s' configuration: %v", deploymentName, err)
				}
			}

			// Gets the parent deployment name
			if !deploymentData.Exists("up") || (deploymentData.Search("up").Data() == nil) {
				err = fmt.Errorf("missing up deployment")
				return
			}
			up := deploymentData.Search("up").Data().(string)

			// Calculate the deployment role in its up deployment
			index := len(up) + 1
			role := deploymentName[index:]

			// Adds a new role in the up deployment
			upArtifact, ok := deployments[up]
			if !ok {
				err = fmt.Errorf("missing artifact of up deployment '%s'", up)
				return
			}
			upServiceArtifact := upArtifact.(ServiceArtifact)
			if _, ok := upServiceArtifact.Description.Roles[role]; ok {
				err = fmt.Errorf("role '%s' already found in artifact '%s'", role, up)
				return
			}
			upServiceArtifact.Description.Roles[role] = Role{
				Artifact: deploymentArtifact,
				Config:   deploymentConfig,
			}
		}

		// Restores the original connectors using the manifest links section
		if g.root.Exists("links") && (g.root.Search("links").Data() != nil) {
			links := g.root.Search("links").Children()
			if len(links) > 0 {
				for _, link := range links {
					// Checks the link format
					for _, section := range []string{"s_c", "s_d", "t_c", "t_d"} {
						if !link.Exists(section) || (link.Search(section).Data() == nil) {
							err = fmt.Errorf("missing %s", section)
							return
						}
					}

					sourceChannel := link.Search("s_c").Data().(string)
					sourceDeployment := link.Search("s_d").Data().(string)
					targetChannel := link.Search("t_c").Data().(string)
					targetDeployment := link.Search("t_d").Data().(string)

					// Checks which is the parent endpoint
					if strings.HasPrefix(sourceDeployment, targetDeployment) {
						parentArtifact := deployments[targetDeployment]
						parentServiceArtifact := parentArtifact.(ServiceArtifact)
						parentRole := sourceDeployment[len(targetDeployment)+1:]
						connectors := parentServiceArtifact.Description.Connectors
						found := false
						for connectorName, connectorData := range connectors {
							for clientIndex, clientData := range connectorData.Clients {
								if (clientData.Channel == targetChannel) && (clientData.Role == "self") {
									connectors[connectorName].Clients[clientIndex] = Endpoint{
										Role:    parentRole,
										Channel: sourceChannel,
									}
									found = true
									break
								}
							}
							if found {
								break
							}
						}

					} else if strings.HasPrefix(targetDeployment, sourceDeployment) {
						parentArtifact := deployments[sourceDeployment]
						parentServiceArtifact := parentArtifact.(ServiceArtifact)
						parentRole := targetDeployment[len(sourceDeployment)+1:]
						connectors := parentServiceArtifact.Description.Connectors
						found := false
						for connectorName, connectorData := range connectors {
							for serverIndex, serverData := range connectorData.Servers {
								for linkIndex, linkData := range serverData.Links {
									if (linkData.Channel == sourceChannel) && (linkData.Role == "self") {
										connectors[connectorName].Servers[serverIndex].Links[linkIndex] = Endpoint{
											Role:    parentRole,
											Channel: targetChannel,
										}
										found = true
										break
									}
								}
								if found {
									break
								}
							}
							if found {
								break
							}
						}
					} else {
						err = fmt.Errorf("invalid links. Deployments are not related")
						return
					}

					// TBD: Metadata
				}
			}
		}
	}

	// Returns the artifact of the top deployment
	var ok bool
	if artifact, ok = deployments[top]; !ok {
		err = fmt.Errorf("missing artifact for top deployment '%s'", top)
	}

	return
}

func (g *generator) processArtifact(id string, data *gabs.Container) (artifact Artifact, err error) {

	// Checks if there is a description for this deployment
	if !data.Exists("description") || (data.Search("description").Data() == nil) {
		err = fmt.Errorf("missing description")
		return
	}

	// Gets and checks the artifact ref
	if !data.Exists("ref", "domain") || (data.Search("ref", "domain").Data() == nil) {
		err = fmt.Errorf("missing artifact reference domain")
		return
	}
	if !data.Exists("ref", "kind") || (data.Search("ref", "kind").Data() == nil) {
		err = fmt.Errorf("missing artifact reference kind")
		return
	}
	if !data.Exists("ref", "name") || (data.Search("ref", "name").Data() == nil) {
		err = fmt.Errorf("missing artifact reference name")
		return
	}
	if !data.Exists("ref", "version") || (data.Search("ref", "version").Data() == nil) {
		err = fmt.Errorf("missing artifact reference version")
		return
	}
	kind := data.Search("ref", "kind").Data().(string)

	// Generates the artifact depending on its kind
	switch kind {
	case ServiceReferenceKind:
		artifact, err = g.processServiceArtifact(id, data)
	case ComponentReferenceKind:
		artifact, err = g.processComponentArtifact(id, data)
	default:
		err = fmt.Errorf("reference kind '%s' is invalid", kind)
	}
	return
}

func (g *generator) processServiceArtifact(id string, data *gabs.Container) (artifact Artifact, err error) {

	// Gets the service reference
	var ref ServiceReference
	ref, err = g.processServiceReference(data.Search("ref"))

	// Gets the builtin section
	if !data.Exists("description", "builtin") || (data.Search("description", "builtin").Data() == nil) {
		err = fmt.Errorf("missing builtin section")
		return
	}
	builtin := data.Search("description", "builtin").Data().(bool)

	// Gets the srv section
	var srv *Srv
	if data.Exists("description", "srv") && (data.Search("description", "srv").Data() != nil) {
		srvData := data.Search("description", "srv")
		srv, err = g.processChannels(srvData)
		if err != nil {
			err = fmt.Errorf("error processing srv section: %v", err)
			return
		}
	}

	// If the service is a builin service, that's all we need
	if builtin {
		artifact = BuiltInServiceArtifact{
			Description: BuiltInDescripton{
				BaseDescription: BaseDescription{
					Srv: *srv,
				},
				BuiltIn: BuiltInValue,
			},
			Id:  id,
			Ref: ref,
		}
		return
	}

	// Otherwise, gets the connectors and roles information

	// Gets connectors
	var connectors map[string]Connector
	if data.Exists("description", "connector") && (data.Search("description", "connector").Data() != nil) {
		connectorsData := data.Search("description", "connector")
		connectors, err = g.processConnectors(connectorsData)
		if err != nil {
			err = fmt.Errorf("connectors processing failed: %v", err)
		}
	}

	// Gets roles
	if !data.Exists("description", "role") || (data.Search("description", "role").Data() == nil) {
		err = fmt.Errorf("missing roles")
		return
	}
	rolesData := data.Search("description", "role").ChildrenMap()
	if len(rolesData) <= 0 {
		err = fmt.Errorf("missing roles")
		return
	}
	roles := make(map[string]Role, len(rolesData))
	for roleName, roleData := range rolesData {
		var role *Role
		role, err = g.processRole(id, roleData)
		if err != nil {
			err = fmt.Errorf("error processing role '%s': %v", roleName, err)
			return
		}
		roles[roleName] = *role
	}

	// Creates service artifact
	artifact = ServiceArtifact{
		Description: ServiceDescription{
			NoBuiltInDescription: NoBuiltInDescription{
				BaseDescription: BaseDescription{
					Srv: *srv,
				},
				BuiltIn: NotBuiltInValue,
			},
			Connectors: connectors,
			Roles:      roles,
		},
		Id:  id,
		Ref: ref,
	}

	return
}

func (g *generator) processComponentArtifact(id string, data *gabs.Container) (artifact Artifact, err error) {

	// Gets the component reference
	var ref ComponentReference
	ref, err = g.processComponentReference(data.Search("ref"))

	// Gets the builtin section
	if !data.Exists("description", "builtin") || (data.Search("description", "builtin").Data() == nil) {
		err = fmt.Errorf("missing builtin section")
		return
	}
	builtin := data.Search("description", "builtin").Data().(bool)

	// Gets the srv section
	var srv *Srv
	if data.Exists("description", "srv") && (data.Search("description", "srv").Data() != nil) {
		srvData := data.Search("description", "srv")
		srv, err = g.processChannels(srvData)
		if err != nil {
			err = fmt.Errorf("error processing srv section: %v", err)
			return
		}
	}

	// If the component is a builin service, that's all we need
	if builtin {
		artifact = BuiltInComponentArtifact{
			Ref: ref,
			Description: BuiltInDescripton{
				BaseDescription: BaseDescription{
					Srv: *srv,
				},
				BuiltIn: BuiltInValue,
			},
		}
		return
	}

	// Get code
	if !data.Exists("description", "code") || (data.Search("description", "code").Data() == nil) {
		err = fmt.Errorf("code section not found in non-builtin component artifact")
		return
	}
	codeData := data.Search("description", "code")
	var code map[string]Code
	code, err = g.processCode(codeData)
	if err != nil {
		return
	}

	// Get probes
	var probes *map[string]Probes
	if data.Exists("description", "probe") && (data.Search("description", "probe").Data() != nil) {
		probesData := data.Search("description", "probe")
		if probes, err = g.processProbes(probesData); err != nil {
			return
		}
	}

	// Get size
	var componentSize ComponentSize
	if data.Exists("description", "size") && (data.Search("description", "size").Data() != nil) {
		componentSizeData := data.Search("description", "size")
		componentSize, err = g.processComponentSize(componentSizeData)
		if err != nil {
			return
		}
	}

	artifact = ComponentArtifact{
		Ref: ref,
		Description: ComponentDescription{
			NoBuiltInDescription: NoBuiltInDescription{
				BaseDescription: BaseDescription{
					Srv: *srv,
				},
				BuiltIn: NotBuiltInValue,
			},
			Code:   code,
			Probes: probes,
			Size:   componentSize,
		},
	}

	return
}

func (g *generator) processComponentSize(data *gabs.Container) (size ComponentSize, err error) {

	// Checks a bandwidth section is included
	if !data.Exists("bandwidth") || (data.Search("bandwidth").Data() == nil) {
		err = fmt.Errorf("bandwidth section not found")
		return
	}

	// Process the bandwidth
	var bandwidth ResourceSize
	bandwidthData := data.Search("bandwidth")
	bandwidth, err = g.processResourceSize(bandwidthData)
	if err != nil {
		err = fmt.Errorf("error processing bandwidth: %v", err)
		return
	}

	// Creates the component bandwidth object
	size = ComponentSize{
		Bandwidth: BandwidthSize(bandwidth),
	}

	return
}
