/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"hash/fnv"
	"sort"
	"strconv"
	"time"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) calculateTimestamp(fieldName string, data *gabs.Container) (timestamp time.Time, err error) {

	// Checks if this JSON document includes a creationTimestamp key. If that's not the case,
	// returns an error
	if !data.Exists(fieldName) || (data.Search(fieldName).Data() == nil) {
		err = fmt.Errorf("%s not found", fieldName)
		return
	}

	// Gets the creationTimestamp as a string
	sTimestamp := data.Search(fieldName).Data().(string)

	// Converts the string to a time
	timestamp, err = time.Parse(time.RFC3339, sTimestamp)

	return

}

func (g *generator) processResourceSize(data *gabs.Container) (size ResourceSize, err error) {

	// Checks if this JSON contains the expected elements
	if !data.Exists("size") || (data.Search("size").Data() == nil) {
		err = fmt.Errorf("missing size")
		return
	}
	if !data.Exists("unit") || (data.Search("unit").Data() == nil) {
		err = fmt.Errorf("missing unit")
		return
	}

	// Retrieves the size as an unsigned integer
	sizeData := data.Search("size").String()
	sizeInt, err := strconv.Atoi(sizeData)
	if err != nil {
		err = fmt.Errorf("size '%s' is not an integer: %v", sizeData, err)
	}
	sizeAmount := uint32(sizeInt)

	// Retrives the size unit
	sizeUnit := data.Search("unit").Data().(string)

	// Constructs and returns size
	size = ResourceSize{
		Size: sizeAmount,
		Unit: sizeUnit,
	}

	return
}

// getKeysOrderedByName returns an array containing the map keys ordered by name
func (g *generator) getKeysOrderedByName(themap map[string]*gabs.Container) []string {
	keys := make([]string, 0, len(themap))
	for key := range themap {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

// Hash returns a hashed version of a given string
func Hash(source string) string {
	hf := fnv.New32()
	hf.Write([]byte(source))
	return string(fmt.Sprintf("%08x", hf.Sum32()))
}
