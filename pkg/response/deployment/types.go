/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"time"
)

type Deployment struct {
	Artifact          Artifact          `json:"artifact"`
	Comment           string            `json:"comment,omitempty"`
	Config            *Configuration    `json:"config,omitempty"`
	CreationTimestamp time.Time         `json:"creationTimestamp"`
	Events            []Event           `json:"events"`
	Id                string            `json:"id"`
	LastModification  time.Time         `json:"lastModification"`
	Links             map[string][]Link `json:"links,omitempty"`
	Owner             string            `json:"owner"`
	Ref               Reference         `json:"ref"`
}

type Artifact interface {
	GetReference() ArtifactReference
}

type BuiltInComponentArtifact struct {
	Description BuiltInDescripton  `json:"description"`
	Ref         ComponentReference `json:"ref"`
}

func (a BuiltInComponentArtifact) GetReference() ArtifactReference {
	return a.Ref
}

type ComponentArtifact struct {
	Description ComponentDescription `json:"description"`
	Ref         ComponentReference   `json:"ref"`
}

func (a ComponentArtifact) GetReference() ArtifactReference {
	return a.Ref
}

type BuiltInServiceArtifact struct {
	Description BuiltInDescripton `json:"description"`
	Id          string            `json:"id"`
	Ref         ServiceReference  `json:"ref"`
}

func (a BuiltInServiceArtifact) GetReference() ArtifactReference {
	return a.Ref
}

type ServiceArtifact struct {
	Description ServiceDescription `json:"description"`
	Id          string             `json:"id"`
	Ref         ServiceReference   `json:"ref"`
}

func (a ServiceArtifact) GetReference() ArtifactReference {
	return a.Ref
}

type BaseDescription struct {
	Srv Srv `json:"srv"`
}

type BuiltInDescripton struct {
	BaseDescription
	BuiltIn BuiltIn `json:"builtin"`
}

type NoBuiltInDescription struct {
	BaseDescription
	BuiltIn NotBuiltIn `json:"builtin"`
}

type ComponentDescription struct {
	NoBuiltInDescription
	Code   map[string]Code    `json:"code"`
	Probes *map[string]Probes `json:"probes,omitempty"`
	Size   ComponentSize      `json:"size"`
}

type ServiceDescription struct {
	NoBuiltInDescription
	Connectors map[string]Connector `json:"connector"`
	Roles      map[string]Role      `json:"role"`
}

type Code struct {
	Image      Image          `json:"image"`
	Init       bool           `json:"init"`
	Entrypoint *[]string      `json:"entrypoint,omitempty"`
	Cmd        *[]string      `json:"cmd,omitempty"`
	Mapping    Mapping        `json:"mapping"`
	Size       *ContainerSize `json:"size"`
	User       *User          `json:"user,omitempty"`
}

// User contains information about the user and group used to execute
// the component process
type User struct {
	UserID  int64 `json:"userid"`
	GroupID int64 `json:"groupid"`
}

// Mapping is blah
type Mapping struct {
	Env        map[string]Env        `json:"env,omitempty"`
	FileSystem map[string]MountPoint `json:"filesystem,omitempty"`
}

// Env contains the environment variable value or the secret storing this value.
type Env struct {
	Value       *string `json:"value,omitempty"`
	ResourceRef `json:",inline"`
}

// ResourceRef contains a reference to a given resource
type ResourceRef struct {
	Secret      string `json:"secret,omitempty"`
	Port        string `json:"port,omitempty"`
	Domain      string `json:"domain,omitempty"`
	Certificate string `json:"certificate,omitempty"`
	CA          string `json:"ca,omitempty"`
}

// MountPointData contains a mounted file data
type MountPointData struct {
	Value       *json.RawMessage `json:"value,omitempty"`
	ResourceRef `json:",inline"`
}

// MountPoint contains information to expose data on a container filesystem. It can mount:
// * A file: the data will be taken from `Data` or from a Secret.
// * A volatlile empty volume: the size of the volume must be specified.
// * A persistent volume: TBD
type MountPoint struct {
	// Data contains the file content if the data is directly included.
	Data *MountPointData `json:"data,omitempty"`
	// Format contains information about the format of the file content. Formats allowed:
	// * text: the content is stored as it is.
	// * json: the content is a json document.
	// * yaml: the content is a yaml document.
	// * latdict: ...
	// This field is only used if the data is provided in the `Value` key
	Format string `json:"format,omitempty"`
	// Group contains the group owning this file in the container
	Group *int64 `json:"group,omitempty"`
	// Mode contains the file permissions
	Mode *int32 `json:"mode,omitempty"`
	// Path indicates where the file should be stored
	Path string `json:"path"`
	// User contains the user owning this file in the container.
	User *int64 `json:"user,omitempty"`
	// RebootOnUpdate must set to true to disable file hot updates. Hence, if the file data changes, the
	// role instances is rebooted.
	RebootOnUpdate *bool `json:"rebootOnUpdate,omitempty"`
	// Volume is used if a volume is mounted in the given path
	Volume string `json:"volume,omitempty"`
}

// Image is blah
type Image struct {
	Hub *Hub   `json:"hub,omitempty"`
	Tag string `json:"tag"`
}

// Hub is blah
type Hub struct {
	Name   string `json:"name"`
	Secret string `json:"secret,omitempty"`
}

type ContainerSize struct {
	CPU    CPUSize    `json:"cpu"`
	Memory MemorySize `json:"memory"`
}

// Probes describes the probes declared for a given container
type Probes struct {
	Liveness          *LivenessProbeAttributes          `json:"liveness,omitempty"`
	Readiness         *ReadinessProbeAttributes         `json:"readiness,omitempty"`
	PrometheusMetrics *PrometheusMetricsProbeAttributes `json:"pmetrics,omitempty"`
}

// ProbeProtocol only one must be != nil
type ProbeProtocol struct {
	HTTP *HTTPProbeProtocol `json:"http,omitempty"`
	TCP  *TCPProbeProtocol  `json:"tcp,omitempty"`
	Exec *ExecProbeProtocol `json:"exec,omitempty"`
}

// HTTPOnlyProbeProtocol allows only HTTP probes
type HTTPOnlyProbeProtocol struct {
	HTTP *HTTPProbeProtocol `json:"http,omitempty"`
}

type HTTPProbeProtocol struct {
	Port int32  `json:"port"`
	Path string `json:"path"`
}

type TCPProbeProtocol struct {
	Port int32 `json:"port"`
}

type ExecProbeProtocol struct {
	Path string `json:"path"`
}

// LivenessProbeAttributes are the attributes required by liveness probe
type LivenessProbeAttributes struct {
	Protocol           ProbeProtocol       `json:"protocol"`
	StartupGraceWindow *StartupGraceWindow `json:"startupGraceWindow,omitempty"`
	Frequency          *json.RawMessage    `json:"frequency,omitempty"`
	Timeout            *int32              `json:"timeout,omitempty"`
}

// ReadinessProbeAttributes are the attributes required by liveness probe
type ReadinessProbeAttributes struct {
	Protocol ProbeProtocol `json:"protocol"`
	// StartupGraceWindow *StartupGraceWindow   `json:"startupGraceWindow,omitempty"`
	Frequency *json.RawMessage `json:"frequency,omitempty"`
	Timeout   *int32           `json:"timeout,omitempty"`
}

// PrometheusMetricsProbeAttributes are the attributes required by a Prometheus metrics probe.
// Only http protocol is allowed
type PrometheusMetricsProbeAttributes struct {
	Protocol HTTPOnlyProbeProtocol `json:"protocol"`
}

// StartupGraceWindow defines the maximum duration of a componen startup. It can
// be represented as an absolute time or as a number of retries.
type StartupGraceWindow struct {
	Unit     StartupGraceWindowUnit `json:"unit"`
	Duration uint                   `json:"duration"`
	Probe    bool                   `json:"probe"`
}

// StartupGraceWindowUnit the units used to represent the maximum startup
// duration for a given components. Can be "attempts" or "ms"
type StartupGraceWindowUnit string

const (
	// AttemptsStartupGraceWindowUnit is used when the duration is represented in attempts
	AttemptsStartupGraceWindowUnit StartupGraceWindowUnit = "attempts"
	// MillisStartupGraceWindowUnit is used when the duration is represented in millis
	MillisStartupGraceWindowUnit StartupGraceWindowUnit = "ms"
)

type ComponentSize struct {
	Bandwidth BandwidthSize `json:"bandwidth"`
}

type ResourceSize struct {
	Size uint32 `json:"size"`
	Unit string `json:"unit"`
}

type BandwidthSize ResourceSize
type MemorySize ResourceSize

type CPUSize struct {
	ResourceSize `json:",inline"`
	Min          uint32 `json:"min"`
}

type BuiltIn bool

const BuiltInValue BuiltIn = true

type NotBuiltIn bool

const NotBuiltInValue NotBuiltIn = false

type Connector struct {
	Clients []Endpoint    `json:"clients,omitempty"`
	Kind    ConnectorKind `json:"kind"`
	Servers []Tag         `json:"servers"`
}

type ConnectorKind string

const (
	LBConnectorKind   ConnectorKind = "lb"
	FullConnectorKind ConnectorKind = "full"
)

type Endpoint struct {
	Channel string `json:"channel"`
	Role    string `json:"role"`
}

type Tag struct {
	Links []Endpoint        `json:"links"`
	Meta  []json.RawMessage `json:"meta"`
}

type Role struct {
	Artifact  Artifact            `json:"artifact"`
	Config    *Configuration      `json:"config,omitempty"`
	Instances map[string]Instance `json:"instances,omitempty"`
	Meta      *json.RawMessage    `json:"meta,omitempty"`
}

type Instance struct {
	Containers        map[string]InstanceContainer `json:"containers"`
	CreationTimestamp time.Time                    `json:"creationTimestamp"`
	Events            []Event                      `json:"events"`
	Id                string                       `json:"id"`
	Metrics           Metrics                      `json:"metrics"`
	Node              string                       `json:"node"`
	Ready             bool                         `json:"ready"`
	Status            string                       `json:"status"`
	StatusReason      *string                      `json:"statusReason,omitempty"`
	StatusMessage     *string                      `json:"statusMessage,omitempty"`
}

type InstanceContainer struct {
	Metrics  Metrics        `json:"metrics"`
	Ready    bool           `json:"ready"`
	Restarts uint32         `json:"restarts"`
	States   InstanceStates `json:"states"`
}

type InstanceStates struct {
	Running    *InstanceRunningState    `json:"running,omitempty"`
	Waiting    *InstanceWaitingState    `json:"waiting,omitempty"`
	Terminated *InstanceTerminatedState `json:"terminated,omitempty"`
}

type InstanceRunningState struct {
	StartedAt *time.Time `json:"startedAt,omitempty"`
}

type InstanceWaitingState struct {
	Message *string `json:"message,omitempty"`
	Reason  *string `json:"reason,omitempty"`
}

type InstanceTerminatedState struct {
	ContainerID *string    `json:"containerID,omitempty"`
	ExitCode    int64      `json:"exitCode"`
	FinishedAt  *time.Time `json:"finishedAt,omitempty"`
	Message     *string    `json:"message,omitempty"`
	Reason      *string    `json:"reason,omitempty"`
	Signal      *int64     `json:"signal,omitempty"`
	StartedAt   *time.Time `json:"startedAt,omitempty"`
}

type Metrics struct {
	Usage UsageMetrics `json:"usage"`
}

type UsageMetrics struct {
	CPU    float64 `json:"cpu"`
	Memory uint64  `json:"memory"`
}

type Srv struct {
	Clients map[string]Client `json:"client,omitempty"`
	Duplex  map[string]Duplex `json:"duplex,omitempty"`
	Servers map[string]Server `json:"server,omitempty"`
}

type Client struct {
	Protocol string `json:"protocol"`
}

type Server struct {
	Port     int32  `json:"port"`
	PortNum  int32  `json:"portnum"`
	Protocol string `json:"protocol"`
}

type Duplex Server

type Configuration struct {
	Parameters map[string]json.RawMessage `json:"parameter,omitempty"`
	Resources  map[string]Resource        `json:"resource,omitempty"`
	Resilience int32                      `json:"resilience"`
	Scale      *Scale                     `json:"scale,omitempty"`
}

type Scale struct {
	HorizontalSize int32 `json:"hsize"`
}

type Resource struct {
	CA          string           `json:"ca,omitempty"`
	Certificate string           `json:"certificate,omitempty"`
	Domain      string           `json:"domain,omitempty"`
	Port        string           `json:"port,omitempty"`
	Secret      string           `json:"secret,omitempty"`
	Volume      *json.RawMessage `json:"volume,omitempty"`
}

type Event struct {
	Counter        int       `json:"counter"`
	FirstTimestamp time.Time `json:"firstTimestamp"`
	LastTimestamp  time.Time `json:"lastTimestamp"`
	Message        string    `json:"message"`
	Source         *Source   `json:"source,omitempty"`
	Reason         string    `json:"reason"`
	Type           string    `json:"type"`
}

type Source struct {
	Kind string `json:"kind"`
	Name string `json:"name"`
	Id   string `json:"id"`
}

type Link struct {
	Channels []string         `json:"channels"`
	Domain   string           `json:"domain"`
	Name     string           `json:"name"`
	Meta     *json.RawMessage `json:"meta,omitempty"`
}

type Reference struct {
	Domain string `json:"domain"`
	Module string `json:"module,omitempty"`
	Name   string `json:"name"`
}

type ArtifactReference interface {
	GetKind() string
}

type VersionedReference struct {
	Reference  `json:",inline"`
	Version    Version `json:"version"`
	PreRelease string  `json:"prerelease,omitempty"`
}

type ComponentReference struct {
	VersionedReference `json:",inline"`
	Kind               ComponentKind `json:"kind"`
}

func (r ComponentReference) GetKind() string {
	return string(r.Kind)
}

type ServiceReference struct {
	VersionedReference `json:",inline"`
	Kind               ServiceKind `json:"kind"`
}

func (r ServiceReference) GetKind() string {
	return string(r.Kind)
}

type ComponentKind string

const ComponentKindValue ComponentKind = "component"

type ServiceKind string

const ServiceKindValue ServiceKind = "service"

type Version [3]int32
