/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/Jeffail/gabs/v2"
)

func (g *generator) processCode(data *gabs.Container) (code map[string]Code, err error) {
	codeMap := data.ChildrenMap()

	// The role must have containers declared
	if len(codeMap) <= 0 {
		err = fmt.Errorf("missing containers")
		return
	}

	code = make(map[string]Code, len(codeMap))

	for containerName, containerData := range codeMap {

		// Process image section
		var image Image
		image, err = g.processImage(containerData)
		if err != nil {
			err = fmt.Errorf("error processing image data in container '%s': %v", containerName, err)
			return
		}

		// Process mapping section
		var mapping *Mapping
		mapping, err = g.processMapping(containerData)
		if err != nil {
			err = fmt.Errorf("error processing mapping data in container '%s': %v", containerName, err)
			return
		}

		// Creates the base code structure
		container := Code{
			Image:   image,
			Mapping: *mapping,
		}

		// Process the entrypoint
		if containerData.Exists("entrypoint") && (containerData.Search("entrypoint").Data() != nil) {
			entrypointData := containerData.Search("entrypoint").Children()
			if len(entrypointData) > 0 {
				entrypoint := make([]string, len(entrypointData))
				for index, elemData := range entrypointData {
					entrypoint[index] = elemData.Data().(string)
				}
				container.Entrypoint = &entrypoint
			}
		}

		// Process the cmd
		if containerData.Exists("cmd") && (containerData.Search("cmd").Data() != nil) {
			cmdData := containerData.Search("cmd").Children()
			if len(cmdData) > 0 {
				cmd := make([]string, len(cmdData))
				for index, cmdData := range cmdData {
					cmd[index] = cmdData.Data().(string)
				}
				container.Cmd = &cmd
			}
		}

		// Process the size
		if containerData.Exists("size") && (containerData.Search("size").Data() != nil) {
			var size ContainerSize
			size, err = g.processContainerSize(containerData)
			if err != nil {
				err = fmt.Errorf("error processing container '%s' size: %v", containerName, err)
				return
			}
			container.Size = &size
		}

		// Process the user
		if containerData.Exists("user") && (containerData.Search("user").Data() != nil) {
			var user User
			user, err = g.processUser(containerData)
			if err != nil {
				err = fmt.Errorf("error processing container '%s' user: %v", containerName, err)
				return
			}
			container.User = &user
		}

		// Process the init flag
		if containerData.Exists("init") && (containerData.Search("init").Data() != nil) {
			initData := containerData.Search("init").Data().(bool)
			container.Init = initData
		} else {
			container.Init = false
		}

		// Adds container info to code map
		code[containerName] = container
	}

	return
}

func (g *generator) processImage(data *gabs.Container) (image Image, err error) {

	// Checks image information
	if !data.Exists("image") || (data.Search("image").Data() == nil) {
		err = fmt.Errorf("missing image section")
		return
	}
	if !data.Exists("image", "tag") || (data.Search("image", "tag").Data() == nil) {
		err = fmt.Errorf("missing image tag")
		return
	}

	// Gets the image tag
	tag := data.Search("image", "tag").Data().(string)

	// Creates base image
	image = Image{
		Tag: tag,
	}

	// Adds the image hub information (if any)
	if data.Exists("image", "hub") && (data.Search("image", "hub").Data() != nil) {
		if !data.Exists("image", "hub", "name") || (data.Search("image", "hub", "name").Data() == nil) {
			err = fmt.Errorf("missing hub name")
			return
		}
		image.Hub = &Hub{
			Name: data.Search("image", "hub", "name").Data().(string),
		}
		if data.Exists("image", "hub", "secret") && (data.Search("image", "hub", "secret").Data() != nil) {
			secret := data.Search("image", "hub", "secret").Data().(string)
			if len(secret) > 0 {
				image.Hub.Secret = secret
			}
		}
	}

	return
}

func (g *generator) processMapping(data *gabs.Container) (mapping *Mapping, err error) {

	// Returns nil if the mapping is missing
	if !data.Exists("mapping") || (data.Search("mapping").Data() == nil) {
		return
	}

	// Initializes de mapping objets
	mapping = &Mapping{}

	// Adds environment variables
	if data.Exists("mapping", "env") && (data.Search("mapping", "env").Data() != nil) {
		var env map[string]Env
		env, err = g.processEnvMapping(data.Search("mapping", "env"))
		if err != nil {
			return
		}
		mapping.Env = env
	}

	// Adds file and volume mappings
	if data.Exists("mapping", "filesystem") && (data.Search("mapping", "filesystem").Data() != nil) {
		var filesystem map[string]MountPoint
		filesystem, err = g.processFileSystemMapping(data.Search("mapping", "filesystem"))
		if err != nil {
			return
		}
		mapping.FileSystem = filesystem
	}

	return
}

func (g *generator) processEnvMapping(data *gabs.Container) (env map[string]Env, err error) {
	envsData := data.ChildrenMap()
	if len(envsData) <= 0 {
		return
	}

	env = make(map[string]Env, len(envsData))

	for envName, envData := range envsData {

		envElement := Env{}

		if envData.Exists("value") && (envData.Search("value").Data() != nil) {
			value := envData.Search("value").Data().(string)
			envElement.Value = &value
		}
		if envData.Exists("secret") && (envData.Search("secret").Data() != nil) {
			value := envData.Search("secret").Data().(string)
			envElement.Secret = value
		}
		if envData.Exists("port") && (envData.Search("port").Data() != nil) {
			value := envData.Search("port").Data().(string)
			envElement.Port = value
		}
		if envData.Exists("domain") && (envData.Search("domain").Data() != nil) {
			value := envData.Search("domain").Data().(string)
			envElement.Domain = value
		}
		if envData.Exists("certificate") && (envData.Search("certificate").Data() != nil) {
			value := envData.Search("certificate").Data().(string)
			envElement.Certificate = value
		}
		if envData.Exists("ca") && (envData.Search("ca").Data() != nil) {
			value := envData.Search("ca").Data().(string)
			envElement.CA = value
		}

		env[envName] = envElement
	}

	return
}

func (g *generator) processFileSystemMapping(data *gabs.Container) (
	filesystem map[string]MountPoint,
	err error,
) {
	fsData := data.ChildrenMap()
	if len(fsData) <= 0 {
		return
	}

	filesystem = make(map[string]MountPoint, len(fsData))

	for fsName, fsData := range fsData {
		fsElement := MountPoint{}

		if fsData.Exists("data") && (fsData.Search("data").Data() != nil) {
			fsElement.Data = &MountPointData{}
			if fsData.Exists("data", "value") && (fsData.Search("data", "value").Data() != nil) {
				valueStr := fsData.Search("data", "value").String()
				value := json.RawMessage([]byte(valueStr))
				fsElement.Data.Value = &value
			}
			if fsData.Exists("data", "secret") && (fsData.Search("data", "secret").Data() != nil) {
				value := fsData.Search("data", "secret").Data().(string)
				fsElement.Data.Secret = value
			}
			if fsData.Exists("data", "port") && (fsData.Search("data", "port").Data() != nil) {
				value := fsData.Search("data", "port").Data().(string)
				fsElement.Data.Port = value
			}
			if fsData.Exists("data", "domain") && (fsData.Search("data", "domain").Data() != nil) {
				value := fsData.Search("data", "domain").Data().(string)
				fsElement.Data.Domain = value
			}
			if fsData.Exists("data", "certificate") && (fsData.Search("data", "certificate").Data() != nil) {
				value := fsData.Search("data", "certificate").Data().(string)
				fsElement.Data.Certificate = value
			}
			if fsData.Exists("data", "ca") && (fsData.Search("data", "ca").Data() != nil) {
				value := fsData.Search("data", "ca").Data().(string)
				fsElement.Data.CA = value
			}
		}

		if fsData.Exists("format") && (fsData.Search("format").Data() != nil) {
			format := fsData.Search("format").Data().(string)
			fsElement.Format = format
		}

		if fsData.Exists("group") && (fsData.Search("group").Data() != nil) {
			groupstr := fsData.Search("group").String()
			var groupint int
			groupint, err = strconv.Atoi(groupstr)
			group := int64(groupint)
			fsElement.Group = &group
		}

		if fsData.Exists("mode") && (fsData.Search("mode").Data() != nil) {
			modestr := fsData.Search("mode").String()
			var modeint int
			modeint, err = strconv.Atoi(modestr)
			mode := int32(modeint)
			fsElement.Mode = &mode
		}

		if fsData.Exists("path") && (fsData.Search("path").Data() != nil) {
			filePath := fsData.Search("path").Data().(string)
			fsElement.Path = filePath
		}

		if fsData.Exists("user") && (fsData.Search("user").Data() != nil) {
			userstr := fsData.Search("user").String()
			var userint int
			userint, err = strconv.Atoi(userstr)
			user := int64(userint)
			fsElement.User = &user
		}

		if fsData.Exists("volume") && (fsData.Search("volume").Data() != nil) {
			volume := fsData.Search("volume").Data().(string)
			fsElement.Volume = volume
		} else {
			rebootOnUpdate := false
			if fsData.Exists("rebootOnUpdate") && (fsData.Search("rebootOnUpdate").Data() != nil) {
				rebootOnUpdate = fsData.Search("rebootOnUpdate").Data().(bool)
			}
			fsElement.RebootOnUpdate = &rebootOnUpdate
		}

		filesystem[fsName] = fsElement

	}

	return
}

func (g *generator) processContainerSize(data *gabs.Container) (size ContainerSize, err error) {

	// Checks size
	if !data.Exists("size", "cpu") || (data.Search("size", "cpu").Data() == nil) {
		err = fmt.Errorf("cpu size missing")
		return
	}
	if !data.Exists("size", "mincpu") || (data.Search("size", "mincpu").Data() == nil) {
		err = fmt.Errorf("cpu size missing")
		return
	}
	if !data.Exists("size", "memory") || (data.Search("size", "memory").Data() == nil) {
		err = fmt.Errorf("memory size missing")
		return
	}

	// Gets mincpu
	mincpuData := data.Search("size", "mincpu").String()
	mincpuInt, err := strconv.Atoi(mincpuData)
	if err != nil {
		err = fmt.Errorf("mincpu '%s' is not an integer: %v", mincpuData, err)
		return
	}
	mincpu := uint32(mincpuInt)

	// Gets cpu resource
	cpuSizeData := data.Search("size", "cpu")
	var cpuResource ResourceSize
	cpuResource, err = g.processResourceSize(cpuSizeData)
	if err != nil {
		err = fmt.Errorf("error processing cpu size: %v", err)
		return
	}
	cpuSize := CPUSize{
		ResourceSize: cpuResource,
		Min:          mincpu,
	}

	// Gets memory resource
	memSizeData := data.Search("size", "memory")
	var memResource ResourceSize
	memResource, err = g.processResourceSize(memSizeData)
	if err != nil {
		err = fmt.Errorf("error processing mem size: %v", err)
	}

	// Creates the size object
	size = ContainerSize{
		CPU:    cpuSize,
		Memory: MemorySize(memResource),
	}

	return
}

func (g *generator) processUser(data *gabs.Container) (user User, err error) {
	if !data.Exists("user", "userid") || (data.Search("user", "userid").Data() == nil) {
		err = fmt.Errorf("missing userid")
		return
	}
	if !data.Exists("user", "groupid") || (data.Search("user", "groupid").Data() == nil) {
		err = fmt.Errorf("missing groupid")
		return
	}

	useridData := data.Search("user", "userid").String()
	var useridInt int
	useridInt, err = strconv.Atoi(useridData)
	if err != nil {
		err = fmt.Errorf("userid '%s' is not a number: %v", useridData, err)
		return
	}

	groupidData := data.Search("user", "groupid").String()
	var groupidInt int
	groupidInt, err = strconv.Atoi(groupidData)
	if err != nil {
		err = fmt.Errorf("groupid '%s' is not a number: %v", groupidData, err)
		return
	}

	user = User{
		UserID:  int64(useridInt),
		GroupID: int64(groupidInt),
	}

	return
}
