/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"strings"
)

func (g *generator) calculateKumoriName(k8sName string, k8sKind string) string {

	switch k8sKind {
	case "KukuSolution":
		return g.findKukuSolutionName()
	case "Pod":
		rolePath, publicName := g.findInstanceKumoriName(k8sName)
		return fmt.Sprintf("%s.%s", rolePath, publicName)
	case "ReplicaSet", "Deployment", "StatefulSet", "PodDisruptionBudget":
		return g.findRolePathKumoriName(k8sName)
	case "V3Deployment":
		return g.findV3DeploymentKumoriName(k8sName)
	case "PersistentVolumeClaim":
		if strings.HasPrefix(k8sName, "kd-") {
			return g.findRolePathKumoriName(k8sName)
		} else if strings.HasPrefix(k8sName, "k-") {
			// For ephemeral PVC names, remove the first section and handle as real PVC
			// Example: k-7e021b81-kd-064225-6b65c4dc-fb10f1a1-deployment-0
			//      -->            kd-064225-6b65c4dc-fb10f1a1-deployment-0
			k8sName = k8sName[11:]
			return g.findRolePathKumoriName(k8sName)
		} else {
			return k8sName
		}
	default:
		return k8sName
	}
}

func (g *generator) findKukuSolutionName() string {
	if !g.root.Exists("ref", "name") || (g.root.Search("ref", "name").Data() == nil) {
		return ""
	}
	return g.root.Search("ref", "name").Data().(string)
}

func (g *generator) findV3DeploymentKumoriName(internalName string) (kname string) {
	deployments := g.root.Search("deployments").ChildrenMap()
	for depName, depData := range deployments {
		if !depData.Exists("id") || (depData.Search("id").Data() == nil) {
			continue
		}
		depInternalName := depData.Search("id").Data().(string)
		if depInternalName == internalName {
			return depName
		}
	}

	return internalName
}

func (g *generator) findRolePathKumoriName(internalName string) (rolePath string) {
	if !g.root.Exists("deployments") || (g.root.Search("deployments").Data() == nil) {
		return
	}
	deployments := g.root.Search("deployments").ChildrenMap()
	for depName, depData := range deployments {
		if !depData.Exists("artifact", "description", "role") || (depData.Search("artifact", "description", "role").Data() == nil) {
			continue
		}
		if !depData.Exists("id") || (depData.Search("id").Data() == nil) {
			continue
		}
		depInternalName := depData.Search("id").Data().(string)
		roles := depData.Search("artifact", "description", "role").ChildrenMap()
		for roleName, roleData := range roles {
			roleHash := Hash(roleName)
			if !roleData.Exists("instances") || (roleData.Search("instances").Data() == nil) {
				continue
			}
			baseName := fmt.Sprintf("%s-%s", depInternalName, roleHash)
			if strings.HasPrefix(internalName, baseName) {
				return fmt.Sprintf("%s.%s", depName, roleName)
			}
		}
	}

	return internalName
}

func (g *generator) findInstanceKumoriName(internalName string) (rolePath string, publicName string) {
	if !g.root.Exists("deployments") || (g.root.Search("deployments").Data() == nil) {
		return
	}
	deployments := g.root.Search("deployments").ChildrenMap()
	for depName, depData := range deployments {
		if !depData.Exists("id") || (depData.Search("id").Data() == nil) {
			continue
		}
		depInternalName := depData.Search("id").Data().(string)
		if !depData.Exists("artifact", "description", "role") || (depData.Search("artifact", "description", "role").Data() == nil) {
			continue
		}
		roles := depData.Search("artifact", "description", "role").ChildrenMap()
		for roleName, roleData := range roles {
			if !roleData.Exists("instances") || (roleData.Search("instances").Data() == nil) {
				continue
			}
			instances := roleData.Search("instances").ChildrenMap()
			for instanceName := range instances {
				if instanceName == internalName {
					rolePath = fmt.Sprintf("%s.%s", depName, roleName)
					publicName = g.calculateInstanceKumoriName(depInternalName, roleName, instanceName)
					return
				}
			}
		}
	}

	return
}

func (g *generator) calculateInstanceKumoriName(
	deployment string,
	role string,
	instance string,
) string {

	// Reference Pod name: kd-185531-352a1f2b-fb10f1a1-deployment-0
	// The magic numer 39 is the length of 'kd-185531-352a1f2b-fb10f1a1-deployment-'
	relativeName := instance[39:]
	return fmt.Sprintf("instance-%s", relativeName)
}
